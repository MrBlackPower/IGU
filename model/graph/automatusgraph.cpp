﻿#include "automatusgraph.h"

AutomatusGraph::AutomatusGraph(QString name, SmartObject* parent) : Graph(name,parent)
{
    time.deltaT = ONE;

    start_node = NULL;
    current_node = NULL;


    lastSolution = NULL;
    bestSolution = NULL;

    hover_add_node = NULL;
    hover_add_edge_start = NULL;
    hover_add_edge_end = NULL;
}

NodeGraph<QString,QString>* AutomatusGraph::getNodeGraph(QString name){
    for (int i = 0; i < nodes.size(); i++) {
        NodeGraph<QString,QString>* aux = nodes[i];
        if(aux->buffer == name)
            return aux;
    }

    return NULL;
}

vector<Field> AutomatusGraph::getPointDataList(){
    vector<Field> list;
    Field f;

    //STATUS
    f.names = "estado state ESTADO STATE";
    f.stream = FIELD_IN;
    f.required = true;
    f.type = stringp;
    list.push_back(f);

    return list;
}

vector<Field> AutomatusGraph::getCellDataList(){
    vector<Field> list;
    Field f;

    //TRANSICTION
    f.names = "transicao transiction TRANSACAO TRANSICTION";
    f.stream = FIELD_IN;
    f.required = true;
    f.type = stringp;
    list.push_back(f);

    return list;
}

vector<Field> AutomatusGraph::getStartParameters(){
    vector<Field> list;
    Field f;
    QString aux;

    for (int i = 0; i < alphabet.size(); i++) {
        aux += QString::asprintf("%c",alphabet[i]);
    }

    f.names = "ALPHABET";
    f.stream = FIELD_IN;
    f.required = false;
    f.type = stringp;
    f.current_value = aux;
    list.push_back(f);

    f.names = "CHAIN";
    f.stream = FIELD_IN;
    f.required = false;
    f.type = stringp;
    f.current_value = chain;
    list.push_back(f);

    f.names = "START_NODE";
    f.stream = FIELD_IN;
    f.required = false;
    f.type = stringp;
    f.current_value = (start_node)? start_node->buffer : "NONE";
    list.push_back(f);

    f.names = "FINISH_NODES";
    f.stream = FIELD_IN;
    f.required = false;
    f.type = stringp;
    f.current_value = "";

    for (int i = 0; i < final_nodes.size(); i++) {
        f.current_value += final_nodes[i]->buffer + " ";
    }

    list.push_back(f);

    return list;
}
vector<Field> AutomatusGraph::getContextVisualParameters(){
    vector<Field> params;

    Field aux;
    aux.names = "SPHERE_RADIUS";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = doublep;
    aux.current_value = aux.names.asprintf("%f",sphere_radius);

    params.push_back(aux);

    aux.names = "DRAW_NODE";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = _boolean;
    aux.current_value = aux.names.asprintf("%s",((draw_node)?"true":"false"));

    params.push_back(aux);

    aux.names = "DRAW_EDGE";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = _boolean;
    aux.current_value = aux.names.asprintf("%s",((draw_edge)?"true":"false"));

    params.push_back(aux);

    return params;
}

bool AutomatusGraph::compatibility(VTKFile* file){
    emit emit_log(SmartLogMessage(getSID(),"WEIGHTED GRAPH COMPATIBILITY RUN",SmartLogType::LOG_COMPATIBILITY));

    int n = file->getN();

    if(file->getPoints().size() != n)
        return false;

    return true;
}

bool AutomatusGraph::load(VTKFile* file){
    clear();

    vector<Point> points = file->getPoints();
    vector<QString> weights;
    vector<QString> transiction;
    vector<Cell> lines = file->getCells();
    vector<QString> extra = file->extra_data;

    vector<QString> aux = file->getPointData("estado state ESTADO STATE");
    for(int i = 0; i < aux.size(); i++){
        QString s = aux[i];
        weights.push_back(s);
    }

    aux = file->getCellData("transicao transiction TRANSACAO TRANSICTION");
    for(int i = 0; i < aux.size(); i++){
        QString s = aux[i];
        transiction.push_back(s);
    }

    for(int i = 0; i < points.size(); i++){
        Point p = points[i];
        addNode(weights[i],p.x,p.y,p.z);
    }

    for(int i = 0; i < lines.size(); i++){
        Cell l = lines[i];

        if(l.n !=2){
            clear();
            return false;
        }
        int a = l.points[ZERO];
        int b = l.points[ONE];
        addEdge(true,nodes[a],nodes[b],transiction[i]);
    }

    QString alphabet;
    for (int i = 0; i < extra.size(); i++) {
        QString line = extra[i];
        QTextStream s(&line);
        QString field;
        QString data;

        s >> field >> data;

        if(field == "SYMBOL"){
            alphabet.push_back(data.front());
        } else if ( field == "FINAL_STATE") {
            NodeGraph<QString,QString>* n = getNodeGraph(data);

            if(n){
                final_nodes.push_back(n);
            }
        } else if ( field == "START_STATE") {
            NodeGraph<QString,QString>* n = getNodeGraph(data);

            if(n){
                start_node = n;
            }
        }
    }

    setAlphabet(alphabet);

    return true;
}

vector<QString> AutomatusGraph::getRanges(){
    vector<QString> params;

    params.push_back("NUMBER_OF_EDGES");

    return params;
}

bool AutomatusGraph::updateStartParameter(QString field, QString value){
    if(field == "ALPHABET"){
        setAlphabet(value);
        return true;
    } else if(field == "CHAIN"){
        chain = value;
        return true;
    } else if(field == "START_NODE"){
        NodeGraph<QString,QString>* n = getNodeGraph(value);

        if(n){
            start_node = n;
        }
        return true;
    } else if(field == "FINAL_NODES"){
        QTextStream s(&value);
        vector<NodeGraph<QString,QString>*> final_nodes_aux;

        while (!s.atEnd()) {
            QString aux;

            NodeGraph<QString,QString>* n = getNodeGraph(aux);

            if(n){
                final_nodes_aux.push_back(n);
            }
        }

        if(!final_nodes_aux.empty())
            final_nodes = final_nodes_aux;

        return true;
    }
    return false;
}
GraphicObject* AutomatusGraph::extrapolateContext(QString extrapolation){
    return NULL;
}

bool AutomatusGraph::setContextRange(QString field){
    if(field == "NUMBER_OF_EDGES"){
        range.parameter = "NUMBER_OF_EDGES";
        range.max = -1.0;
        range.min = 1.0;
        return true;
    }

    return false;
}

//VIRTUAL GRAPH METHODS
bool AutomatusGraph::validSolution(Solution* s){
    return false;
}

double AutomatusGraph::scoreSolution(Solution* s){
    return 0;
}

void AutomatusGraph::drawContext(){
    if(hover_add_node != NULL){
        QColor selected_color("Red");
        drawSphere(hover_add_node,sphere_radius,selected_color);

        delete hover_add_node;
        hover_add_node = NULL;
    }

    if(current_node != NULL){
        QColor selected_color("Red");
        Point p = current_node->getPoint();
        drawSphere(&p,sphere_radius * 1.5,selected_color);
    }

    if(hover_add_edge_start != NULL && hover_add_edge_end != NULL){
        QColor selected_color("Red");
        drawArrow(hover_add_edge_start,hover_add_edge_end,sphere_radius,0.5,selected_color);
    }
}


bool AutomatusGraph::isInAlphabet(QString c){
    isInAlphabet(c.front());
}

bool AutomatusGraph::isInAlphabet(QChar c){
    for (int i = 0; i < alphabet.size(); i++) {
        if(alphabet[i] == c)
            return true;
    }

    return false;
}

void AutomatusGraph::setAlphabet(QString alphabet){
    this->alphabet.clear();

    for(int i = 0; i < alphabet.size(); i++){
        QChar c = alphabet.operator[](i);

        bool found = false;
        for(int j = 0; j < this->alphabet.size() && !found; j++)
            if(this->alphabet[i] == c)
                found = true;

        if(!found && (c.isDigit() || c.isSpace() || c.isLetter()))// (a-b or 0-9 or ` `)
            this->alphabet.push_back(c);
    }
}

bool AutomatusGraph::initialize(){
    time.deltaT = ONE;

    remaining_chain = chain;

    current_node = start_node;

    return true;
}

double AutomatusGraph::iteration(){
    clock_t begin = std::clock();

    if(!hasIterated()){
        remaining_chain = chain;

        current_node = start_node;
    }

    if(current_node == NULL){
        crash();
        return 0.0;
    }

    if(remaining_chain.isEmpty()){
        bool found = false;

        for (int i =0; i < final_nodes.size() && !found; i++) {
            NodeGraph<QString,QString>* n = final_nodes[i];

            if(n->getId() == current_node->getId())
                found = true;
        }

        if(found){
            finish();

            emit emit_log(SmartLogMessage(getSID(),QString::asprintf("FINISHED ITERATING ON A FINAL NODE!")));
        } else {
            crash();
            emit emit_log(SmartLogMessage(getSID(),QString::asprintf("FINISHED ITERATING ON A NON-FINAL NODE!")));
        }
    } else {
        QChar character = remaining_chain.front();

        if(!isInAlphabet(character)){
            crash();
            emit emit_log(SmartLogMessage(getSID(),QString::asprintf("FINISHED ITERATING ON A NON-RECOGNIZED CHARACTER!")));

            return 0.0;
        }

        remaining_chain.remove(0,1);

        emit emit_log(SmartLogMessage(getSID(),QString::asprintf("ITERATING CHARACTER=%c REMAINING_CHAIN=`%s`",character,remaining_chain.toStdString().data())));

        bool hasTransiction = false;//CHECK IF HAS EDGE WITH THE SAME COST AS THE CHARACTER
        int id;
        vector<QString> costs = current_node->getCosts();
        for (int i = 0; i < costs.size() && !hasTransiction; i++) {
            QString c = costs[i];

            if(c == character){
                hasTransiction = true;
                id = i;
            }
        }

        if(!hasTransiction){
            current_node = NULL;
        } else {
            NodeGraph<QString,QString>* aux = current_node->getEdges().operator[](id);
            emit emit_log(SmartLogMessage(getSID(),QString::asprintf("FROM %d TO %d",current_node->getId(),aux->getId())));
            current_node = aux;
        }
    }

    clock_t end = std::clock();

    return double(end - begin) / CLOCKS_PER_SEC;
}

void AutomatusGraph::finish(){
    status = Finished;
}

bool AutomatusGraph::updateContextVisualParameter(QString field, QString value){
    if(field == "SPHERE_RADIUS"){
        sphere_radius = value.toDouble();
        return true;
    } else if(field == "DRAW_NODE"){
        draw_node = (value.toInt() == 1)? true : false;
        return true;
    } else if(field == "DRAW_EDGE"){
        draw_edge = (value.toInt() == 1)? true : false;
        return true;
    }

    return false;
}

vector<QString> AutomatusGraph::getExtrapolations(){
    vector<QString> aux;

    return aux;
}

QString AutomatusGraph::getLiveParameterList(){
    QString r;

    r =  "CURRENT_STATE_ID";

    return r;
}

QString AutomatusGraph::getLiveParameter(QString name){
    QString r;

    if(name == "CURRENT_STATE_ID"){
        if(current_node != NULL)
            r = QString::asprintf("%d",current_node->getId());
        else
            r = "-1";
    }

    return r;
}
DataType AutomatusGraph::getLiveParameterDataType(QString name){
    DataType r;

    if(name == "CURRENT_STATE_ID"){
        r = integer;
    }

    return r;
}

void AutomatusGraph::mouse_press_event(int width, int height, Point eye, QMouseEvent *event, GraphicTools tool){
    switch (tool){
    case SELECT:{
        if(N == 0 || nodes.empty())
                return;

        if (event->buttons() & Qt::LeftButton) {
            NodeGraph<QString,QString>* n = mouse_ray(width,height,eye,mouse_last_pos);

            if(n){
                select(n);
            } else {
                unselect();
            }
        } else if (event->buttons() & Qt::RightButton) {
            unhover();
            unselect();
        }

        break;
    }
    case ADD_NODE:{
        if (event->buttons() & Qt::LeftButton) {
            if(mouse_being_used){
                GraphicObjectComposer component = new_node(getModelPoint(mouse_last_pos,eye,width,height));

                setGraphicComposerDialog(&component);

                new_node(component);

                mouse_being_used = false;
            } else {
                mouse_being_used = true;
            }
        } else if (event->buttons() & Qt::RightButton) {
            if(!mouse_being_used){
                NodeGraph<QString,QString>* n = mouse_ray(width,height,eye,mouse_last_pos);

                if(n){
                    mouse_being_used = true;

                    select(n);
                    hover_add_edge_start = n->getPoint().clone();
                    hover_add_edge_start_node = n;
                }
            } else {
                NodeGraph<QString,QString>* n = mouse_ray(width,height,eye,mouse_last_pos);

                if(n){
                    GraphicObjectComposer comp = new_edge(hover_add_edge_start_node,n);

                    setGraphicComposerDialog(&comp);

                    new_edge(hover_add_edge_start_node,n,comp);
                }

                mouse_being_used = false;

                delete  hover_add_edge_start;
                hover_add_edge_start_node = NULL;

                unselect();
                unhover();
            }
        } else if(mouse_being_used) {
            mouse_being_used = false;
        }

        break;
    }
    case EDIT_NODE:{
        if(N == 0 || nodes.empty())
                return;

        if (event->buttons() & Qt::LeftButton) {
            NodeGraph<QString,QString>* n = mouse_ray(width,height,eye,mouse_last_pos);

            if(n){
                GraphicObjectComposer comp = edit_node(n);

                setGraphicComposerDialog(&comp);

                edit_node(n,comp);

            }
        } else if (event->buttons() & Qt::RightButton) {
            if(!mouse_being_used){
                NodeGraph<QString,QString>* n = mouse_ray(width,height,eye,mouse_last_pos);

                if(n){
                    mouse_being_used = true;

                    select(n);
                    hover_add_edge_start = n->getPoint().clone();
                    hover_add_edge_start_node = n;
                }
            } else {
                NodeGraph<QString,QString>* n = mouse_ray(width,height,eye,mouse_last_pos);

                if(n){
                    if(hasEdge(hover_add_edge_start_node,n)){
                        GraphicObjectComposer comp = edit_edge(hover_add_edge_start_node,n);

                        setGraphicComposerDialog(&comp);

                        edit_edge(hover_add_edge_start_node,n,comp);
                    }
                }

                mouse_being_used = false;

                delete  hover_add_edge_start;
                hover_add_edge_start_node = NULL;

                unselect();
                unhover();
            }
        }

        break;
    }

    case REMOVE_NODE:{
        if(N == 0 || nodes.empty())
                return;

        if (event->buttons() & Qt::LeftButton) {
            NodeGraph<QString,QString>* n = mouse_ray(width,height,eye,mouse_last_pos);

            if(n){
                deleteNode(n);
            }
        } else if (event->buttons() & Qt::RightButton) {
            if(!mouse_being_used){
                NodeGraph<QString,QString>* n = mouse_ray(width,height,eye,mouse_last_pos);

                if(n){
                    mouse_being_used = true;

                    select(n);
                    hover_add_edge_start = n->getPoint().clone();
                    hover_add_edge_start_node = n;
                }
            } else {
                NodeGraph<QString,QString>* n = mouse_ray(width,height,eye,mouse_last_pos);

                if(n){
                    if(hasEdge(hover_add_edge_start_node,n)){
                        deleteEdge(hover_add_edge_start_node,n);
                    }
                }

                mouse_being_used = false;

                delete  hover_add_edge_start;
                hover_add_edge_start_node = NULL;

                unselect();
                unhover();
            }
        }

        break;
    }
    }
}

void AutomatusGraph::mouse_move_event(int width, int height, Point eye, QMouseEvent *event, GraphicTools tool){
    switch (tool){
    case SELECT:{
        if(N == 0 || nodes.empty())
                return;

        if (event->buttons() & Qt::LeftButton) {
            NodeGraph<QString,QString>* n = mouse_ray(width,height,eye,mouse_last_pos);

            if(n){
                hover(n);
            } else {
                unhover();
            }
        } else if (event->buttons() & Qt::RightButton) {
            unhover();
            unselect();
        } else {
            NodeGraph<QString,QString>* n = mouse_ray(width,height,eye,mouse_last_pos);

            if(n){
                hover(n);
            } else {
                unhover();
            }
        }

        break;
    }
    case ADD_NODE:{
        if (event->buttons() & Qt::LeftButton) {
            if(mouse_being_used){
                hover_add_node = getModelPoint(mouse_last_pos,eye,width,height).clone();
            }
        } else if (event->buttons() & Qt::RightButton) {
            if(mouse_being_used){
                NodeGraph<QString,QString>* n = mouse_ray(width,height,eye,mouse_last_pos);

                if(n){
                    hover(n);
                } else {
                    unhover();
                }
            }
        } else {
            if(mouse_being_used){
                hover_add_node = getModelPoint(mouse_last_pos,eye,width,height).clone();
            }
        }

        break;
    }
    case EDIT_NODE:{
        if(N == 0 || nodes.empty())
                return;

        if (event->buttons() & Qt::LeftButton) {

        } else if (event->buttons() & Qt::RightButton) {
            if(mouse_being_used){
                NodeGraph<QString,QString>* n = mouse_ray(width,height,eye,mouse_last_pos);

                if(n){
                    hover(n);
                } else {
                    unhover();
                }
            }
        } else {
            if(mouse_being_used){
                NodeGraph<QString,QString>* n = mouse_ray(width,height,eye,mouse_last_pos);

                if(n){
                    hover(n);
                } else {
                    unhover();
                }
            }
        }

        break;
    }
    case REMOVE_NODE:{
        if(N == 0 || nodes.empty())
                return;

        if (event->buttons() & Qt::LeftButton) {

        } else if (event->buttons() & Qt::RightButton) {
            if(mouse_being_used){
                NodeGraph<QString,QString>* n = mouse_ray(width,height,eye,mouse_last_pos);

                if(n){
                    hover(n);
                } else {
                    unhover();
                }
            }
        } else {
            NodeGraph<QString,QString>* n = mouse_ray(width,height,eye,mouse_last_pos);

            if(n){
                hover(n);
            } else {
                unhover();
            }
        }

        break;
    }
    }
}


NodeGraph<QString,QString>* AutomatusGraph::mouse_ray(int width, int height, Point eye, Point mouse){
    for (int i = 0; i < nodes.size(); i++) {
        NodeGraph<QString,QString>* n = nodes[i];
        Point p1 = getCanvasPoint(n->getPoint(),eye,width,height);

        float prox = MathHelper::euclideanDistance(&p1,&mouse);

        if(prox <= MOUSE_PROX)
            return n;
    }

    return NULL;
}
