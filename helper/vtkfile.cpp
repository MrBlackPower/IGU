#include "vtkfile.h"

VTKFile::VTKFile(VTKType type)
{
    clear();
    this->type = type;
    hasProgressBar = false;
    progressBar = NULL;
}

QString VTKFile::multiply(QString data, DataType type, double scalar){
    switch (type) {
        case integer:{
            int aux = data.toInt();
            return data.asprintf("%d",(aux * scalar));
        }
        case floatp:{
            float aux = data.toFloat();
            return data.asprintf("%f",(aux * scalar));
        }
        case doublep:{
            double aux = data.toDouble();
            return data.asprintf("%f",(aux * scalar));
        }
        case stringp:{
            return data;
        }
        case normals:{
            Point p;
            QTextStream s;
            s.setString(&data);
            s >> p.x >> p.y >> p.z;
            p = p * scalar;
            return data.asprintf("%f %f %f", p.x, p.y, p.z);
        }
        case vectors:{
            Point p;
            QTextStream s;
            s.setString(&data);
            s >> p.x >> p.y >> p.z;
            p = p * scalar;
            return data.asprintf("%f %f %f", p.x, p.y, p.z);
        }
        case tensor:{
            return data;
        }
        case undefined:{
            return data;
        }
    }

    return data;
}

void VTKFile::percent(int value){
    if(hasProgressBar){
        if(value < 0)
            value = 0;

        if(value > 100)
            value = 100;

        progressBar->setValue(value);
    }
}

VTKType VTKFile::getType(){
    return type;
}

QString VTKFile::getFileName(){
    return filename;
}

bool VTKFile::addPoint(Point p){
    points.push_back(p);
    points_n ++;

    return true;
}

bool VTKFile::addLine(vector<int> points){
    if(isPolygon == -1)
        isPolygon = 0;

    if(isPolygon == 1)
        return false;

    Line l;
    l.n = points.size();

    for(int i = 0; i < l.n; i++)
        l.points.push_back(points[i]);

    lines.push_back(l);
    lines_n ++;

    return true;
}

bool VTKFile::addCell(vector<int> points){
    if(isPolygon == -1)
        isPolygon = 1;

    if(isPolygon == 0)
        return false;

    Cell c;
    c.n = points.size();

    for(int i = 0; i < c.n; i++)
        c.points.push_back(points[i]);

    cells.push_back(c);
    cells_n ++;

    return true;
}

bool VTKFile::addPointInfo(DataType type, QString name){
    Data d;
    d.ID = points_info.size();
    d.name = name;
    d.type = type;

    points_info.push_back(d);

    return true;
}

bool VTKFile::addCellInfo(DataType type, QString name){
    Data d;
    d.ID = cells_info.size();
    d.name = name;
    d.type = type;

    cells_info.push_back(d);

    return true;
}

bool VTKFile::addPointData(QString name, QString data){
    int i = 0;

    for(i = 0; i < points_info.size(); i++){
        QString info_name = points_info[i].name;

        if(info_name == name)
            break;
    }

    if(i == points_info.size())
        return false;

    while(i >= points_data.size())
        points_data.push_back(vector<QString>());

    vector<QString>* aux = &points_data[i];
    aux->push_back(data);

    return true;
}

bool VTKFile::addCellData(QString name, QString data){
    int i = 0;

    for(i = 0; i < cells_info.size(); i++){
        QString info_name = cells_info[i].name;

        if(info_name == name)
            break;
    }

    if(i == cells_info.size())
        return false;

    while(i >= cells_data.size())
        cells_data.push_back(vector<QString>());

    vector<QString>* aux = &cells_data[i];
    aux->push_back(data);
}

bool VTKFile::load(QString filename){
    clear();
    FILE *file;

    char* fileName = filename.toUtf8().data();
    file = fopen(fileName,"r");

    if(file == NULL){
        clear();
        return false;
    }

    char line[100];

    int nPoints = 0;

    int nLines = 0;

    int nCells = 0;

    emit percent(0);

    while(fgets(line, 80, file) != NULL){
        /**
          * Points Coordinates
          *********************/
        if(strstr(line,"POINTS") != NULL){
            //Gets number of points
            sscanf (line, "%*s %d %*s", &nPoints);

            //Sets size of points vector
            points.reserve(nPoints);

            //Gets all points
            for(int i = 0; i < nPoints && fgets(line, 80, file) != NULL; i++){
                QString s(line);
                QTextStream stream(&s);
                float x = 0.0;
                float y = 0.0;
                float z = 0.0;

                stream >> x >> y >> z;

               addPoint(Point(i, x, y, z));
                emit percent(25 * ((double)(i+1) / (double)nPoints));
            }
        }
        /**
          * Lines
          *********************/
        if(strstr(line,"LINES") != NULL){
            sscanf (line, "%*s %d %*s", &nLines);

            //Gets all lines
            for(int i = 0; i < nLines && fgets(line, 80, file) != NULL; i++){
                QString s(line);
                QTextStream stream(&s);
                int n = 0;
                stream >> n;

                vector<int> id;
                for(int i = 0; i < n; i++){
                    int aux = 0;
                    stream >> aux;
                    id.push_back(aux);
                }

                Line l;
                l.n = n;
                l.points = id;

                lines.push_back(l);
                emit percent(50 * ((double)(i+1) / (double)nLines));
            }
        }

        /**
          * Cells
          *********************/
        if(strstr(line,"CELLS") != NULL || strstr(line,"POLYGON") != NULL){
            sscanf (line, "%*s %d %*s", &nCells);

            //Gets all cells
            for(int i = 0; i < nCells && fgets(line, 80, file) != NULL; i++){
                QString s(line);
                QTextStream stream(&s);
                int n = 0;
                stream >> n;

                vector<int> id;
                for(int i = 0; i < n; i++){
                    int aux = 0;
                    stream >> aux;
                    id.push_back(aux);
                }

                Cell c;
                c.n = n;
                c.points = id;

                cells.push_back(c);
                emit percent(50 * ((double)(i+1) / (double)nCells));
            }
        }

        /**
          * Cell Types
          *********************/
        if((strstr(line,"CELL_TYPES") != NULL || strstr(line,"POLYGON") != NULL) && nCells > 0){
            int nTypes = 0;
            sscanf (line, "%*s %d", &nTypes);

            if(nCells != nTypes){
                clear();
                return false;
            }

            //Gets all cell types
            for(int i = 0; i < nCells && fgets(line, 80, file) != NULL; i++){
                QString s(line);
                QTextStream stream(&s);

                int type = 0;
                stream >> type;

                cells[i].type = type;
            }
        }

        /**
          * Point Data
          *********************/
        if(strstr(line,"POINT_DATA") != NULL){
            int aux;
            sscanf (line, "%*s %d", &aux);

            if(aux != nPoints || !FileHelper::jumpReadLine(line,file)){
                clear();
                return false;
            }

            while(true){
                QString tag = "";
                QString name = "";
                QString type = "";

                QString s(line);
                QTextStream stream(&s);
                stream >> tag >> name >> type;

                DataType datatype;

                if(tag == "SCALARS" || tag == "scalars"){
                    if(type == "float"){
                        datatype = doublep;
                    } else if(type == "double"){
                        datatype = doublep;
                    } else if(type == "int"){
                        datatype = integer;
                    } else if(type == "string"){
                        datatype = stringp;
                    } else {
                        datatype = undefined;
                    }
                } else if(tag == "VECTORS"){
                    datatype = tensor;
                } else if(tag == "NORMALS"){
                    datatype = tensor;

                } else if(tag == "TENSORS"){
                    datatype = tensor;
                    return false; // TENSORS WILL PROVOKE AN ERROR IN LOADING <<HAS TO READ 3 LINES>>
                } else {
                    datatype = undefined;
                }

                if(datatype == undefined)
                    break;

                addPointInfo(datatype,name);

                //LOOKUP TABLE IGNORED
                if(!FileHelper::jumpReadLine(line,file)){
                    clear();
                    return false;
                }

                //DATA
                for(int i = 0; i < nPoints; i++){
                    if(!FileHelper::jumpReadLine(line,file)){
                        clear();
                        return false;
                    }

                    QString data = QString(line);
                    addPointData(name,data);
                    emit percent(75 * ((double)(i+1) / (double)nPoints));
                }

                //JUMPS END LINE
                if(!FileHelper::jumpReadLine(line,file))
                    break;
            }
        }
        /**
          * Cell Data
          *********************/
        if(strstr(line,"CELL_DATA") != NULL){
            int aux;
            sscanf (line, "%*s %d", &aux);

            if((aux != nCells && aux != nLines) || !FileHelper::jumpReadLine(line,file)){
                clear();
                return false;
            }

            while(true){
                QString tag = "";
                QString name = "";
                QString type = "";

                QString s(line);
                QTextStream stream(&s);
                stream >> tag >> name >> type;

                DataType datatype;

                if(tag == "SCALARS" || tag == "scalars"){
                    if(type == "float"){
                        datatype = doublep;
                    } else if(type == "double"){
                        datatype = doublep;
                    } else if(type == "int"){
                        datatype = integer;
                    } else if(type == "string"){
                        datatype = stringp;
                    } else {
                        datatype = undefined;
                    }
                } else if(tag == "VECTORS"){
                    datatype = tensor;
                } else if(tag == "NORMALS"){
                    datatype = tensor;

                } else if(tag == "TENSORS"){
                    datatype = tensor;
                } else {
                    datatype = undefined;
                }

                if(datatype == undefined)
                    break;

                addCellInfo(datatype,name);

                int aux = (isPolygon == 1)? nCells : nLines ;

                //LOOKUP TABLE IGNORED
                if(!FileHelper::jumpReadLine(line,file)){
                    clear();
                    return false;
                }

                //DATA
                for(int i = 0; i < aux; i++){
                    if(!FileHelper::jumpReadLine(line,file)){
                        clear();
                        return false;
                    }

                    QString data = QString(line);
                    addCellData(name,data);
                    emit percent(100 * ((double)(i+1) / (double)nPoints));
                }

                //JUMPS END LINE
                if(!FileHelper::jumpReadLine(line,file))
                    break;
            }
        }
    }

    cells_n = cells.size();
    lines_n = lines.size();
    points_n = points.size();
    this->filename = filename;

    emit percent(100);

    fclose(file);

    return true;
}

bool VTKFile::loadMWCDSInstance(QString filename){
    clear();
    QFile file(filename);

    char* fileName = filename.toUtf8().data();

    if(!file.open(QIODevice::ReadOnly))
        return false;


    int nPoints = 0;

    int nWeight = 0;

    emit percent(0);

    QTextStream in(&file);
    while(!in.atEnd()){
        QString line = in.readLine();
        /**
          * Points Coordinates
          *********************/
        if(line.contains("NumberOfNodes")){
            line = in.readLine();
            if(in.atEnd()){
                clear();
                return false;
            }

            sscanf (line.toStdString().data(), "%d", &nPoints);


            if(nPoints > 0){
                line = in.readLine();
                if(!line.contains("Positions"))
                    return false;
            } else {
                return false;
            }

            line = in.readLine();

            //Gets all points
            for(int i = 0; i < nPoints; i++){
                line = in.readLine();

                QTextStream stream(&line);
                float x = 0.0;
                float y = 0.0;
                float z = 0.0;

                stream >> x >> y >> z;

                addPoint(Point(i, x, y, z));
                emit percent(25 * ((double)(i+1) / (double)nPoints));
            }
        }
        /**
          * Lines
          *********************/
        if(line.contains("CONNECTIONS") || nWeight != 0){
            int nConnectionsLines = 0;

            while(nConnectionsLines < nPoints){
                line = in.readLine();
                QString s(line);
                QTextStream stream(&s);

                for (int j = 0; j < nPoints; j++){
                    int aux;
                    stream >> aux;
                    bool edge = (aux == 1)? true : false;

                    if(edge && nConnectionsLines != j){
                        vector<int> p;
                        p.push_back(nConnectionsLines);
                        p.push_back(j);

                        addLine(p);
                    }
                }
                nConnectionsLines++;
            }
        }

        /**
          * WEIGHTS
          *********************/
        if(line.contains("WEIGHTS")){

            addPointInfo(DataType::doublep,"WEIGHT");
            nWeight = 0;

            while(nWeight < nPoints){
                line = in.readLine();

                int weight = 0;

                QString s(line);
                QTextStream stream(&s);
                stream >> weight;

                addPointData("WEIGHT",QString::asprintf("%d",weight));
                nWeight++;
            }
        }
    }

    cells_n = cells.size();
    lines_n = lines.size();
    points_n = points.size();
    this->filename = filename;

    emit percent(100);

    file.close();
    return true;
}

bool VTKFile::loadAutomatusInstance(QString filename){
    clear();
    QFile file(filename);

    char* fileName = filename.toUtf8().data();

    if(!file.open(QIODevice::ReadOnly))
        return false;


    emit percent(0);

    QTextStream in(&file);
    while(!in.atEnd()){
        QString line = in.readLine();
        /**
          * Points Coordinates
          *********************/
        if(line.contains("estados") && !line.contains("estadosFinais")){
            line = in.readLine();
            if(in.atEnd()){
                clear();
                return false;
            }

            addPointInfo(DataType::stringp,"STATE");

            //Gets all points
            while(!line.contains("estados")){
                vector<QString> aux = findAllBetween('"',line);

                if(aux.size() != 1)
                    return false;


                addPoint(generate_point(points_n));
                addPointData("STATE",aux.operator[](0));
                line = in.readLine();
            }
        }

        if(line.contains("estadosFinais")){
            line = in.readLine();
            if(in.atEnd()){
                clear();
                return false;
            }


            //Gets all points
            while(!line.contains("estadosFinais")){
                vector<QString> aux = findAllBetween('"',line);

                if(aux.size() != 1)
                    return false;


                extra_data.push_back(QString::asprintf("FINAL_STATE %s",aux.operator[](0).toStdString().data()));
                line = in.readLine();
            }
        }

        if(line.contains("simbolos")){
            line = in.readLine();
            if(in.atEnd()){
                clear();
                return false;
            }


            //Gets all points
            while(!line.contains("simbolos")){
                vector<QString> aux = findAllBetween('"',line);

                if(aux.size() != 1)
                    return false;


                extra_data.push_back(QString::asprintf("SYMBOL %s",aux.operator[](0).toStdString().data()));
                line = in.readLine();
            }
        }

        if(line.contains("estadoInicial")){
            if(in.atEnd()){
                clear();
                return false;
            }


            vector<QString> aux = findAllBetween('"',line);

            if(aux.size() != 1)
                return false;


            extra_data.push_back(QString::asprintf("START_STATE %s",aux.operator[](0).toStdString().data()));
        }
        /**
          * Cells
          *********************/
        if(line.contains("funcaoPrograma") ){
            line = in.readLine();
            if(in.atEnd()){
                clear();
                return false;
            }

            addCellInfo(DataType::stringp,"TRANSICTION");

            //Gets all cells
            while(!line.contains("funcaoPrograma")){
                vector<QString> aux = findAllBetween('"',line);

                if(aux.size() != 3)
                    return false;

                bool found_a = false;
                bool found_b = false;
                int id_a = 0;
                int id_b = 0;

                vector<QString> states = getPointData("STATE");
                vector<int> ids;
                for (int i = 0; i < states.size() && !(found_a && found_b); i++) {
                    QString s = states[i];

                    if(s == aux[0]){
                        found_a = true;
                        id_a = i;
                        ids.push_back(id_a);
                    }

                    if(s == aux[1]){
                        found_b = true;
                        id_b = i;
                        ids.push_back(id_b);
                    }
                }

                if(found_a && found_b){
                    addCell(ids);
                    addCellData("TRANSICTION",aux.operator[](2));
                }
                line = in.readLine();
            }
        }
    }

    cells_n = cells.size();
    lines_n = lines.size();
    points_n = points.size();
    this->filename = filename;

    emit percent(100);

    file.close();
    return true;
}

Point VTKFile::generate_point(int i){
    Point p;

    p.y = (i % (2));
    p.x = i - (i % (2));

    return p;
}


bool VTKFile::checkIntegrity(){
    if(points_n != points.size())
        return false;

    if(points_data.size() != points_info.size())
        return false;

    for(int i = 0; i < points_data.size(); i++){
        vector<QString> it = points_data[i];
        if(it.size() != points_n)
            return false;
    }

    if(isPolygon == 1){
        if(cells_n != cells.size())
            return false;

        if(cells_data.size() != cells_info.size())
            return false;

        for(int i = 0; i < cells_data.size(); i++){
            vector<QString> it = cells_data[i];
            if(it.size() != cells_n)
                return false;
        }
    }

    if(isPolygon == 0){
        if(lines_n != lines.size())
            return false;

        if(cells_data.size() != cells_info.size())
            return false;

        for(int i = 0; i < cells_data.size(); i++){
            vector<QString> it = cells_data[i];
            if(it.size() != lines_n)
                return false;
        }
    }

    return true;
}

void VTKFile::clear(){
    type = Polydata;
    filename = "";
    isPolygon = -1;

    points_n = 0;
    points.clear();
    points_info.clear();
    points_data.clear();

    lines_n = 0;
    lines.clear();

    cells_n = 0;
    cells.clear();
    cells_info.clear();
    cells_data.clear();
}

void VTKFile::setProgressBar(QProgressBar* p){
    if(p != NULL){
        hasProgressBar = true;
        progressBar = p;
    }
}

QString VTKFile::getType(DataType type){
    QString aux;
    switch (type) {
    case integer:
        return aux.asprintf("integer");
    case floatp:
        return aux.asprintf("float");
    case doublep:
        return aux.asprintf("double");
    case stringp:
        return aux.asprintf("string");
    case normals:
        return aux.asprintf("normals[3]");
    case vectors:
        return aux.asprintf("vector[3]");
    case tensor:
        return aux.asprintf("tensor[3,3]");
    case undefined:
        return aux.asprintf("UNKNOWN");
    default:
        return aux.asprintf("double");
        break;
    }
}

int VTKFile::getN(){
    return points_n;
}

vector<Point> VTKFile::getPoints(){
    return points;
}

int VTKFile::getLineN(){
    return lines_n;
}

vector<Line> VTKFile::getLines(){
    return lines;
}

int VTKFile::getCellN(){
    cells_n;
}

vector<Cell> VTKFile::getCells(){
    return cells;
}

vector<QString> VTKFile::print(){
    vector<QString> txt;
    QString line;

    line = line.asprintf("# vtk DataFile Version 3.0\n");
    txt.push_back(line);

    line = line.asprintf("VTK OUTPUT\n");
    txt.push_back(line);

    line = line.asprintf("ASCII\n");
    txt.push_back(line);

    line = line.asprintf("DATASET POLYDATA\n");
    txt.push_back(line);

    line = line.asprintf("POINTS  %d  double", points_n );
    txt.push_back(line);

    //PRINTS POINTS
    for(int i = 0; i < points.size(); i++){
        Point p = points[i];
        line = line.asprintf("%f  %f  %f", p.x, p.y, p.z);
        txt.push_back(line);
    }

    //PRINTS LINES (FOLLOWED BY WEIGHT)
    line = line.asprintf("");
    txt.push_back(line);
    line = line.asprintf("LINES %d %d", lines_n, lines_n * 3);
    txt.push_back(line);

    for(int i = 0; i < lines.size(); i++){
        Line lAux = lines[i];
        line = line.asprintf("2  %d %d", lAux.points[0], lAux.points[1]);
        txt.push_back(line);
    }

    line = line.asprintf("POINT_DATA  %d", points_n);
    txt.push_back(line);

    for (int i = 0; i < points_info.size(); i++) {
        Data point_data = points_info[i];
        QString name_aux = point_data.name;
        QString type_aux = getType(point_data.type);

        line = line.asprintf("scalars %s %s",name_aux.toStdString().data(),type_aux.toStdString().data());
        txt.push_back(line);
        line = line.asprintf("LOOKUP_TABLE default");
        txt.push_back(line);

        for (int j = 0; j < points_n; j++) {
            line = points_data[i][j];
            txt.push_back(line);
        }

    }

    line = line.asprintf("CELL_DATA  %d",lines_n);
    txt.push_back(line);

    for (int i = 0; i < cells_info.size(); i++) {
        Data cell_data = cells_info[i];
        QString name_aux = cell_data.name;
        QString type_aux = getType(cell_data.type);

        line = line.asprintf("scalars %s %s",name_aux.toStdString().data(),type_aux.toStdString().data());
        txt.push_back(line);
        line = line.asprintf("LOOKUP_TABLE default");
        txt.push_back(line);

        for (int j = 0; j < lines_n; j++) {
            line = cells_data[i][j];
            txt.push_back(line);
        }

    }

    return txt;
}

vector<QString> VTKFile::getPointDataList(){
    vector<QString> dataList;

    for(int i = 0; i < points_info.size(); i++){
        QString aux = points_info[i].name;
        dataList.push_back(aux);
    }

    return dataList;
}

vector<DataType> VTKFile::getPointDataType(){
    vector<DataType> dataList;

    for(int i = 0; i < points_info.size(); i++){
        DataType aux = points_info[i].type;
        dataList.push_back(aux);
    }

    return dataList;
}

DataType VTKFile::getPointDataType(QString names){
    int i = 0;
    DataType ans = undefined;

    for(i = 0; i < points_info.size(); i++){
        Data d = points_info[i];
        if(names.contains(d.name))
            break;
    }

    if(i == points_info.size())
        return ans;

    ans = points_info[i].type;

    return ans;
}

vector<QString> VTKFile::getPointDataInfo(){
    vector<QString> dataList;

    for(int i = 0; i < points_info.size(); i++){
        Data d = points_info[i];
        QString aux = getType(d.type);
        aux = aux.asprintf("%s : [%d]<%s>",d.name.toStdString().data(),d.ID,aux.toStdString().data());
        dataList.push_back(aux);
    }

    return dataList;
}

vector<QString> VTKFile::getCellDataList(){
    vector<QString> dataList;

    for(int i = 0; i < cells_info.size(); i++){
        QString aux = cells_info[i].name;
        dataList.push_back(aux);
    }

    return dataList;
}

vector<DataType> VTKFile::getCellDataType(){
    vector<DataType> dataList;

    for(int i = 0; i < cells_info.size(); i++){
        DataType aux = cells_info[i].type;
        dataList.push_back(aux);
    }

    return dataList;
}

DataType VTKFile::getCellDataType(QString names){
    int i = 0;
    DataType ans = undefined;

    for(i = 0; i < cells_info.size(); i++){
        Data d = cells_info[i];
        if(names.contains(d.name))
            break;
    }

    if(i == cells_info.size())
        return ans;

    ans = cells_info[i].type;

    return ans;
}

vector<QString> VTKFile::getCellDataInfo(){
    vector<QString> dataList;

    for(int i = 0; i < cells_info.size(); i++){
        Data d = cells_info[i];
        QString aux = getType(d.type);
        aux = aux.asprintf("%s : [%d]<%s>",d.name.toStdString().data(),d.ID,aux.toStdString().data());
        dataList.push_back(aux);
    }

    return dataList;
}

vector<QString> VTKFile::getPointData(QString names){
    int i = 0;
    vector<QString> ans;

    for(i = 0; i < points_info.size(); i++){
        Data d = points_info[i];
        if(names.contains(d.name))
            break;
    }

    if(i == points_info.size())
        return ans;

    ans = points_data[i];

    return ans;
}

vector<QString> VTKFile::getCellData(QString names){
    int i = 0;
    vector<QString> ans;

    for(i = 0; i < cells_info.size(); i++){
        Data d = cells_info[i];
        if(names.contains(d.name))
            break;
    }

    if(i == cells_info.size())
        return ans;

    ans = cells_data[i];

    return ans;
}

VTKFile VTKFile::scaleThrough(vector<QString> pointConversions, vector<QString> cellConversions){
    VTKFile file = VTKFile(type);
    //POINT COORDINATES
    int i = 0;
    for(i = 0; i < pointConversions.size(); i++){
        QString conversion = pointConversions[i];
        QTextStream stream;
        stream.setString(&conversion);
        QString name;
        double scalar = 0.0;
        stream >> name >> scalar;
        if(name == "POINT_COORDINATES"){
            //ADDS POINTS
            for(int j = 0; j < points.size(); j++){
                Point p = points[j] * scalar;
                file.addPoint(p);
            }
            //ADDS LINES & CELLS
            for(int j = 0; j < cells.size(); j++){
                Cell c = cells[j];
                file.addCell(c.points);
            }
            for(int j = 0; j < lines.size(); j++){
                Line l = lines[j];
                file.addLine(l.points);
            }

            i = -1;
            break;
        }
    }

    if(i != -1)
        return file;

    //POINT DATA
    for(int k = 0; k < pointConversions.size(); k++){
        QString conversion = pointConversions[k];
        QTextStream stream;
        stream.setString(&conversion);
        QString name;
        double scalar = 0.0;
        stream >> name >> scalar;
        if(name != "POINT_COORDINATES"){
            vector<QString> data = getPointData(name);
            DataType dataType = getPointDataType(name);

            file.addPointInfo(dataType,name);

            for(int j = 0; j < data.size(); j++){
                QString line = data[j];
                file.addPointData(name,multiply(line,dataType,scalar));
            }
        }
    }
    //CELL DATA
    for(int k = 0; k < cellConversions.size(); k++){
        QString conversion = cellConversions[k];
        QTextStream stream;
        stream.setString(&conversion);
        QString name;
        double scalar = 0.0;
        stream >> name >> scalar;
        vector<QString> data = getCellData(name);
        DataType dataType = getCellDataType(name);


        file.addCellInfo(dataType,name);

        for(int j = 0; j < data.size(); j++){
            QString line = data[j];
            file.addCellData(name,multiply(line,dataType,scalar));
        }

    }

    return file;
}

void VTKFile::operator =(VTKFile b){
    clear();
    vector<Point> p = b.getPoints();
    for(int i = 0; i < p.size(); i++)
        addPoint(p[i]);

    vector<Line> l = b.getLines();
    for(int i = 0; i < l.size(); i++)
        addLine(l[i].points);

    vector<Cell> c = b.getCells();
    for(int i = 0; i < c.size(); i++)
        addCell(c[i].points);

    vector<QString> pointData = b.getPointDataList();
    vector<DataType> pointDataType = b.getPointDataType();
    for(int i = 0; i < pointData.size(); i++){
        addPointInfo(pointDataType[i],pointData[i]);
        vector<QString> data = b.getPointData(pointData[i]);
        for(int j = 0; j < data.size(); j++){
            addPointData(pointData[i],data[j]);
        }
    }

    vector<QString> cellData = b.getCellDataList();
    vector<DataType> cellDataType = b.getCellDataType();
    for(int i = 0; i < cellData.size(); i++){
        addCellInfo(cellDataType[i],cellData[i]);
        vector<QString> data = b.getCellData(cellData[i]);
        for(int j = 0; j < data.size(); j++){
            addCellData(cellData[i],data[j]);
        }
    }
}

void VTKFile::operator =(VTKFile* b){
    clear();
    vector<Point> p = b->getPoints();
    for(int i = 0; i < p.size(); i++)
        addPoint(p[i]);

    vector<Line> l = b->getLines();
    for(int i = 0; i < l.size(); i++)
        addLine(l[i].points);

    vector<Cell> c = b->getCells();
    for(int i = 0; i < c.size(); i++)
        addCell(c[i].points);

    vector<QString> pointData = b->getPointDataList();
    vector<DataType> pointDataType = b->getPointDataType();
    for(int i = 0; i < pointData.size(); i++){
        addPointInfo(pointDataType[i],pointData[i]);
        vector<QString> data = b->getPointData(pointData[i]);
        for(int j = 0; j < data.size(); j++){
            addPointData(pointData[i],data[j]);
        }
    }

    vector<QString> cellData = b->getCellDataList();
    vector<DataType> cellDataType = b->getCellDataType();
    for(int i = 0; i < cellData.size(); i++){
        addCellInfo(cellDataType[i],cellData[i]);
        vector<QString> data = b->getCellData(pointData[i]);
        for(int j = 0; j < data.size(); j++){
            addCellData(pointData[i],data[j]);
        }
    }
}

void* VTKFile::getValue(QString value, DataType type){
    switch (type) {
    case doublep:
        return new double(value.toDouble());
    case floatp:
        return new float(value.toFloat());
    case integer:
        return new int(value.toInt());
    case stringp:
        return new QString(value);
    default:
        return NULL;
    }
    return NULL;
}

vector<QString> VTKFile::findAllBetween(QChar c, QString s){
    QString aux;
    vector<QString> r;

    bool inside = false;
    for (int i = 0; i < s.size(); i++) {
        QChar current_c = s.at(i);
        if(inside){
            if(current_c != c)
                aux.push_back(current_c);
            else{
                r.push_back(aux);
                inside = false;
                aux = "";
            }
        } else {
            if(current_c == c)
                inside = true;
        }
    }

    return r;
}
