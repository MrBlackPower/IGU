#include "ktgraphic.h"
#include "../graphic2d.h"
#include "../graphic3d.h"

KTGraphic::KTGraphic(int example, SmartObject* parent) : Graphic2D(("KT GRAPHIC " + example),parent)
{
    switch (example) {
    case 0:
        startParams.a = 0.0;
        startParams.b = 2.0 * PI;
        startParams.coutour_type = 0;
        break;
    case 1:
        startParams.a = 0.0;
        startParams.b = 2.0 * PI;
        startParams.coutour_type = 0;
        break;
    case 2:
        startParams.a = -ONE;
        startParams.b = ONE;
        startParams.coutour_type = 1;
        break;
    default:
        startParams.example = 0;
        startParams.a = 0.0;
        startParams.b = 2.0 * PI;
        startParams.coutour_type = 0;
        break;
    }
    error.absolute = 0.0;
    error.numel = startParams.n;
    startParams.example = example;
    startParams.dx = (double)(startParams.b - startParams.a) / (startParams.n-1);
    setContextRange("Y-AXIS");
}

KTGraphic::KTGraphic(int N, int example, SmartObject* parent) : Graphic2D(("KT GRAPHIC " + example),parent)
{
    switch (example) {
    case 0:
        startParams.a = 0.0;
        startParams.b = 2.0 * PI;
        startParams.coutour_type = 0;
        break;
    case 1:
        startParams.a = 0.0;
        startParams.b = 2.0 * PI;
        startParams.coutour_type = 0;
        break;
    case 2:
        startParams.a = -ONE;
        startParams.b = ONE;
        startParams.coutour_type = 1;
        break;
    default:
        startParams.example = 0;
        startParams.a = 0.0;
        startParams.b = 2.0 * PI;
        startParams.coutour_type = 0;
        break;
    }
    error.absolute = 0.0;
    error.numel = N;
    startParams.example = example;
    startParams.n = N;
    startParams.dx = (double)(startParams.b - startParams.a) / (N-1);
    setContextRange("Y-AXIS");
}

KTGraphic::KTGraphic(QString name, int example, SmartObject *parent) : Graphic2D(name,parent){
    KTGraphic(example,parent);
}

bool KTGraphic::initialize(){
    if(raw_data.size() < 2){
        N = 0;
        switch (startParams.example) {
        case 0:
        {
            vector<Point*> exact;
            Graphic2D* g;
            clearData();
            for(int i = 0; i < startParams.n; i++){
                double x = startParams.a + (startParams.dx * i);
                Point* p = new Point(0, x, MathHelper::sine(x * (180 / PI)));
                addData(p);
            }

            break;
        }
        case 1:
        {
            Graphic2D* g;
            clearData();
            for(int i = 0; i < startParams.n; i++){
                double x = startParams.a + (startParams.dx * i);
                Point* p = new Point(0, x, 0.5 + MathHelper::sine(x * (180 / PI)));
                addData(p);
            }


            break;
        }
        case 2:
        {
            Graphic2D* g;
            clearData();
            for(int i = 0; i < startParams.n; i++){
                double x = startParams.a + (startParams.dx * i);
                if(x > 0){
                    Point* p = new Point(0, x, 2.0);
                    addData(p);
                } else {
                    Point* p = new Point(0, x, -2.0);
                    addData(p);
                }
            }

            break;
        }
        default:
        {
            vector<Point*> exact;
            Graphic2D* g;
            clearData();
            for(int i = 0; i < startParams.n; i++){
                double x = (double)startParams.a + (((double)(startParams.b - startParams.a) / (double)EXACT_N) * (double)i);
                Point* p = new Point(0, x, MathHelper::sine(x * (180 / PI)));
                addData(p);
                exact.push_back(p);
            }

            startParams.example = -1;
            break;
        }
        }
    }


    startParams.dx = (double)(startParams.b - startParams.a) / (N-1);

    Point aux = *raw_data[0];
    double max_val = aux.y;

    for(int i = 1; i < N; i++){
        aux = *raw_data[i];

        if(max_val < aux.y)
            max_val = aux.y;
    }

    time.deltaT = startParams.cfl * startParams.dx / max_val;

    return true;
}

double KTGraphic::iteration(){
    vector<double> du = KT();
    vector<double> u;
    u.reserve(N);

    for(int i = 0; i < N; i++){
        Point* p = raw_data[i];
        u[i] = p->y + time.deltaT * du[i];
    }

    time.iterations ++;
    time.modelTime += time.deltaT;


    //UPDATES THE RAW DATA VALUE
    for(int i = 0; i < N; i++)
        raw_data[i]->y = u[i];

    error.absolute = 0.0;
    error.numel = N;

    for(int i = 0; i < N; i++){
        if(abs(u[i] - MathHelper::sine(raw_data[i]->x - time.modelTime + time.deltaT)) > error.absolute)
            error.absolute = abs(u[i] - MathHelper::sine((raw_data[i]->x - time.modelTime + time.deltaT) * (180 / PI)));
    }
}

bool KTGraphic::updateContextVisualParameter(QString field, QString value){
    return false;
}

QString KTGraphic::getLiveParameterList(){
    return QString("");
}

QString KTGraphic::getLiveParameter(QString name){
    return QString("");
}

DataType KTGraphic::getLiveParameterDataType(QString name){
    return undefined;
}

bool KTGraphic::setContextRange(QString field){
    if(field == "Y-AXIS"){
        range.max = limit.top;
        range.min = limit.bottom;
        range.parameter = "Y-AXIS";
        return true;
    }

    return false;
}

vector<Field> KTGraphic::getStartParameters(){
    vector<Field> params;

   Field aux;
   aux.names = "EXAMPLE";
   aux.required = false;
   aux.stream = FIELD_IN;
   aux.type = integer;
   aux.current_value = aux.current_value.asprintf("%d",startParams.example);

    params.push_back(aux);

    return params;
}

vector<Field> KTGraphic::getContextVisualParameters(){
    vector<Field> params;

    return params;
}

vector<QString> KTGraphic::getRanges(){
    vector<QString> params;

    params.push_back("Y-AXIS");

    return params;
}

bool KTGraphic::updateStartParameter(QString field, QString value){
    if(field == "EXAMPLE"){
        startParams.example = value.toInt();
        return true;
    }
    return false;
}

void KTGraphic::mouse_press_event(int width, int height, Point eye, QMouseEvent *event, GraphicTools tool){

}

void KTGraphic::mouse_move_event(int width, int height, Point eye, QMouseEvent *event, GraphicTools tool){

}

vector<double> KTGraphic::KT(){
    vector<double> raw_data;
    for(int i = 0; i < this->raw_data.size(); i++){
        Point* p =this->raw_data[i];
        raw_data.push_back(p->y);
    }

    if(raw_data.size() != N)
        return raw_data;

    vector<double> ux = doUx();
    vector<double> du;
    vector<double> H1; //Hj +1/2
    vector<double> H2; //Hj -1/2
    vector<double> a1; //Hj +1/2
    vector<double> a2; //Hj -1/2
    vector<double> uminus1; //Hj +1/2
    vector<double> uminus2; //Hj -1/2
    vector<double> uplus1; //Hj +1/2
    vector<double> uplus2; //Hj -1/2

    H1.reserve(N);
    H2.reserve(N);
    a1.reserve(N);
    a2.reserve(N);
    uminus1.reserve(N);
    uminus2.reserve(N);
    uplus1.reserve(N);
    uplus2.reserve(N);
    du.reserve(N);

    // U + 1/2
    switch(startParams.coutour_type){
    case 0:
        uplus1[N-1] = raw_data[0] - startParams.dx * ux[0] / 2.0;
        break;
    case 1:
        uplus1[N-1] = raw_data[N-1] - startParams.dx * ux[N-1] / 2.0;
        break;
    default:
        uplus1[N-1] = raw_data[0] - startParams.dx * ux[0] / 2.0;
        break;
    }

    for(int i = 0; i < N - 1; i ++){
        uplus1[i] = raw_data[i+1] - startParams.dx * ux[i+1] / 2.0;
    }

    for(int i = 0; i < N; i++){
        uminus1[i] = raw_data[i] + startParams.dx * ux[i] / 2.0;
    }

    // U - 1/2
    for(int i = 0; i < N; i++){
        uplus2[i] = raw_data[i] - startParams.dx * ux[i] / 2.0;
    }

    switch(startParams.coutour_type){
    case 0:
        uminus2[0] = raw_data[N-1] - startParams.dx * ux[N-1] / 2.0;
        break;
    case 1:
        uminus2[0] = raw_data[0] - startParams.dx * ux[0] / 2.0;
        break;
    default:
        uminus2[0] = raw_data[N-1] - startParams.dx * ux[N-1] / 2.0;
        break;
    }

    for(int i = 1; i < N; i++){
        uminus2[i] = raw_data[i-1] + startParams.dx * ux[i-1] / 2.0;
    }

    for(int i = 0; i < N; i++){
        vector<double> aux1;
        vector<double> aux2;

        aux1.push_back(abs(df(uplus1[i])));
        aux1.push_back(abs(df(uminus1[i])));

        aux2.push_back(abs(df(uplus2[i])));
        aux2.push_back(abs(df(uminus2[i])));

        a1[i] = MathHelper::max(aux1);
        a2[i] = MathHelper::max(aux2);
    }

    for(int i = 0; i < N; i ++){
        H1[i] = ( f(uplus1[i]) + f(uminus1[i]) )/2.0 - a1[i] * (uplus1[i]-uminus1[i])/2.0;
        H2[i] = ( f(uplus2[i])+f(uminus2[i]) ) / 2.0 -  a2[i] * (uplus2[i]-uminus2[i]) / 2.0;
        du[i] = (H2[i] - H1[i]) / startParams.dx;
    }

    return du;
}

double KTGraphic::f(double u){
    switch (startParams.example) {
    case 0:
        return u;
    case 1:
        return pow(u,2)/2;
        break;
    case 2:
        return ((pow(u,2) - 1)*(pow(u,2) - 4)/4.0);
    default:
        return u;
    }
}

double KTGraphic::df(double u){
    switch (startParams.example) {
    case 0:
        return ONE;
    case 1:
        return u;
    case 2:
        return ((2 * u - 1)*(2 * u - 4)/4.0);
    default:
        return ONE;
    }
}

vector<double> KTGraphic::doUx(){
    vector<double> raw_data;
    for(int i = 0; i < this->raw_data.size(); i++){
        Point* p =this->raw_data[i];
        raw_data.push_back(p->y);
    }
    double theta = 2.0;
    vector<double> a;
    vector<double> b;
    vector<double> c;
    vector<double> ux;

    a.reserve(N);
    b.reserve(N);
    c.reserve(N);
    ux.reserve(N);

    switch (startParams.coutour_type) {
    //CIRCULAR
    case 0:
        a[0] = (raw_data[0] - raw_data[N-1])/startParams.dx;
        a[N-1] = (raw_data[N-1] - raw_data[N-2])/startParams.dx;

        b[0] = (raw_data[1] - raw_data[N-1])/(2*startParams.dx);
        b[N-1] = (raw_data[0] - raw_data[N-2])/(2*startParams.dx);

        c[0] = (raw_data[1] - raw_data[0])/startParams.dx;
        c[N-1] = (raw_data[0] - raw_data[N-1])/startParams.dx;
        break;

    //DUPLICATED
    case 1:
        a[0] = (raw_data[0] - raw_data[0])/startParams.dx;
        a[N-1] = (raw_data[N-1] - raw_data[N-2])/startParams.dx;

        b[0] = (raw_data[1] - raw_data[0])/(2*startParams.dx);
        b[N-1] = (raw_data[N-1] - raw_data[N-2])/(2*startParams.dx);

        c[0] = (raw_data[1] - raw_data[0])/startParams.dx;
        c[N-1] = (raw_data[N-1] - raw_data[N-1])/startParams.dx;
        break;
    default:
        a[0] = (raw_data[0] - raw_data[N-1])/startParams.dx;
        a[N-1] = (raw_data[N-1] - raw_data[N-2])/startParams.dx;

        b[0] = (raw_data[1] - raw_data[N-1])/(2*startParams.dx);
        b[N-1] = (raw_data[0] - raw_data[N-2])/(2*startParams.dx);

        c[0] = (raw_data[1] - raw_data[0])/startParams.dx;
        c[N-1] = (raw_data[0] - raw_data[N-1])/startParams.dx;
        break;
    }

    for(int i = 1; i < N-1; i++){
        a[i] = (raw_data[i] - raw_data[i-1])/startParams.dx;
        b[i] = (raw_data[i+1] - raw_data[i-1])/(2.0 * startParams.dx);
        c[i] = (raw_data[i+1] - raw_data[i])/startParams.dx;
    }

    for(int i = 0; i < N; i++){
        ux[i] = minmod(theta * a[i],b[i],theta * c[i]);
    }

    return ux;
}

double KTGraphic::minmod(double a, double b, double c){
    if(a > 0 && b > 0 && c > 0){
        vector<double> aux;
        aux.push_back(a);
        aux.push_back(b);
        aux.push_back(c);
        return MathHelper::min(aux);
    } else if(a < 0 && b < 0 && c < 0) {
        vector<double> aux;
        aux.push_back(a);
        aux.push_back(b);
        aux.push_back(c);
        return MathHelper::max(aux);
    }

    return 0;
}
