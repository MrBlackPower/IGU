#include "summer.h"

Summer::Summer() : ColorBar("SUMMER"){
    QColor c;
    addColor(c.fromRgb(241,48,48));
    addColor(c.fromRgb(241,241,48));
}
