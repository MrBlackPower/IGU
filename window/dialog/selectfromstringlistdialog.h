#ifndef SELECTFROMSTRINGLISTDIALOG_H
#define SELECTFROMSTRINGLISTDIALOG_H

#include <QDialog>
#include <QString>
#include <QTextStream>

#define DEFAULT_TEXT "Select desired value for parameter "

namespace Ui {
class SelectFromStringListDialog;
}

using namespace std;

class SelectFromStringListDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SelectFromStringListDialog(QString name, QString possible_values, QWidget *parent = nullptr);
    ~SelectFromStringListDialog();

    QString getString();

signals:
    void stringListSet();

private slots:
    void on_cbxStringList_activated(const QString &arg1);

    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

    void on_cbxStringList_currentIndexChanged(const QString &arg1);

private:
    Ui::SelectFromStringListDialog *ui;

    QString answer;
};

#endif // SELECTFROMSTRINGLISTDIALOG_H
