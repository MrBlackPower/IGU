#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QFileDialog>
#include <QMainWindow>
#include <math.h>
#include <iostream>
#include <sstream>
#include <string>
#include <cstring>
#include <QThread>
#include "consolewindow.h"
#include "messagewindow.h"
#include "generateccotreewindow.h"
#include "generateweightedgraphwindow.h"
#include "contourwindow.h"
#include "loadwizard.h"
#include "graphicobjectswindow.h"
#include "helper/fileHelper.h"
#include "helper/arterialtreecoreHelper.h"
#include "window/dialog/igusettingsdialog.h"
#include "window/dialog/inputstringdialog.h"
#include "window/schedulerdialog.h"
#include "model/arteryTree.h"
#include "model/graph/automatusgraph.h"
#include "model/colorbar/aero.h"
#include "model/colorbar/brasil.h"
#include "model/graphic2d/ktgraphic.h"
#include "model/graphic2d/lfgraphic.h"
#include "model/graphic2d/nsgraphic.h"
#include "model/graphic3d/ktgraphic3d.h"
#include "model/graphic3d/staticgraphic3d.h"
#include "model/smartlog.h"
#include "canvas/oglsidecanvas.h"
#include "model/graph/weightedgraph.h"

#define CCO_POINT_CONVERSION "POINT_COORDINATES 100.000000"
#define CCO_RADIUS_CONVERSION "raio 0.100000"
#define VERSION "1.2"

/************************************************************************************
  Name:        mainwindow.h
  Copyright:   Version 1.1
  Author:      Igor Pires dos Santos
  Last Update: 17/02/2017 (DD/MM/YYYY)
  Release:     --/--/---- (DD/MM/YYYY)
  Description: Header class of a load window for an artery tree structure.
************************************************************************************/

namespace Ui {
class MainWindow;
}

using namespace std;

namespace window {
    class MainWindow : public QMainWindow
    {
        Q_OBJECT

    public:
    /************************************************************************************
            CONSTRUCTOR
    ************************************************************************************/
        explicit MainWindow(QWidget *parent = 0);

    /************************************************************************************
            DESTRUCTOR
    ************************************************************************************/
        ~MainWindow();

    /************************************************************************************
            Purges the loaded artery tree (if it has one)
    ************************************************************************************/
        void purge();


    public slots:
        void setMainCanvas(GraphicObject* g);
        void setMainCanvas(int gid);
        void purgeMainCanvas();

        void setSideCanvas(GraphicObject* g);
        void setSideCanvas(int gid);
        void purgeSideCanvas();

        void updateLogWindow(vector<QString> log);

        void threadError(QString msg);

        void updateConfig(SystemSettingsHelper config);

        void on_act_Main_Start_triggered();

        void on_act_Main_Iterate_triggered();

        void on_act_Side_Start_triggered();

        void on_act_Side_Iterate_triggered();

    signals:
    /************************************************************************************
            Signals a repaint needed.
    ************************************************************************************/
        void repaint();

    /************************************************************************************
            Signals a message log to the console.
    ************************************************************************************/
        void sendLog(string msg);

    /************************************************************************************
            Signals a text to the console.
    ************************************************************************************/
        void sendConsoleText(vector<QString> txt);
        void emit_log(SmartLogMessage data);

    private slots:
        /************************************************************************************
                Slot responsible for loading the tree after the parameters have been set
                in the LoadWindow.

                @param params parameters set in the LoadWindow.
        ************************************************************************************/
        void setContour(double val);

        void generateCCOTree(GenerateCCOTreeWindow::GenerateParams params);

        void generateGraph(GenerateWeightedGraphWindow::GenerateParams params);

        void on_act_loadWizard_triggered();

        void on_act_Main_Stop_triggered();

        void on_act_Main_Save_triggered();

        void on_act_Main_Dump_triggered();

        void on_act_Side_Save_triggered();

        void on_act_Main_Export_triggered();

        void on_act_Side_Stop_triggered();

        void on_act_Side_Dump_triggered();

        void on_act_Side_Export_triggered();

        void on_btn_main_iterate_clicked();

        void on_btn_main_start_clicked();

        void on_spn_main_time_valueChanged(double arg1);

        void on_btn_main_pause_clicked();

        void on_btn_main_stop_clicked();

        void on_btn_main_details_clicked();

        void on_btn_side_iterate_clicked();

        void on_btn_side_play_clicked();

        void on_spn_side_time_valueChanged(double arg1);

        void on_btn_side_pause_clicked();

        void on_btn_side_details_clicked();

        void on_btn_side_stop_clicked();

        void on_btn_log_save_clicked();

        void on_btn_log_delete_clicked();

        void on_act_GraphcObjectList_triggered();

        void on_act_CCOWindow_triggered();

        void on_act_WeightedGraphGenerator_triggered();

        void on_act_Loadt_WeightedGraph_triggered();

        void on_toolSelect_clicked();

        void on_toolAddNode_clicked();

        void on_toolEditNode_clicked();

        void on_toolDeleteNode_clicked();

        void on_btn_main_onetoone_clicked();

        void on_btn_side_onetoone_clicked();

        void on_toolLimit_clicked();

        void on_toolRotate_clicked();

        void on_toolScale_clicked();

        void on_toolMove_clicked();

        void on_act_settings_triggered();

        void on_act_empty_ArteryTree_triggered();

        void on_act_empty_AutomatusGraph_triggered();

        void on_act_Load_AutomatusGraph_triggered();

        void on_act_scheduler_triggered();

    private:
        //LOG PARAMS START
        SmartLog* log;

        QThread* SmartLogThread;

        SmartThreadPool* thread_pool;

        QTimer* log_timer;

        double log_timeout = 20;

        //LOG PARAMS END

        //WINDOWS

        SchedulerDialog* scheduler;

        IGUSettingsDialog* settingsW;

        MessageWindow* mw;

        GenerateCCOTreeWindow* ccoW;

        GenerateWeightedGraphWindow* graphW;

        ContourWindow* ctW;

        LoadWizard* loadWizard;

        GraphicObjectsWindow* gObjw;

        vector<GraphicObjectDetailsWindow*> detailWindows;

        Ui::MainWindow *ui;

        GraphicSpace::GraphicTools selectedTool;


        void updateTool(GraphicSpace::GraphicTools selection);

    };
}
#endif // MAINWINDOW_H
