#include "pointdatalistwidget.h"

PointDataListWidget::PointDataListWidget(QWidget *parent) : QListWidget(parent)
{

}

bool PointDataListWidget::addDataList(VTKFile file){
    clear();

    if(file.getN() > 0)
        addItem("POINT_COORDINATES");
    else
        return false;

    vector<QString> dataList = file.getPointDataList();
    vector<DataType> dataType = file.getPointDataType();

    for(int i = 0; i < dataType.size(); i++){
        DataType t = dataType[i];

        switch (t) {
        case integer:
            addItem(dataList[i]);
            break;
        case floatp:
            addItem(dataList[i]);
            break;
        case doublep:
            addItem(dataList[i]);
            break;
        case stringp:

            break;
        case normals:

            break;
        case vectors:

            break;
        case tensor:

            break;
        case undefined:

            break;
        default:
            break;
        }

    }
}
