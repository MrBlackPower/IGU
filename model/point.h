#ifndef POINT_H
#define POINT_H

/************************************************************************************
  Name:        point.h
  Copyright:   Version 1.0
  Author:      Igor Pires dos Santos
  Last Update: 17/02/2017 (DD/MM/YYYY)
  Release:     --/--/---- (DD/MM/YYYY)
  Description: Header of a class with (x,y,z) float points representation.
************************************************************************************/

#include <iostream>
#include <math.h>
#include <QPoint>

#define PI 3.14159265

using namespace std;

class Point
{
    public:

/************************************************************************************
        CONSTRUCTOR
        With:
            x = 0
            y = 0
            z = 0
            id = 0
************************************************************************************/
        Point();

        Point(QPoint point);

/************************************************************************************
        CONSTRUCTOR
        With z = 0.

        @param id the id of this point.
        @param x the x-coordinate value of this point.
        @param y the y-coordinate value of this point.
************************************************************************************/
        Point(int id, float x, float y);

/************************************************************************************
        CONSTRUCTOR

        @param id the id of this point.
        @param x the x-coordinate value of this point.
        @param y the y-coordinate value of this point.
        @param z the z-coordinate value of this point.
************************************************************************************/
        Point(int id, float x, float y, float z);

/************************************************************************************
        DESTRUCTOR
************************************************************************************/
        virtual ~Point();

/************************************************************************************
        Clones this point.

        @return a clone pointer to this point.
************************************************************************************/
        Point* clone();

/************************************************************************************
        Checks if this point is equal another point.

        @param p the second point to check equality
        @return if this point if equal p.
************************************************************************************/
        bool equals(Point* p);
        bool equals(Point p);


/********************************************************
*                  GETS & SETS                          *
********************************************************/

        double getAbsoluteValue();

/************************************************************************************
        Getter for this point id.

        @return the id of this point.
************************************************************************************/
        int getID();

        double getPhi();

        double getTheta();

/************************************************************************************
        LIST OF PUBLIC ATTRIBUTES

        x...................value of the x-coordinate

        y...................value of the y-coordinate

        z...................value of the z-coordinate

************************************************************************************/
        //x-coordinate
        double x;

        //y-coordinate
        double y;

        //z-coordinate
        double z;


        /********************************************************
        *                  OPERATORS                            *
        ********************************************************/

        bool operator = (Point b);
        bool operator == (Point b);
        bool operator != (Point b);
        Point operator + (Point b);
        Point operator + (double b);
        Point operator + (float b);
        Point operator + (int b);
        Point operator - (Point b);
        Point operator - (double b);
        Point operator - (float b);
        Point operator - (int b);
        Point operator * (Point b);
        Point operator * (double b);
        Point operator * (float b);
        Point operator * (int b);
        Point operator / (double b);
        Point operator / (float b);
        Point operator / (int b);
    protected:

    private:
/************************************************************************************
        LIST OF PRIVATE ATTRIBUTES

        id..................id of this point

************************************************************************************/

        //ID of this point
        int id;

};


#endif // POINT_H
