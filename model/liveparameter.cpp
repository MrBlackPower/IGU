#include "liveparameter.h"
int LiveParameter::CURRENT_LP_ID= 0;

LiveParameter::LiveParameter(int GID,QString parameterName, DataType type, QString current_value) : LP_ID(CURRENT_LP_ID++)
{
    this->GID = GID;
    this->current_value = current_value;
    parameter_type = type;
    parameter_name = parameterName;
}

DataType LiveParameter::get_data_type(){
    return parameter_type;
}

void* LiveParameter::get_current_value(){
    return VTKFile::getValue(current_value,parameter_type);
}

void* LiveParameter::update_live_parameter(){
    GraphicObject* gObj = GraphicObject::getGraphic(GID);

    if(gObj != NULL){
        current_value = gObj->liveParameter(parameter_name);

        return get_current_value();
    }

    return NULL;
}
