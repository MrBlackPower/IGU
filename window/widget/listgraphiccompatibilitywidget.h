#ifndef LISTGRAPHICCOMPATIBILITYWIDGET_H
#define LISTGRAPHICCOMPATIBILITYWIDGET_H

#include <QListWidget>
#include <QListWidgetItem>
#include "../../model/arteryTree.h"
#include "../../model/graphic2d.h"
#include "../../model/graphic3d.h"
#include "../../model/graphic3d/lines3d.h"
#include "../../model/graphic3d/staticgraphic3d.h"
#include "../../model/graph/weightedgraph.h"

class ListGraphicCompatibilityWidget : public QListWidget
{
    Q_OBJECT
public:
    ListGraphicCompatibilityWidget(QWidget *parent = 0);

public slots:

    void populate(VTKFile file);
};

#endif // LISTGRAPHICCOMPATIBILITYWIDGET_H
