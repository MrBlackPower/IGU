#ifndef ARTERIALTREECOREHELPER_H
#define ARTERIALTREECOREHELPER_H

#include <QString>
#include "../window/generateccotreewindow.h"
#include <QProcess>
#include <iostream>
#include <QFile>
#include <QDir>
#include <QFileDialog>
#include <QTime>
#include <QIODevice>
#include <QTextStream>
#include "helper/fileHelper.h"
#include "vtkfile.h"

#define ONE 1

using namespace std;

class ArterialTreeCoreHelper
{
public:
    static VTKFile generateArterialTree(GenerateCCOTreeWindow::GenerateParams params, QObject* parent = NULL);
};

#endif // ARTERIALTREECOREHELPER_H
