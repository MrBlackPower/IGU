#ifndef MATHHELPER_H
#define MATHHELPER_H

/************************************************************************************
  Name:        mathHelper.h
  Copyright:   Version 1.0
  Author:      Igor Pires dos Santos
  Last Update: 17/02/2017 (DD/MM/YYYY)
  Release:     --/--/---- (DD/MM/YYYY)
  Description: Header of a static class to help manage mathemathical
               expressions.
************************************************************************************/

#include <math.h>
#include <iostream>
#include <vector>
#include <random>
#include <QTime>
#include "../model/point.h"
#include "../model/graph/solution.h"
#include "../model/graph/cost.h"
#include "smartthread/smartthreadpool.h"
#include <omp.h>

#define PI 3.14159265359

#define ONE 1
#define ZERO 0

#define ESSENTIALY_ZERO 0.1e-40

using namespace std;

class MathHelper
{

    public:

    enum ParallelParadigma{
        OPENMP,
        MPI,
        QTHREAD
    };

    enum ListOrder{
        CRESCENT,
        DECRESCENT
    };

    enum SolutionOrder{
        SCORE,
        COST
    };

        static int chose_from_cdf(vector<float> cdf);
        static int chose_from_cdf(vector<double> cdf);

        static int chose_from_cdf(vector<float>* cdf);
        static int chose_from_cdf(vector<double>* cdf);

        static bool orderSolutions(vector<Solution>* solutions, SolutionOrder sol_field = COST, ListOrder order = CRESCENT);
        static bool orderCosts(vector<Cost>* costs, ListOrder order = CRESCENT);

        static bool orderVector(vector<int>* vec, ListOrder order = CRESCENT);

        static bool isIn(vector<int>* vec, int value);
        static double euclideanDistance(Point* p1, Point* p2);
        static bool isBetween(float val, float max, float min);
        static bool isPositive(double val);
        static double secant(double val);
        static double cotangent(double val);
        static double tangent(double val);
        static double cosine(double val);
        static double sine(double val);
        static double degreeToRadian(double val);
        static float max(vector<float> v);
        static float max(vector<double> v);
        static float min(vector<float> v);
        static float min(vector<double> v);
        static void randomSeed();
        static double random(double max, double min = 0);
        static Point rotateX(Point p, double rx);
        static Point rotateY(Point p, double ry);
        static Point rotateZ(Point p, double rz);
        static double sign(double a, double b);
        static float sign(float a, float b);
        static int sign(int a, int b);
        static Point interpolate(Point a, Point b, double a_scalar, double b_scalar, double f);

        static QString paradigmaToString(ParallelParadigma paradigma);
        static MathHelper::ParallelParadigma stringToParadigma(QString string);

        static bool setThreadCount(int threads);
        static int getThreads();

        static MathHelper::ParallelParadigma default_paradigma;
        static bool parallel;
        static int threads;
        static int threads_in_blocks;
        static int thread_blocks;
        static SmartThreadPool* thread_pool;
    protected:

    private:
};

#endif // MATHHELPER_H
