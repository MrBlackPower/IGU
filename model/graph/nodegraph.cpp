#include "nodegraph.h"

template <class n_type, class e_type>
NodeGraph<n_type,e_type>::NodeGraph(int id, QString name, n_type data, double x, double y, double z, SmartObject* parent) : Node<n_type>(id,name,data,x,y,z,parent)
{
    E = 0;

    hovered = false;
    selected = false;
}

template <class n_type, class e_type>
int NodeGraph<n_type,e_type>::getE(){
    return E;
}

template <class n_type, class e_type>
void NodeGraph<n_type,e_type>::addEdge(NodeGraph* n, e_type cost){
    edges.push_back(n);
    this->cost.push_back(cost);
    edge_hovered.push_back(false);
    edge_selected.push_back(false);
    E++;
}

template <class n_type, class e_type>
bool NodeGraph<n_type,e_type>::neighboorOf(NodeGraph<n_type,e_type>* n){
    for(int i = 0; i < edges.size(); i++)
        if(n->equals(edges[i]))
            return true;

    return false;
}

template <class n_type, class e_type>
void NodeGraph<n_type,e_type>::removeEdge(NodeGraph* n){
    for(int i = 0; i < edges.size(); i++){
        if(n->equals(edges[i])){
            NodeGraph* nAux = edges[i];
            edges[i] = edges[edges.size() - 1];
            edges[edges.size() - 1] = nAux;
            edges.pop_back();

            e_type cAux = cost[i];
            cost[i] = cost[cost.size() - 1];
            cost[cost.size() - 1] = cAux;
            cost.pop_back();

            bool bnAux = edge_hovered[i];
            edge_hovered[i] = edge_hovered[edge_hovered.size() - 1];
            edge_hovered[edge_hovered.size() - 1] = nAux;
            edge_hovered.pop_back();

            bnAux = edge_selected[i];
            edge_selected[i] = edge_selected[edge_selected.size() - 1];
            edge_selected[edge_selected.size() - 1] = nAux;
            edge_selected.pop_back();

            E--;
            return;
        }
    }
}

template <class n_type, class e_type>
vector<NodeGraph<n_type,e_type>*> NodeGraph<n_type,e_type>::getEdges(){
    vector<NodeGraph<n_type,e_type>*> aux = edges;
    return aux;
}

template <class n_type, class e_type>
void NodeGraph<n_type,e_type>::getEdges(vector<NodeGraph<n_type,e_type>*>* vec){
    vec->clear();

    for (int i = 0; i < edges.size(); i++) {
        vec->push_back(edges[i]);
    }
}

template <class n_type, class e_type>
vector<e_type> NodeGraph<n_type,e_type>::getCosts(){
    vector<e_type> aux = cost;
    return aux;
}

template <class n_type, class e_type>
vector<bool> NodeGraph<n_type,e_type>::getHovered(){
    vector<bool> aux = edge_hovered;
    return aux;
}

template <class n_type, class e_type>
vector<bool> NodeGraph<n_type,e_type>::getSelected(){
    vector<bool> aux = edge_selected;
    return aux;
}

template <class n_type, class e_type>
bool NodeGraph<n_type,e_type>::updateCost(NodeGraph<n_type,e_type>* endNode, e_type cost){
    for(int i = 0; i < edges.size(); i++){
        NodeGraph<n_type,e_type>* n = edges[i];
        if(n->equals(endNode)){
            this->cost[i] = cost;
            return true;
        }
    }

    return false;
}

template <class n_type, class e_type>
void NodeGraph<n_type,e_type>::hover(){
    hovered = true;
}

template <class n_type, class e_type>
void NodeGraph<n_type,e_type>::unhover(){
    hovered = false;
}

template <class n_type, class e_type>
void NodeGraph<n_type,e_type>::select(){
    selected = true;
}
template <class n_type, class e_type>
void NodeGraph<n_type,e_type>::unselect(){
    selected = false;
}

template <class n_type, class e_type>
void NodeGraph<n_type,e_type>::hover_edge(int m){
    if(m >= edges.size())
        return;

    edge_hovered[m] = true;
}

template <class n_type, class e_type>
void NodeGraph<n_type,e_type>::unhover_edge(int m){
    if(m >= edges.size())
        return;

    edge_hovered[m] = false;
}

template <class n_type, class e_type>
void NodeGraph<n_type,e_type>::select_edge(int m){
    if(m >= edges.size())
        return;

    edge_selected[m] = true;
}

template <class n_type, class e_type>
void NodeGraph<n_type,e_type>::unselect_edge(int m){
    if(m >= edges.size())
        return;

    edge_selected[m] = false;
}

template <class n_type, class e_type>
NodeGraph<n_type,e_type>::~NodeGraph(){

}

template class NodeGraph<int,int>;
template class NodeGraph<int,float>;
template class NodeGraph<int,double>;
template class NodeGraph<int,QString>;
template class NodeGraph<float,int>;
template class NodeGraph<float,float>;
template class NodeGraph<float,double>;
template class NodeGraph<float,QString>;
template class NodeGraph<double,int>;
template class NodeGraph<double,float>;
template class NodeGraph<double,double>;
template class NodeGraph<double,QString>;
template class NodeGraph<QString,int>;
template class NodeGraph<QString,float>;
template class NodeGraph<QString,double>;
template class NodeGraph<QString,QString>;

