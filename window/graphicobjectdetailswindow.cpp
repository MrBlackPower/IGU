#include "graphicobjectdetailswindow.h"
#include "ui_graphicobjectdetailswindow.h"

GraphicObjectDetailsWindow::GraphicObjectDetailsWindow(GraphicObject* gObj, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::GraphicObjectDetailsWindow)
{
    this->gObj = gObj;
    ui->setupUi(this);

    update();
}

GraphicObjectDetailsWindow::~GraphicObjectDetailsWindow()
{
    delete ui;
}

void GraphicObjectDetailsWindow::on_btnOK_clicked()
{
    destroy();
}

void GraphicObjectDetailsWindow::on_btnStart_clicked()
{
    if(gObj != NULL){
        gObj->start();
    }

    update();
}

void GraphicObjectDetailsWindow::on_btnUpdate_clicked()
{
    update();
}

void GraphicObjectDetailsWindow::update(){
    if(gObj != NULL){
        ui->startListWidget->flush();
        ui->visualListWidget->flush();
        ui->rangeListWidget->flush();
        ui->graphicObjectTreeWidget->clear();
        ui->concurrentGraphicObjectsListWidget->flush();
        ui->extrapolationListWidget->flush();
        ui->currentRangeBrowser->clear();
        ui->currentColorBarBrowser->clear();
        ui->ColorbarListWidget->flush();

        vector<QString> startParams;
        vector<Field> startParams_raw = gObj->getStartParameters();
        vector<QString> visualParams;
        vector<Field> visualParams_raw = gObj->getVisualParameters();
        vector<QString> ranges = gObj->getRanges();
        vector<QString> concurrentGraphicObjects;
        vector<QString> extrapolationList;
        QString currentRange = gObj->getCurrentRange();
        vector<ColorBar> colorBarList = GraphicObject::getPossibleColorbars();

        for(int i = 0; i < startParams_raw.size(); i++){
            QString aux;
            Field curr = startParams_raw[i];
            startParams.push_back(aux.asprintf("%s %s",curr.names.toStdString().data(),curr.current_value.toStdString().data()));
        }
        ui->startListWidget->addItem(startParams);

        for(int i = 0; i < visualParams_raw.size(); i++){
            QString aux;
            Field curr = visualParams_raw[i];
            visualParams.push_back(aux.asprintf("%s %s",curr.names.toStdString().data(),curr.current_value.toStdString().data()));
        }
        ui->visualListWidget->addItem(visualParams);

        ui->rangeListWidget->addItem(ranges);

        ui->graphicObjectTreeWidget->populate();

        vector<GraphicObject*> concurrents = gObj->getConcurrents();
        for(int i = 0; i < concurrents.size(); i++){
            GraphicObject* concurrent = concurrents[i];
            QString aux;
            aux = aux.asprintf("%d : %s",concurrent->getGID(),concurrent->getName().toStdString().data());
            concurrentGraphicObjects.push_back(aux);
        }

        ui->concurrentGraphicObjectsListWidget->addItem(concurrentGraphicObjects);

        if(gObj)
            extrapolationList = gObj->extrapolations();

        ui->extrapolationListWidget->addItem(extrapolationList);

        ui->currentRangeBrowser->setText(currentRange);

        for(int i = 0; i < colorBarList.size(); i++){
            ColorBar a = colorBarList[i];
            ui->ColorbarListWidget->addItem(a.getName());
        }

        if(gObj)
            ui->currentColorBarBrowser->setText(gObj->getColorBar());
    }
}

void GraphicObjectDetailsWindow::on_startListWidget_itemDoubleClicked(QListWidgetItem *item){
    if(gObj != NULL){
        QString s = item->text();
        QTextStream stream(&s);
        QString name;
        stream >> name;

        bool found = false;
        vector<Field> startParams_raw = gObj->getStartParameters();
        Field selection;
        for(int i = 0; i < startParams_raw.size(); i++){
            Field f = startParams_raw[i];
            if(f.names == name){
                found = true;
                selection = f;
                break;
            }
        }

        if(!found)
            return;

        if(selection.type == doublep || selection.type == floatp|| selection.type == integer){
            InputScalarDialog* dialog = new InputScalarDialog(this);
            dialog->setScalar(name);
            QEventLoop loop;
            QTimer timeout;

            connect(dialog,SIGNAL(scalarSet()),&loop,SLOT(quit()));
            connect(&timeout,SIGNAL(timeout()),&loop,SLOT(quit()));

            timeout.start(TIMEOUT);
            dialog->show();
            loop.exec();

            if(dialog->Set()){
                gObj->updateStartParameter(name,name.asprintf("%f",dialog->getScalar()));
            }

            delete dialog;
        } else if(selection.type == stringlist){
            SelectFromStringListDialog* dialog = new SelectFromStringListDialog(selection.names,selection.possible_values,this);
            QEventLoop loop;
            QTimer timeout;

            connect(dialog,SIGNAL(stringListSet()),&loop,SLOT(quit()));
            connect(&timeout,SIGNAL(timeout()),&loop,SLOT(quit()));

            timeout.start(TIMEOUT);
            dialog->show();
            loop.exec();

            gObj->updateStartParameter(name,dialog->getString());

            delete dialog;

        } else if(selection.type == stringp){
            InputStringDialog* dialog = new InputStringDialog(selection.names,selection.current_value,this);
            QEventLoop loop;
            QTimer timeout;

            connect(dialog,SIGNAL(stringSet()),&loop,SLOT(quit()));
            connect(&timeout,SIGNAL(timeout()),&loop,SLOT(quit()));

            timeout.start(TIMEOUT);
            dialog->show();
            loop.exec();

            if(dialog->Set())
                gObj->updateStartParameter(name,dialog->getString());

            delete dialog;

        } else if(selection.type == _boolean){
            InputBooleanDialog* dialog = new InputBooleanDialog(this);
            dialog->setBoolean(name);
            QEventLoop loop;
            QTimer timeout;

            connect(dialog,SIGNAL(booleanSet()),&loop,SLOT(quit()));
            connect(&timeout,SIGNAL(timeout()),&loop,SLOT(quit()));

            timeout.start(TIMEOUT);
            dialog->show();
            loop.exec();

            gObj->updateStartParameter(name,name.asprintf("%d",((dialog->getBoolean())?1:0)));

            delete dialog;
        }
    }

    update();
}

void GraphicObjectDetailsWindow::on_visualListWidget_itemDoubleClicked(QListWidgetItem *item){
    if(gObj != NULL){
        QString s = item->text();
        QTextStream stream(&s);
        QString name;
        stream >> name;

        bool found = false;
        vector<Field> visualParams_raw = gObj->getVisualParameters();
        Field selection;
        for(int i = 0; i < visualParams_raw.size(); i++){
            Field f = visualParams_raw[i];
            if(f.names == name){
                found = true;
                selection = f;
                break;
            }
        }

        if(!found)
            return;

        if(selection.type == doublep || selection.type == floatp|| selection.type == integer){
            InputScalarDialog* dialog = new InputScalarDialog(this);
            dialog->setScalar(name);
            QEventLoop loop;
            QTimer timeout;

            connect(dialog,SIGNAL(scalarSet()),&loop,SLOT(quit()));
            connect(&timeout,SIGNAL(timeout()),&loop,SLOT(quit()));

            timeout.start(TIMEOUT);
            dialog->show();
            loop.exec();

            if(dialog->Set()){
                gObj->updateVisualParameter(name,name.asprintf("%f",dialog->getScalar()));
            }

            delete dialog;
        } else if(selection.type == stringlist){
            SelectFromStringListDialog* dialog = new SelectFromStringListDialog(selection.names,selection.possible_values,this);
            QEventLoop loop;
            QTimer timeout;

            connect(dialog,SIGNAL(stringListSet()),&loop,SLOT(quit()));
            connect(&timeout,SIGNAL(timeout()),&loop,SLOT(quit()));

            timeout.start(TIMEOUT);
            dialog->show();
            loop.exec();

            gObj->updateStartParameter(name,dialog->getString());

            delete dialog;

        } else if(selection.type == stringp){
            InputStringDialog* dialog = new InputStringDialog(selection.names,selection.current_value,this);
            QEventLoop loop;
            QTimer timeout;

            connect(dialog,SIGNAL(stringSet()),&loop,SLOT(quit()));
            connect(&timeout,SIGNAL(timeout()),&loop,SLOT(quit()));

            timeout.start(TIMEOUT);
            dialog->show();
            loop.exec();

            if(dialog->Set())
                gObj->updateStartParameter(name,dialog->getString());

            delete dialog;

        } else if(selection.type == _boolean){
            InputBooleanDialog* dialog = new InputBooleanDialog(this);
            dialog->setBoolean(name);
            QEventLoop loop;
            QTimer timeout;

            connect(dialog,SIGNAL(booleanSet()),&loop,SLOT(quit()));
            connect(&timeout,SIGNAL(timeout()),&loop,SLOT(quit()));

            timeout.start(TIMEOUT);
            dialog->show();
            loop.exec();

            gObj->updateVisualParameter(name,name.asprintf("%d",((dialog->getBoolean())?1:0)));

            delete dialog;
        }
    }

    update();
}

void GraphicObjectDetailsWindow::on_rangeListWidget_itemDoubleClicked(QListWidgetItem *item){
    if(gObj != NULL){
        QString name = item->text();
        gObj->setRange(name);
    }

    update();
}

void GraphicObjectDetailsWindow::on_graphicObjectTreeWidget_itemDoubleClicked(QTreeWidgetItem *item, int column)
{
    GraphicObject* selection = GraphicObject::getGraphic(item->text(0).toInt());

    if(gObj && selection){
        if(!selection->operator ==(gObj))
            gObj->addConcurrent(selection);
    }

    update();
}

void GraphicObjectDetailsWindow::on_concurrentGraphicObjectsListWidget_itemDoubleClicked(QListWidgetItem *item)
{
    QString aux = item->text();
    QTextStream s(&aux);
    int gid = -1;
    s >> gid;

    if(gObj && gid != -1){
        if(gObj->getGID() != gid)
            gObj->removeConcurrent(gid);
    }

    update();
}

void GraphicObjectDetailsWindow::on_extrapolationListWidget_itemDoubleClicked(QListWidgetItem *item)
{
    if(gObj){
        gObj->extrapolate(item->text());
    }
}

void GraphicObjectDetailsWindow::on_ColorbarListWidget_itemDoubleClicked(QListWidgetItem *item)
{
    if(gObj){
        gObj->setColorBar(item->text());
    }

    update();
}

void GraphicObjectDetailsWindow::on_btnSchedule_clicked()
{
    if(gObj){
        QString schedule_file = QFileDialog::getSaveFileName(this,"Schedule File");

        QString fileName = QFileDialog::getOpenFileName(this, tr("Open Instance File"),"/home/");

        QString logfileName = QFileDialog::getSaveFileName(this,"Export Log");

        Schedule s(gObj,fileName,logfileName,10);

        s.xml(s,schedule_file);
    }
}
