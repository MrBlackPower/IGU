#include "smartlog.h"

SmartLog::SmartLog() : SmartObject("SMART LOG")
{
    N = 0;
    record_file = NULL;
    toFinish = false;

    record_file = new QFile(DEFAULT_LOG);
    record_file->open(QFile::WriteOnly | QFile::Truncate);
}

SmartLog::~SmartLog(){
    if(record_file != NULL){
        if(record_file->isOpen())
            record_file->close();

        delete record_file;

        for (int i = 0; i < files_open.size(); i++) {
            QFile* rf;
            if(rf->isOpen())
                rf->close();

            delete rf;        }
    }
}

void SmartLog::clean(){
    N = 0;
    emit_log.clear();
}

void SmartLog::updateLogWindow(int lines){
    if(lines <= 0)
        return;

    int N = (this->N > MAX_SIZE)? this->N % MAX_SIZE : this->N;

    int first = N - 1 - lines;
    vector<int> ids;

    if(first < 0){
        if(this->N > MAX_SIZE){
            for (int i = MAX_SIZE - first; i < MAX_SIZE; i++) {
                ids.push_back(i);
            }
            first = 0;
        } else {
            first = 0;
        }
    }

    for (int i = first; i < N; i++) {
        ids.push_back(i);
    }

    vector<QString> logWindow;

    for (int i = 0; i < ids.size(); i++) {
        SmartLogMessage line = emit_log[(ids[i])];

        logWindow.push_back(line.toString());
    }

    emit updateLog(logWindow);
}

vector<QString> SmartLog::print(){
    vector<QString> aux;
    SmartLogMessage* line;

    for(int i = 0; i < emit_log.size(); i++){
        line = &emit_log[i];
        aux.push_back(line->toString());
    }

    return aux;
}

vector<QString> SmartLog::raw(){
    return print();
}

vector<QString> SmartLog::data(){
    return print();
}

void SmartLog::process(){
    if(record_file != NULL){
        if(!record_file->isOpen())
            record_file->open(QFile::WriteOnly | QFile::Truncate);
    } else {
        record_file = new QFile(DEFAULT_LOG);
        record_file->open(QFile::WriteOnly | QFile::Truncate);
    }

    if(!record_file->isOpen()){
        emit finished();
        return;
    }

    if(!buffer.vtks.empty()){
        for(int i = 0; i < buffer.vtks.size(); i++){
            QString fileName = buffer.vtks_filenames[i];

            if(fileName == DEFAULT_LOG){
                if(record_file->isOpen()){
                    QTextStream rec(record_file);
                    VTKFile vtk = buffer.vtks[i];
                    vector<QString> raw = vtk.print();

                    for(int j = 0; j < raw.size(); j++)
                        rec << raw[i].toStdString().data() << endl;

                    SmartLogMessage msg(getSID(),fileName.asprintf("FILE <%s> SAVED!",fileName.toStdString().data()));

                    addToBuffer(msg);
                }
            } else {
                QFile* f = new QFile(fileName);
                f->open(QIODevice::ReadWrite | QIODevice::Truncate | QIODevice::Text);


                if(f->isOpen()){
                    QTextStream rec(f);
                    VTKFile vtk = buffer.vtks[i];
                    vector<QString> raw = vtk.print();

                    for(int j = 0; j < raw.size(); j++)
                        rec << raw[i].toStdString().data() << endl;

                    SmartLogMessage msg(getSID(),fileName.asprintf("FILE <%s> SAVED!",fileName.toStdString().data()));

                    addToBuffer(msg);
                }

                f->close();
                f->deleteLater();
            }
            buffer.buffer_size --;

            buffer.vtks.erase(buffer.vtks.begin() + i);
            buffer.vtks_filenames.erase(buffer.vtks_filenames.begin() + i);
            i--;
        }
    }

    if(!buffer.raws.empty()){
        for(int i = 0; i < buffer.raws.size(); i++){
            QString fileName = buffer.raws_filenames[i];


            if(fileName == DEFAULT_LOG){
                if(record_file->isOpen()){
                    QTextStream rec(record_file);
                    vector<QString> raw = buffer.raws[i];

                    for(int j = 0; j < raw.size(); j++)
                        rec << raw[j].toStdString().data() << endl;

                    SmartLogMessage msg(getSID(),fileName.asprintf("FILE <%s> SAVED!",fileName.toStdString().data()));

                    addToBuffer(msg);
                }
            } else {
                QFile* f = new QFile(fileName);
                f->open(QIODevice::ReadWrite | QIODevice::Truncate | QIODevice::Text);


                if(f->isOpen()){
                    QTextStream rec(f);
                    vector<QString> raw = buffer.raws[i];

                    for(int j = 0; j < raw.size(); j++)
                        rec << raw[j].toStdString().data() << endl;

                    SmartLogMessage msg(getSID(),fileName.asprintf("FILE <%s> SAVED!",fileName.toStdString().data()));

                    addToBuffer(msg);
                }

                f->close();
                f->deleteLater();
            }

            buffer.buffer_size --;

            buffer.raws.erase(buffer.raws.begin() + i);
            buffer.raws_filenames.erase(buffer.raws_filenames.begin() + i);
            i--;
        }
    }

    if(!buffer.logs.empty()){
        for(int i = 0; i < buffer.logs.size(); i++){
            QString fileName = buffer.logs_filenames[i];

            if(fileName == DEFAULT_LOG){
                QTextStream rec(record_file);
                SmartLogMessage msg = buffer.logs[i];

                if(N < MAX_SIZE){
                    emit_log.push_back(msg);
                } else {
                    int id = N % MAX_SIZE;

                    emit_log[id] = msg;
                }

                N++;

                rec << msg.toString() << endl;
            } else {
                bool found = false;
                int id = 0;

                for (int j = 0; j < files_open_name.size() && !found; j++) {
                    if(files_open_name[j] == fileName){
                        found = true;
                        id = j;
                    }
                }

                QFile* f;

                if(!found){
                    f = new QFile(fileName);
                    f->open(QFile::Truncate | QFile::ReadWrite);

                    files_open.push_back(f);
                    files_open_name.push_back(fileName);
                } else {
                    f = files_open[id];
                }


                if(f->isOpen()){
                    QTextStream rec(f);
                    SmartLogMessage msg = buffer.logs[i];


                    if(N < MAX_SIZE){
                        emit_log.push_back(msg);
                    } else {
                        int id = N % MAX_SIZE;

                        emit_log[id] = msg;
                    }
                    N++;

                    rec  << msg.toString() << endl;
                }
            }


            buffer.buffer_size --;

            buffer.logs.erase(buffer.logs.begin() + i);
            buffer.logs_filenames.erase(buffer.logs_filenames.begin() + i);
            i--;
        }
    }

    updateLogWindow();

    if(toFinish)
        emit finished();
}

void SmartLog::addToBuffer(SmartLogMessage data, QString filename){
    if(filename == "")
        return;

    if(buffer.buffer_size >= MAX_BUFFER)
        return;

    buffer.logs.push_back(data);
    buffer.logs_filenames.push_back(filename);

    buffer.buffer_size ++;
}

void SmartLog::addToBuffer(VTKFile data, QString filename){
    if(buffer.buffer_size >= MAX_BUFFER)
        return;

    if(filename == ""){
        QString format("dd_MM_yyyy_hh_mm_ss_zzz");
        QDateTime current_time = QDateTime::currentDateTime();
        filename = filename.asprintf("raw/igu_raw_%s",current_time.toString(format).toStdString().data());
    }

    buffer.vtks.push_back(data);
    buffer.vtks_filenames.push_back(filename);

    buffer.buffer_size ++;
}

void SmartLog::addToBuffer(vector<QString> data, QString filename){
    if(buffer.buffer_size >= MAX_BUFFER)
        return;

    if(filename == ""){
        QString format("dd_MM_yyyy_hh_mm_ss_zzz");
        QDateTime current_time = QDateTime::currentDateTime();
        filename = filename.asprintf("raw/igu_raw_%s",current_time.toString(format).toStdString().data());
    }

    buffer.raws.push_back(data);
    buffer.raws_filenames.push_back(filename);

    buffer.buffer_size ++;
}

void SmartLog::finish(){
    toFinish = true;
}
