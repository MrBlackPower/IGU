#include "loadwizard.h"
#include "ui_loadwizard.h"

LoadWizard::LoadWizard(QWidget *parent) :
    QWizard(parent),
    ui(new Ui::LoadWizard)
{
    ui->setupUi(this);
    params.file.clear();
    params.file.setProgressBar(ui->loadProgress);
    viewDataDialog = new ViewDataDialog(this);
    result = NULL;
    mainCanvas = false;
    sideCanvas = false;
}

LoadWizard::~LoadWizard()
{
    delete ui;
}

void LoadWizard::on_btn_browse_clicked()
{
    params.fileName = QFileDialog::getOpenFileName(this, tr("Open VTK File"),"/home/",tr("VTK File (*.vtk)"));
    ui->ln_file->clear();
    ui->ln_file->insert(params.fileName);
}

void LoadWizard::on_ln_name_editingFinished()
{
    params.name = ui->ln_name->text();
}

void LoadWizard::on_ln_file_editingFinished()
{
    params.fileName = ui->ln_file->text();
}

void LoadWizard::on_LoadWizard_currentIdChanged(int id)
{
    switch (id) {
        case STEP1:{
            if(params.last_page == STEP2 && params.loaded){
                ui->ln_name->clear();
                params.name = "";
                ui->ln_file->clear();
                params.file = VTKFile();
                params.loaded = false;
            }

            params.last_page = STEP1;
            break;
        }
        case STEP2:{
            if(!params.loaded){
                back();
                return;
            }

            if(params.last_page == STEP3){
                params.file = params.original;
            }

            ui->PointDataList->addDataList(params.file);
            ui->CellDataList->addDataList(params.file);
            ui->PointConversionList->flush();
            ui->CellConversionList->flush();

            params.last_page = STEP2;
            break;
        }
        case STEP3:{
            if(!params.loaded){
                back();
                back();
                return;
            }

            //SCALES VTK THROUGH PRE-SET CONVERSIONS
            vector<QString> pointConversions = ui->PointConversionList->getItems();
            if(pointConversions.size() < 1){
                back();
                return;
            }

            bool found = false;
            for(int i = 0; i < pointConversions.size() && !found; i++){
                QString conv = pointConversions[i];
                if(conv.contains("POINT_COORDINATES")){
                    found = true;
                }
            }

            if(!found){
                back();
                return;
            }

            vector<QString> cellConversions = ui->CellConversionList->getItems();

            for(int i = 0; i < pointConversions.size(); i++)
                cout << pointConversions[i].toStdString().data() << endl;

            for(int i = 0; i < cellConversions.size(); i++)
                cout << cellConversions[i].toStdString().data() << endl;

            params.file = params.file.scaleThrough(pointConversions,cellConversions);

            //POINTS
            vector<Point> points = params.file.getPoints();
            for(int i = 0; i < points.size(); i++){
                QString aux;
                Point p = points[i];
                aux = aux.asprintf("%f %f %f",p.x,p.y,p.z);
                ui->lstPointsWidget->addItem(aux);
            }

            //#POINTS
            ui->ln_points->clear();
            ui->ln_points->setText(params.name.asprintf("%d",points.size()));

            //POINTS_DATA
            ui->lstPointDataWidget->addDataList(params.file);

            //LINES
            vector<vtk::Line> lines = params.file.getLines();
            for(int i = 0; i < lines.size(); i++){
                QString aux = "";
                vtk::Line l = lines[i];


                aux += aux.asprintf("%d",l.n);

                for(int j = 0; j < l.points.size(); j++)
                    aux += aux.asprintf(" %d",l.points[j]);

                ui->lstLinesWidget->addItem(aux);
            }

            //#LINES
            ui->ln_lines->clear();
            ui->ln_lines->setText(params.name.asprintf("%d",lines.size()));

            //CELLS
            vector<Cell> cells = params.file.getCells();
            for(int i = 0; i < cells.size(); i++){
                QString aux = "";
                Cell c = cells[i];

                aux += aux.asprintf("%d",c.n);

                for(int j = 0; j < c.points.size(); j++)
                    aux += aux.asprintf(" %d",c.points[j]);

                ui->lstCellsWidget->addItem(aux);
            }

            //#CELLS
            ui->ln_cells->clear();
            ui->ln_cells->setText(params.name.asprintf("%d",cells.size()));

            //CELL_DATA
            ui->lstCellDataListWidget->addDataList(params.file);

            params.last_page = STEP3;
            break;
        }
        case STEP4:{
            if(params.last_page = STEP5){
                delete result;
            }

            ui->graphicCompatibilityListWidget->populate(params.file);

            params.last_page = STEP4;
            break;
        }
        case STEP5:{
            if(params.last_page == STEP4 && result == NULL){
                QListWidgetItem* selectedItem = ui->graphicCompatibilityListWidget->selectedItems().first();
                on_graphicCompatibilityListWidget_itemDoubleClicked(selectedItem);
            }
            //CHECKS IF OBJECT IS NOT NULL
            if(result == NULL){
                back();
                return;
            }

            if(!ui->chkMainCanvas->isChecked())
                ui->chkMainCanvas->click();

            params.last_page = STEP5;
            break;
        }
        default:{
            break;
        }
    }
}

void LoadWizard::clear(){
    params.last_page = 0;
    params.stat = LoadWizardParams::Initialized;
    params.name = "";
    params.fileName = "";
    params.loaded = false;
    params.file.clear();
    ui->ln_cells->clear();
    ui->lstCellsWidget->clear();
    ui->lstCellsWidget->flush();
    ui->ln_lines->clear();
    ui->lstLinesWidget->clear();
    ui->lstLinesWidget->flush();
    ui->ln_points->clear();
    ui->lstPointsWidget->clear();
    ui->lstPointsWidget->flush();
}

void LoadWizard::on_LoadWizard_finished(int result){
    if(mainCanvas)
        emit registerMainCanvas(this->result);

    if(sideCanvas)
        emit registerSideCanvas(this->result);
}

void LoadWizard::on_btn_load_clicked()
{
    if(params.fileName == "" || params.name == "")
        return;

    params.file.load(params.fileName);
    params.original = params.file;

    params.loaded = params.file.checkIntegrity();
    params.stat = Loaded;

    next();
}

void LoadWizard::on_PointDataList_itemDoubleClicked(QListWidgetItem *item)
{
    QString name = item->text();
    InputScalarDialog* dialog = new InputScalarDialog(this);
    dialog->setScalar(name);
    QEventLoop loop;
    QTimer timeout;

    connect(dialog,SIGNAL(scalarSet()),&loop,SLOT(quit()));
    connect(&timeout,SIGNAL(timeout()),&loop,SLOT(quit()));

    timeout.start(TIMEOUT);
    dialog->show();
    loop.exec();

    if(dialog->Set()){
        QString conversion;
        conversion = name + conversion.asprintf(" %f", dialog->getScalar());
        ui->PointConversionList->addItem(conversion);

        //REMOVES CURRENT ROW
        ui->PointDataList->takeItem(ui->PointDataList->row(item));
    }

    delete dialog;
}

void LoadWizard::on_CellDataList_itemDoubleClicked(QListWidgetItem *item)
{
    QString name = item->text();
    InputScalarDialog* dialog = new InputScalarDialog(this);
    dialog->setScalar(name);
    QEventLoop loop;
    QTimer timeout;

    connect(dialog,SIGNAL(scalarSet()),&loop,SLOT(quit()));
    connect(&timeout,SIGNAL(timeout()),&loop,SLOT(quit()));

    timeout.start(TIMEOUT);
    dialog->show();
    loop.exec();

    if(dialog->Set()){
        QString conversion;
        conversion = name + conversion.asprintf(" %f", dialog->getScalar());
        ui->CellConversionList->addItem(conversion);

        //REMOVES CURRENT ROW
        ui->CellDataList->takeItem(ui->CellDataList->row(item));
    }

    delete dialog;
}

void LoadWizard::on_lstPointDataWidget_itemDoubleClicked(QListWidgetItem *item)
{
    if(params.last_page == STEP3){
        viewDataDialog->fillPointData(params.file,item->text());
        viewDataDialog->show();
    }
}

void LoadWizard::on_lstCellDataListWidget_itemDoubleClicked(QListWidgetItem *item)
{
    if(params.last_page == STEP3){
        viewDataDialog->fillCellData(params.file,item->text());
        viewDataDialog->show();
    }
}

void LoadWizard::on_graphicCompatibilityListWidget_itemDoubleClicked(QListWidgetItem *item)
{
    QString object = item->text();
    if (object == "Artery Tree"){
        ArteryTree* at = new ArteryTree(params.name);
        at->load(&params.file);
        result = at;
        next();
    } else if (object == "KT Graphic [2D]"){
        KTGraphic* kt = new KTGraphic(params.name);
        kt->load(&params.file);
        result = kt;
        next();
    } else if (object == "LF Graphic [2D]"){
        LFGraphic* lf = new LFGraphic(params.name);
        lf->load(&params.file);
        result = lf;
        next();
    } else if (object == "Static Graphic [2D]"){
        StaticGraphic* g = new StaticGraphic(params.name);
        g->load(&params.file);
        result = g;
        next();
    } else if (object == "KT Graphic [3D]"){
        KTGraphic3D* kt = new KTGraphic3D(params.name);
        kt->load(&params.file);
        result = kt;
        next();
    } else if (object == "Static Graphic [3D]"){
        StaticGraphic3D* g = new StaticGraphic3D(params.name);
        g->load(&params.file);
        result = g;
        next();
    } else if (object == "3D Lines"){
        Lines3D* l3d = new Lines3D(params.name);
        l3d->load(&params.file);
        result = l3d;
        next();
    } else if (object == "Graph"){
        WeightedGraph* g = new WeightedGraph(params.name);
        g->load(&params.file);
        result = g;
        next();
    } else {
        return;
    }
}

void LoadWizard::on_chkMainCanvas_clicked(bool checked)
{
    if(checked){
        ui->chkSideCanvas->setChecked(!checked);
        sideCanvas = !checked;
    }

    mainCanvas = checked;
}

void LoadWizard::on_chkSideCanvas_clicked(bool checked)
{
    if(checked){
        ui->chkMainCanvas->setChecked(!checked);
        mainCanvas = !checked;
    }

    sideCanvas = checked;
}
void LoadWizard::on_CellConversionList_itemDoubleClicked(QListWidgetItem *item)
{
    if(params.last_page == STEP2){
        QString conversion;
        QString field;
        conversion = item->text();
        QTextStream s(&conversion);
        s >> field;

        ui->CellDataList->addItem(field);

        //REMOVES CURRENT ROW
        ui->CellConversionList->remove(ui->CellConversionList->row(item));
    }
}

void LoadWizard::on_PointConversionList_itemDoubleClicked(QListWidgetItem *item)
{
    if(params.last_page == STEP2){
        QString conversion;
        QString field;
        conversion = item->text();
        QTextStream s(&conversion);
        s >> field;

        ui->PointDataList->addItem(field);

        //REMOVES CURRENT ROW
        ui->PointConversionList->remove(ui->PointConversionList->row(item));
    }
}
