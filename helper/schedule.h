#ifndef SCHEDULE_H
#define SCHEDULE_H

#include "../model/graphicobject.h"

#include <QFile>
#include <QtCore>
#include <QtXml/QDomDocument>
#include <QtXml/QDomElement>
#include <QtXml/QDomEntity>
#include <QXmlStreamReader>
#include <QDebug>
#include<QString>
#include<vector>

using namespace std;

class Schedule
{
public:
    Schedule(GraphicObject* gobj, QString filename, QString log_filename, int times);
    Schedule(QString filename, QString log_filename,int times,vector<QString> startParams,vector<QString> visualParams);

    QString getFileName();
    QString getLogFileName();
    int getTimes();
    vector<QString> getStartParams();
    vector<QString> getVisualParams();

    void operator=(Schedule s);
    void operator=(Schedule* s);

    int id();

    static void xml(Schedule job, QString filename);

    static void xml(vector<Schedule> jobs, QString filename);

    static vector<Schedule> fromXml(QString filename);

    static void fromXml(vector<Schedule>* jobs, QString filename);

private:
    static unsigned int CURR_ID;

    const unsigned int SCHEDULE_ID;

    QString filename;
    QString log_filename;
    int times;

    vector<QString> startParams;
    vector<QString> visualParams;
};

#endif // SCHEDULE_H
