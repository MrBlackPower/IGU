#ifndef AUTOMATUSGRAPH_H
#define AUTOMATUSGRAPH_H

#include "../graph.h"

#define MOUSE_PROX 100

class AutomatusGraph : public Graph<QString,QString>
{
public:
    AutomatusGraph(QString name, SmartObject* parent = NULL);

    NodeGraph<QString,QString>* getNodeGraph(QString name);

    vector<Field> getPointDataList();
    vector<Field> getCellDataList();
    vector<Field> getStartParameters();
    vector<Field> getContextVisualParameters();
    bool compatibility(VTKFile* file);
    bool load(VTKFile* file);
    vector<QString> getRanges();
    bool updateStartParameter(QString field, QString value);
    GraphicObject* extrapolateContext(QString extrapolation);
    bool setContextRange(QString field);

    //VIRTUAL GRAPH METHODS
    bool validSolution(Solution* s);
    double scoreSolution(Solution* s);
    void drawContext();
protected:

private:
    vector<QChar> alphabet; // ' ' counts as empty character accepts {a-z 0-9}
    QString chain;
    QString remaining_chain;

    NodeGraph<QString,QString>* current_node;

    vector<NodeGraph<QString,QString>*> final_nodes;
    NodeGraph<QString,QString>* start_node;


    Point* hover_add_node;
    Point* hover_add_edge_start;
    NodeGraph<QString,QString>* hover_add_edge_start_node;
    Point* hover_add_edge_end;

    bool isInAlphabet(QString c);
    bool isInAlphabet(QChar c);

    void setAlphabet(QString alphabet = "abcdefghijklmnopqrstuvwxyz");

    void paintGraph();

    //GRAPHIC OBJECT METHODS
    bool initialize();
    double iteration();
    void finish();
    bool updateContextVisualParameter(QString field, QString value);
    vector<QString> getExtrapolations();
    QString getLiveParameterList();
    QString getLiveParameter(QString name);
    DataType getLiveParameterDataType(QString name);
    void mouse_press_event(int width, int height, Point eye, QMouseEvent *event, GraphicTools tool);
    void mouse_move_event(int width, int height, Point eye, QMouseEvent *event, GraphicTools tool);
    NodeGraph<QString,QString>* mouse_ray(int width, int height, Point eye, Point mouse);
};

#endif // AUTOMATUSGRAPH_H
