#ifndef GRAPHICCOMPONENTDIALOG_H
#define GRAPHICCOMPONENTDIALOG_H

#include <QDialog>
#include <QListWidgetItem>
#include <QTimer>
#include "inputstringdialog.h"
#include "inputbooleandialog.h"
#include "inputscalardialog.h"
#include "selectfromstringlistdialog.h"
#include "../../model/point.h"
#include "../../model/smartobject.h"
#include "../../helper/vtkfile.h"

#define CILYNDER_SLICES 12
#define CILYNDER_STACKS 20
#define SPHERE_SLICES CILYNDER_SLICES
#define SPHERE_STACKS 5

#define ZERO 0
#define ONE 1

#define TIMEOUT 1000000

namespace GraphicSpace {
    enum GraphicTools{
        SELECT,
        ADD_NODE,
        EDIT_NODE,
        REMOVE_NODE,
        LIMIT,
        ROTATE,
        SCALE,
        MOVE
    };

    enum GraphicObjectStatus{
        Fresh,
        Initialized,
        Iterated,
        Finished,
        Crashed
    };

    struct Limit{
        double left = 0.0;
        double right = 0.0;
        double bottom = 0.0;
        double top = 0.0;
        double _near = 0.0;
        double _far = 0.0;
    };

    struct Range{
        QString parameter;
        double max = -1.0;
        double min = 1.0;
    };

    struct Transform{
        double scale = 1.0;
        Point rotation = Point(1,0.0,0.0,0.0);
        Point translate = Point(1,0.0,0.0,0.0);
    };

    struct StartParameter{
        QString field;
        QString data;
    };

    struct Clock{
        double frames_time[5] = {1.0,1.0,1.0,1.0,1.0};
        double iterations_times[5] = {1.0,1.0,1.0,1.0,1.0};
        double deltaT = 0.0;
        double modelTime = 0.0;
        double elapsedTime = 0.0;
        double fps = 0.0;
        double ips = 0.0;
        int frames = 0.0;
        int iterations = 0.0;
        int iterator_count = 0;
        bool iterating = false;

    };

    enum IOField{
        FIELD_IN,
        FIELD_OUT,
        FIELD_INOUT
    };

    struct GenericVisualParams{
        bool printRawEveryIteration = false;
        int cilynder_slices = CILYNDER_SLICES;
        int cilynder_stacks = CILYNDER_STACKS;
        int sphere_slices = SPHERE_SLICES;
        int sphere_stacks = SPHERE_STACKS;
        double frame_delay = ZERO;
    };

    struct Field{
        QString names = "";
        QString current_value = "";
        QString possible_values = "";
        DataType type = doublep;
        IOField stream = FIELD_IN;
        bool required = false;
    };

    struct GraphicObjectComposer{
        QString name= "";
        Point point = Point();
        bool xyz[3] = {true};
        vector<Field> fields;
    };
}

namespace Ui {
class GraphicComponentDialog;
}

using namespace GraphicSpace;

class GraphicComponentDialog : public QDialog
{
    Q_OBJECT

public:
    explicit GraphicComponentDialog(QWidget *parent = nullptr);
    explicit GraphicComponentDialog(GraphicObjectComposer component, QWidget *parent = nullptr);
    ~GraphicComponentDialog();

    void setComponent(GraphicObjectComposer component);

    GraphicObjectComposer getComponent();

    bool Set();

signals:
    void pushComponent(GraphicObjectComposer component);

    void componentSet();

    void cancelled();


public slots:
    void updateInterface();

private slots:
    void on_graphicComponentFieldListWidget_itemDoubleClicked(QListWidgetItem *item);

    void on_spn_x_valueChanged(double arg1);

    void on_spn_y_valueChanged(double arg1);

    void on_spn_z_valueChanged(double arg1);

    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

private:
    Ui::GraphicComponentDialog *ui;

    GraphicObjectComposer component;

    bool set;
};

#endif // GRAPHICCOMPONENTDIALOG_H
