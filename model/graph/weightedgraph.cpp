#include "weightedgraph.h"

WeightedGraph::WeightedGraph(QString name, SmartObject* parent) : Graph<int,double>(name,parent){
    graphStatus = GENERATING;
    time.deltaT = ONE;


    lastSolution = NULL;
    bestSolution = NULL;
}


vector<double> WeightedGraph::delta_tau(Solution* sol){
    vector<double> r;
    vector<int> ids = sol->getVector();

    r.assign(N,ZERO);

    for (int i = 0; i < ids.size(); i ++) {
        r[ids[i]] = 1 / sol->cost;
    }

    return r;
}

void WeightedGraph::pheromone(vector<double>* tau,vector<double>* delta_tau){
    if(tau->empty() || delta_tau->empty())
        return;

    if(tau->size() != delta_tau->size())
        return;

    for (int i = 0; i < tau->size(); i++) {
        tau->at(i) = ((ONE - params.ant_colony_p) * tau->operator[](i)) + delta_tau->operator[](i);
    }
}

void WeightedGraph::local_pheromone(vector<double>* tau,vector<double>* local_tau,vector<double>* local_tau_0){
    if(tau->empty() || local_tau->empty() || local_tau_0->empty())
        return;

    if((tau->size() != local_tau->size()) || (local_tau_0->size() != local_tau->size()))
        return;

    for (int i = 0; i < tau->size(); i++) {
        local_tau->at(i) = ((ONE - params.ant_colony_phi) * tau->operator[](i)) + local_tau_0->operator[](i);
    }
}

WeightedGraph::WeightedGraph(int n, int m, int x, int y, int z, QString name, bool treeD,  SmartObject* parent) : Graph<int,double>(name,parent){
    populate(n,m,x,y,((treeD)? z : ZERO));

    graphStatus = ITERATING;
    time.deltaT = ONE;

    while(!connex())
        populateEdges(m,x,y,((treeD)? z : ZERO));

    lastSolution = NULL;
    bestSolution = NULL;
}

void WeightedGraph::addTabu(Tabu t){
    if(tabu_n == 4294967290)
        tabu_n = 0;

    int current_id = tabu_n % params.tabu_size;

    //CHECKS TABU LIST
    if(!tabuList.empty()){
        if(tabuList.size() == params.tabu_size){
            if(tabuList.size() != params.tabu_size){
                tabuList.clear();
                addTabu(t);
            }

            tabuList[current_id] = t;
        } else {
            tabuList.push_back(t);
        }
    } else {
        tabuList.push_back(t);
    }

    tabu_n ++;
}

void WeightedGraph::mouse_press_event(int width, int height, Point eye, QMouseEvent *event, GraphicTools tool){

}

void WeightedGraph::mouse_move_event(int width, int height, Point eye, QMouseEvent *event, GraphicTools tool){

}

void WeightedGraph::perturbateSolution(Solution* s, double pct){
    vector<int> ids = (s->getVector());
    vector<double> p = scoreSolution(s,params.desire);
    vector<double> p_ids;

    for(int i = 0; i < ids.size(); i++)
        p_ids.push_back(p[ids[i]]);

    vector<int> r;

    if(!MathHelper::isBetween(pct,ONE,ZERO))
        return;

    if(pct == ONE)
        return;

    if(pct == ZERO)
        return;

    double keep = ONE - pct;
    int size = keep * ids.size();

    //ADDS IDS WHICH ALGORITHM IS KEEPING
    for(int i = 0; i < size; i++){
        //CHOOSES RANDOM ID TO ADD
        int random_id = MathHelper::chose_from_cdf(&p_ids);

        //ADDSIT
        r.push_back(ids[random_id]);
        ids.erase(ids.begin() + random_id);
        p_ids.erase(p_ids.begin() + random_id);
    }

    s->operator=(Solution(r));

    validSolution(s);
    scoreSolution(s);
    costSolution(s,params.problem);
}

vector<Field> WeightedGraph::getPointDataList(){
    vector<Field> list;    
    Field f;

    //WEIGHT
    f.names = "peso weight PESO WEIGHT";
    f.stream = FIELD_IN;
    f.required = true;
    f.type = doublep;
    list.push_back(f);

    return list;
}

vector<Field> WeightedGraph::getCellDataList(){
    vector<Field> params;

    Field f;

    //WEIGHT
    f.names = "peso weight PESO WEIGHT";
    f.stream = FIELD_IN;
    f.required = false;
    f.type = doublep;
    params.push_back(f);

    return params;
}

vector<Field> WeightedGraph::getStartParameters(){
    vector<Field> params;
    WeightedGraphStartParams startParams = this->params;

    Field aux;
    aux.names = "PROBLEM";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = stringp;
    aux.current_value = problemtoString(startParams.problem);

    params.push_back(aux);

    aux.names = "MINIMUM";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = _boolean;
    aux.current_value = aux.names.asprintf("%s",((startParams.minimum)?"true":"false"));

    params.push_back(aux);
    
    aux.names = "CONSTRUCTION_TYPE";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = stringlist;
    aux.current_value = constructiontoString(startParams.construction);
    aux.possible_values = "GREEDY ALPHA_GREEDY GRASP";

    params.push_back(aux);
    aux.possible_values = "";
    
    /*************************START
     * CONSTRUCTION DEPENDANT
     * ***************************/
    switch (startParams.construction) {
        case GREEDY:
            break;
        case ALPHA_GREEDY:
            aux.names = "ALPHA";
            aux.required = false;
            aux.stream = FIELD_IN;
            aux.type = doublep;
            aux.current_value = QString::asprintf("%f",startParams.alpha);
        
            params.push_back(aux);
        break;

        case REACTIVE_GREEDY:
            aux.names = "N_ALPHA";
            aux.required = false;
            aux.stream = FIELD_IN;
            aux.type = integer;
            aux.current_value = QString::asprintf("%d",startParams.nAlpha);

            params.push_back(aux);
        break;
    }
    
    
    /***************************END
     * CONSTRUCTION DEPENDANT
     * ***************************/

    aux.names = "COST_TYPE";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = stringlist;
    aux.current_value = costtypetoString(startParams.costType);
    aux.possible_values = "C1_WEIGHT C2_NO_OF_CONNECTIONS C3_WEIGHT_NO_OF_CONNECTIONS C4_DOMINANCY C5_WEIGHT_DOMINANCY";

    params.push_back(aux);
    aux.possible_values = "";

    aux.names = "DESIRE_TYPE";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = stringlist;
    aux.current_value = desiretoString(startParams.desire);
    aux.possible_values = "D1_WEIGHT D2_NO_OF_CONNECTIONS D3_WEIGHT_NO_OF_CONNECTIONS D4_DOMINANCY D5_WEIGHT_DOMINANCY";

    params.push_back(aux);
    aux.possible_values = "";

    aux.names = "MOVE_SET";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = stringlist;
    aux.current_value = movesettoString(startParams.moveSet);
    aux.possible_values = "M1_ADD_REMOVE M2_ADD_REMOVE_SWAP M3_ADD_REMOVE_SWAP_DSWAP M4_ADD_REMOVE_DSWAP";

    params.push_back(aux);
    aux.possible_values = "";

    aux.names = "LOCAL_SEARCH_HEURISTIC";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = stringlist;
    aux.current_value = localheuristictoString(startParams.localSearchHeuristic);
    aux.possible_values = "VND ANT_COLONY TABU";

    params.push_back(aux);
    aux.possible_values = "";

    aux.names = "PENALIZE_ALPHA_MAX";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = doublep;
    aux.current_value = aux.names.asprintf("%f",(startParams.penalize_infeasible_alpha_max));

    params.push_back(aux);

    aux.names = "PENALIZE_ALPHA_MIN";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = doublep;
    aux.current_value = aux.names.asprintf("%f",(startParams.penalize_infeasible_alpha_min));

    params.push_back(aux);

    aux.names = "PERTURBATE_LOCAL_SEARCH";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = _boolean;
    aux.current_value = aux.names.asprintf("%s",((startParams.perturbated)?"true":"false"));

    params.push_back(aux);

    aux.names = "N_PERTURBATE";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = integer;
    aux.current_value = QString::asprintf("%d",startParams.nperturbation);

    params.push_back(aux);

    aux.names = "PAINT_DOMINANT";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = _boolean;
    aux.current_value = aux.names.asprintf("%s",((startParams.paint_dominant)?"true":"false"));

    params.push_back(aux);

    aux.names = "PAINT_DOMINATED";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = _boolean;
    aux.current_value = aux.names.asprintf("%s",((startParams.paint_dominated)?"true":"false"));

    params.push_back(aux);

    aux.names = "PAINT_NON_DOMINATED";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = _boolean;
    aux.current_value = aux.names.asprintf("%s",((startParams.paint_non_dominated)?"true":"false"));

    params.push_back(aux);

    aux.names = "DOMINANT_SET_LAST_SOLUTION";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = _boolean;
    aux.current_value = aux.names.asprintf("%s",((startParams.dominant_set_last_solution)?"true":"false"));

    params.push_back(aux);

    aux.names = "TABU_SIZE";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = integer;
    aux.current_value = QString::asprintf("%d",startParams.tabu_size);

    params.push_back(aux);

    aux.names = "TABU_PENALIZE_ALPHA_STEPS";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = integer;
    aux.current_value = QString::asprintf("%d",startParams.penalize_steps);

    params.push_back(aux);

    aux.names = "ANT_COLONY_SIZE";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = integer;
    aux.current_value = aux.names.asprintf("%d",startParams.ant_colony_size);

    params.push_back(aux);

    aux.names = "ANT_P_PHEROMONE_TRAIL";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = doublep;
    aux.current_value = aux.names.asprintf("%f",startParams.ant_colony_p);

    params.push_back(aux);

    aux.names = "ANT_PHI_LOCAL_STRENGTH";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = doublep;
    aux.current_value = aux.names.asprintf("%f",startParams.ant_colony_phi);

    params.push_back(aux);

    aux.names = "ANT_Q0_EXPLOTATION_EXPLORATION_RATE";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = doublep;
    aux.current_value = aux.names.asprintf("%f",startParams.ant_colony_q0);

    params.push_back(aux);


    return params;
}

vector<Field> WeightedGraph::getContextVisualParameters(){
    vector<Field> params;

    Field aux;
    aux.names = "SPHERE_RADIUS";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = doublep;
    aux.current_value = aux.names.asprintf("%f",sphere_radius);

    params.push_back(aux);

    aux.names = "DRAW_NODE";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = _boolean;
    aux.current_value = aux.names.asprintf("%s",((draw_node)?"true":"false"));

    params.push_back(aux);

    aux.names = "DRAW_EDGE";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = _boolean;
    aux.current_value = aux.names.asprintf("%s",((draw_edge)?"true":"false"));

    params.push_back(aux);

    return params;
}

bool WeightedGraph::compatibility(VTKFile* file){
    emit emit_log(SmartLogMessage(getSID(),"WEIGHTED GRAPH COMPATIBILITY RUN",SmartLogType::LOG_COMPATIBILITY));

    int n = file->getN();

    if(file->getPoints().size() != n)
        return false;

    return true;
}

bool WeightedGraph::load(VTKFile* file){
    clear();

    vector<Point> points = file->getPoints();
    vector<int> weights;
    vector<Line> lines = file->getLines();

    vector<QString> aux = file->getPointData("WEIGHT weight peso PESO");
    for(int i = 0; i < aux.size(); i++){
        QString s = aux[i];
        weights.push_back(s.toInt());
    }

    for(int i = 0; i < points.size(); i++){
        Point p = points[i];
        addNode(weights[i],p.x,p.y,p.z);
    }

    for(int i = 0; i < lines.size(); i++){
        Line l = lines[i];

        if(l.n !=2){
            clear();
            return false;
        }
        int a = l.points[ZERO];
        int b = l.points[ONE];
        addEdge(true,nodes[a],nodes[b],MathHelper::euclideanDistance(&points[a],&points[b]));
    }

    return true;
}

vector<QString> WeightedGraph::getRanges(){
    vector<QString> params;

    params.push_back("NUMBER_OF_EDGES");

    params.push_back("NODE_WEIGHT");

    return params;
}

bool WeightedGraph::updateStartParameter(QString field, QString value){
    if(field == "PROBLEM"){
        if(value == "MINIMUM_WEIGHTED_DOMINANT_SET"){
            params.problem = MINIMUM_WEIGHTED_DOMINANT_SET;
            return true;
        } else if(value == "MINIMUM_WEIGHTED_CONNEX_DOMINANT_SET") {
            params.problem = MINIMUM_WEIGHTED_CONNEX_DOMINANT_SET;
            return true;
        }
        return false;
    } else if(field == "MINIMUM"){
        params.minimum = ((value.toInt() == 1)|| value == "true")? true : false;
        return true;
    } else if(field == "CONSTRUCTION_TYPE"){
        if(value == "GREEDY"){
            params.construction = GREEDY;
            return true;
        } else if(value == "ALPHA_GREEDY") {
            params.construction = ALPHA_GREEDY;
            return true;
        }
        return false;
    } else if(field == "ALPHA"){
        params.alpha = value.toDouble();
        return true;
    } else if(field == "NALPHA"){
        params.nAlpha = value.toInt();
        return true;
    } else if(field == "COST_TYPE"){
        if(value == "C1_WEIGHT"){
            params.costType = C1_WEIGHT;
            return true;
        } else if(value == "C2_NO_OF_CONNECTIONS") {
            params.costType = C2_NO_OF_CONNECTIONS;
            return true;
        } else if(value == "C3_WEIGHT_NO_OF_CONNECTIONS") {
            params.costType = C3_WEIGHT_NO_OF_CONNECTIONS;
            return true;
        } else if(value == "C4_DOMINANCY") {
            params.costType = C4_DOMINANCY;
            return true;
        } else if(value == "C5_WEIGHT_DOMINANCY") {
            params.costType = C5_WEIGHT_DOMINANCY;
            return true;
        }
        return false;
    } else if(field == "MOVE_SET"){
        if(value == "M1_ADD_REMOVE"){
            params.moveSet = M1_ADD_REMOVE;
            return true;
        } else if(value == "M2_ADD_REMOVE_SWAP") {
            params.moveSet = M2_ADD_REMOVE_SWAP;
            return true;
        } else if(value == "M3_ADD_REMOVE_SWAP_DSWAP") {
            params.moveSet = M3_ADD_REMOVE_SWAP_DSWAP;
            return true;
        } else if(value == "M4_ADD_REMOVE_DSWAP") {
            params.moveSet = M4_ADD_REMOVE_DSWAP;
            return true;
        }
        return false;
    } else if(field == "LOCAL_SEARCH_HEURISTIC"){
        if(value == "VND"){
            params.localSearchHeuristic = VND;
            return true;
        } else if(value == "ANT_COLONY") {
            params.localSearchHeuristic = ANT_COLONY;
            return true;
        } else if(value == "TABU") {
            params.localSearchHeuristic = TABU;
            return true;
        }
        return false;
    } else if(field == "PENALIZE_ALPHA_MAX"){
        params.penalize_infeasible_alpha_max = value.toDouble();
        return true;
    } else if(field == "PENALIZE_ALPHA_MIN"){
        params.penalize_infeasible_alpha_min = value.toDouble();
        return true;
    } else if(field == "PERTURBATE_LOCAL_SEARCH"){
        params.perturbated = (value.toInt() == 1)? true : false;
        return true;
    } else if(field == "N_PERTURBATE"){
        params.nAlpha = value.toInt();
        return true;
    } else if(field == "PAINT_DOMINANT"){
        params.paint_dominant = ((value.toInt() == 1)|| value == "true")? true : false;
        return true;
    } else if(field == "PAINT_DOMINATED"){
        params.paint_dominated = ((value.toInt() == 1)|| value == "true")? true : false;
        return true;
    } else if(field == "PAINT_NON_DOMINATED"){
        params.paint_non_dominated = ((value.toInt() == 1)|| value == "true")? true : false;
        return true;
    } else if(field == "DOMINANT_SET_LAST_SOLUTION"){
        params.dominant_set_last_solution = ((value.toInt() == 1)|| value == "true")? true : false;
        return true;
    } else if(field == "DESIRE_TYPE"){
        params.desire = desireFromString(value);
        return true;
    } else if(field == "TABU_SIZE"){
        params.tabu_size = value.toInt();
        return true;
    } else if(field == "TABU_PENALIZE_ALPHA_STEPS"){
        params.penalize_steps = value.toDouble();
        return true;
    } else if(field == "ANT_COLONY_SIZE"){
        params.ant_colony_size = value.toDouble();
        return true;
    } else if(field == "ANT_P_PHEROMONE_TRAIL"){
        params.ant_colony_p = value.toDouble();
        return true;
    } else if(field == "ANT_PHI_LOCAL_STRENGTH"){
        params.ant_colony_phi = value.toDouble();
        return true;
    } else if(field == "ANT_Q0_EXPLOTATION_EXPLORATION_RATE"){
        params.ant_colony_q0 = value.toDouble();
        return true;
    }
    return false;
}

vector<QString> WeightedGraph::getExtrapolations(){
    vector<QString> aux;

    aux.push_back("DOMINANT_SOLUTION_POPULATION_GRAPHS");

    return aux;
}

bool WeightedGraph::setContextRange(QString field){
    if(field == "NUMBER_OF_EDGES"){
        range.parameter = "NUMBER_OF_EDGES";
        range.max = -1.0;
        range.min = 1.0;
        return true;
    }

    if(field == "NODE_WEIGHT"){
        range.parameter = "NODE_WEIGHT";
        range.max = -1.0;
        range.min = 1.0;
        return true;
    }

    return false;
}

vector<Solution> WeightedGraph::getNeighboorSolutions(Solution* sol, WeightedGraphSolutionMovementSet moveSet){
    vector<WeightedGraphSolutionMovement> movements  = movesetMovements(moveSet);

    return getNeighboorSolutions(sol,movements);
}

vector<Solution> WeightedGraph::getNeighboorSolutions(Solution* sol, vector<WeightedGraphSolutionMovement> movement){
    vector<Solution> solutions;

    for(int i = 0; i < movement.size(); i++){
        vector<Solution> newSolutions = getNeighboorSolutions(sol,movement[i]);

        for(int j = 0; j < newSolutions.size(); j++)
            solutions.push_back(newSolutions[j]);
    }

    return solutions;
}
vector<Solution> WeightedGraph::getNeighboorSolutions(Solution* sol, WeightedGraphSolutionMovement movement){
    vector<Solution> answer;
    vector<int> dominant = sol->getVector();
    vector<int> non_dominant;

    for(int i = 0; i < N; i++)
        if(!MathHelper::isIn(&dominant,i))
            non_dominant.push_back(i);

    switch (movement) {
    case ADD:{
        if(!params.parallel){
            //ADD A NON DOMINANT NODE TO THE SOLUTIONS
            for(int i = 0; i < non_dominant.size(); i++){
                vector<int> aux(dominant);
                aux.push_back(non_dominant[i]);

                Solution newS(aux);
                newS.last_id = non_dominant[i];
                newS.last_movement = movementtoString(movement);
                validSolution(&newS,params.problem);
                scoreSolution(&newS,params.problem,params.penalize_infeasible_alpha);
                costSolution(&newS,params.problem);
                answer.push_back(newS);
            }
        } else {
            //ADD A NON DOMINANT NODE TO THE SOLUTIONS
            int threads = MathHelper::getThreads();
            # pragma omp parallel for num_threads(threads)  default(none) shared(dominant,non_dominant,movement,answer)
            for(int i = 0; i < non_dominant.size(); i++){
                vector<int> aux(dominant);
                aux.push_back(non_dominant[i]);

                Solution newS(aux);
                newS.last_id = non_dominant[i];
                newS.last_movement = movementtoString(movement);
                validSolution(&newS,params.problem);
                scoreSolution(&newS,params.problem,params.penalize_infeasible_alpha);
                costSolution(&newS,params.problem);

                #pragma omp critical
                answer.push_back(newS);
            }
        }
        break;
    }
    case REMOVE:{
        if(!params.parallel){
            //REMOVE A DOMINANT MOVE FROM THE SOLUTION
            for(int i = 0; i < dominant.size(); i++){
                vector<int> aux(dominant);
                aux.erase(aux.begin() + i);
                Solution newS(aux);
                newS.last_id = dominant[i];
                newS.last_movement = movementtoString(movement);
                validSolution(&newS,params.problem);
                scoreSolution(&newS,params.problem,params.penalize_infeasible_alpha);
                costSolution(&newS,params.problem);
                answer.push_back(newS);
            }
        } else {
            //REMOVE A DOMINANT MOVE FROM THE SOLUTION
            int threads = MathHelper::getThreads();
            # pragma omp parallel for num_threads(threads)  default(none) shared(dominant,movement,answer)
            for(int i = 0; i < dominant.size(); i++){
                vector<int> aux(dominant);
                aux.erase(aux.begin() + i);
                Solution newS(aux);
                newS.last_id = dominant[i];
                newS.last_movement = movementtoString(movement);
                validSolution(&newS,params.problem);
                scoreSolution(&newS,params.problem,params.penalize_infeasible_alpha);
                costSolution(&newS,params.problem);

                #pragma omp critical
                answer.push_back(newS);
            }
        }
        break;
    }
    case SWAP:{
        //SWAPS ANY DOMINANT NODE FOR ANY NON DOMINANT NODE
        vector<Solution> r1, r2;
        r1 = getNeighboorSolutions(sol,ADD);
        r2 = getNeighboorSolutions(sol,REMOVE);

        Solution::orderScoreCrescent(&r1);
        Solution::orderScoreCrescent(&r2);

        if(!params.parallel){
            for(int i = 0; i < sqrt(r1.size()); i++){
                Solution toAdd = r1[i];

                for(int j = 0; j < sqrt(r2.size()); j++){
                    vector<int> aux(dominant);
                    Solution toRemove = r2[j];

                    //FINDS AND REMOVES BEST ITEM
                    bool found = false;
                    for(int k = 0; k < aux.size(); k++){
                        if(aux[k] == toRemove.last_id){
                            aux[k] = toAdd.last_id;
                            found = true;
                            break;
                        }
                    }

                    if(!found)
                        emit emit_log(SmartLogMessage(getSID(),"MESSED UP SWAP MOVEMENT"));

                    Solution newS(aux);

                    newS.last_id = non_dominant[j];
                    newS.last_movement = movementtoString(movement);

                    newS.last_id_two = dominant[i];

                    validSolution(&newS,params.problem);
                    scoreSolution(&newS,params.problem,params.penalize_infeasible_alpha);
                    costSolution(&newS,params.problem);
                    answer.push_back(newS);
                }
            }
        } else {
            int threads = MathHelper::getThreads();
            int i;
            int j;
            int n = sqrt(r1.size());
            # pragma omp parallel for num_threads(threads)  default(none) shared(r1,r2,dominant,non_dominant,movement,answer,n) private(i,j)
            for(i = 0; i < n; i++){
                Solution toAdd = r1[i];

                for(j = 0; j < sqrt(r2.size()); j++){
                    vector<int> aux(dominant);
                    Solution toRemove = r2[j];

                    //FINDS AND REMOVES BEST ITEM
                    bool found = false;
                    for(int k = 0; k < aux.size(); k++){
                        if(aux[k] == toRemove.last_id){
                            aux[k] = toAdd.last_id;
                            found = true;
                            break;
                        }
                    }

                    if(!found)
                        emit emit_log(SmartLogMessage(getSID(),"MESSED UP SWAP MOVEMENT"));

                    Solution newS(aux);
                    newS.last_id = non_dominant[j];
                    newS.last_movement = movementtoString(movement);
                    validSolution(&newS,params.problem);
                    scoreSolution(&newS,params.problem,params.penalize_infeasible_alpha);
                    costSolution(&newS,params.problem);

                    #pragma omp critical
                    answer.push_back(newS);
                }
            }
        }
        break;
    }
    case D_SWAP:
        //SWAPS TWO ADJACENT DOMINANT NODE WITH ANY TWO ADJACENT NON DOMINANT NODE
        for(int i = 1; i < dominant.size() - 1; i++){

            for(int j = 1; j < non_dominant.size() - 1; j++){
                vector<int> aux(dominant);

                aux[i] = non_dominant[j];
                aux[(i-1)] = non_dominant[(j-1)];

                Solution newS(aux);
                newS.last_id = non_dominant[j];
                newS.last_id_two = non_dominant[j-1];
                newS.last_movement = movementtoString(movement);
                validSolution(&newS,params.problem);
                scoreSolution(&newS,params.problem,params.penalize_infeasible_alpha);
                costSolution(&newS,params.problem);
                answer.push_back(newS);
            }
        }
        break;
    }

    return answer;
}


vector<Cost> WeightedGraph::getCost(vector<int> dominantID, WeightedGraphNodeCostType costType){
    vector<NodeGraph<int,double>*> dominantNodes;

    for (int i = 0; i < dominantID.size(); i++) {
        dominantNodes.push_back(this->operator[](dominantID[i]));
    }

    return getCost(dominantNodes,costType);
}

vector<Cost> WeightedGraph::getCost(vector<NodeGraph<int,double>*> dominant, WeightedGraphNodeCostType costType){
    vector<int> dominant_id;
    vector<int> dominated;
    vector<int> non_dominated;
    vector<Cost> costs;

    for(int i = 0; i < dominant.size(); i++)
        dominant_id.push_back(dominant.operator[](i)->getId());

    for(int i = 0; i < N; i ++){
        if(!MathHelper::isIn(&dominant_id,i)){
            //VERIFIES IF NODE IS DOMINATED
            bool dominated_bool = false;

            for (int j = 0; j < dominant.size() && !dominated_bool; j++) {
                NodeGraph<int,double>* n_aux = dominant.operator[](j);

                vector<NodeGraph<int,double>*> neighboors = n_aux->getEdges();
                for (int k = 0; k < neighboors.size() && !dominated_bool; k++) {
                    NodeGraph<int,double>* neighboor = neighboors.operator[](k);
                    if(neighboor->getID() == i)
                        dominated_bool = true;
                }
            }

            if(dominated_bool){
                dominated.push_back(i);
            } else {
                non_dominated.push_back(i);
            }
        }
    }

    //ADDS COST TO EVERY NODE THAT IS NOT DOMINANT
    for(int i = 0; i < (dominated.size() + non_dominated.size()); i++){
        int current_id = 0;

        if(i < dominated.size()){
            current_id = dominated.operator[](i);
        } else {
            current_id = non_dominated.operator[](i - dominated.size());
        }

        NodeGraph<int,double>* n = this->operator[](current_id);

        switch (costType) {
        case C1_WEIGHT:{
            double cost = (double)n->buffer;
            costs.push_back(Cost(n,current_id,cost));
        }
        case C2_NO_OF_CONNECTIONS:{
            double cost = 1.0 / (double)n->getE();
            costs.push_back(Cost(n,current_id,cost));
        }
        case C3_WEIGHT_NO_OF_CONNECTIONS:{
            double cost = (double)n->buffer/(double)n->getE();
            costs.push_back(Cost(n,current_id,cost));
        }
        case C4_DOMINANCY:{
            int dominancy = 0;

            vector<NodeGraph<int,double>*> neighboors = n->getEdges();
            for (int k = 0; k < neighboors.size(); k++) {
                NodeGraph<int,double>* neighboor = neighboors.operator[](k);
                if(MathHelper::isIn(&non_dominated,neighboor->getId()))
                    dominancy ++;
                else if(MathHelper::isIn(&dominated,neighboor->getId()))
                    dominancy++;
            }

            double cost = (dominancy != 0)? 1.0/(double)dominancy : 1.0 / 0.00000000001 ;

            costs.push_back(Cost(n,current_id,cost));
        }
        case C5_WEIGHT_DOMINANCY:{
            int dominancy = 0;

            vector<NodeGraph<int,double>*> neighboors = n->getEdges();
            for (int k = 0; k < neighboors.size(); k++) {
                NodeGraph<int,double>* neighboor = neighboors.operator[](k);
                if(MathHelper::isIn(&non_dominated,neighboor->getId()))
                    dominancy ++;
                else if(MathHelper::isIn(&dominated,neighboor->getId()))
                    dominancy++;
            }

            double cost = (dominancy != 0)? (double)n->buffer/(double)dominancy : (double)n->buffer / 0.00000000001 ;

            costs.push_back(Cost(n,current_id,cost));
        }
        }
    }

    return costs;
}

Solution* WeightedGraph::addHeuristicNode(Solution* sol, WeightedGraphSolutionConstructionType construction){
    int chosen = ZERO;
    //BUILDS LRC (LOCAL RANDOM COST)
    vector<Cost> lrc;
    switch (construction) {
    case GREEDY:{
        Cost best_cost = costs.operator[](ZERO);
        for(int i = 0; i < costs.size(); i++){
            if(best_cost == costs[i])
                lrc.push_back(costs[i]);
            else
                break;
        }
        break;
    }
    case ALPHA_GREEDY:{
    Cost best_cost = costs.operator[](ZERO);
    Cost worst_cost = costs.operator[](costs.size() - ONE);
    double h = abs(best_cost.cost - worst_cost.cost);
    for(int i = 0; i < costs.size(); i++){
        double diff = abs(best_cost.cost - costs[i].cost);
        if(diff <= params.alpha * h)
            lrc.push_back(costs[i]);
        else
            break;
    }
    break;
    }
    case REACTIVE_GREEDY:{
        //SETS UP PROBABILITY OF ALPHAS AND LEARINING VECTOR
        if(params.iterations[CONSTRUCTING_HEURISTIC] == 0){
            params.p_alphas.clear();
            params.q_alphas.clear();
            params.alphas.clear();

            for(int i = 0; i < params.nAlpha; i++){
                params.q_alphas.push_back(ONE);
                params.p_alphas.push_back(ONE/(params.nAlpha - 1));
                params.alphas.push_back(i * (ONE/(params.nAlpha - 1)));
            }
        } else if (params.alphas.empty()  || params.p_alphas.empty()  || params.q_alphas.empty() ||
                   params.alphas.size() != params.nAlpha || params.p_alphas.size() != params.nAlpha || params.q_alphas.size() != params.nAlpha) {
            params.iterations[CONSTRUCTING_HEURISTIC] = 0;
            return NULL;
        }

        //CHOOSES ALPHA BY CHANCE
        if(params.chose_alpha){
            double r = MathHelper::random(ONE);
            double p = params.p_alphas[ZERO];
            chosen = ZERO;

            while(p > r){
                chosen ++;
                p += params.p_alphas[chosen];
            }

            params.last_alpha_id = chosen;
            params.alpha = params.alphas[chosen];

            params.chose_alpha = false;
        }

        //CONSTRUCTS LRC WITH SAID ALPHA
        Cost best_cost = costs.operator[](ZERO);
        Cost worst_cost = costs.operator[](costs.size() - ONE);
        double h = abs(best_cost.cost - worst_cost.cost);
        for(int i = 0; i < costs.size(); i++){
            double diff = abs(best_cost.cost - costs[i].cost);
            if(diff <= params.alpha * h)
                lrc.push_back(costs[i]);
            else
                break;
        }
        break;
        }
    }

    //CHOOSES RANDOM FROM LRC
    if(lrc.empty())
        return NULL;

    Cost chosen_cost = lrc.operator[](MathHelper::random(lrc.size() - ONE));

    //ADDS TO NEW SOLTION AND ADDS IT TO THE ANSWER
    vector<int> ans = sol->getVector();
    ans.push_back(chosen_cost.node->getId());

    //REMOVES USED COST FROM COSTS
    for(int i = 0; i < costs.size(); i++){
        Cost c = costs[i];
        if(c.node == chosen_cost.node){
            costs.erase(costs.begin() + i);
            i--;
        }
    }

    Solution* answer = new Solution(ans);

    return answer;
}

bool WeightedGraph::validSolution(Solution* sol, WeightedGraphProblems problem){
    switch (problem) {
        case MINIMUM_WEIGHTED_DOMINANT_SET:
        if(dominant(sol)){
            sol->valid = true;
            return true;
        }
        case MINIMUM_WEIGHTED_CONNEX_DOMINANT_SET:
        if(dominant(sol)){
            if(connex(sol)){
                sol->valid = true;
                return true;
            }
        }
    }

    sol->valid = false;
    return false;
}

bool WeightedGraph::validSolution(Solution* s){
    return validSolution(s, params.problem);
}

double WeightedGraph::scoreSolution(Solution* sol, WeightedGraphProblems problem, double penalize_infeasible_alpha){
    int r = 0;
    switch (problem) {
    case MINIMUM_WEIGHTED_DOMINANT_SET:{
        //SCORE IS SUM OF WEIGHTS divided by # of dominated nodes
        vector<int> dominant;
        sol->getVector(&dominant);

        if(dominant.empty())
            return r;

        int non_dominated = countNonDominated(dominant);

        vector<NodeGraph<int,double>*> aux = getSolutionNodes(sol);
        int weight_max = aux[ZERO]->buffer;
        for(int i = 0; i < aux.size(); i++){
            NodeGraph<int,double>* it = aux[i];

            r += it->buffer;

            if(it->buffer > weight_max)
                weight_max = it->buffer;
        }

        sol->score = (double)r + (penalize_infeasible_alpha * (double)(weight_max * non_dominated));
        return r;
    }
    case MINIMUM_WEIGHTED_CONNEX_DOMINANT_SET:{
        //SCORE IS SUM OF WEIGHTS divided by # of dominated nodes
        vector<int> dominant;
        sol->getVector(&dominant);

        if(dominant.empty())
            return r;

        int non_dominated = countNonDominated(dominant);

        vector<NodeGraph<int,double>*> aux = getSolutionNodes(sol);
        int weight_max = aux[ZERO]->buffer;
        for(int i = 0; i < aux.size(); i++){
            NodeGraph<int,double>* it = aux[i];

            r += it->buffer;

            if(it->buffer > weight_max)
                weight_max = it->buffer;
        }

        sol->score = (double)r + (penalize_infeasible_alpha * (double)(weight_max * non_dominated));
        return r;
    }
    }


    sol->score = r;
    return r;
}

double WeightedGraph::scoreSolution(Solution* s){
    return scoreSolution(s, params.problem);
}

void WeightedGraph::drawContext(){

}

QString WeightedGraph::getLiveParameterList(){
    QString r;

    r =  "LAST_SOLUTION_COST LAST_SOLUTION_SCORE BEST_SOLUTION_COST BEST_SOLUTION_SCORE";
    r += " CONSTRUCTION_ALPHA ITERATIONS_GENERATING ITERATIONS_CONSTRUCTING_ARTICULATION";
    r += " ITERATIONS_CONSTRUCTING_HEURISTIC ITERATIONS_ITERATING ITERATIONS_WITHOUT_CHANGE_IN_BEST_SOLUTION";

    return r;
}

QString WeightedGraph::getLiveParameter(QString name){
    QString r;

    if(name == "LAST_SOLUTION_COST"){
        if(lastSolution != NULL)
            r = QString::asprintf("%f",lastSolution->cost);
        else
            r = "0";
    } else if(name == "LAST_SOLUTION_SCORE") {
        if(lastSolution != NULL)
            r = QString::asprintf("%f",lastSolution->score);
        else
            r = "0";
    } else if(name == "BEST_SOLUTION_COST") {
        if(bestSolution != NULL)
            r = QString::asprintf("%f",bestSolution->cost);
        else
            r = "0";
    } else if(name == "BEST_SOLUTION_SCORE") {
        if(bestSolution != NULL)
            r = QString::asprintf("%f",bestSolution->score);
        else
            r = "0";
    } else if(name == "CONSTRUCTION_ALPHA") {
        r = QString::asprintf("%f",params.alpha);
    } else if(name == "ITERATIONS_GENERATING") {
        r = QString::asprintf("%d",params.iterations[GENERATING_INDEX]);
    } else if(name == "ITERATIONS_CONSTRUCTING_ARTICULATION") {
        r = QString::asprintf("%d",params.iterations[GENERATING_INDEX]);
    } else if(name == "ITERATIONS_CONSTRUCTING_HEURISTIC") {
        r = QString::asprintf("%d",params.iterations[GENERATING_INDEX]);
    } else if(name == "ITERATIONS_ITERATING") {
        r = QString::asprintf("%d",params.iterations[GENERATING_INDEX]);
    } else if(name == "ITERATIONS_WITHOUT_CHANGE_IN_BEST_SOLUTION") {
        r = QString::asprintf("%d",params.without_improvement_count);
    }

    return r;
}

DataType WeightedGraph::getLiveParameterDataType(QString name){
    DataType r = undefined;

    if(name == "LAST_SOLUTION_COST"){
        r = doublep;
    } else if(name == "LAST_SOLUTION_SCORE") {
        r = doublep;
    } else if(name == "BEST_SOLUTION_COST") {
        r = doublep;
    } else if(name == "BEST_SOLUTION_SCORE") {
        r = doublep;
    } else if(name == "CONSTRUCTION_ALPHA") {
        r = doublep;
    } else if(name == "ITERATIONS_GENERATING") {
        r = integer;
    } else if(name == "ITERATIONS_CONSTRUCTING_ARTICULATION") {
        r = integer;
    } else if(name == "ITERATIONS_CONSTRUCTING_HEURISTIC") {
        r = integer;
    } else if(name == "ITERATIONS_ITERATING") {
        r = integer;
    } else if(name == "ITERATIONS_WITHOUT_CHANGE_IN_BEST_SOLUTION") {
        r = integer;
    }

    return r;
}

double WeightedGraph::costSolution(Solution* sol,  WeightedGraphProblems problem){
    double r = 0;
    switch (problem) {
    case MINIMUM_WEIGHTED_DOMINANT_SET:{
        //COST IS SUM OF WEIGHTS
        vector<int> dominant = sol->getVector();

        vector<NodeGraph<int,double>*> aux = getSolutionNodes(sol);
        for(int i = 0; i < aux.size(); i++){
            NodeGraph<int,double>* it = aux[i];

            r += it->buffer;
        }

        sol->cost = r;
        return r;
    }
    case MINIMUM_WEIGHTED_CONNEX_DOMINANT_SET:{
        //COST IS SUM OF WEIGHTS
        vector<int> dominant = sol->getVector();

        vector<NodeGraph<int,double>*> aux = getSolutionNodes(sol);
        for(int i = 0; i < aux.size(); i++){
            NodeGraph<int,double>* it = aux[i];

            r += it->buffer;
        }

        sol->cost = r;
        return r;
    }
    }


    sol->cost = r;
    return r;
}

vector<double> WeightedGraph::scoreSolution(Solution* sol,  WeightedGraphNodeDesirabiltyType desire){
    vector<double> r;
    vector<int> ids = sol->getVector();

    switch (desire) {
    case D1_WEIGHT:{
        for (int i = 0; i < N; i++) {
            r.push_back(1/this->operator[](i)->buffer);
        }
        break;
    }
    case D2_NO_OF_CONNECTIONS:{
        for (int i = 0; i < N; i++) {
            r.push_back(this->operator[](i)->getE());
        }
        break;
    }
    case D3_WEIGHT_NO_OF_CONNECTIONS:{
        for (int i = 0; i < N; i++) {
            r.push_back(this->operator[](i)->getE()/this->operator[](i)->buffer);
        }
        break;
    }
    case D4_DOMINANCY:{
        vector<int> dominated;
        vector<int> non_dominated;

        r.assign(ids.size(),ZERO);

        this->dominant(lastSolution,&dominated,&non_dominated);

        for (int i = 0; i < N; i++) {
            NodeGraph<int,double>* d = this->operator[](i);

            r[i] += d->getE();
            vector<NodeGraph<int,double>*> edges(d->getEdges());

            if(MathHelper::isIn(&ids,i)){
                for (int j = 0; j < edges.size(); j++) {
                    r[edges[j]->getId()] --;
                }
            }
        }
        break;
    }
    case D5_WEIGHT_DOMINANCY:{
        vector<int> dominated;
        vector<int> non_dominated;

        r.assign(ids.size(),ZERO);

        this->dominant(lastSolution,&dominated,&non_dominated);

        for (int i = 0; i < N; i++) {
            NodeGraph<int,double>* d = this->operator[](i);

            r[i] += d->getE() / d->buffer;
            vector<NodeGraph<int,double>*> edges(d->getEdges());

            if(MathHelper::isIn(&ids,i)){
                for (int j = 0; j < edges.size(); j++) {
                    r[edges[j]->getId()] -= ONE / edges[j]->buffer;
                }
            }
        }
        break;
    }
    }

    return r;
}

bool WeightedGraph::removeDuplicateNode(Solution* sol){
    return false;
}

double WeightedGraph::nodeCost(NodeGraph<int,double>*n , WeightedGraphNodeCostType costType){
    switch (costType) {
        case C1_WEIGHT:
            return n->buffer;
        case C2_NO_OF_CONNECTIONS:
            return (1/n->getE());
        case C3_WEIGHT_NO_OF_CONNECTIONS:
            return (n->buffer/n->getE());
        case C4_DOMINANCY:{
            return (1/n->getE());
        }
        case C5_WEIGHT_DOMINANCY:{
        return (n->buffer/n->getE());
        }
    }

    return 0;
}

double WeightedGraph::nodeScore(NodeGraph<int,double>*n, WeightedGraphProblems problem){
    return n->buffer;
}

QString WeightedGraph::problemtoString(WeightedGraphProblems problem){
    switch (problem) {
        case MINIMUM_WEIGHTED_DOMINANT_SET:
            return "MINIMUM_WEIGHTED_DOMINANT_SET";

        case MINIMUM_WEIGHTED_CONNEX_DOMINANT_SET:
            return "MINIMUM_WEIGHTED_CONNEX_DOMINANT_SET";
    }

    return "UNKNOWN";
}

QString WeightedGraph::constructiontoString(WeightedGraphSolutionConstructionType construction){
    switch (construction) {
        case GREEDY:
            return "GREEDY";

        case ALPHA_GREEDY:
            return "ALPHA_GREEDY";

        case REACTIVE_GREEDY:
            return "REACTIVE_GREEDY";
    }

    return "UNKNOWN";
}

QString WeightedGraph::localheuristictoString(WeightedGraphLocalSearchHeuristic heuristic){
    switch (heuristic) {
        case VND:
            return "VND";

        case TABU:
            return "TABU_SEARCH";

        case ANT_COLONY:
            return "ANT_COLONY";
    }

    return "UNKNOWN";
}

QString WeightedGraph::movementtoString(WeightedGraphSolutionMovement movement){
    switch (movement) {
        case ADD:
            return "ADD";

        case REMOVE:
            return "REMOVE";

        case SWAP:
            return "SWAP";

        case D_SWAP:
            return "D_SWAP";
    }

    return "UNKNOWN";
}

WeightedGraphSolutionMovement WeightedGraph::movementFromString(QString movement){
    if(movement == "ADD"){
        return ADD;
    } else if (movement == "REMOVE") {
        return REMOVE;
    }  else if (movement == "SWAP") {
        return SWAP;
    }  else if (movement == "D_SWAP") {
        return D_SWAP;
    }

    return ADD;
}

QString WeightedGraph::desiretoString(WeightedGraphNodeDesirabiltyType desire){
    switch (desire) {
        case D1_WEIGHT:
            return "D1_WEIGHT";

        case D2_NO_OF_CONNECTIONS:
            return "D2_NO_OF_CONNECTIONS";

        case D3_WEIGHT_NO_OF_CONNECTIONS:
            return "D3_WEIGHT_NO_OF_CONNECTIONS";

        case D4_DOMINANCY:
            return "D4_DOMINANCY";

        case D5_WEIGHT_DOMINANCY:
            return "D5_WEIGHT_DOMINANCY";
    }

    return "UNKNOWN";
}

WeightedGraphNodeDesirabiltyType WeightedGraph::desireFromString(QString desire){
    if(desire == "D1_WEIGHT"){
        return D1_WEIGHT;
    } else if (desire == "D2_NO_OF_CONNECTIONS") {
        return D2_NO_OF_CONNECTIONS;
    }  else if (desire == "D3_WEIGHT_NO_OF_CONNECTIONS") {
        return D3_WEIGHT_NO_OF_CONNECTIONS;
    }  else if (desire == "D4_DOMINANCY") {
        return D4_DOMINANCY;
    }  else if (desire == "D5_WEIGHT_DOMINANCY") {
        return D5_WEIGHT_DOMINANCY;
    }

    return D1_WEIGHT;
}

QString WeightedGraph::movesettoString(WeightedGraphSolutionMovementSet moveset){
    switch (moveset) {
        case M1_ADD_REMOVE:
            return "M1_ADD_REMOVE";

        case M2_ADD_REMOVE_SWAP:
            return "M2_ADD_REMOVE_SWAP";

        case M3_ADD_REMOVE_SWAP_DSWAP:
            return "M3_ADD_REMOVE_SWAP_DSWAP";

        case M4_ADD_REMOVE_DSWAP:
            return "M4_ADD_REMOVE_DSWAP";
    }

    return "UNKNOWN";
}

vector<WeightedGraphSolutionMovement> WeightedGraph::movesetMovements(WeightedGraphSolutionMovementSet moveset){
    vector<WeightedGraphSolutionMovement> aux;
    switch (moveset) {
    case M1_ADD_REMOVE:
        aux.push_back(ADD);
        aux.push_back(REMOVE);
        break;
    case M2_ADD_REMOVE_SWAP:
        aux.push_back(ADD);
        aux.push_back(REMOVE);
        aux.push_back(SWAP);
        break;
    case M3_ADD_REMOVE_SWAP_DSWAP:
        aux.push_back(ADD);
        aux.push_back(REMOVE);
        aux.push_back(SWAP);
        aux.push_back(D_SWAP);
        break;
    case M4_ADD_REMOVE_DSWAP:
        aux.push_back(ADD);
        aux.push_back(REMOVE);
        aux.push_back(D_SWAP);
        break;
    }

    return aux;
}

QString WeightedGraph::costtypetoString(WeightedGraphNodeCostType costType){
    switch (costType) {
        case C1_WEIGHT:
            return "C1_WEIGHT";

        case C2_NO_OF_CONNECTIONS:
            return "C2_NO_OF_CONNECTIONS";

        case C3_WEIGHT_NO_OF_CONNECTIONS:
            return "C3_WEIGHT_NO_OF_CONNECTIONS";

        case C4_DOMINANCY:
            return "C4_DOMINANCY";

        case C5_WEIGHT_DOMINANCY:
            return "C5_WEIGHT_DOMINANCY";
    }

    return "UNKNOWN";
}

void WeightedGraph::paintGraph(){
    if(params.dominant_set_last_solution){
        if(lastSolution == NULL)
            return;

        vector<int> dominant = lastSolution->getVector();
        vector<int> dominated;
        vector<int> non_dominated;

        this->dominant(lastSolution,&dominated,&non_dominated);

        for (int i = 0; i < dominated.size(); i++) {
            NodeGraph<int,double>* n = this->operator[](dominated[i]);
            n->c = params.paint_dominated_color;
        }

        for (int i = 0; i < dominant.size(); i++) {
            NodeGraph<int,double>* n = this->operator[](dominant[i]);
            n->c = params.paint_dominant_color;
        }

        for (int i = 0; i < non_dominated.size(); i++) {
            NodeGraph<int,double>* n = this->operator[](non_dominated[i]);
            n->c = params.paint_non_dominated_color;
        }

    } else {
        if(bestSolution == NULL)
            return;

        vector<int> dominant = lastSolution->getVector();
        vector<int> dominated;
        vector<int> non_dominated;

        this->dominant(lastSolution,&dominated,&non_dominated);

        for (int i = 0; i < dominated.size(); i++) {
            NodeGraph<int,double>* n = this->operator[](dominated[i]);
            n->c = params.paint_dominated;
        }

        for (int i = 0; i < dominant.size(); i++) {
            NodeGraph<int,double>* n = this->operator[](dominant[i]);
            n->c = params.paint_dominant;
        }

        for (int i = 0; i < non_dominated.size(); i++) {
            NodeGraph<int,double>* n = this->operator[](non_dominated[i]);
            n->c = params.paint_non_dominated;
        }
    }
}

bool WeightedGraph::initialize(){
    time.deltaT = 0.1;

    graphStatus = ITERATING;

    return true;
}

double WeightedGraph::iteration(){
    clock_t begin = std::clock();

    if(lastSolution != NULL){
        scoreSolution(lastSolution);
        costSolution(lastSolution,params.problem);
        validSolution(lastSolution,params.problem);
    }

    if(bestSolution != NULL){
        scoreSolution(bestSolution);
        costSolution(bestSolution,params.problem);
        validSolution(bestSolution,params.problem);
    }

    //SETS RANGE MAX AND MINIMUM
    if(range.parameter == "NUMBER_OF_EDGES"){
        //O(n)
        for(int i = 0; i < nodes.size(); i++){
            setMinMax(nodes[i]->getE());
        }
        //O(n)
        for(int i = 0; i < nodes.size(); i++){
            nodes[i]->c = colorBar.interpolate(nodes[i]->getE());
        }
    } else if(range.parameter == "WEIGHT") {
        //O(n)
        for(int i = 0; i < nodes.size(); i++){
            setMinMax(nodes[i]->buffer);
        }
        //O(n)
        for(int i = 0; i < nodes.size(); i++){
            nodes[i]->c = colorBar.interpolate(nodes[i]->buffer);
        }
    }

    paintGraph();

    switch (graphStatus) {
    case GENERATING:{
        populate(DEFAULT_N,DEFAULT_M,DEFAULT_X,DEFAULT_Y,false);

        emit emit_log(SmartLogMessage(getSID(),"GENERATING GRAPH STEP EXECUTED"));
        params.iterations[GENERATING_INDEX]++;

        break;
    }
    case CONSTRUCTING_ARTICULATION:{
        if(!connex()){
            graphStatus = GENERATING;
            return 0.0;
        }

        if(lastSolution != NULL){
            delete  lastSolution;
            lastSolution = NULL;
        }

        //FINDS ARTICULATION POINTS
        int rand = MathHelper::random(N - ONE);
        NodeGraph<int,double>* n = this->operator [](rand);
        vector<NodeGraph<int,double>*> articulation;
        vector<int> articulation_aux;

        //depth tree
        vector<int> visited;
        vector<int> parent;
        int root_cardinality = this->depthTree(&visited,&parent,n);

        //checks if root is articulated
        if(root_cardinality >= TWO){
            articulation.push_back(n);
            articulation_aux.push_back(n->getId());
        }

        //checks if rest of the points are articulation as well
        for(int i = ONE; i < visited.size(); i++){
            int id = visited[i];
            vector<int> parents;
            NodeGraph<int,double>* it = this->operator [](id);
            vector<NodeGraph<int,double>*> ax = it->getEdges();

            for(int j = 0; j < ax.size(); j++)
                parents.push_back(ax[j]->getId());

            bool found = false;

            for(int j = ONE; j < i; j++)
                if(!MathHelper::isIn(&parents,visited[j])){
                    found = true;
                }

            if(!found){
                articulation.push_back(it);
                articulation_aux.push_back(it->getId());
            }
        }

        //creates a solution
        lastSolution = new Solution(articulation_aux);
        graphStatus = CONSTRUCTING_HEURISTIC;

        emit emit_log(SmartLogMessage(getSID(),"CONSTRUCTING_ARTICULATION GRAPH STEP EXECUTED"));
        params.iterations[CONSTRUCTING_ARTICULATION_INDEX]++;

        break;
        }
    case CONSTRUCTING_HEURISTIC:{
        if(lastSolution == NULL){
            graphStatus = CONSTRUCTING_ARTICULATION;
            return 0.0;
        }

        //CREATES COST VECTOR IF IT HASN'T ONE
        //OR IF IS REFRESHING WITH EVERY UPDATE
        if(costs.empty() || params.refresh_cost){
            if(!costs.empty()){
                costs.clear();
            }

            costs.operator=(getCost(lastSolution->getVector(),params.costType));

            if(params.minimum){
                MathHelper::orderCosts(&costs);
            } else {
                MathHelper::orderCosts(&costs,MathHelper::ListOrder::DECRESCENT);
            }

            //PRINTS COST LIST
            //emitLog(Cost::printCost(&costs));
        }

        //ADDS A NODE ACCORDING TO THE CONSTRUCTION HEURISTIC
        //AND REMOVES RELATING COST
        //AND UPDATES QALPHA
        Solution* s = addHeuristicNode(lastSolution,params.construction);

        if(s == NULL)
            return 0.0;
        else {
            lastSolution->operator=(s);
            delete  s;
        }

        //VERIFY IF SOLUTION IS FEASIBLE (CHANGES STATUS IF IT IS)
        if(validSolution(lastSolution,params.problem))
            graphStatus = ITERATING;

        emit emit_log(SmartLogMessage(getSID(),"CONSTRUCTING_HEURISTIC GRAPH STEP EXECUTED"));
        params.iterations[CONSTRUCTING_HEURISTIC_INDEX]++;
        break;
    }
    case ITERATING:{
        if(lastSolution == NULL){
            graphStatus = CONSTRUCTING_ARTICULATION;
            return 0.0;
        }

        scoreSolution(lastSolution);
        costSolution(lastSolution,params.problem);
        validSolution(lastSolution);

        if(params.iterations[ITERATING] == ZERO){
            if(bestSolution == NULL){
                bestSolution = new Solution();
                bestSolution->operator=(lastSolution);
            }

            scoreSolution(bestSolution);
            costSolution(bestSolution,params.problem);
            validSolution(bestSolution);

        }

        switch (params.localSearchHeuristic) {
        case VND:{
            vector<Solution> neighboors(getNeighboorSolutions(lastSolution,params.moveSet));

            if(params.minimum)
                MathHelper::orderSolutions(&neighboors,MathHelper::SolutionOrder::COST);
            else
                MathHelper::orderSolutions(&neighboors,MathHelper::SolutionOrder::COST,MathHelper::ListOrder::DECRESCENT);

            Solution* best_neighboor = &neighboors.front();

            if(params.minimum){
                if(best_neighboor->cost <= bestSolution->cost && best_neighboor->valid){
                    bestSolution->operator=(best_neighboor);
                    lastSolution->operator=(best_neighboor);

                    if(params.construction == REACTIVE_GREEDY)
                        params.q_alphas[params.last_alpha_id]++;
                } else if(best_neighboor->cost <= lastSolution->cost && best_neighboor->valid){
                    lastSolution->operator=(best_neighboor);

                    if(params.construction == REACTIVE_GREEDY)
                        params.q_alphas[params.last_alpha_id]++;
                } else {
                    if(params.n_restart_count >= params.n_restart){
                        this->status = Finished;
                    } else {
                        emit emit_log(SmartLogMessage(getSID(),"ITERATING GRAPH RESTART STEP EXECUTED"));
                        graphStatus = CONSTRUCTING_ARTICULATION;
                        params.n_restart_count ++;
                        return 0.0;
                    }
                }
            } else {
                if(best_neighboor->cost >= bestSolution->cost && best_neighboor->valid){
                    bestSolution->operator=(best_neighboor);
                    lastSolution->operator=(best_neighboor);
                } else if(best_neighboor->cost >= lastSolution->cost && best_neighboor->valid){
                    lastSolution->operator=(best_neighboor);
                } else {
                    if(params.n_restart_count >= params.n_restart){
                        this->status = Finished;
                    } else {
                        emit emit_log(SmartLogMessage(getSID(),"ITERATING GRAPH RESTART STEP EXECUTED"));
                        graphStatus = CONSTRUCTING_ARTICULATION;
                        params.n_restart_count++;
                        return 0.0;
                    }
                }
            }

            break;
        }
        case TABU:{
            vector<Solution> neighboors(getNeighboorSolutions(lastSolution,params.moveSet));

            if(params.minimum)
                MathHelper::orderSolutions(&neighboors,MathHelper::SolutionOrder::SCORE);
            else
                MathHelper::orderSolutions(&neighboors,MathHelper::SolutionOrder::SCORE,MathHelper::ListOrder::DECRESCENT);



            Solution* best_neighboor = &neighboors.front();


            //MINIMUM VALUE PROBLEM
            //BEST NEIGHBOOR IS BETTER THAN THE BEST SOLUTION
            if((params.minimum && (best_neighboor->cost < bestSolution->cost && best_neighboor->valid)) or (!params.minimum && (best_neighboor->cost > bestSolution->cost && best_neighboor->valid))){
                bestSolution->operator=(best_neighboor);
                lastSolution->operator=(best_neighboor);
                params.without_improvement_count = 0;

                if(params.construction == REACTIVE_GREEDY)
                    params.q_alphas[params.last_alpha_id]++;
            //BEST NEIGHBOOR IS NOT BETTER THAN THE BEST SOLUTION
            } else{
                bool found_neighboor = false; //Flags the heuristic has found the next movement
                int it = ZERO;
                Solution* current_solution = NULL;

                //FINDS BEST NEIGHBOOR THAT IS NOT TABU
                while(!found_neighboor){
                    current_solution = &neighboors[it];

                    if(!isTabu(current_solution)){
                        found_neighboor = true;
                    } else {
                        //ASPIRATION CRITERIA
                        if((ONE - params.tabu_aspiration_criteria) * lastSolution->score >= current_solution->score)
                            found_neighboor = true;
                    }

                    it++;
                }

                if(!found_neighboor){
                    //CHECKS IF RESTARTED ALL TIMES
                    if(params.n_restart_count >= params.n_restart){
                        this->status = Finished;
                        return 0.0;
                    } else {
                        emit emit_log(SmartLogMessage(getSID(),"ITERATING GRAPH RESTART STEP EXECUTED"));
                        graphStatus = CONSTRUCTING_ARTICULATION;
                        params.n_restart_count++;
                        return 0.0;
                    }
                }

                //ADDS LAST MOVEMENT TO TABU LIST
                WeightedGraphSolutionMovement movement = movementFromString(current_solution->last_movement);
                switch (movement) {
                case ADD:{
                    // IF ADDED A NODE TO SOLUTION IS TABU TO REMOVE IT
                    Tabu t;
                    t.node_id = current_solution->last_id;
                    t.movement = REMOVE;

                    addTabu(t);
                    break;
                }
                case REMOVE:{
                    // IF ADDED A NODE TO SOLUTION IS TABU TO ADD IT
                    Tabu t;
                    t.node_id = current_solution->last_id;
                    t.movement = ADD;

                    addTabu(t);
                    break;
                }
                case SWAP:{
                    // IF ADDED A NODE TO SOLUTION IS TABU TO REMOVE IT
                    Tabu t;
                    t.node_id = current_solution->last_id;
                    t.movement = REMOVE;

                    addTabu(t);

                    t.node_id = current_solution->last_id_two;
                    t.movement = ADD;

                    addTabu(t);
                    break;
                }
                case D_SWAP:{
                    // IF ADDED A NODE TO SOLUTION IS TABU TO REMOVE IT
                    Tabu t;
                    t.node_id = current_solution->last_id;
                    t.movement = REMOVE;

                    addTabu(t);

                    t.node_id = current_solution->last_id_two;
                    t.movement = REMOVE;

                    addTabu(t);
                    break;
                }
                }

                lastSolution->operator=(current_solution);//PERFORMS MOVEMENT

                params.without_improvement_count++;
            }

                //UPDATES ALPHA
                params.penalize_infeasible_alpha = params.penalize_infeasible_alpha_min + ((params.iterations[ITERATING] % params.penalize_steps) * ((params.penalize_infeasible_alpha_max - params.penalize_infeasible_alpha_min)/params.penalize_steps)) ;

                //CHECKS THE NEED TO PERTURBATE
                if((params.iterations[ITERATING] != ZERO) && (params.iterations[ITERATING] % params.nperturbation == ZERO)){
                    perturbateSolution(lastSolution,params.perturbate_pct);
                    graphStatus = CONSTRUCTING_HEURISTIC;
                    params.iterations[ITERATING]++;
                    break;
                }

                //CHECKS END OF HEURISTIC
                if(params.tabu_maximum_iterations <= params.iterations[ITERATING] or
                        params.tabu_maximum_without_improvemet <= params.without_improvement_count){
                    //CHECKS IF RESTARTED ALL TIMES
                    if(true){
                        this->status = Finished;
                        break;
                    } else {
                        emit emit_log(SmartLogMessage(getSID(),"ITERATING GRAPH RESTART STEP EXECUTED"));
                        graphStatus = CONSTRUCTING_ARTICULATION;
                        params.n_restart_count++;
                        break;
                    }
                }

            break;
        }
        case ANT_COLONY:{
            //CONSTRUCTING SOLUTIONS
            if(ants.size() < params.ant_colony_size){
                if(ants.empty()){
                    ant_tau.clear();
                    ant_tau_0.clear();
                    ant_global_tau.clear();

                    ant_first_aprimorant_complete.assign(params.ant_colony_size,false);

                    for (int i = 0; i < params.ant_colony_size; i++) {
                        vector<double> aux;
                        aux.assign(N,ZERO);

                        ant_tau.push_back(aux);
                        ant_tau_0.push_back(aux);
                    }

                    bestSolution->operator=(lastSolution);
                }

                if(ant_tau.empty() || ant_tau_0.empty()){
                    crash();
                    return 0.0;
                }

                if(ant_tau.size() != params.ant_colony_size || ant_tau_0.size() != params.ant_colony_size){
                    crash();
                    return 0.0;
                }

                //BUILDS TAU-0 OF THAT ANT
                vector<double>* p = &ant_tau_0[ants.size()];
                vector<int> tau0_ids = lastSolution->getVector();
                for (int i = 0; i < tau0_ids.size(); i++) {
                    p->at(tau0_ids[i]) = 1 / lastSolution->cost;
                }

                Solution s = *lastSolution;
                ants.push_back(s);

                emit emit_log(SmartLogMessage(getSID(),"ADDED ANT TO COLONY !"));

                //MINIMUM VALUE PROBLEM
                //LAST SOLUTION IS BETTER THAN THE BEST SOLUTION
                if(!ant_global_tau.empty()){
                    if(params.minimum && (lastSolution->cost <= bestSolution->cost) || !params.minimum && (lastSolution->cost >= bestSolution->cost)){
                        vector<double> delta_tau = this->delta_tau(lastSolution);
                        pheromone(&ant_global_tau,&delta_tau);

                        bestSolution->operator=(lastSolution);
                    }
                } else {
                    ant_global_tau.assign(p->begin(),p->end());
                }


                if(ants.size() != params.ant_colony_size){
                    emit emit_log(SmartLogMessage(getSID(),"ADDING NEW ANT TO COLONY !"));
                    graphStatus = CONSTRUCTING_ARTICULATION;

                    clock_t end = std::clock();

                    return double(end - begin) / CLOCKS_PER_SEC;
                }
            }

            //HAS ENOUGH SOLUTIONS

            //CHECKS IF TAUS MATRIXES HAVE BEEN SET
            if(ant_tau.empty() || ant_global_tau.empty() || ant_tau_0.empty()){
                crash();
                return 0.0;
            }

            if(ant_tau.size() != params.ant_colony_size || ant_global_tau.size() != N || ant_tau_0.size() != params.ant_colony_size){
                crash();
                return 0.0;
            }

            //ANTS ITERATION
            bool bestUpdate = false;
            for (int a = 0; a < ants.size(); a++) {
                Solution ant = ants[a];
                vector<double>* local_tau = &ant_tau[a];
                vector<double>* local_tau_0 = &ant_tau_0[a];
                local_pheromone(&ant_global_tau,local_tau,local_tau_0);
                if(ant_first_aprimorant_complete[a]){
                    double q = MathHelper::random(ONE);
                    vector<int> ids = ant.getVector();
                    vector<double> n(scoreSolution(&ant,params.desire));
                    double p_sum = 0.0;
                    double max_n = 0.0;
                    int max_id = 0;

                    for (int i = 0; i < n.size(); i++) {
                        n[i] *= local_tau->operator[](i);

                        if(i != 0){
                            if(max_n < n[i]){
                                max_n = n[i];
                                max_id = i;
                            }
                        } else {
                            max_n = n[i];
                        }
                    }

                    if(q > params.ant_colony_q0){
                        for (int i = 0; i < n.size(); i++) {
                            if(i == max_id){
                                n[i] = ONE;
                            } else {
                                n[i] = ZERO;
                            }
                        }
                    }

                    //IF DOMINANT PROBABILITY VECTOR n GOES TO ZERO
                    for (int i = 0; i < n.size(); i++) {
                        if(MathHelper::isIn(&ids,i))
                            n[i] = ZERO;

                        p_sum += n[i];
                    }

                    //ID TO ADD TO SOLUTION
                    int new_id = MathHelper::chose_from_cdf(n);

                    ant += new_id;

                    if(ant.valid)
                        ant_first_aprimorant_complete[a] = false;

                    emit emit_log(SmartLogMessage(getSID(),QString::asprintf("ANT COLONY PHEROMONE STEP EXECUTED ANT = %d  ANT_COST = %f VALID = %s",a, ant.cost,((ant.valid)?"true":"false"))));
                } else {
                    vector<Solution> neighboors(getNeighboorSolutions(&ant,params.moveSet));

                    if(params.minimum)
                        MathHelper::orderSolutions(&neighboors,MathHelper::SolutionOrder::COST);
                    else
                        MathHelper::orderSolutions(&neighboors,MathHelper::SolutionOrder::COST,MathHelper::ListOrder::DECRESCENT);

                    Solution* best_neighboor = &neighboors.front();

                    if(params.minimum){
                        if(best_neighboor->cost <= ant.cost && best_neighboor->valid){
                            ant = (best_neighboor);

                            if(params.construction == REACTIVE_GREEDY)
                                params.q_alphas[params.last_alpha_id]++;
                        } else {
                            ant_first_aprimorant_complete[a] = true;
                        }
                    } else {
                        if(best_neighboor->cost >= ant.cost && best_neighboor->valid){
                            ant = (best_neighboor);

                            if(params.construction == REACTIVE_GREEDY)
                                params.q_alphas[params.last_alpha_id]++;
                        } else {
                            ant_first_aprimorant_complete[a] = true;
                        }
                    }

                    emit emit_log(SmartLogMessage(getSID(),QString::asprintf("ANT COLONY FIRST APRIMORANT STEP EXECUTED ANT = %d  ANT_COST = %f VALID = %s",a,ant.cost,((ant.valid)?"true":"false"))));
                }


                scoreSolution(&ant);
                costSolution(&ant,params.problem);
                validSolution(&ant);

                if(((params.minimum && (ant.cost <= bestSolution->cost)) || (!params.minimum && (ant.cost >= bestSolution->cost))) && ant.valid){
                    vector<double> delta_tau = this->delta_tau(lastSolution);
                    pheromone(&ant_global_tau,&delta_tau);

                    bestSolution->operator=(ant);
                    bestUpdate = true;
                }

                ants[a].operator=(ant);
            }

            MathHelper::orderSolutions(&ants,MathHelper::SolutionOrder::COST,MathHelper::ListOrder::CRESCENT);
            lastSolution = &ants.front();

            if(!bestUpdate)
                params.without_improvement_count ++;

            //LAST SOLUTION IS BEST SOLUTION BETWEEN CURRENT ANTS

            //CHECKS THE NEED TO PERTURBATE
            if((params.iterations[ITERATING] != ZERO) && (params.iterations[ITERATING] % params.nperturbation == ZERO)){
                for (int i = 0; i < ants.size(); i++) {
                    Solution* ant = &ants[i];
                    double pct = MathHelper::random(ONE);
                    perturbateSolution(ant,pct);
                }
            }

            //CHECKS END OF HEURISTIC
            if(params.ant_maximum_iterations <= params.iterations[ITERATING] or
                    params.ant_maximum_without_improvemet <= params.without_improvement_count){
                    this->status = Finished;
                    break;
            }
            break;
        }
        }

        emit emit_log(SmartLogMessage(getSID(),"ITERATING GRAPH STEP EXECUTED"));

        params.iterations[ITERATING]++;
        break;
    }
    }

    if(lastSolution != NULL){
        scoreSolution(lastSolution);
        costSolution(lastSolution,params.problem);
        validSolution(lastSolution);

        emit emit_log(SmartLogMessage(getSID(),QString::asprintf(" -- LAST SOLUTION COST : %f",lastSolution->cost)));

        if(removeDuplicateNode(lastSolution)){
            emit emit_log(SmartLogMessage(getSID(),"REMOVED DUPLICATE!"));
        }
    }

    if(bestSolution != NULL){
        scoreSolution(bestSolution);
        costSolution(bestSolution,params.problem);
        validSolution(bestSolution);
        emit emit_log(SmartLogMessage(getSID(),QString::asprintf(" -- BEST SOLUTION COST : %f",bestSolution->cost)));
    }

    clock_t end = std::clock();

    emit emit_log(SmartLogMessage(getSID(),QString::asprintf(" -- ELAPSED : %f",(time.elapsedTime + (double(end - begin) / CLOCKS_PER_SEC)))));

    return double(end - begin) / CLOCKS_PER_SEC;
}

void WeightedGraph::finish(){
    status = Finished;
}

bool WeightedGraph::updateContextVisualParameter(QString field, QString value){
    if(field == "SPHERE_RADIUS"){
        sphere_radius = value.toDouble();
        return true;
    } else if(field == "DRAW_NODE"){
        draw_node = ((value.toInt() == 1) || value == "true")? true : false;
        return true;
    } else if(field == "DRAW_EDGE"){
        draw_edge = ((value.toInt() == 1) || value == "true")? true : false;
        return true;
    }

    return false;
}

bool WeightedGraph::isTabu(Solution* s){
    WeightedGraphSolutionMovement last_movement = movementFromString(s->last_movement);
    int last_node_id = s->last_id;

    for (int i = 0; i < tabuList.size(); i++) {
        Tabu t = tabuList[i];
        if(t.movement == last_movement && t.node_id == last_node_id)
            return true;
    }

    return false;
}
