#include "mathHelper.h"

/************************************************************************************
  Name:        mathHelper.cpp
  Copyright:   Version 1.0
  Author:      Igor Pires dos Santos
  Last Update: 17/02/2017 (DD/MM/YYYY)
  Release:     --/--/---- (DD/MM/YYYY)
  Description: Class implementation of a static class to help manage mathemathical
               expressions.
************************************************************************************/

bool MathHelper::parallel = true;

int MathHelper::threads = 16;
int MathHelper::threads_in_blocks = 16;
int MathHelper::thread_blocks = 1;
MathHelper::ParallelParadigma MathHelper::default_paradigma = MathHelper::OPENMP;
SmartThreadPool* MathHelper::thread_pool = NULL;

int MathHelper::chose_from_cdf(vector<float> cdf){
    vector<double> aux;

    for (int i = 0; i < cdf.size(); i++) {
        if(cdf[i] < ESSENTIALY_ZERO)
            aux.push_back(ZERO);
        else
            aux.push_back(cdf[i]);
    }

    double sum = 0.0;
    unsigned seed = chrono::system_clock::now().time_since_epoch().count();
    default_random_engine generator(seed);

    for (int i = 0; i < aux.size(); i++) {
        if(aux[i] < 0.0)
            return 0.0;

        sum += aux[i];
    }

    if(sum == 0.0)
        return 0.0;

    discrete_distribution<> d(aux.begin(),aux.end());

    return d(generator);
}

int MathHelper::chose_from_cdf(vector<double> cdf){
    vector<double> aux;
    double sum = 0.0;
    unsigned seed = chrono::system_clock::now().time_since_epoch().count();
    default_random_engine generator(seed);

    for (int i = 0; i < cdf.size(); i++) {
        if(cdf[i] < 0.0)
            return 0.0;

        if(cdf[i] < ESSENTIALY_ZERO)
            aux.push_back(ZERO);
        else
            aux.push_back(cdf[i]);

        sum = sum + cdf[i];
    }

    if(sum == 0.0)
        return 0.0;

    discrete_distribution<> d(aux.begin(),aux.end());

    return d(generator);
}

int MathHelper::chose_from_cdf(vector<float>* cdf){
    vector<double> aux;

    for (int i = 0; i < cdf->size(); i++) {
        if(cdf->operator[](i) < ESSENTIALY_ZERO)
            aux.push_back(ZERO);
        else
            aux.push_back(cdf->operator[](i));
    }

    double sum = 0.0;
    unsigned seed = chrono::system_clock::now().time_since_epoch().count();
    default_random_engine generator(seed);

    for (int i = 0; i < aux.size(); i++) {
        if(aux[i] < 0.0)
            return 0.0;

        sum += aux[i];
    }

    if(sum == 0.0)
        return 0.0;

    discrete_distribution<> d(aux.begin(),aux.end());

    return d(generator);
}

int MathHelper::chose_from_cdf(vector<double>* cdf){
    vector<double> aux;

    for (int i = 0; i < cdf->size(); i++) {
        if(cdf->operator[](i) < ESSENTIALY_ZERO)
            aux.push_back(ZERO);
        else
            aux.push_back(cdf->operator[](i));
    }

    double sum = 0.0;
    unsigned seed = chrono::system_clock::now().time_since_epoch().count();
    default_random_engine generator(seed);

    for (int i = 0; i < aux.size(); i++) {
        if(aux[i] < 0.0)
            return 0.0;

        sum += aux[i];
    }

    if(sum == 0.0)
        return 0.0;

    discrete_distribution<> d(aux.begin(),aux.end());

    return d(generator);
}

bool MathHelper::orderSolutions(vector<Solution>* solutions, SolutionOrder sol_field, ListOrder order){
    if(parallel){
        if(default_paradigma == OPENMP){
            switch (order) {
            case CRESCENT:{
                switch (sol_field) {
                case COST:{
                    int n = solutions->size();
                    int phase;
                    int i;
                    Solution tmp;
#pragma omp parallel num_threads(threads) default(none) shared(solutions,n) private(i,tmp,phase)
                    for(phase = 0; phase < n; phase ++){
                        if(phase % 2 == 0){
#pragma omp for
                            for(i = 1; i < n; i += 2){
                                if(solutions->operator[](i-1).cost > solutions->operator[](i).cost){
                                    tmp = solutions->operator[](i-1);
                                    solutions->at(i-1) = solutions->operator[](i);
                                    solutions->at(i) = tmp;
                                }
                            }
                        } else {
#pragma omp for
                            for(i = 1; i < n-1; i += 2){
                                if(solutions->operator[](i).cost > solutions->operator[](i+1).cost){
                                    tmp = solutions->operator[](i+1);
                                    solutions->at(i+1) = solutions->operator[](i);
                                    solutions->at(i) = tmp;
                                }
                            }
                        }
                    }
                    return true;
                }
                case SCORE:{
                    int n = solutions->size();
                    int phase;
                    int i;
                    Solution tmp;
#pragma omp parallel num_threads(threads) default(none) shared(solutions,n) private(i,tmp,phase)
                    for(phase = 0; phase < n; phase ++){
                        if(phase % 2 == 0){
#pragma omp for
                            for(i = 1; i < n; i += 2){
                                if(solutions->operator[](i-1).score > solutions->operator[](i).score){
                                    tmp = solutions->operator[](i-1);
                                    solutions->at(i-1) = solutions->operator[](i);
                                    solutions->at(i) = tmp;
                                }
                            }
                        } else {
#pragma omp for
                            for(i = 1; i < n-1; i += 2){
                                if(solutions->operator[](i).score > solutions->operator[](i+1).score){
                                    tmp = solutions->operator[](i+1);
                                    solutions->at(i+1) = solutions->operator[](i);
                                    solutions->at(i) = tmp;
                                }
                            }
                        }
                    }
                    return true;
                }
                }

                return true;
            }
            case DECRESCENT:{
                switch (sol_field) {
                case COST:{
                    int n = solutions->size();
                    int phase;
                    int i;
                    Solution tmp;
#pragma omp parallel num_threads(threads) default(none) shared(solutions,n) private(i,tmp,phase)
                    for(phase = 0; phase < n; phase ++){
                        if(phase % 2 == 0){
#pragma omp for
                            for(i = 1; i < n; i += 2){
                                if(solutions->operator[](i-1).cost < solutions->operator[](i).cost){
                                    tmp = solutions->operator[](i-1);
                                    solutions->at(i-1) = solutions->operator[](i);
                                    solutions->at(i) = tmp;
                                }
                            }
                        } else {
#pragma omp for
                            for(i = 1; i < n-1; i += 2){
                                if(solutions->operator[](i).cost < solutions->operator[](i+1).cost){
                                    tmp = solutions->operator[](i+1);
                                    solutions->at(i+1) = solutions->operator[](i);
                                    solutions->at(i) = tmp;
                                }
                            }
                        }
                    }
                    return true;
                }
                case SCORE:{
                    int n = solutions->size();
                    int phase;
                    int i;
                    Solution tmp;
#pragma omp parallel num_threads(threads) default(none) shared(solutions,n) private(i,tmp,phase)
                    for(phase = 0; phase < n; phase ++){
                        if(phase % 2 == 0){
#pragma omp for
                            for(i = 1; i < n; i += 2){
                                if(solutions->operator[](i-1).score < solutions->operator[](i).score){
                                    tmp = solutions->operator[](i-1);
                                    solutions->at(i-1) = solutions->operator[](i);
                                    solutions->at(i) = tmp;
                                }
                            }
                        } else {
#pragma omp for
                            for(i = 1; i < n-1; i += 2){
                                if(solutions->operator[](i).score < solutions->operator[](i+1).score){
                                    tmp = solutions->operator[](i+1);
                                    solutions->at(i+1) = solutions->operator[](i);
                                    solutions->at(i) = tmp;
                                }
                            }
                        }
                    }
                    return true;
                }
                }

                return true;
            }
            }

        } else if(default_paradigma == QTHREAD) {
            if(thread_pool != NULL){
                bool crescent = (order == CRESCENT);
                bool useScore = (sol_field == SCORE);

                thread_pool->order(solutions,useScore,crescent);
            } else {
                return false;
            }
        }
    } else {
        switch (order) {
        case CRESCENT:{
            switch (sol_field) {
            case COST:{
                Solution::orderCrescent(solutions);
                break;
            }
            case SCORE:{
                Solution::orderScoreCrescent(solutions);
                break;
            }
            }

            break;
        }
        case DECRESCENT:{
            switch (sol_field) {
            case COST:{
                Solution::orderDecrescent(solutions);
                break;
            }
            case SCORE:{
                Solution::orderScoreDecrescent(solutions);
                break;
            }
            }
            break;
        }
        }
    }
}

bool MathHelper::orderCosts(vector<Cost>* costs, ListOrder order){
    if(parallel){
        if(default_paradigma == OPENMP){
            switch(order){
            case CRESCENT:{
                int n = costs->size();
                int phase;
                int i;
                Cost tmp;
#pragma omp parallel num_threads(threads) default(none) shared(costs,n) private(i,tmp,phase)
                for(phase = 0; phase < n; phase ++){
                    if(phase % 2 == 0){
#pragma omp for
                        for(i = 1; i < n; i += 2){
                            if(costs->operator[](i-1) > costs->operator[](i)){
                                tmp = costs->operator[](i-1);
                                costs->at(i-1) = costs->operator[](i);
                                costs->at(i) = tmp;
                            }
                        }
                    } else {
#pragma omp for
                        for(i = 1; i < n-1; i += 2){
                            if(costs->operator[](i) > costs->operator[](i+1)){
                                tmp = costs->operator[](i+1);
                                costs->at(i+1) = costs->operator[](i);
                                costs->at(i) = tmp;
                            }
                        }
                    }
                }
                return true;
            }
            case DECRESCENT:{
                int n = costs->size();
                int phase;
                int i;
                Cost tmp;
#pragma omp parallel num_threads(threads) default(none) shared(costs,n) private(i,tmp,phase)
                for(phase = 0; phase < n; phase ++){
                    if(phase % 2 == 0){
#pragma omp for
                        for(i = 1; i < n; i += 2){
                            if(costs->operator[](i-1) < costs->operator[](i)){
                                tmp = costs->operator[](i-1);
                                costs->at(i-1) = costs->operator[](i);
                                costs->at(i) = tmp;
                            }
                        }
                    } else {
#pragma omp for
                        for(i = 1; i < n-1; i += 2){
                            if(costs->operator[](i) < costs->operator[](i+1)){
                                tmp = costs->operator[](i+1);
                                costs->at(i+1) = costs->operator[](i);
                                costs->at(i) = tmp;
                            }
                        }
                    }
                }
                return true;
            }
            }

        } else if(default_paradigma == QTHREAD){
            if(thread_pool != NULL){
                bool crescent = (order == CRESCENT);

                thread_pool->order(costs,crescent);
            } else {
                return false;
            }
        }

    } else {
        switch(order){
        case CRESCENT:
            Cost::orderCrescent(costs);
            break;
        case DECRESCENT:
            Cost::orderDecrescent(costs);
            break;
        }
    }
}

bool MathHelper::orderVector(vector<int>* vec, ListOrder order){
    if(!parallel){
        switch (order) {
        case CRESCENT:{
            sort(vec->begin(),vec->end());
            return true;
        }
        case DECRESCENT:{
            int aux;
            sort(vec->begin(),vec->end());

            for(int i = 0; i < vec->size()/2; i++){
                int c_i = (vec->size() - ONE) - i;
                aux = vec->operator[](i);
                vec->at(i) = vec->operator[](c_i);
                vec->at(c_i) = aux;
            }

            return true;
        }
        }
    } else {
        if(default_paradigma == OPENMP){
            switch (order) {
            case CRESCENT:{
                int n = vec->size();
                int phase;
                int i;
                int tmp;
#pragma omp parallel num_threads(threads) default(none) shared(vec,n) private(i,tmp,phase)
                for(phase = 0; phase < n; phase ++){
                    if(phase % 2 == 0){
#pragma omp for
                        for(i = 1; i < n; i += 2){
                            if(vec->operator[](i-1) > vec->operator[](i)){
                                tmp = vec->operator[](i-1);
                                vec->at(i-1) = vec->operator[](i);
                                vec->at(i) = tmp;
                            }
                        }
                    } else {
#pragma omp for
                        for(i = 1; i < n-1; i += 2){
                            if(vec->operator[](i) > vec->operator[](i+1)){
                                tmp = vec->operator[](i+1);
                                vec->at(i+1) = vec->operator[](i);
                                vec->at(i) = tmp;
                            }
                        }
                    }
                }
                return true;
            }
            case DECRESCENT:{
                int n = vec->size();
                int phase;
                int i;
                int tmp;
#pragma omp parallel num_threads(threads) default(none) shared(vec,n) private(i,tmp,phase)
                for(phase = 0; phase < n; phase ++){
                    if(phase % 2 == 0){
#pragma omp for
                        for(i = 1; i < n; i += 2){
                            if(vec->operator[](i-1) < vec->operator[](i)){
                                tmp = vec->operator[](i-1);
                                vec->at(i-1) = vec->operator[](i);
                                vec->at(i) = tmp;
                            }
                        }
                    } else {
#pragma omp for
                        for(i = 1; i < n-1; i += 2){
                            if(vec->operator[](i) < vec->operator[](i+1)){
                                tmp = vec->operator[](i+1);
                                vec->at(i+1) = vec->operator[](i);
                                vec->at(i) = tmp;
                            }
                        }
                    }
                }
                return true;
            }
            }

        } else if (default_paradigma == QTHREAD) {
            if(thread_pool != NULL){
                bool crescent = (order == CRESCENT);

                thread_pool->order(vec,crescent);
            } else {
                return false;
            }
        }
    }
}

bool MathHelper::isIn(vector<int>* vec, int value){
    if(parallel){
        if(default_paradigma == OPENMP){
            int sum = 0;
# pragma omp parallel for num_threads(threads)  default(none) shared(vec,value) reduction(+:sum)
            for(int i = 0; i < vec->size(); i++)
                if(value == vec->operator[](i))
                    sum += ONE;

            return (sum > ZERO);
//        } else if (default_paradigma == QTHREAD) {
//            if(thread_pool != NULL){

//            } else {
//                return false;
//            }
        } else {
            for(int i = 0; i < vec->size(); i++)
                if(value == vec->operator[](i))
                    return true;

            return false;
        }
    } else {
        for(int i = 0; i < vec->size(); i++)
            if(value == vec->operator[](i))
                return true;

    }

    return false;
}

bool MathHelper::isPositive(double val){
    return val > 0;
}


bool MathHelper::isBetween(float val, float max, float min){
    if(val <= max)
        if(val >= min)
            return true;

    return false;
}


double MathHelper::euclideanDistance(Point* p1, Point* p2){
    return (double) sqrt(pow(p1->x - p2->x,2)  + pow(p1->y - p2->y,2) + pow(p1->z - p2->z,2));
}

double MathHelper::secant(double val){
    return 1 /cos(val);
}

double MathHelper::cotangent(double val){
    return cos(val * PI / 180) / sin(val * PI / 180);
}

double MathHelper::tangent(double val){
    return tan(val * PI / 180);
}

double MathHelper::cosine(double val){
    return cos(val * PI / 180);
}

double MathHelper::sine(double val){
    return sin(val * PI / 180);
}

double MathHelper::degreeToRadian(double val){
    return val * PI / 180;
}

float MathHelper::max(vector<float> v){
    if(v.size() == 0)
        return -1;

    float max = v[0];

    for(int i = 1; i < v.size(); i++){
        if(v[i] > max)
            max = v[i];
    }

    return max;

}

float MathHelper::max(vector<double> v){
    if(v.size() == 0)
        return -1;

    float max = v[0];

    for(int i = 1; i < v.size(); i++){
        if(v[i] > max)
            max = v[i];
    }

    return max;

}

float MathHelper::min(vector<float> v){
    if(v.size() == 0)
        return -1;

    float min = v[0];

    for(int i = 1; i < v.size(); i++){
        if(v[i] < min)
            min = v[i];
    }

    return min;

}

float MathHelper::min(vector<double> v){
    if(v.size() == 0)
        return -1;

    float min = v[0];

    for(int i = 1; i < v.size(); i++){
        if(v[i] < min)
            min = v[i];
    }

    return min;

}

void MathHelper::randomSeed(){
    QTime time = QTime::currentTime();

    srand(time.msecsSinceStartOfDay() * time.msec());
}

double MathHelper::random(double max, double min){
    if(min == max)
        return min;

    if(min > max){
        double aux = max;
        max = min;
        min = aux;
    }
    uniform_real_distribution<double> unif(min,max);
    unsigned seed = chrono::system_clock::now().time_since_epoch().count();
    default_random_engine generator(seed);

    double a_random_double = unif(generator);

    return a_random_double;
}

Point MathHelper::rotateX(Point p, double rx){
    float y = p.y;
    float z = p.z;

    p.y = ((y * cosine(rx)) - (z * sine(rx)));
    p.z = ((z * cosine(rx)) + (y * sine(rx)));

    return p;
}

Point MathHelper::rotateY(Point p, double ry){
    float x = p.x;
    float z = p.z;

    p.x = ((x * cosine(ry)) + (z * sine(ry)));
    p.z = ((z * cosine(ry)) - (x * sine(ry)));

    return p;
}

Point MathHelper::rotateZ(Point p, double rz){
    float x = p.x;
    float y = p.y;

    p.x = ((x * cosine(rz)) - (y * sine(rz)));
    p.y = ((y * cosine(rz)) + (x * sine(rz)));

    return p;
}

double MathHelper::sign(double a, double b){
    if(b < 0)
        return abs(a) * (-1);

    return abs(a);
}

float MathHelper::sign(float a, float b){
    if(b < 0)
        return abs(a) * (-1);

    return abs(a);
}

int MathHelper::sign(int a, int b){
    if(b < 0)
        return abs(a) * (-1);

    return abs(a);
}

Point MathHelper::interpolate(Point a, Point b, double a_scalar, double b_scalar, double f){
    if(b_scalar <= a_scalar){
        double aux = a_scalar;
        a_scalar = b_scalar;
        b_scalar = aux;

        Point aux_p = a;
        a = b;
        b = aux_p;
    }

    if(!isBetween(f,b_scalar,a_scalar))
        return Point();


    double pct = abs((f-a_scalar)/(b_scalar-a_scalar));
    Point p = (b * pct) + (a * (1 - pct));

    return p;
}

QString MathHelper::paradigmaToString(ParallelParadigma paradigma){
    switch (paradigma) {
    case OPENMP:
        return "OPENMP";
    case MPI:
        return "MPI";
    case QTHREAD:
        return "QTHREAD";
    }

    return "";
}

MathHelper::ParallelParadigma MathHelper::stringToParadigma(QString string){
    if(string == "OPENMP"){
        return OPENMP;
    } else if (string == "MPI") {
        return MPI;
    }  else if (string == "QTHREAD") {
        return QTHREAD;
    }

    return OPENMP;
}


bool MathHelper::setThreadCount(int threads){
    if(threads > omp_get_max_threads())
        return false;

    MathHelper::threads = threads;

    return true;
}

int MathHelper::getThreads(){
    return threads;
}
