#ifndef LINES3D_H
#define LINES3D_H

#include "../graphicobject.h"
#include "../colorbar/black.h"

#define TWO 2
#define THREE 3

namespace Lines3DParams{
    struct Line{
        int A;
        int B;
    };
}

using namespace std;
using namespace Lines3DParams;

class Lines3D : public GraphicObject
{
public:
    Lines3D(SmartObject* parent = NULL);
    Lines3D(QString name, SmartObject* parent = NULL);
    Lines3D(ColorBar cb, SmartObject* parent = NULL);
    Lines3D(QString name, ColorBar cb, SmartObject* parent = NULL);

    void resizeTransform();

    int getN();
    int getM();

    /********************************************************
    *  SMART OBJECT VIRTUAL METHODS                       *
    ********************************************************/
    vector<QString> print();
    vector<QString> data();
    vector<QString> raw();

    /********************************************************
    *  GRAPHIC OBJECT VIRTUAL METHODS  *
    ********************************************************/
    bool compatibility(VTKFile *file);
    bool load(VTKFile* file);
    void draw();
    vector<Field> getPointDataList();
    vector<Field> getCellDataList();
    vector<Field> getStartParameters();
    vector<Field> getContextVisualParameters();
    vector<QString> getRanges();
    bool updateStartParameter(QString field, QString value);
    virtual GraphicObject* extrapolateContext(QString extrapolation);

    bool addData(vector<Point*> data);
    bool addData(vector<Point> data);
    bool addData(Point* p);
    bool addLine(int a, int b);
    bool addLine();

protected:

private:
    int N;
    int M;
    vector<Point*> points;
    vector<Lines3DParams::Line> lines;

    /********************************************************
    *  GRAPHIC OBJECT VIRTUAL METHODS  *
    ********************************************************/
    bool initialize();
    double iteration();
    void finish();
    bool updateContextVisualParameter(QString field, QString value);
    bool setContextRange(QString field);
    vector<QString> getExtrapolations();
    QString getLiveParameterList();
    QString getLiveParameter(QString name);
    DataType getLiveParameterDataType(QString name);
    void mouse_press_event(int width, int height, Point eye, QMouseEvent *event, GraphicTools tool);
    void mouse_move_event(int width, int height, Point eye, QMouseEvent *event, GraphicTools tool);
};

#endif // LINES3D_H
