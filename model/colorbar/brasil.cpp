#include "brasil.h"

Brasil::Brasil() : ColorBar("BRASIL"){
    QColor c;
    addColor(c.fromRgb(0,155,58));
    addColor(c.fromRgb(254,223,0));
    addColor(c.fromRgb(0,39,118));
}
