#ifndef ARTERY_H
#define ARTERY_H

/************************************************************************************
  Name:        artery.h
  Copyright:   Version 1.0
  Author:      Igor Pires dos Santos
  Last Update: 17/02/2017 (DD/MM/YYYY)
  Release:     --/--/---- (DD/MM/YYYY)
  Description: Header class of an artery structure.
************************************************************************************/

#include <math.h>
#include <iostream>
#include <fstream>
#include <complex>
#include "helper/mathHelper.h"
#include "point.h"
#include "graphic2d.h"
#include "graphic2d/staticgraphic.h"

#define PI 3.14159265359
#define SIZE 1.0
#define DEFAULT_GRAPHIC_SIZE 100

using namespace std;

class  Artery : public SmartObject
{

public:

    enum ArteryParameter{
        POINTS,
        LINES,
        PRESSURE,
        FLOW,
        RADIUS,
        YOUNG_MOD,
        DENSITY,
        VISCOSITY,
        LENGTH,
        WAVESPEED,
        BETA,
        ADMITTANCE,
        REFLECTION_COEF,
        EFFECTIVE_ADMITTANCE,
        MEDIUM_PRESSURE,
        PRESSURE_WAVE,
        FLOW_WAVE
    };
    /************************************************************************************
        CONSTRUCTOR

        @param id identificator of artery.
        @param p point armazenated in the artery, in this case, the distal node.
        @param length the length of the artery (in centimeters)
        @param youngModulus the Young`s Modulus of this artery
        @param density the Density in this artery
        @param viscosity the fluid Viscosity of this artery
        @param radius the Radius of this artery (in centimeters)
        @param isLeaf if this artery is a leaf in the tree representation
************************************************************************************/
    Artery(int id, Point* p, double length, double youngModulus, double density, double viscosity,
           double radius, bool isLeaf, GraphicObject* arteryTree, int arteryTreeGID);

/************************************************************************************
        DESTRUCTOR
************************************************************************************/
    virtual ~Artery();

/************************************************************************************
        Checks equality between arteries.

        @param a another artery to check equality with this one.
        @return equality between arteries
************************************************************************************/
    bool equals(Artery* a);

/************************************************************************************
        Finds the artery with the distal node in the following point.

        @param p the distal node searched for.
        @return artery with p as a distal node.
************************************************************************************/
    Artery* findArtery(Point* point);

/************************************************************************************
        Returns a boolean correponding if this artery has a father. Useful to know if
        this is a root artery.

        @return if this artery has a father.
************************************************************************************/
    bool hasFather();

/************************************************************************************
        Returns a boolean, true if this artery has a left sided son and false
        otherwise.

        @return if this artery has a left sided son.
************************************************************************************/
    bool hasLeft();

/************************************************************************************
        Returns a boolean, true if this artery has a right sided son and false
        otherwise.

        @return if this artery has a right sided son.
************************************************************************************/
    bool hasRight();

/************************************************************************************
        Returns a boolean, true if this artery has a right or left sided son and
        false otherwise.

        @return if this artery has a right or left sided son.
************************************************************************************/
    bool hasSon();

/************************************************************************************
        Hovers the artery. If has parent sends the selection upwards and tries
        to unselect its brother if brother is selected.
************************************************************************************/
    void hover();

/************************************************************************************
        Unhovers the artery and its descendants.
************************************************************************************/
    void unhover();

/************************************************************************************
        Returns a boolean, true if this artery is hovered, false otherwise.

        @return if this artery is hovered.
************************************************************************************/
    bool isHovered();

/************************************************************************************
        Returns a boolean, true if this is a leaf, false otherwise.

        @return if this artery is a leaf.
************************************************************************************/
    bool isLeaf();

/************************************************************************************
        Returns a boolean, true if this is selected, false otherwise.

        @return if this artery is selected.
************************************************************************************/
    bool isSelected();

/************************************************************************************
        Selects the artery. If it has a parent sends the selection upwards and
        tries to unselects its brother
************************************************************************************/
    void select();

/************************************************************************************
        Unselects the artery and its descendants.
************************************************************************************/
    void unselect();

        /********************************************************
        *                  PRINTS                               *
        * Recursive prints walking first to the left side.      *
        ********************************************************/
/************************************************************************************
        Prints a desired field.

        @param file the file opened to save the point representation.
************************************************************************************/
    void printField(ArteryParameter fieldCode, vector<QString>* raw);

/********************************************************
*                  GETS & SETS                          *
********************************************************/

/************************************************************************************
        Getter for admittance Y.

        -1 flag for not set yet.

        @return the admittance value of this artery.
************************************************************************************/
    complex<double> getAdmittance();

/************************************************************************************
        Setter for admittance Y. Returns a booleans that shows if the algorithm was
        able to set or not the admittance value for this artery.

        @param viscous if this is a viscous model or not.
        @param viscousMultiplier viscous multiplier for this iteration.
        @param phi0 initial phase angle between pressure and vessel wall displacement.
        @return if the algorithm was able to set the admittance.
************************************************************************************/
    bool setAdmittance(bool viscous, double viscousMultiplier, double phi0);

/************************************************************************************
        Setter for admittance Y. Returns a booleans that shows if the algorithm was
        able to set or not the admittance value for this artery.

        @return if the algorithm was able to set the admittance.
************************************************************************************/
    bool setAdmittance();

/************************************************************************************
        Getter for alpha.

        -1 flag for not set yet.

        @return the alpha value of this artery.
************************************************************************************/
    double getAlpha();

/************************************************************************************
        Setter for alpha. Returns a booleans that shows if the algorithm was
        able to set or not the alpha value for this artery.

        @return if the algorithm was able to set the alpha.
************************************************************************************/
    bool setAlpha(double viscousMultiplier);

/************************************************************************************
        Getter for angular fequency w.

        -1 flag for not set yet.

        @return the angular fequency value of this artery.
************************************************************************************/
    double getAngularFrequency();

/************************************************************************************
        Setter for angular fequency. Returns a booleans that shows if the algorithm was
        able to set or not the angular fequency value for this artery.

        @return if the algorithm was able to set the angular fequency.
************************************************************************************/
    bool setAngularFrequency(double frequency);

/************************************************************************************
        Getter for area value using the radius.

        -1 flag for radius not set yet.

        @return the area value of this artery.
************************************************************************************/
    double getArea();

/************************************************************************************
        Getter for beta value using the length, wave speed and angular frequency.

        -1 flag for beta not set yet.

        @return the beta value of this artery.
************************************************************************************/
    complex<double> getBeta();

/************************************************************************************
        Setter for beta. Returns a booleans that shows if the algorithm was
        able to set or not the beta value for this artery.

        @return if the algorithm was able to set beta.
************************************************************************************/
    bool setBeta();

/************************************************************************************
        Getter for density value given at the start.

        @return the beta value of this artery.
************************************************************************************/
    double getDensity();

/************************************************************************************
        Setter for the density value of this artery.

        @param val value to substitute the density value.
************************************************************************************/
    void setDensity(double val);

/************************************************************************************
        Getter for diameter value using the radius.

        @return the diameter value of this artery.
************************************************************************************/
    double getDiameter();


    /************************************************************************************
        Getter for effective admittance value using the effective admittance of it's
        sons and it's own admittance Y.

        -1 flag for effective admittance not set yet.

        @return the effective admittance value of this artery.
************************************************************************************/
    complex<double> getEffectiveAdmittance();
    bool setEffectiveAdmittance();
    Artery* getFather();
    void setFather(Artery* a);
    complex<double> getFlow();
    vector<Point*> getFlowWave();
    complex<double> getFlow(double x, complex<double> rootAdmittance);
    bool setFlow(complex<double> rootAdmittance);
    double getFrequency();
    int getID();
    Artery* getLeft();
    void setLeft(Artery* a);
    void setLeaf(bool val);
    double getLength();
    void setLength(double val);
    complex<double> getMediumPressure();
    bool setMediumPressure();
    bool setMediumPressure(double p0);
    Point getPoint();
    void setPoint(Point p);
    double getPhaseAngle();
    bool setPhaseAngle(double phi0);

/************************************************************************************
        Recursive method to set the following attributes using recursion.

        Wave Speed                                      [c]  in  [cm/s]
        Beta                                            [^b] in  []
        Admittance                                      [Y]  in  []
        Reflection Coefficient                          [R]  in  []
        Effective Admittance                            [Ye] in  []

        Because of the way these equations are solved this recursion is done from
        bottom to top. Check the full report for further information.

        @return if all steps were successfull.
************************************************************************************/
    bool setPhaseOne(double frequency);

/************************************************************************************
        Recursive method to set the following attributes using recursion.

        Wave Speed                                      [c]  in  [cm/s]
        Beta                                            [^b] in  []
        Admittance                                      [Y]  in  []
        Reflection Coefficient                          [R]  in  []
        Effective Admittance                            [Ye] in  []

        Because of the way these equations are solved this recursion is done from
        bottom to top. Check the full report for further information.

        @return if all steps were successfull.
************************************************************************************/
    bool setPhaseOne(double frequency, bool viscous, double viscousMultiplier, double phi0);

/************************************************************************************
        Recursive method to set the following attributes using recursion.

        Medium Pressure                                 [_p] in  [cm/s]
        Pressure at the distal node                     [P]  in  []
        Flow at the distal node                         [Q]  in  []

        Because of the way these equations are solved this recursion is done from
        top to bottom. Check the full report for further information.

        @return if all steps were successfull.
************************************************************************************/
    bool setPhaseTwo(double p0, complex<double> rootAdmittance);

/************************************************************************************
        Getter for pressure value in this artery's distal node value.

        -1 flag for pressure not set yet.

        @return the pressure value of this artery's distal node.
************************************************************************************/
    complex<double> getPressure();
    vector<Point*> getPressureWave();

    /************************************************************************************
        Getter for pressure value in a x-point inside the artery using the medium
        pressure and the pressure coming from this artery's father.

        -1 flag for not able to calculate.

        @param x an x point inside (0,1) where the pressure will be found.
        @return the pressure value of this artery's in the x- point.
************************************************************************************/
    complex<double> getPressure(double x);

    /************************************************************************************
        Setter for this artery's pressure in the distal node.

        @return if the algorithm was able to set the artery's pressure in the distal
                node.
************************************************************************************/
    bool setPressure();

    /************************************************************************************
        Getter for radius value given at the start.

        @return the radius value of this artery.
************************************************************************************/
    double getRadius();

/************************************************************************************
        Setter for the radius value of this artery.

        Automatically sets the wall thickness h.

        @param val value to substitute the radius value.
************************************************************************************/
    void setRadius(double val);

/************************************************************************************
        Getter for reflection coefficient value in this artery.

        -1 flag for reflection coefficient not set yet.

        @return the reflection coefficient value of this artery.
************************************************************************************/
    complex<double> getReflectionCoef();

/************************************************************************************
        Setter for this artery's reflection coefficient.

        @return if the algorithm was able to set the artery's reflection coefficient
        in the distal node.
************************************************************************************/
    bool setReflectionCoef();

/************************************************************************************
        Getter for right sided son artery pointer.

        WARNING: NULL if has no right sided son, use hasRight() for protection.

        @return the pointer to this artery's right sided son.
************************************************************************************/
    Artery* getRight();

/************************************************************************************
        Setter for right sided son artery pointer.

        WARNING: Right sided son can be set as NULL.

        @param a an artery pointer to be set as this artery's right sided son.
************************************************************************************/
    void setRight(Artery* a);

/************************************************************************************
        Getter for wall thickness value given at the start.

        @return the wall thickness value of this artery.
************************************************************************************/
    double getThickness();

/************************************************************************************
        Setter for the wall thickness value of this artery.

        Automatically sets the radius r.

        @param val value to substitute the wall thickness value.
************************************************************************************/
    void setThickness(double val);

/************************************************************************************
        Getter for viscosity value given at the start.

        @return the viscosity value of this artery.
************************************************************************************/
    double getViscosity();

/************************************************************************************
        Setter for the viscosity value of this artery.

        @param val value to substitute the viscosity value.
************************************************************************************/
    void setViscosity(double val);

/************************************************************************************
        Setter for beta. In this case beta will be calculated with th viscous wave-
        speed.

        @return if the algorithm was able to set beta.
************************************************************************************/
    bool setViscousBeta();

/************************************************************************************
        Getter for viscous factor value given at the start.

        -1 flag for viscous factor not set yet.

        @return the viscous factor value of this artery.
************************************************************************************/
    complex<double> getViscousFactor();

/************************************************************************************
        Setter for viscous factor value given at the start.

        @return the viscous factor value of this artery.
************************************************************************************/
    bool setViscousFactor();

/************************************************************************************
        Getter for wave speed for viscous fluid value in this artery.

        -1 flag for wave speed for viscous fluid not set yet.

        @return the wave speed for viscous fluid value of this artery.
************************************************************************************/
    complex<double> getViscousWaveSpeed();

/************************************************************************************
        Setter for this artery's wave speed.

        @return if the algorithm was able to set the artery's wave speed.
************************************************************************************/
    bool setViscousWaveSpeed();

/************************************************************************************
        Getter Young's Modulus for viscoelastic walls in this artery.

        -1 flag for Young's Modulus for viscoelastic walls not set yet.

        @return the Young's Modulus for viscoelastic walls value of this artery.
************************************************************************************/
    complex<double> getViscousYoungsModulus();

/************************************************************************************
        Setter for this artery's Young's Modulus for viscoelastic walls.

        @return if the algorithm was able to set the artery's Young's Modulus for viscoelastic walls.
************************************************************************************/
    bool setViscousYoungsModulus();

/************************************************************************************
        Getter for Young's modulus value given at the start.

        @return the Young's modulus value of this artery.
************************************************************************************/
    double getYoungModulus();

/************************************************************************************
        Setter for the Young's modulus value of this artery.

        @param val value to substitute the Young's modulus value.
************************************************************************************/
    void setYoungModulus(double val);

/************************************************************************************
        Getter for wave speed value in this artery.

        -1 flag for wave speed not set yet.

        @return the wave speed value of this artery.
************************************************************************************/
    double getWaveSpeed();

/************************************************************************************
        Setter for this artery's wave speed.

        @return if the algorithm was able to set the artery's wave speed.
************************************************************************************/
    bool setWaveSpeed();

    double getPressureMax();

    double getPressureMin();

    double getFlowMax();

    double getFlowMin();

    void clearMinMax();

    vector<QString> print();
    vector<QString> data();
    vector<QString> raw();
    void solveLog(SmartLogMessage msg);

protected:

private:
/************************************************************************************
        LIST OF ATTRIBUTES

        father..............pointer to father artery.

        left................pointer to left sided son artery.

        right...............pointer to right sided son artery.

        p...................pointer to this artery distal node (x,y) point
                            representation.

        pressure_wave.......LineGraphic element of this artery pressure wave.

        flow_wave...........LineGraphic element of this artery flow wave.

        beta................beta value of this artery.

        cv..................wave speed for viscous fluid value of this artery.

        Ec..................young's modulus for viscoelastic walls value of
                            this artery.

        flow................flow value at this artery's distal node.

        pressure............pressure value at this artery's distal node.

        _pressure...........medium pressure value of this artery.

        R...................reflection coefficiente of this artery.

        viscousFactor.......viscous factor value of this artery.

        Y...................admittance value of this artery.

        Ye..................effective admittance of this artery.

        alpha...............womersley's non-dimensional number value for this artery.

        c...................wave speed value of this artery

        density.............density value of this artery.

        E...................young's modulus value of this artery.

        h...................wall thickness value of this artery.

        length..............length value of this artery.

        phi.................phase angle between pressure and wall displacement.

        r...................radius value of this artery.

        viscosity...........viscosity value of this artery.

        w...................angular frequency value of this artery.

        id..................identity number of this artery.

        hovered.............if this artery is being hovered or not

        leaf................if this artery is a leaf or not.

        selected............if this artery is selected or not.
************************************************************************************/

    int ArteryTreeSID;

    int ArteryTreeGID;

    Artery* father;

    Artery* left;

    Artery* right;

    Point p;

    Graphic2D* flow_wave;

    static double flow_max;

    static double flow_min;

    Graphic2D* pressure_wave;

    static double pressure_max;

    static double pressure_min;

    complex<double> beta;

    complex<double> cv;

    complex<double> Ec;

    complex<double> flow;

    complex<double> pressure;

    complex<double> _pressure;

    complex<double> R;

    complex<double> viscousFactor;

    complex<double> _Y;

    complex<double> Ye;

    double alpha;

    double c;

    double density;

    double E;

    double h;

    double length;

    double phi;

    double r;

    double viscosity;

    double w;

    int graphicSize;

    int id;

    bool hovered;

    bool leaf;

    bool selected;

/************************************************************************************
        PRIVATE METHODS
************************************************************************************/
/************************************************************************************
        Plots a (x,y) representation of a graphic in the file opened. X here has a
        SIZE of 1.0 for every artery. And every artery has n points distributed
        equally inside this space. Y is calculated finding the pressure in this
         x point.

        After ploting this artery the iteration will be sent to it's left sided son.

        @param file the file opened to save the plot.
        @param n number of points each artery represents
        @param x0 the x this artery starts on.
        @param rootAdmittance admittance in root's artery
************************************************************************************/
    void f_plotAux(FILE* file, int n, int x0, complex<double> rootAdmittance);

/************************************************************************************
        Auxiliar recursive method to set the following attributes using recursion.

        Medium Pressure                                 [_p] in  [cm/s]
        Pressure at the distal node                     [P]  in  []
        Flow at the distal node                         [Q]  in  []

        Because of the way these equations are solved this recursion is done from
        top to bottom. Check the full report for further information.

        @return if all steps were successfull.
************************************************************************************/
    bool setPhaseTwoAux(complex<double> q0);
};

#endif // ARTERY_H
