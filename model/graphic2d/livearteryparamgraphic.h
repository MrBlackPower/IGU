#ifndef LIVEARTERYPARAMGRAPHIC_H
#define LIVEARTERYPARAMGRAPHIC_H

#include "../graphic2d.h"
#include "../../model/artery.h"

class LiveArteryParamGraphic : public Graphic2D
{
public:
    LiveArteryParamGraphic(vector<Artery*> arteries, QString name = "STATIC GRAPHIC", SmartObject* parent = NULL);
    LiveArteryParamGraphic(vector<Artery*> arteries, ColorBar* cb, SmartObject* parent = NULL);

    /********************************************************
    *  GRAPHIC OBJECT VIRTUAL METHODS  *
    ********************************************************/
    vector<Field> getStartParameters();
    vector<Field> getContextVisualParameters();
    vector<QString> getRanges();
    bool updateStartParameter(QString field, QString value);
    bool setContextRange(QString field);


private:
    vector<Artery*> arteries;

    void update();
    /********************************************************
    *  GRAPHIC OBJECT VIRTUAL METHODS  *
    ********************************************************/
    bool initialize();
    double iteration();
    bool updateContextVisualParameter(QString field, QString value);
    QString getLiveParameterList();
    QString getLiveParameter(QString name);
    DataType getLiveParameterDataType(QString name);
    void mouse_press_event(int width, int height, Point eye, QMouseEvent *event, GraphicTools tool);
    void mouse_move_event(int width, int height, Point eye, QMouseEvent *event, GraphicTools tool);
};

#endif // LIVEARTERYPARAMGRAPHIC_H
