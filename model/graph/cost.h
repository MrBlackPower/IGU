#ifndef COST_H
#define COST_H

#include "nodegraph.h"
#include <vector>
#include <algorithm>

class Cost
{
public:
    Cost();
    Cost(NodeGraph<int,double>* node, int id, double cost);

    bool operator=(Cost b);

    bool operator<(Cost b);

    bool operator>(Cost b);

    bool operator<=(Cost b);

    bool operator>=(Cost b);

    bool operator==(Cost b);

    static bool compare(Cost a, Cost b);

    static bool compareDecrescent(Cost a, Cost b);

    static void orderCrescent(vector<Cost>* costs);

    static void orderDecrescent(vector<Cost>* costs);

    static vector<QString> printCost(vector<Cost>* costs);

    NodeGraph<int,double>* node;

    int id;

    double cost;
private:
};

#endif // COST_H
