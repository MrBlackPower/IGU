#ifndef KTGRAPHIC_H
#define KTGRAPHIC_H

#include "../graphic3d.h"
#include "../graphic2d.h"
#include "../colorbar/blue.h"
#include "staticgraphic.h"

struct KTGraphicStartParams{
    double a = 0.0;
    double b = 2 * PI;
    int n = 200;
    double cfl = 0.25;
    int example = 0;
    int coutour_type = 0;
    double dx;
};

class KTGraphic : public Graphic2D{
public:
    KTGraphic(QString name, int example = 0, SmartObject* parent = 0);
    KTGraphic(int example = 0, SmartObject* parent = 0);
    KTGraphic(int N, int example, SmartObject* parent = 0);

    /********************************************************
    *  GRAPHIC OBJECT VIRTUAL METHODS  *
    ********************************************************/
    vector<Field> getStartParameters();
    vector<Field> getContextVisualParameters();
    vector<QString> getRanges();
    bool updateStartParameter(QString field, QString value);
    bool setContextRange(QString field);

private:
    KTGraphicStartParams startParams;
    Error error;

    /********************************************************
    *  GRAPHIC OBJECT VIRTUAL METHODS  *
    ********************************************************/
    bool initialize();
    double iteration();
    bool updateContextVisualParameter(QString field, QString value);
    QString getLiveParameterList();
    QString getLiveParameter(QString name);
    DataType getLiveParameterDataType(QString name);

    vector<double> KT();
    virtual double f(double u);
    virtual double df(double u);
    vector<double> doUx();
    double minmod(double a, double b, double c);
    void mouse_press_event(int width, int height, Point eye, QMouseEvent *event, GraphicTools tool);
    void mouse_move_event(int width, int height, Point eye, QMouseEvent *event, GraphicTools tool);
};

#endif // KTGRAPHIC_H
