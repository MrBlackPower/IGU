#include "graphic2d.h"

/************************************************************************************
  Name:        lineargraphic.cpp
  Copyright:   Version 1.0
  Author:      Igor Pires dos Santos
  Last Update: 17/02/2017 (DD/MM/YYYY)
  Release:     --/--/---- (DD/MM/YYYY)
  Description: Class implementation of an linear graphic structure.
************************************************************************************/

Graphic2D::Graphic2D(QString name, SmartObject* parent) : GraphicObject(name,parent)
{
    this->setColorBar(Red());
    N = 0;
}

Graphic2D::Graphic2D(QString name, ColorBar cb, SmartObject* parent) : GraphicObject(name,parent)
{
    this->setColorBar(cb);
    N = 0;
}

Graphic2D::~Graphic2D(){

}

void Graphic2D::resizeTransform(){
    vector<Point> points;

    for(int i = 0; i < raw_data.size(); i++)
        points.push_back(*raw_data[i]);

    resetLimit();
    updateLimit(points);
}

void Graphic2D::draw(){
    for(int i = 0; i < raw_data.size(); i++){
        Point p = *raw_data[i];


        glPointSize(5.0);
        setMaterialColor(interpolateColor(getMax()));
        glBegin(GL_POINTS);
        glVertex3f(p.x, p.y, p.z);
        glEnd();
    }
}

vector<Point*> Graphic2D::getData(){
    return raw_data;
}

double Graphic2D::getY(double x){
    Point max = getLimitMax();
    Point min = getLimitMin();

    if(x > max.x || x < min.x)
        return -1;

    if(raw_data.size() < 2)
        return -1;

    for(int i = 0; i < N - 1; i++){
        Point* a = raw_data[i];
        Point* b = raw_data[i+1];

        if(MathHelper::isBetween(x,b->x,a->x)){
            double pct = (x - a->x)/(b->x - a->x);
            return ((pct * b->y) + ((1 - pct) * a->y));
        }
    }
}

bool Graphic2D::addData(Graphic2D* graphic){
    if(N != 0){
        vector<Point*> data = graphic->getData();
        data.erase(data.cbegin());
        Point p = *this->raw_data.operator [](N - 1);
        float startX = p.x;

        //CLONES AND ADDS EVERY POINT
        for(int i = 0; i < data.size(); i++){
            Point* p = data[i];
            p = p->clone();
            p->x = (p->x + startX);
            addData(p);
        }

        return true;
    } else {
        //CLONES AND ADDS EVERY POINT
        vector<Point*> data = graphic->getData();
        for(int i = 0; i < data.size(); i++){
            Point* p = data[i];
            p = p->clone();
            p->x = (p->x);
            addData(p);
        }

        return true;
    }
}

bool Graphic2D::addData(vector<Point*> data){
    //CLONES AND ADDS EVERY POINT
    for(int i = 0; i < data.size(); i++){
        Point* p = data[i];
        p = p->clone();
        addData(p);
    }

    return true;
}

bool Graphic2D::addData(vector<Point> data){
    //CLONES AND ADDS EVERY POINT
    for(int i = 0; i < data.size(); i++){
        Point* p = data[i].clone();
        addData(p);
    }

    return true;
}

bool Graphic2D::addData(Point p){
    raw_data.push_back(p.clone());
    updateLimit(&p);
    N++;

    setMin(p.y);
    setMax(p.y);

    return true;
}

bool Graphic2D::addData(Point* p){
    raw_data.push_back(p);
    updateLimit(p);
    N++;

    setMin(p->y);
    setMax(p->y);

    return true;
}

bool Graphic2D::clearData(){
    raw_data.clear();
    N = 0;
    resetLimit();

    range.max = -1;
    range.min = 1;

    return true;
}

bool Graphic2D::load(VTKFile* file){
    emit emit_log(SmartLogMessage(getSID(),"GRAPHIC 2D LOAD RUN",SmartLogType::LOG_LOAD));

    raw_data.clear();
    N = 0;

    vector<Point> points = file->getPoints();

    for(int i = 0; i < points.size(); i++)
        addData(points[i]);

    return true;
}

vector<Field> Graphic2D::getPointDataList(){
    vector<Field> list;
    return list;
}

vector<Field> Graphic2D::getCellDataList(){
    vector<Field> list;
    return list;
}

vector<QString> Graphic2D::getExtrapolations(){
    vector<QString> aux;

    return aux;
}

GraphicObject* Graphic2D::extrapolateContext(QString extrapolation){

}

vector<QString> Graphic2D::print(){
    vector<QString> ans;
    QString buffer;
    ans.push_back(buffer.asprintf("%d",N));
    for(int i = 0; i < raw_data.size(); i++){
        Point* p = raw_data[i];
        buffer = buffer.asprintf("%f %f",p->x,p->y);
        ans.push_back(buffer);
    }

    emit emit_log(SmartLogMessage(getSID(),"GRAPHIC 2D PRINTED"));
    return ans;
}

vector<QString> Graphic2D::data(){
    vector<QString> ans;
    QString buffer;

    buffer = buffer.asprintf("2-DIMENSIONAL GRAPHIC");
    ans.push_back(buffer);

    buffer = buffer.asprintf("(X,Y)");
    ans.push_back(buffer);

    for(int i = 0; i < raw_data.size(); i++){
        Point* p = raw_data[i];
        buffer = buffer.asprintf("(%.4f,%.4f)",p->x,p->y);
        ans.push_back(buffer);
    }

    emit emit_log(SmartLogMessage(getSID(),"GRAPHIC 2D DATA SAVED"));
    return ans;
}

vector<QString> Graphic2D::raw(){
    vector<QString> raw;
    QString line;

    line = line.asprintf("# vtk DataFile Version 3.0\n");
    raw.push_back(line);
    line = line.asprintf("PRESSURE PEAKING 2D GRAPHIC OUTPUT SmartID = %d\n",getID());
    raw.push_back(line);
    line = line.asprintf("ASCII\n");
    raw.push_back(line);
    line = line.asprintf("DATASET POLYDATA\n");
    raw.push_back(line);
    line = line.asprintf("POINTS  %d  double\n", N);
    raw.push_back(line);

    for(int i = 0; i < raw_data.size(); i++){
        Point* p = raw_data[i];
        line = line.asprintf("%f  %f  %f\n", p->x, p->y, p->z);
        raw.push_back(line);
    }

    emit emit_log(SmartLogMessage(getSID(),"GRAPHIC 2D RAW DATA SAVED"));
    return raw;
}

void Graphic2D::finish(){

}

int Graphic2D::getN(){
    return N;
}

bool Graphic2D::compatibility(VTKFile *file){
    emit emit_log(SmartLogMessage(getSID(),"GRAPHIC 2D COMPATIBILITY RUN",SmartLogType::LOG_COMPATIBILITY));

    //CHECKS IF ALL POINTS ARE IN THE SAME Z PLANE
    vector<Point> points = file->getPoints();
    double z = points[ZERO].z;
    for(int i = ONE; i < points.size(); i++){
        if(z != points[i].z)
            return false;
    }

    return true;
}

Point* Graphic2D::operator [](int x){
    if(x < 0 || x >= N)
        return NULL;
    Point* aux = raw_data[x];
    return aux;
}
