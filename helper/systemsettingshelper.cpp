#include "systemsettingshelper.h"

SystemSettingsHelper::SystemSettingsHelper()
{

}

QString SystemSettingsHelper::get(SystemParameter param){
    QString r;

    switch (param) {
    case PARALLEL:
        r = QString::asprintf("%s",((raw_data.parallel)?"true":"false"));
        break;
    case PARALLEL_PARADIGMA:
        r = MathHelper::paradigmaToString(raw_data.parallel_paradigma);
        break;
    case NUMBER_OF_THREADS:
        r = QString::asprintf("%d",raw_data.number_of_threads);
        break;
    case NUMBER_OF_THREAD_BLOCKS:
        r = QString::asprintf("%d",raw_data.number_of_thread_blocks);
        break;
    case NUMBER_OF_THREADS_IN_BLOCKS:
        r = QString::asprintf("%d",raw_data.number_of_threads_in_blocks);
        break;
    case ANGLES_PER_PIXEL:
        r = QString::asprintf("%d",raw_data.angles_per_pixel);
        break;
    case SCALAR_STEP_PER_PIXEL:
        r = QString::asprintf("%f",raw_data.scalar_step_per_pixel);
        break;
    case MOVE_PER_PIXEL:
        r = QString::asprintf("%d",raw_data.move_per_pixel);
        break;
    }

    return r;
}

vtk::DataType SystemSettingsHelper::type(SystemParameter param){
    vtk::DataType r = undefined;

    switch (param) {
    case PARALLEL:
        r = _boolean;
        break;
    case PARALLEL_PARADIGMA:
        r = stringlist;
        break;
    case NUMBER_OF_THREADS:
        r = integer;
        break;
    case NUMBER_OF_THREAD_BLOCKS:
        r = integer;
        break;
    case NUMBER_OF_THREADS_IN_BLOCKS:
        r = integer;
        break;
    case ANGLES_PER_PIXEL:
        r = doublep;
        break;
    case SCALAR_STEP_PER_PIXEL:
        r = doublep;
        break;
    case MOVE_PER_PIXEL:
        r = doublep;
        break;
    case Z_OFFSET:
        r = doublep;
        break;

    }


    return r;
}

bool SystemSettingsHelper::set(SystemParameter param, QString data){
    switch (param) {
    case PARALLEL:
        raw_data.parallel = ((data.toInt() == 1 || data == "true")? true: false);
        break;
    case PARALLEL_PARADIGMA:
        raw_data.parallel_paradigma = MathHelper::stringToParadigma(data);
        break;
    case NUMBER_OF_THREADS:
        raw_data.number_of_threads = data.toInt();
        break;
    case NUMBER_OF_THREAD_BLOCKS:
        raw_data.number_of_thread_blocks = data.toInt();
        break;
    case NUMBER_OF_THREADS_IN_BLOCKS:
        raw_data.number_of_threads_in_blocks = data.toInt();;
        break;
    case ANGLES_PER_PIXEL:
        raw_data.angles_per_pixel = data.toDouble();
        break;
    case SCALAR_STEP_PER_PIXEL:
        raw_data.scalar_step_per_pixel = data.toDouble();
        break;
    case MOVE_PER_PIXEL:
        raw_data.move_per_pixel = data.toDouble();
        break;
    case Z_OFFSET:
        raw_data.z_offset = data.toDouble();
        break;
    }
}

bool SystemSettingsHelper::load(QString config_filename){
    vector<QString> buffer;
    QFile f(config_filename);

    f.open(QFile::ReadOnly | QIODevice::Text);

    QString line;
    QTextStream in(&f);
    while (!in.atEnd()) {
        line = in.readLine();
        buffer.push_back(line);
    }

    f.close();
    load(buffer);
}

bool SystemSettingsHelper::load(vector<QString> config_file){
    bool read[SIZE_PARAMETER_LIST] = {false};
    SystemSettingsHelper aux;

    aux.operator=(this);

    for (int i = 0; i < config_file.size(); i++) {
        QString line = config_file[i];
        line.replace(QChar('='),QChar(' '));

        QTextStream s(&line);

        QString field;
        QString data;

        s >> field >> data;

        aux.set(stringToParameter(field),data);

        read[(parameterToInt(stringToParameter(field)))] = true;

    }

    for(int i = 0; i < SIZE_PARAMETER_LIST; i++)
        if(!read[i])
            return false;

    operator=(aux);

    return true;
}

QString SystemSettingsHelper::parameterToString(SystemParameter parameter){
    switch (parameter) {
    case PARALLEL:
        return "PARALLEL";
    case PARALLEL_PARADIGMA:
        return "PARALLEL_PARADIGMA";
    case NUMBER_OF_THREADS:
        return "NUMBER_OF_THREADS";
    case NUMBER_OF_THREAD_BLOCKS:
        return "NUMBER_OF_THREAD_BLOCKS";
    case NUMBER_OF_THREADS_IN_BLOCKS:
        return "NUMBER_OF_THREADS_IN_BLOCKS";
    case ANGLES_PER_PIXEL:
        return "ANGLES_PER_PIXEL";
    case SCALAR_STEP_PER_PIXEL:
        return "SCALAR_STEP_PER_PIXEL";
    case MOVE_PER_PIXEL:
        return "MOVE_PER_PIXEL";
    case Z_OFFSET:
        return "Z_OFFSET";
    }

    return "";
}

int SystemSettingsHelper::parameterToInt(SystemParameter parameter){
    switch (parameter) {
    case PARALLEL:
        return 0;
    case PARALLEL_PARADIGMA:
        return 1;
    case NUMBER_OF_THREADS:
        return 2;
    case NUMBER_OF_THREAD_BLOCKS:
        return 3;
    case NUMBER_OF_THREADS_IN_BLOCKS:
        return 4;
    case ANGLES_PER_PIXEL:
        return 5;
    case SCALAR_STEP_PER_PIXEL:
        return 6;
    case MOVE_PER_PIXEL:
        return 7;
    case Z_OFFSET:
        return 8;
    }

    return 0;
}

SystemSettingsHelper::SystemParameter SystemSettingsHelper::stringToParameter(QString string){
    if(string == "PARALLEL"){
        return PARALLEL;
    } else if (string == "PARALLEL_PARADIGMA") {
        return PARALLEL_PARADIGMA;
    }  else if (string == "NUMBER_OF_THREADS") {
        return NUMBER_OF_THREADS;
    } else if (string == "PARALLEL_PARADIGMA") {
        return PARALLEL_PARADIGMA;
    }  else if (string == "NUMBER_OF_THREADS") {
        return NUMBER_OF_THREADS;
    } else if (string == "NUMBER_OF_THREAD_BLOCKS") {
        return NUMBER_OF_THREAD_BLOCKS;
    } else if (string == "NUMBER_OF_THREADS_IN_BLOCKS") {
        return NUMBER_OF_THREADS_IN_BLOCKS;
    }  else if (string == "ANGLES_PER_PIXEL") {
        return ANGLES_PER_PIXEL;
    } else if (string == "SCALAR_STEP_PER_PIXEL") {
        return SCALAR_STEP_PER_PIXEL;
    }  else if (string == "MOVE_PER_PIXEL") {
        return MOVE_PER_PIXEL;
    }  else if (string == "Z_OFFSET") {
        return Z_OFFSET;
    }
}

vector<QString> SystemSettingsHelper::print(){
    vector<QString> raw;

    raw.push_back(QString::asprintf("PARALLEL=%s",get(PARALLEL).toStdString().data()));
    raw.push_back(QString::asprintf("PARALLEL_PARADIGMA=%s",get(PARALLEL_PARADIGMA).toStdString().data()));
    raw.push_back(QString::asprintf("NUMBER_OF_THREADS=%s",get(NUMBER_OF_THREADS).toStdString().data()));
    raw.push_back(QString::asprintf("NUMBER_OF_THREAD_BLOCKS=%s",get(NUMBER_OF_THREAD_BLOCKS).toStdString().data()));
    raw.push_back(QString::asprintf("NUMBER_OF_THREADS_IN_BLOCKS=%s",get(NUMBER_OF_THREADS_IN_BLOCKS).toStdString().data()));
    raw.push_back(QString::asprintf("ANGLES_PER_PIXEL=%s",get(ANGLES_PER_PIXEL).toStdString().data()));
    raw.push_back(QString::asprintf("SCALAR_STEP_PER_PIXEL=%s",get(SCALAR_STEP_PER_PIXEL).toStdString().data()));
    raw.push_back(QString::asprintf("MOVE_PER_PIXEL=%s",get(MOVE_PER_PIXEL).toStdString().data()));
    raw.push_back(QString::asprintf("Z_OFFSET=%s",get(Z_OFFSET).toStdString().data()));

    return raw;
}

void SystemSettingsHelper::print(vector<QString>* aux){
    aux->push_back(QString::asprintf("PARALLEL=%s",get(PARALLEL).toStdString().data()));
    aux->push_back(QString::asprintf("PARALLEL_PARADIGMA=%s",get(PARALLEL_PARADIGMA).toStdString().data()));
    aux->push_back(QString::asprintf("NUMBER_OF_THREADS=%s",get(NUMBER_OF_THREADS).toStdString().data()));
    aux->push_back(QString::asprintf("NUMBER_OF_THREAD_BLOCKS=%s",get(NUMBER_OF_THREAD_BLOCKS).toStdString().data()));
    aux->push_back(QString::asprintf("NUMBER_OF_THREADS_IN_BLOCKS=%s",get(NUMBER_OF_THREADS_IN_BLOCKS).toStdString().data()));
    aux->push_back(QString::asprintf("ANGLES_PER_PIXEL=%s",get(ANGLES_PER_PIXEL).toStdString().data()));
    aux->push_back(QString::asprintf("SCALAR_STEP_PER_PIXEL=%s",get(SCALAR_STEP_PER_PIXEL).toStdString().data()));
    aux->push_back(QString::asprintf("MOVE_PER_PIXEL=%s",get(MOVE_PER_PIXEL).toStdString().data()));
    aux->push_back(QString::asprintf("Z_OFFSET=%s",get(Z_OFFSET).toStdString().data()));
}

void SystemSettingsHelper::operator=(SystemSettingsHelper b){
    raw_data.parallel = b.raw_data.parallel;
    raw_data.parallel_paradigma = b.raw_data.parallel_paradigma;
    raw_data.number_of_threads = b.raw_data.number_of_threads;
    raw_data.number_of_thread_blocks = b.raw_data.number_of_thread_blocks;
    raw_data.number_of_threads_in_blocks = b.raw_data.number_of_threads_in_blocks;
    raw_data.angles_per_pixel = b.raw_data.angles_per_pixel;
    raw_data.scalar_step_per_pixel = b.raw_data.scalar_step_per_pixel;
    raw_data.move_per_pixel = b.raw_data.move_per_pixel;
    raw_data.z_offset = b.raw_data.z_offset;
}

void SystemSettingsHelper::operator=(SystemSettingsHelper* b){
    raw_data.parallel = b->raw_data.parallel;
    raw_data.parallel_paradigma = b->raw_data.parallel_paradigma;
    raw_data.number_of_threads = b->raw_data.number_of_threads;
    raw_data.number_of_thread_blocks = b->raw_data.number_of_thread_blocks;
    raw_data.number_of_threads_in_blocks = b->raw_data.number_of_threads_in_blocks;
    raw_data.angles_per_pixel = b->raw_data.angles_per_pixel;
    raw_data.scalar_step_per_pixel = b->raw_data.scalar_step_per_pixel;
    raw_data.move_per_pixel = b->raw_data.move_per_pixel;
    raw_data.z_offset = b->raw_data.z_offset;

}

