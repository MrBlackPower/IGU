#ifndef SMARTTHREADMANAGER_H
#define SMARTTHREADMANAGER_H

#include "../../model/graph/cost.h"
#include "../../model/graph/solution.h"
#include "../../model/smartobject.h"
#include <vector>
#include <QString>
#include <QThread>

enum JobOperation{
    ORDER_VECTOR_INT,
    ORDER_VECTOR_SOLUTION,
    ORDER_VECTOR_COST,
    GENERATE_NEIGHBOOR_SOLUTION
};

struct Job{
    int id = 0;
    JobOperation type = ORDER_VECTOR_INT;

    bool useScore = false;
    bool crescent = true;
    int phase = 0;
    int i_min = 0;
    int i_max = 0;
    vector<int>* vector_int;
    vector<Cost>* vector_cost;
    vector<Solution>* vector_solution;

//    WeightedGraphSolutionMovement movement;
//    QSharedPointer<Solution> neighboor_solution;
//    QSharedPointer<WeightedGraph> neighboor_WeightedGraph;
};

class SmartThreadManager : public SmartObject
{
Q_OBJECT
public:
    SmartThreadManager(QString name, SmartObject* parent = NULL);

    int getSTID();

    vector<QString> print();
    vector<QString> data();
    vector<QString> raw();

public slots:
    void process();

    void addToBuffer(vector<int>* vector_int, int thread_id, int job_id,int phase, int i_min, int i_max, bool crescent);

    void addToBuffer(vector<Cost>* vector_cost, int thread_id, int job_id,int phase, int i_min, int i_max, bool crescent);

    void addToBuffer(vector<Solution>* vector_solution, int thread_id, int job_id,int phase, int i_min, int i_max,bool useScore, bool crescent);

 //   void addToBuffer(QSharedPointer<Solution> neighboor_solution, QSharedPointer<WeightedGraph> neighboor_WeightedGraph, int thread_id, int job_id, WeightedGraphSolutionMovement movement);

    void finish();

signals:
    void finished_step(int thread_id,int job_id);

    void finished();

    void error(QString err);

private:
    vector<Job> crashed_jobs;

    vector<Job> jobs;

    static unsigned int CURR_ID;

    const unsigned int STID;

    bool toFinish;
};

#endif // SMARTTHREADMANAGER_H
