#include "inputscalardialog.h"
#include "ui_inputscalardialog.h"

InputScalarDialog::InputScalarDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::InputScalarDialog)
{
    ui->setupUi(this);
    name = "";
    value = 1.0;
    set = false;
}

double InputScalarDialog::getScalar(){
    return value;
}

bool InputScalarDialog::Set(){
    return set;
}


void InputScalarDialog::setScalar(QString name){
    this->name = name;
    ui->lblScalar->setText(name);
}

InputScalarDialog::~InputScalarDialog()
{
    delete ui;
}

void InputScalarDialog::on_spnScalar_editingFinished()
{
    value = ui->spnScalar->value();
    set = true;
}

void InputScalarDialog::on_buttonBox_accepted()
{
    value = ui->spnScalar->value();
    set = true;

    emit pushScalar(name, value);
    emit scalarSet();
}
