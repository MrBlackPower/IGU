﻿#ifndef SOLUTION_H
#define SOLUTION_H

#include <vector>
#include <algorithm>
#include <QString>

using namespace std;

class Solution
{
public:
    Solution();
    Solution(Solution* representation);
    Solution(vector<int> representation);

    vector<QString> toString();

    vector<int> getVector();

    void getVector(vector<int>* v);

    void operator+=(int b);

    void operator=(Solution b);

    bool operator==(Solution b);

    bool operator>(Solution b);

    bool operator>=(Solution b);

    bool operator<(Solution b);

    bool operator<=(Solution b);

    static vector<QString> toString(vector<Solution> solutions);

    static bool compare(Solution a, Solution b);

    static bool compareDecrescent(Solution a, Solution b);

    static bool compareScore(Solution a, Solution b);

    static bool compareScoreDecrescent(Solution a, Solution b);

    static void orderCrescent(vector<Solution>* list);

    static void orderDecrescent(vector<Solution>* list);

    static void orderScoreCrescent(vector<Solution>* list);

    static void orderScoreDecrescent(vector<Solution>* list);

    bool valid;//If  it is a valid solution to the current problem

    double score;//Current score

    double cost;//Current cost

    QString last_movement;//Last movement associated with solution

    int last_id = 0;//Last movement node_id associated with solution

    int last_id_two = 0;//Last movement node_id associated with solution

private:
    const int SOLUTION_ID;
    vector<int> vec;//Solution representation in int
    static int CURR_SOLUTION;

};

#endif // SOLUTION_H
