#include "celldatalistwidget.h"

CellDataListWidget::CellDataListWidget(QWidget *parent) : QListWidget(parent)
{

}

bool CellDataListWidget::addDataList(VTKFile file){
    clear();

    vector<QString> dataList = file.getCellDataList();
    vector<DataType> dataType = file.getCellDataType();

    for(int i = 0; i < dataType.size(); i++){
        DataType t = dataType[i];

        switch (t) {
        case integer:
            addItem(dataList[i]);
            break;
        case floatp:
            addItem(dataList[i]);
            break;
        case doublep:
            addItem(dataList[i]);
            break;
        case stringp:

            break;
        case normals:

            break;
        case vectors:

            break;
        case tensor:

            break;
        case undefined:

            break;
        default:
            break;
        }

    }
}
