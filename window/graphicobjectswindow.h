#ifndef GRAPHICOBJECTSWINDOW_H
#define GRAPHICOBJECTSWINDOW_H

#include <QDialog>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include "../model/graphicobject.h"
#include "../canvas/oglcanvas.h"
#include "graphicobjectdetailswindow.h"

namespace Ui {
class GraphicObjectsWindow;
}

class GraphicObjectsWindow : public QDialog
{
    Q_OBJECT

public:
    explicit GraphicObjectsWindow(QWidget *parent = 0);
    ~GraphicObjectsWindow();

signals:

    void setSideCanvas(GraphicObject* g);

    void setMainCanvas(GraphicObject* g);

private slots:
    void on_btnSide_clicked();

    void on_btnMain_clicked();

    void on_btnSet_clicked();

    void on_btnOK_clicked();

    void on_treeWidget_itemClicked(QTreeWidgetItem *item, int column);

    void on_btnDeatils_clicked();

    void on_btnUpdate_clicked();

private:
    Ui::GraphicObjectsWindow *ui;

    GraphicObjectDetailsWindow* details;

    GraphicObject* selection;

    bool selected;
};

#endif // GRAPHICOBJECTSWINDOW_H
