#ifndef STATICGRAPHIC3D_H
#define STATICGRAPHIC3D_H

#include "../graphic3d.h"
#include "lines3d.h"

class StaticGraphic3D : public Graphic3D
{
public:
    StaticGraphic3D(int m, int n, SmartObject* parent = NULL);
    StaticGraphic3D(QString name, int m, int n, SmartObject* parent = NULL);
    StaticGraphic3D(QString name,int m = 0, SmartObject* parent = NULL);

    Lines3D* contour(double f);

    /********************************************************
    *  GRAPHIC OBJECT VIRTUAL METHODS  *
    ********************************************************/
    vector<Field> getStartParameters();
    vector<Field> getContextVisualParameters();
    vector<QString> getRanges();
    bool updateStartParameter(QString field, QString value);
    bool setContextRange(QString field);

private:
    /********************************************************
    *  GRAPHIC OBJECT VIRTUAL METHODS  *
    ********************************************************/
    bool initialize();
    double iteration();
    bool updateContextVisualParameter(QString field, QString value);
    QString getLiveParameterList();
    QString getLiveParameter(QString name);
    DataType getLiveParameterDataType(QString name);
    void mouse_press_event(int width, int height, Point eye, QMouseEvent *event, GraphicTools tool);
    void mouse_move_event(int width, int height, Point eye, QMouseEvent *event, GraphicTools tool);
};

#endif // STATICGRAPHIC3D_H
