#ifndef CONSOLEWINDOW_H
#define CONSOLEWINDOW_H

#include <QDialog>
#include <QString>
#include <iostream>
#include "helper/fileHelper.h"

namespace Ui {
class ConsoleWindow;
}

namespace window {
    class ConsoleWindow : public QDialog
    {
        Q_OBJECT

    public:
        explicit ConsoleWindow(QWidget *parent = 0);
        ~ConsoleWindow();

    signals:
        void emitCommand(string msg);

    public slots:
        void appendText(QString text);

    private slots:
        void on_btnRun_clicked();

    private:
        Ui::ConsoleWindow *ui;
        QString text;
    };
}

#endif // CONSOLEWINDOW_H
