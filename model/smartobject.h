#ifndef SMARTOBJECT_H
#define SMARTOBJECT_H

#include <QObject>
#include <QString>
#include <vector>
#include <iostream>
#include "helper/fileHelper.h"
#include "smartlogmessage.h"
#include "smartclassstack.h"

using namespace std;

class SmartObject : public QObject
{
Q_OBJECT
public:
    SmartObject(QString name, SmartObject* parent = NULL);
    ~SmartObject();

    bool operator ==(SmartObject* b);

    int getID();
    SmartObject* getParent();
    vector<SmartObject*> getChildren();
    void registerChild(SmartObject* child);
    void removeChild(SmartObject* child);

    bool log_as_also(QString filename);

    bool savePrint(QString fileName);
    bool saveData(QString fileName);
    bool saveRaw(QString fileName);

    virtual vector<QString> print() = 0;
    virtual vector<QString> data() = 0;
    virtual vector<QString> raw() = 0;

    static vector<SmartObject*> getSmarts();
    static SmartObject* getSmart(int sid);

    static void setDefaultLogPool(SmartObject* default_log);
    static void purgeDefaultLogPool();;

    QString getName();
    int getSID();

    SmartObject* previous_smart;
    SmartObject* next_smart;

signals:
    void emit_log(SmartLogMessage msg);

    void emit_log_to_file(SmartLogMessage msg, QString filename);

private slots:
     void pass_log_to_file(SmartLogMessage msg);

protected:

    SmartClassStack* classStack;


private:

    //SMART ID
    static unsigned int CURR_SID;
    static SmartObject* first_smart;
    static SmartObject* last_smart;
    static SmartObject* default_log;

    const unsigned int SID;

    QString name;

    //GENERATION TREE
    SmartObject* parent;
    vector<SmartObject*> children;

    vector<QString> logs;
};


#endif // SMARTOBJECT_H
