#ifndef SMARTCLASSSTACK_H
#define SMARTCLASSSTACK_H

#include <QString>
#include <vector>

using namespace std;

struct SmartClass{
    int level = 0;
    QString className = "";
    int classID = 0;
};

class SmartClassStack
{
public:

    SmartClassStack(int SID);
    ~SmartClassStack();

    void addClass(QString className, int classID);

    int size();

    vector<QString> getStartHeader();
    vector<QString> getFinishHeader();

private:
    QString tab(int n);

    int N = 0;
    vector<SmartClass> stack;
};

#endif // SMATCLASSSTACK_H
