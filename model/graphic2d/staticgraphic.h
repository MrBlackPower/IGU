#ifndef STATICGRAPHIC_H
#define STATICGRAPHIC_H

#include "../graphic2d.h"

class StaticGraphic : public Graphic2D
{
public:
    StaticGraphic(QString name = "STATIC GRAPHIC", SmartObject* parent = NULL);
    StaticGraphic(ColorBar cb, SmartObject* parent = NULL);

    /********************************************************
    *  GRAPHIC OBJECT VIRTUAL METHODS  *
    ********************************************************/
    vector<Field> getStartParameters();
    vector<Field> getContextVisualParameters();
    vector<QString> getRanges();
    bool updateStartParameter(QString field, QString value);
    bool setContextRange(QString field);
private:
    /********************************************************
    *  GRAPHIC OBJECT VIRTUAL METHODS  *
    ********************************************************/
    bool initialize();
    double iteration();
    bool updateContextVisualParameter(QString field, QString value);
    QString getLiveParameterList();
    QString getLiveParameter(QString name);
    DataType getLiveParameterDataType(QString name);
    void mouse_press_event(int width, int height, Point eye, QMouseEvent *event, GraphicTools tool);
    void mouse_move_event(int width, int height, Point eye, QMouseEvent *event, GraphicTools tool);
};

#endif // STATICGRAPHIC_H
