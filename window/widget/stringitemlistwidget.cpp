#include "stringitemlistwidget.h"

StringItemListWidget::StringItemListWidget(QWidget *parent) : QListWidget(parent)
{

}

vector<QString> StringItemListWidget::getItems(){
    return list;
}

void StringItemListWidget::addItem(QString conv){
    QListWidget::addItem(conv);
    list.push_back(conv);
}

void StringItemListWidget::addItem(vector<QString> conv){
    for(int i = 0; i < conv.size(); i++){
        QString aux = conv[i];
        QListWidget::addItem(aux);
        list.push_back(aux);
    }
}

void StringItemListWidget::flush(){
    QListWidget::clear();
    list.clear();
}

void StringItemListWidget::remove(int row){
    QListWidget::takeItem(row);
    list.erase(list.begin() + row);
}
