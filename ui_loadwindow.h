/********************************************************************************
** Form generated from reading UI file 'loadwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOADWINDOW_H
#define UI_LOADWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_LoadWindow
{
public:
    QDialogButtonBox *buttonBox;
    QGroupBox *groupBox;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout_3;
    QCheckBox *chk_YoungModulus;
    QCheckBox *chk_Viscosity;
    QCheckBox *chk_Density;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout_4;
    QDoubleSpinBox *spin_E;
    QDoubleSpinBox *spin_visc;
    QDoubleSpinBox *spin_dens;
    QGroupBox *groupBox_2;
    QWidget *layoutWidget1;
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QWidget *layoutWidget2;
    QVBoxLayout *verticalLayout_2;
    QDoubleSpinBox *spin_points;
    QDoubleSpinBox *spin_radius;
    QDoubleSpinBox *spin_youngmodulus;
    QDoubleSpinBox *spin_viscosity;
    QDoubleSpinBox *spin_density;
    QLabel *label_7;
    QLabel *label_6;

    void setupUi(QDialog *LoadWindow)
    {
        if (LoadWindow->objectName().isEmpty())
            LoadWindow->setObjectName(QStringLiteral("LoadWindow"));
        LoadWindow->resize(372, 439);
        buttonBox = new QDialogButtonBox(LoadWindow);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setGeometry(QRect(10, 400, 301, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        groupBox = new QGroupBox(LoadWindow);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(10, 250, 351, 131));
        layoutWidget = new QWidget(groupBox);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(10, 30, 133, 91));
        verticalLayout_3 = new QVBoxLayout(layoutWidget);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        chk_YoungModulus = new QCheckBox(layoutWidget);
        chk_YoungModulus->setObjectName(QStringLiteral("chk_YoungModulus"));
        chk_YoungModulus->setChecked(true);

        verticalLayout_3->addWidget(chk_YoungModulus);

        chk_Viscosity = new QCheckBox(layoutWidget);
        chk_Viscosity->setObjectName(QStringLiteral("chk_Viscosity"));
        chk_Viscosity->setChecked(true);

        verticalLayout_3->addWidget(chk_Viscosity);

        chk_Density = new QCheckBox(layoutWidget);
        chk_Density->setObjectName(QStringLiteral("chk_Density"));
        chk_Density->setChecked(true);

        verticalLayout_3->addWidget(chk_Density);

        verticalLayoutWidget = new QWidget(groupBox);
        verticalLayoutWidget->setObjectName(QStringLiteral("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(190, 30, 147, 92));
        verticalLayout_4 = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        verticalLayout_4->setContentsMargins(0, 0, 0, 0);
        spin_E = new QDoubleSpinBox(verticalLayoutWidget);
        spin_E->setObjectName(QStringLiteral("spin_E"));
        spin_E->setMaximum(1e+10);

        verticalLayout_4->addWidget(spin_E);

        spin_visc = new QDoubleSpinBox(verticalLayoutWidget);
        spin_visc->setObjectName(QStringLiteral("spin_visc"));
        spin_visc->setMaximum(1e+10);

        verticalLayout_4->addWidget(spin_visc);

        spin_dens = new QDoubleSpinBox(verticalLayoutWidget);
        spin_dens->setObjectName(QStringLiteral("spin_dens"));
        spin_dens->setMaximum(1e+10);

        verticalLayout_4->addWidget(spin_dens);

        groupBox_2 = new QGroupBox(LoadWindow);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        groupBox_2->setGeometry(QRect(10, 0, 351, 251));
        layoutWidget1 = new QWidget(groupBox_2);
        layoutWidget1->setObjectName(QStringLiteral("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(210, 30, 131, 171));
        verticalLayout = new QVBoxLayout(layoutWidget1);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(layoutWidget1);
        label->setObjectName(QStringLiteral("label"));

        verticalLayout->addWidget(label);

        label_2 = new QLabel(layoutWidget1);
        label_2->setObjectName(QStringLiteral("label_2"));

        verticalLayout->addWidget(label_2);

        label_3 = new QLabel(layoutWidget1);
        label_3->setObjectName(QStringLiteral("label_3"));

        verticalLayout->addWidget(label_3);

        label_4 = new QLabel(layoutWidget1);
        label_4->setObjectName(QStringLiteral("label_4"));

        verticalLayout->addWidget(label_4);

        label_5 = new QLabel(layoutWidget1);
        label_5->setObjectName(QStringLiteral("label_5"));

        verticalLayout->addWidget(label_5);

        layoutWidget2 = new QWidget(groupBox_2);
        layoutWidget2->setObjectName(QStringLiteral("layoutWidget2"));
        layoutWidget2->setGeometry(QRect(11, 32, 155, 171));
        verticalLayout_2 = new QVBoxLayout(layoutWidget2);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        spin_points = new QDoubleSpinBox(layoutWidget2);
        spin_points->setObjectName(QStringLiteral("spin_points"));
        spin_points->setDecimals(3);
        spin_points->setMaximum(1e+10);
        spin_points->setValue(1);

        verticalLayout_2->addWidget(spin_points);

        spin_radius = new QDoubleSpinBox(layoutWidget2);
        spin_radius->setObjectName(QStringLiteral("spin_radius"));
        spin_radius->setDecimals(3);
        spin_radius->setMaximum(1e+10);
        spin_radius->setValue(1);

        verticalLayout_2->addWidget(spin_radius);

        spin_youngmodulus = new QDoubleSpinBox(layoutWidget2);
        spin_youngmodulus->setObjectName(QStringLiteral("spin_youngmodulus"));
        spin_youngmodulus->setDecimals(3);
        spin_youngmodulus->setMaximum(1e+10);
        spin_youngmodulus->setValue(1);

        verticalLayout_2->addWidget(spin_youngmodulus);

        spin_viscosity = new QDoubleSpinBox(layoutWidget2);
        spin_viscosity->setObjectName(QStringLiteral("spin_viscosity"));
        spin_viscosity->setDecimals(3);
        spin_viscosity->setMaximum(1e+10);
        spin_viscosity->setValue(1);

        verticalLayout_2->addWidget(spin_viscosity);

        spin_density = new QDoubleSpinBox(layoutWidget2);
        spin_density->setObjectName(QStringLiteral("spin_density"));
        spin_density->setDecimals(3);
        spin_density->setMaximum(1e+10);
        spin_density->setValue(1);

        verticalLayout_2->addWidget(spin_density);

        label_7 = new QLabel(groupBox_2);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(220, 230, 67, 17));
        label_6 = new QLabel(groupBox_2);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(220, 210, 101, 17));

        retranslateUi(LoadWindow);
        QObject::connect(buttonBox, SIGNAL(accepted()), LoadWindow, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), LoadWindow, SLOT(reject()));

        QMetaObject::connectSlotsByName(LoadWindow);
    } // setupUi

    void retranslateUi(QDialog *LoadWindow)
    {
        LoadWindow->setWindowTitle(QApplication::translate("LoadWindow", "Dialog", Q_NULLPTR));
        groupBox->setTitle(QApplication::translate("LoadWindow", "Optional Parameters", Q_NULLPTR));
        chk_YoungModulus->setText(QApplication::translate("LoadWindow", "Young Modulus", Q_NULLPTR));
        chk_Viscosity->setText(QApplication::translate("LoadWindow", "Viscosity", Q_NULLPTR));
        chk_Density->setText(QApplication::translate("LoadWindow", "Density", Q_NULLPTR));
        groupBox_2->setTitle(QApplication::translate("LoadWindow", "Conversions", Q_NULLPTR));
        label->setText(QApplication::translate("LoadWindow", "Points Coordinates", Q_NULLPTR));
        label_2->setText(QApplication::translate("LoadWindow", "Radius", Q_NULLPTR));
        label_3->setText(QApplication::translate("LoadWindow", "Young Modulus", Q_NULLPTR));
        label_4->setText(QApplication::translate("LoadWindow", "Viscosity", Q_NULLPTR));
        label_5->setText(QApplication::translate("LoadWindow", "Density", Q_NULLPTR));
        label_7->setText(QApplication::translate("LoadWindow", "0.01", Q_NULLPTR));
        label_6->setText(QApplication::translate("LoadWindow", "i.e. [cm] to [m]", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class LoadWindow: public Ui_LoadWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOADWINDOW_H
