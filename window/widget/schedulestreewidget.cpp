#include "schedulestreewidget.h"

SchedulesTreeWidget::SchedulesTreeWidget(QWidget *parent) : QTreeWidget (parent)
{

}

void SchedulesTreeWidget::populate(vector<Schedule>* schedules){
    clear();

    for (int i = 0; i < schedules->size(); i++) {
        Schedule s = schedules->operator[](i);

        QString filename = s.getFileName();
        QString log_filename = s.getLogFileName();
        int times = s.getTimes();
        vector<QString> start = s.getStartParams();
        vector<QString> visual = s.getVisualParams();

        QTreeWidgetItem* root = new QTreeWidgetItem(this);
        root->setText(0,QString::asprintf("%d",s.id()));
        root->setText(1,QString::asprintf("%d",times));
        root->setText(2,filename);
        root->setText(3,log_filename);

        for (int j = 0; j < start.size(); j++) {
            QTreeWidgetItem* leaf = new QTreeWidgetItem(root);

            QTextStream s(&start[j]);
            QString name, value;
            s >> name >> value;

            leaf->setText(0,name);
            leaf->setText(1,value);

            root->addChild(leaf);
        }

        for (int j = 0; j < visual.size(); j++) {
            QTreeWidgetItem* leaf = new QTreeWidgetItem(root);

            QTextStream s(&visual[j]);
            QString name, value;
            s >> name >> value;

            leaf->setText(0,name);
            leaf->setText(1,value);

            root->addChild(leaf);
        }
    }
}
