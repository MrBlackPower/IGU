#include "cost.h"


Cost::Cost(){
    node = NULL;
    id = 0;
    cost = 0;
}

Cost::Cost(NodeGraph<int,double>* node, int id, double cost)
{
    this->node = node;
    this->id = id;
    this->cost = cost;
}

bool Cost::operator=(Cost b){
    this->cost = b.cost;
    this->id = b.id;
    this->node = b.node;
}

bool Cost::operator<(Cost b){
    return cost < b.cost;
}

bool Cost::operator>(Cost b){
    return cost > b.cost;
}

bool Cost::operator<=(Cost b){
    return cost <= b.cost;
}

bool Cost::operator>=(Cost b){
    return cost >= b.cost;
}

bool Cost::operator==(Cost b){
    return (cost == b.cost);
}

bool Cost::compare(Cost a, Cost b){
    return a < b;
}

bool Cost::compareDecrescent(Cost a, Cost b){
    return a > b;
}

void Cost::orderCrescent(vector<Cost>* costs){
    sort(costs->begin(),costs->end());
}

void Cost::orderDecrescent(vector<Cost>* costs){
    sort(costs->begin(),costs->end(),compareDecrescent);
}

vector<QString> Cost::printCost(vector<Cost>* costs){
    vector<QString> buffer;

    for(int i = 0; i < costs->size(); i++){
        Cost c = costs->operator[](i);
        QString line = QString::asprintf("COST: %f    : NODE:    %d",c.cost, c.node->getId());
        buffer.push_back(line);
    }

    return buffer;
}
