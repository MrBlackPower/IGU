/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[26];
    char stringdata0[477];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 7), // "repaint"
QT_MOC_LITERAL(2, 19, 0), // ""
QT_MOC_LITERAL(3, 20, 11), // "sendMessage"
QT_MOC_LITERAL(4, 32, 6), // "string"
QT_MOC_LITERAL(5, 39, 3), // "msg"
QT_MOC_LITERAL(6, 43, 15), // "sendConsoleText"
QT_MOC_LITERAL(7, 59, 3), // "txt"
QT_MOC_LITERAL(8, 63, 13), // "setArteryTree"
QT_MOC_LITERAL(9, 77, 22), // "LoadWindow::Parameters"
QT_MOC_LITERAL(10, 100, 6), // "params"
QT_MOC_LITERAL(11, 107, 26), // "on_act_f_Ye_Test_triggered"
QT_MOC_LITERAL(12, 134, 21), // "on_act_load_triggered"
QT_MOC_LITERAL(13, 156, 25), // "on_act_pressure_triggered"
QT_MOC_LITERAL(14, 182, 22), // "on_act_print_triggered"
QT_MOC_LITERAL(15, 205, 21), // "on_act_save_triggered"
QT_MOC_LITERAL(16, 227, 23), // "on_act_saveas_triggered"
QT_MOC_LITERAL(17, 251, 26), // "on_act_save_plot_triggered"
QT_MOC_LITERAL(18, 278, 23), // "on_btn_iterate_released"
QT_MOC_LITERAL(19, 302, 32), // "on_spn_frequency_editingFinished"
QT_MOC_LITERAL(20, 335, 27), // "on_spn_phi0_editingFinished"
QT_MOC_LITERAL(21, 363, 40), // "on_spn_viscousMultiplier_edit..."
QT_MOC_LITERAL(22, 404, 27), // "on_chk_viscous_stateChanged"
QT_MOC_LITERAL(23, 432, 6), // "status"
QT_MOC_LITERAL(24, 439, 29), // "on_act_open_console_triggered"
QT_MOC_LITERAL(25, 469, 7) // "checked"

    },
    "MainWindow\0repaint\0\0sendMessage\0string\0"
    "msg\0sendConsoleText\0txt\0setArteryTree\0"
    "LoadWindow::Parameters\0params\0"
    "on_act_f_Ye_Test_triggered\0"
    "on_act_load_triggered\0on_act_pressure_triggered\0"
    "on_act_print_triggered\0on_act_save_triggered\0"
    "on_act_saveas_triggered\0"
    "on_act_save_plot_triggered\0"
    "on_btn_iterate_released\0"
    "on_spn_frequency_editingFinished\0"
    "on_spn_phi0_editingFinished\0"
    "on_spn_viscousMultiplier_editingFinished\0"
    "on_chk_viscous_stateChanged\0status\0"
    "on_act_open_console_triggered\0checked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      17,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   99,    2, 0x06 /* Public */,
       3,    1,  100,    2, 0x06 /* Public */,
       6,    1,  103,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       8,    1,  106,    2, 0x08 /* Private */,
      11,    0,  109,    2, 0x08 /* Private */,
      12,    0,  110,    2, 0x08 /* Private */,
      13,    0,  111,    2, 0x08 /* Private */,
      14,    0,  112,    2, 0x08 /* Private */,
      15,    0,  113,    2, 0x08 /* Private */,
      16,    0,  114,    2, 0x08 /* Private */,
      17,    0,  115,    2, 0x08 /* Private */,
      18,    0,  116,    2, 0x08 /* Private */,
      19,    0,  117,    2, 0x08 /* Private */,
      20,    0,  118,    2, 0x08 /* Private */,
      21,    0,  119,    2, 0x08 /* Private */,
      22,    1,  120,    2, 0x08 /* Private */,
      24,    1,  123,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4,    5,
    QMetaType::Void, QMetaType::QString,    7,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 9,   10,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   23,
    QMetaType::Void, QMetaType::Bool,   25,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->repaint(); break;
        case 1: _t->sendMessage((*reinterpret_cast< string(*)>(_a[1]))); break;
        case 2: _t->sendConsoleText((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: _t->setArteryTree((*reinterpret_cast< LoadWindow::Parameters(*)>(_a[1]))); break;
        case 4: _t->on_act_f_Ye_Test_triggered(); break;
        case 5: _t->on_act_load_triggered(); break;
        case 6: _t->on_act_pressure_triggered(); break;
        case 7: _t->on_act_print_triggered(); break;
        case 8: _t->on_act_save_triggered(); break;
        case 9: _t->on_act_saveas_triggered(); break;
        case 10: _t->on_act_save_plot_triggered(); break;
        case 11: _t->on_btn_iterate_released(); break;
        case 12: _t->on_spn_frequency_editingFinished(); break;
        case 13: _t->on_spn_phi0_editingFinished(); break;
        case 14: _t->on_spn_viscousMultiplier_editingFinished(); break;
        case 15: _t->on_chk_viscous_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 16: _t->on_act_open_console_triggered((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (MainWindow::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::repaint)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)(string );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::sendMessage)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::sendConsoleText)) {
                *result = 2;
                return;
            }
        }
    }
}

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 17)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 17;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 17)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 17;
    }
    return _id;
}

// SIGNAL 0
void MainWindow::repaint()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void MainWindow::sendMessage(string _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void MainWindow::sendConsoleText(QString _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_END_MOC_NAMESPACE
