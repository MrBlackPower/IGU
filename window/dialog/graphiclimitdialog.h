#ifndef GRAPHICLIMITDIALOG_H
#define GRAPHICLIMITDIALOG_H

#include <QDialog>
#include "graphiccomponentdialog.h"

namespace Ui {
class GraphicLimitDialog;
}

using namespace GraphicSpace;

class GraphicLimitDialog : public QDialog
{
    Q_OBJECT

public:
    explicit GraphicLimitDialog(QWidget *parent = nullptr);
    explicit GraphicLimitDialog(Point max, Point min,QWidget *parent = nullptr);
    ~GraphicLimitDialog();

    void getLimit(Point* max, Point* min);

    bool Set();

    void updateInterface();

signals:
    void pushLimit(Point max, Point min);

    void limitSet();

    void cancelled();

private slots:

    void on_spn_max_x_valueChanged(double arg1);

    void on_spn_max_y_valueChanged(double arg1);

    void on_spn_max_z_valueChanged(double arg1);

    void on_spn_min_x_valueChanged(double arg1);

    void on_spn_min_y_valueChanged(double arg1);

    void on_spn_min_z_valueChanged(double arg1);

    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

private:
    Ui::GraphicLimitDialog *ui;

    Point max;
    Point min;

    bool set;
};

#endif // GRAPHICLIMITDIALOG_H
