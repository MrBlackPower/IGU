#include "colorbar.h"

ColorBar::ColorBar(QString name, ColorBarType type)
{
    this->name = name;
    this->type = type;
    N = 0;
    max = 0;
    min = 0;
}

ColorBar::ColorBar(QColor c, QString name, ColorBarType type)
{
    this->name = name;
    this->type = type;
    N = 0;
    max = 0;
    min = 0;

    addColor(c);
}

QColor ColorBar::interpolate(double val){
    double range = 1.0/(N - 1);
    if(min != max){
        if(val < min)
            return colors[ONE];

        if(val > max)
            return colors.back();

        range = (max - min)/(N - 1);
    }

    if(N == ZERO)
        return QColor("black");

    if(N == ONE)
        return colors[ZERO];

    double x = (max == min) ? val : val - min;
    int iMax = (int)(x / range) + 1;

    if(iMax >= N)
        iMax = N - 1;

    int iMin = (int)(x /range);

    if(iMin < ZERO)
        iMin = ZERO;

    double pct = ((x - ((int)(x / range) * range))/range);

    //FIRST COLOR
    QColor c1 = colors[iMax];
    int r1, g1, b1;
    c1.getRgb(&r1,&g1,&b1);

    if(type == ColorBarType::Discrete)
        return c1;

    //SECOND COLOR
    QColor c2 = colors[iMin];
    int r2, g2, b2;
    c2.getRgb(&r2,&g2,&b2);

    int r = (int)((double)r1 * pct) + ((double)r2 * (1-pct));
    int g = (int)((double)g1 * pct) + ((double)g2 * (1-pct));
    int b = (int)((double)b1 * pct) + ((double)b2 * (1-pct));

    QColor c3 = c1.fromRgb(r,g,b);
    return c3;
}

void ColorBar::addColor(QColor c){
    colors.emplace_back(c);
    N ++;
}

QColor ColorBar::get_back(){
    return colors.back();
}

void ColorBar::pop_back(){
    if(N > 0){
        colors.pop_back();
    }
}

double ColorBar::getMax(){
    return max;
}

void ColorBar::setMax(double max){
    if(min > max)
        min = max;

    this->max = max;
}

double ColorBar::getMin(){
    return min;
}

void ColorBar::setMin(double min){
    if(max < min)
        max = min;

    this->min = min;
}

QString ColorBar::getName(){
    return name;
}

ColorBarType ColorBar::getType(){
    return type;
}

vector<QColor> ColorBar::getColors(){
    return colors;
}

void ColorBar::operator =(ColorBar c){
    colors = c.getColors();
    N = colors.size();
    type = c.getType();
    name = c.getName();
    max = c.getMax();
    min = c.getMin();
}

void ColorBar::operator =(ColorBar* c){
    colors = c->getColors();
    N = colors.size();
    type = c->getType();
    name = c->getName();
    max = c->getMax();
    min = c->getMin();
}
