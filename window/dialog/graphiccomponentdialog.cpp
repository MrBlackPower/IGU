#include "graphiccomponentdialog.h"
#include "ui_graphiccomponentdialog.h"

GraphicComponentDialog::GraphicComponentDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::GraphicComponentDialog)
{
    ui->setupUi(this);
    set = false;

    updateInterface();
}

GraphicComponentDialog::GraphicComponentDialog(GraphicObjectComposer component, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::GraphicComponentDialog)
{
    ui->setupUi(this);
    this->component = component;
    set = false;

    updateInterface();
}

void GraphicComponentDialog::setComponent(GraphicObjectComposer component){
    this->component.operator=(component);
}

GraphicObjectComposer GraphicComponentDialog::getComponent(){
    return component;
}

bool GraphicComponentDialog::Set(){
    return set;
}

void GraphicComponentDialog::updateInterface(){
    ui->spn_x->setEnabled(component.xyz[0]);
    ui->spn_y->setEnabled(component.xyz[1]);
    ui->spn_z->setEnabled(component.xyz[2]);

    ui->spn_x->setValue(component.point.x);
    ui->spn_y->setValue(component.point.y);
    ui->spn_z->setValue(component.point.z);

    ui->ln_name->setText(component.name);

    ui->graphicComponentFieldListWidget->clear();
    vector<QString> aux;

    for(int i = 0; i < component.fields.size(); i++){
        Field f = component.fields[i];
        QString line = line.asprintf("%s %s", f.names.toStdString().data(), f.current_value.toStdString().data());

        aux.push_back(line);
    }

    ui->graphicComponentFieldListWidget->addItem(aux);
}

GraphicComponentDialog::~GraphicComponentDialog()
{
    delete ui;
}

void GraphicComponentDialog::on_graphicComponentFieldListWidget_itemDoubleClicked(QListWidgetItem *item)
{
    QString s = item->text();
    QTextStream stream(&s);
    QString name;
    QString value;
    stream >> name >> value;

    bool found = false;
    vector<Field> fields = component.fields;
    Field selection;
    int i;
    for(i = 0; i < fields.size() && !found; i++){
        Field f = fields[i];
        if(f.names == name){
            found = true;
            selection = f;
        }
    }

    if(!found)
        return;

    i--;

    if(selection.type == doublep || selection.type == floatp|| selection.type == integer){
        InputScalarDialog* dialog = new InputScalarDialog(this);
        dialog->setScalar(name);
        QEventLoop loop;
        QTimer timeout;

        connect(dialog,SIGNAL(scalarSet()),&loop,SLOT(quit()));
        connect(&timeout,SIGNAL(timeout()),&loop,SLOT(quit()));

        timeout.start(TIMEOUT);
        dialog->show();
        loop.exec();

        if(dialog->Set()){
            selection.current_value = QString::asprintf("%f",dialog->getScalar());
        }

        delete dialog;
    } else if(selection.type == stringp){
        InputStringDialog* dialog = new InputStringDialog(selection.names,selection.current_value,this);
        QEventLoop loop;
        QTimer timeout;

        connect(dialog,SIGNAL(stringSet()),&loop,SLOT(quit()));
        connect(&timeout,SIGNAL(timeout()),&loop,SLOT(quit()));

        timeout.start(TIMEOUT);
        dialog->show();
        loop.exec();

        if(dialog->Set())
            selection.current_value = dialog->getString();

        delete dialog;

    } else if(selection.type == stringlist){
        SelectFromStringListDialog* dialog = new SelectFromStringListDialog(selection.names,selection.possible_values,this);
        QEventLoop loop;
        QTimer timeout;

        connect(dialog,SIGNAL(stringListSet()),&loop,SLOT(quit()));
        connect(&timeout,SIGNAL(timeout()),&loop,SLOT(quit()));

        timeout.start(TIMEOUT);
        dialog->show();
        loop.exec();

        selection.current_value = dialog->getString();

        delete dialog;

    } else if(selection.type == _boolean){
        InputBooleanDialog* dialog = new InputBooleanDialog(this);
        dialog->setBoolean(name);
        QEventLoop loop;
        QTimer timeout;

        connect(dialog,SIGNAL(booleanSet()),&loop,SLOT(quit()));
        connect(&timeout,SIGNAL(timeout()),&loop,SLOT(quit()));

        timeout.start(TIMEOUT);
        dialog->show();
        loop.exec();

        selection.current_value = name.asprintf("%d",((dialog->getBoolean())?1:0));

        delete dialog;
    }

    component.fields[i] = selection;

    updateInterface();
}

void GraphicComponentDialog::on_spn_x_valueChanged(double arg1)
{
    if(component.xyz[0]){
        component.point.x = arg1;
    }
}

void GraphicComponentDialog::on_spn_y_valueChanged(double arg1)
{
    if(component.xyz[1]){
        component.point.y = arg1;
    }
}

void GraphicComponentDialog::on_spn_z_valueChanged(double arg1)
{
    if(component.xyz[2]){
        component.point.z = arg1;
    }
}

void GraphicComponentDialog::on_buttonBox_accepted()
{
    set = true;

    emit pushComponent(component);
    emit componentSet();
}

void GraphicComponentDialog::on_buttonBox_rejected()
{
    emit cancelled();
}
