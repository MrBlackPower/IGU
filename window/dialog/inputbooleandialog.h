#ifndef INPUTBOOLEANDIALOG_H
#define INPUTBOOLEANDIALOG_H

#include <QDialog>

namespace Ui {
class InputBooleanDialog;
}

class InputBooleanDialog : public QDialog
{
    Q_OBJECT

public:
    explicit InputBooleanDialog(QWidget *parent = 0);
    ~InputBooleanDialog();

    bool getBoolean();

public slots:
    void setBoolean(QString name);

signals:
    void pushBoolean(QString name, bool boolean);

    void booleanSet();

private slots:
    void on_buttonBox_accepted();

private:
    Ui::InputBooleanDialog *ui;
    QString name;
    bool value;
    bool set;
};

#endif // INPUTBOOLEANDIALOG_H
