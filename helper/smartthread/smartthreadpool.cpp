﻿#include "smartthreadpool.h"

SmartThreadPool::SmartThreadPool(int n, QString name, SmartObject* parent) : SmartObject (name,parent)
{
    if(n < 2)
        n = 2;

    N = n;
    jobs_posted = 0;

    qRegisterMetaType<vector<int>*>("vector<int>*");
    qRegisterMetaType<vector<Cost>*>("vector<Cost>*");
    qRegisterMetaType<vector<Solution>*>("vector<Solution>*");

    for (int i = 0; i < N; i++) {
        QThread* t = new QThread(this);
        SmartThreadManager* manager = new SmartThreadManager(QString::asprintf("SMART THREAD %d",i),this);

        QObject::connect(this,SIGNAL(post_job(vector<int>*, int, int,int, int, int, bool)),manager,SLOT(addToBuffer(vector<int>*, int, int,int, int, int, bool)));
        QObject::connect(this,SIGNAL(post_job(vector<Cost>*, int, int,int, int, int, bool)),manager,SLOT(addToBuffer(vector<Cost>*, int, int,int, int, int, bool)));
        QObject::connect(this,SIGNAL(post_job(vector<Solution>*, int, int,int,int, int, bool, bool)),manager,SLOT(addToBuffer(vector<Solution>*, int, int, int,int,int, bool, bool)));

        QObject::connect(manager,SIGNAL(finished_step(int,int)),this,SLOT(thread_finished(int,int)));

        manager->moveToThread(t);

        QTimer* threadTimer = new QTimer();

        connect(threadTimer, SIGNAL(timeout()), manager, SLOT(process()));
        threadTimer->start();
//                threadTimer->moveToThread(t);

        t->start(QThread::HighPriority);
//        manager->process();

        timers.push_back(threadTimer);
        threads.push_back(t);
        thread_managers.push_back(manager);
    }
}

SmartThreadPool::~SmartThreadPool(){
    for (int i = 0; i < N; i++) {
        QThread* t = threads[i];
        SmartThreadManager* manager = thread_managers[i];

        t->quit();

        delete t;
        delete manager;
    }
}

void SmartThreadPool::order(vector<int>* costs,  bool crescent){
    for (int phase = 0; phase < costs->size(); phase++) {
        vector<JobReceipt> toWait;

        //POSTS ALL JOBS FOR CURRENT PHASE
        int max = (phase % 2 == 0)? costs->size(): (costs->size())-1;
        for (int j = 1; j < max; j+= (max / N)) {
            JobReceipt jr;

            int i_min = j;
            int i_max = j + (max/N);

            if(i_max > max)
                i_max = max;

            jr.thread_id = (jobs_posted % N);
            jr.job_id = jobs_posted;

            emit post_job(costs,jr.thread_id,jr.job_id,phase,i_min,i_max,crescent);

            posted_jobs.push_back(jr);

            jobs_posted++;
        }

        //WAITS FOR ALL JOBS OF CURRENT PHASE
        QEventLoop loop;
        QTimer timeout;

        connect(this,SIGNAL(all_jobs_done()),&loop,SLOT(quit()));
        connect(&timeout,SIGNAL(timeout()),&loop,SLOT(quit()));

        timeout.start(1000);
        loop.exec();

        if(!posted_jobs.empty()){
            emit emit_log(SmartLogMessage(getSID(),"NOT ALL JOBS FINISHED!"));
            return;
        }
    }
}

void SmartThreadPool::thread_finished(int thread_id, int job_id){
    bool found = false;

    for (int i = 0; i < posted_jobs.size() && !found; i ++) {
        JobReceipt jr = posted_jobs[i];
        if(jr.thread_id == thread_id && jr.job_id == job_id){
            found = true;

            posted_jobs.erase(posted_jobs.begin() + i);
        }
    }

    if(!found)
        emit emit_log(SmartLogMessage(getSID(),QString::asprintf("RECEIVED NOT RECOGNIZED JOB!")));

    if(posted_jobs.empty()){
        emit all_jobs_done();
    }
}

void SmartThreadPool::order(vector<Cost>* costs,  bool crescent){
    for (int phase = 0; phase < costs->size(); phase++) {
        vector<JobReceipt> toWait;

        //POSTS ALL JOBS FOR CURRENT PHASE
        int max = (phase % 2 == 0)? costs->size(): (costs->size()-1);
        for (int j = 1; j < max; j+= (max / N)) {
            JobReceipt jr;

            int i_min = j;
            int i_max = j + (max/N);

            if(i_max > max)
                i_max = max;


            jr.thread_id = (jobs_posted % N);
            jr.job_id = jobs_posted;

            emit post_job(costs,jr.thread_id,jr.job_id,phase,i_min,i_max,crescent);

            posted_jobs.push_back(jr);

            jobs_posted++;
        }

        //WAITS FOR ALL JOBS OF CURRENT PHASE
        QEventLoop loop;
        QTimer timeout;

        connect(this,SIGNAL(all_jobs_done()),&loop,SLOT(quit()));
        connect(&timeout,SIGNAL(timeout()),&loop,SLOT(quit()));

        timeout.start(1000);
        loop.exec();

        if(!posted_jobs.empty()){
            emit emit_log(SmartLogMessage(getSID(),"NOT ALL JOBS FINISHED!"));
            posted_jobs.clear();
            return;
        }
    }
}

void SmartThreadPool::order(vector<Solution>* costs,bool useScore,  bool crescent){
    for (int phase = 0; phase < costs->size(); phase++) {
        vector<JobReceipt> toWait;

        //POSTS ALL JOBS FOR CURRENT PHASE
        int max = (phase % 2 == 0)? costs->size(): (costs->size())-1;
        for (int j = 1; j < max; j+= (max / N)) {
            JobReceipt jr;

            int i_min = j;
            int i_max = j + (max/N);

            if(i_max > max)
                i_max = max;

            jr.thread_id = (jobs_posted % N);
            jr.job_id = jobs_posted;

            emit post_job(costs,jr.thread_id,jr.job_id,phase,i_min,i_max,useScore,crescent);

            posted_jobs.push_back(jr);

            jobs_posted++;
        }

        //WAITS FOR ALL JOBS OF CURRENT PHASE
        QEventLoop loop;
        QTimer timeout;

        connect(this,SIGNAL(all_jobs_done()),&loop,SLOT(quit()));
        connect(&timeout,SIGNAL(timeout()),&loop,SLOT(quit()));

        timeout.start(10000);
        loop.exec();

        if(!posted_jobs.empty()){
            emit emit_log(SmartLogMessage(getSID(),"NOT ALL JOBS FINISHED!"));
            return;
        }
    }
}

//void SmartThreadPool::neighboorSolution(Solution sol, WeightedGraph* graph, WeightedGraphSolutionMovement movement,  bool crescent){

//}


vector<QString> SmartThreadPool::print(){
    vector<QString> aux;

    return aux;
}

vector<QString> SmartThreadPool::data(){
    vector<QString> aux;

    return aux;
}

vector<QString> SmartThreadPool::raw(){
    vector<QString> aux;

    return aux;
}
