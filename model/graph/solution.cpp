#include "solution.h"

int Solution::CURR_SOLUTION = 0;


Solution::Solution() : SOLUTION_ID(CURR_SOLUTION++){
    vec = vector<int>();
    last_movement = "";
}

Solution::Solution(Solution* representation) : SOLUTION_ID(CURR_SOLUTION++)
{
    vec = representation->getVector();
    last_movement = "";
}

Solution::Solution(vector<int> representation) : SOLUTION_ID(CURR_SOLUTION++)
{
    vec = representation;
    last_movement = "";
}



vector<QString> Solution::toString(){
    QString line("");
    vector<QString> s;

    line = QString::asprintf("< SOLUTION ID = %d, COST = %f, SCORE = %f >", SOLUTION_ID, cost, score);
    s.push_back(line);
    line = QString::asprintf("< LAST MOVEMENT = %s >", last_movement.toStdString().data());
    s.push_back(line);

    for(int i = 0; i < vec.size(); i++){
        line = QString::asprintf("< NODE SOLUTION ID = %d >", vec[i]);
        s.push_back(line);
    }

    line = QString::asprintf("< END SOLUTION ID = %d>", SOLUTION_ID);
    s.push_back(line);
    return s;
}

vector<int> Solution::getVector(){
    return vec;
}

void Solution::getVector(vector<int>* v){
    v->clear();

    for (int i = 0; i < vec.size(); i++) {
        v->push_back(vec[i]);
    }
}

void Solution::operator+=(int b){
    for (int i = 0; i < vec.size(); i++) {
        if(vec[i] == b)
            return;
    }

    vec.push_back(b);
}

void Solution::operator=(Solution b){
    vec.clear();

    vector<int> aux = b.getVector();

    for (int i = 0; i < aux.size(); i++) {
        vec.push_back(aux[i]);
    }

    this->valid = b.valid;
    this->score = b.score;
    this->cost = b.cost;
    this->last_movement = b.last_movement;
}

bool Solution::operator==(Solution b){
    return score == b.score;
}

bool Solution::operator>(Solution b){
    if(cost == b. cost)
        return score > b.score;

    return cost > b.cost;
}

bool Solution::operator>=(Solution b){
    return cost >= b.cost;
}

bool Solution::operator<(Solution b){
    if(this->cost == b. cost)
        return (this->score < b.score);

    return (this->cost < b.cost);
}

bool Solution::operator<=(Solution b){
    return cost <= b.cost;
}

vector<QString> Solution::toString(vector<Solution> solutions){
    vector<QString> s;

    for(int i = 0; i < solutions.size(); i++){
        vector<QString> aux = solutions[i].toString();

        for (int j = 0; j < aux.size(); j++) {
            s.push_back(aux[j]);
        }
    }

    return s;
}

bool Solution::compare(Solution a, Solution b){
    return a < b;
}

bool Solution::compareDecrescent(Solution a, Solution b){
    return a.score > b.score;
}

bool Solution::compareScore(Solution a, Solution b){
    return a.score < b.score;
}

bool Solution::compareScoreDecrescent(Solution a, Solution b){
    return a > b;
}

void Solution::orderCrescent(vector<Solution>* list){
    sort(list->begin(),list->end());
}

void Solution::orderDecrescent(vector<Solution>* list){
    sort(list->begin(),list->end(),compareDecrescent);
}

void Solution::orderScoreCrescent(vector<Solution>* list){
    sort(list->begin(),list->end(),compareScore);
}

void Solution::orderScoreDecrescent(vector<Solution>* list){
    sort(list->begin(),list->end(),compareScoreDecrescent);
}
