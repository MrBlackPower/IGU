#include "messagewindow.h"
#include "ui_messagewindow.h"
using namespace window;

MessageWindow::MessageWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MessageWindow)
{
    ui->setupUi(this);
}

void MessageWindow::setMessage(string msg){
    this->msg = msg;
    QString buffer = "";
    buffer = buffer.fromStdString(msg);

    ui->lblMessage->setText(buffer);
}

MessageWindow::~MessageWindow()
{
    delete ui;
}

void MessageWindow::on_btnOK_clicked()
{
    this->close();
}
