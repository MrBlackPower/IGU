#ifndef VIEWDATADIALOG_H
#define VIEWDATADIALOG_H

#include <QDialog>
#include <vector>
#include "../../helper/vtkfile.h"

using namespace std;

namespace Ui {
class ViewDataDialog;
}

class ViewDataDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ViewDataDialog(QWidget *parent = 0);
    ~ViewDataDialog();

public slots:

    void fillCellData(VTKFile file, QString field);

    void fillPointData(VTKFile file, QString field);

private slots:
    void on_btnOk_clicked();

private:
    Ui::ViewDataDialog *ui;
};

#endif // VIEWDATADIALOG_H
