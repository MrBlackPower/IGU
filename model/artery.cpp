#include "artery.h"

double Artery::pressure_max = -99999999;
double Artery::pressure_min = 99999999;
double Artery::flow_max = -99999999;
double Artery::flow_min = 99999999;

/************************************************************************************
  Name:        artery.cpp
  Copyright:   Version 1.0
  Author:      Igor Pires dos Santos
  Last Update: 17/02/2017 (DD/MM/YYYY)
  Release:     --/--/---- (DD/MM/YYYY)
  Description: Class implementation of an artery.
************************************************************************************/

/************************************************************************************
        CONSTRUCTOR & DESTRUCTOR
************************************************************************************/
Artery::Artery(int id, Point* p, double length, double youngModulus,
               double density, double viscosity, double radius, bool isLeaf, GraphicObject* arteryTree, int arteryTreeGID) : SmartObject(QString::asprintf("ARTERY #%d",id),arteryTree){
    ArteryTreeGID = arteryTreeGID;
    ArteryTreeSID = arteryTree->getSID();
    alpha = -1;
    father = NULL;
    left = NULL;
    right = NULL;
    flow_wave = NULL;
    pressure_wave = NULL;
    this->id = id;
    this->length = length;
    phi = 0.0;
    pressure = complex<double> (-1.0,0.0);
    _pressure = complex<double> (-1.0,0.0);
    flow = complex<double> (-1.0,0.0);
    r = radius;
    _Y = complex<double> (-1.0,0.0);
    Ye = complex<double> (-1.0,0.0);
    R = complex<double> (-1.0,0.0);
    E = youngModulus;
    Ec = complex<double> (-1.0,0.0);
    this->viscosity = viscosity;
    this->density = density;
    viscousFactor = complex<double> (-1.0,0.0);
    c = -1;
    cv = complex<double> (-1.0,0.0);
    beta = complex<double> (-1.0,0.0);
    w = 0.0;
    leaf = isLeaf;
    selected = false;
    hovered = false;
    this->p = *p;
    h = r * 0.1;
    graphicSize = DEFAULT_GRAPHIC_SIZE;

    classStack->addClass("ARTERY",id);
}

Artery::~Artery(){
    //dtor
    if(hasLeft())
        delete left;

    if(hasRight())
        delete right;
}

/************************************************************************************
*                              CLASS METHODS                                        *                                       
************************************************************************************/
bool Artery::equals(Artery* a){
    if(a->getPoint().equals(p))
        return true;

    return false;
}

void Artery::clearMinMax(){
    Artery::pressure_max = -99999999;
    Artery::pressure_min = 99999999;
    Artery::flow_max = -99999999;
    Artery::flow_min = 99999999;
}

vector<QString> Artery::print(){
    emit emit_log(SmartLogMessage(getSID(),"ARTERY DATA SAVED <ERROR: IT SHOULDN'T>",SmartLogType::LOG_CRASH));
}

vector<QString> Artery::data(){
    emit emit_log(SmartLogMessage(getSID(),"ARTERY DATA SAVED <ERROR: IT SHOULDN'T>",SmartLogType::LOG_CRASH));
}

vector<QString> Artery::raw(){
    emit emit_log(SmartLogMessage(getSID(),"ARTERY DATA SAVED <ERROR: IT SHOULDN'T>",SmartLogType::LOG_CRASH));
}

void Artery::solveLog(SmartLogMessage msg){

}

Artery* Artery::findArtery(Point* point){
    if(point->equals(p))
        return this;

    Artery* a = NULL;

    if(hasLeft())
        a = left->findArtery(point);

    if(a)
        return a;

    if(hasRight())
        a = right->findArtery(point);

    return a;
}

bool Artery::hasFather(){
    if(father != NULL)
        return true;

    return false;
}

bool Artery::hasLeft(){
    if(left != NULL)
        return true;

    return false;
}

bool Artery::hasRight(){
    if(right != NULL)
        return true;

    return false;
}

bool Artery::hasSon(){
    return hasRight() || hasLeft();
}

bool Artery::isHovered(){
    return hovered;
}

bool Artery::isLeaf(){
    return leaf;
}

bool Artery::isSelected(){
    return selected;
}

void Artery::select(){
    //IF it has a father have to send up the selection
    if(hasFather()){
        Artery* f = getFather();
        f->select();

        //checks if brother is selected
        //if it is unselect it unselects
        if(f->hasLeft() && f->hasRight()){
            if(equals(f->getRight())){
                Artery* brother = f->getLeft();
                if(brother->isSelected())
                    brother->unselect();
            } else {
                Artery* brother = f->getRight();
                if(brother->isSelected())
                    brother->unselect();
            }
        }
    }

    //IF it has sons unselects them
    if(hasLeft())
        if(left->isSelected())
            left->unselect();

    if(hasRight())
        if(right->isSelected())
            right->unselect();

    selected = true;
}

void Artery::unselect(){
    selected = false;

    if(hasLeft())
        if(left->isSelected())
            left->unselect();

    if(hasRight())
        if(right->isSelected())
            right->unselect();
}

void Artery::hover(){
    //IF it has a father have to send up the hovereing
    if(hasFather()){
        Artery* f = getFather();
        f->hover();

        //checks if brother is hovered
        //if it is hovered it unhovers
        if(f->hasLeft() && f->hasRight()){
            if(equals(f->getRight())){
                Artery* brother = f->getLeft();
                if(brother->isHovered())
                    brother->unhover();
            } else {
                Artery* brother = f->getRight();
                if(brother->isHovered())
                    brother->unhover();
            }
        }
    }

    //IF has sons unhovers them
    if(hasLeft())
        if(left->isHovered())
            left->unhover();

    if(hasRight())
        if(right->isHovered())
            right->unhover();

    hovered = true;
}

void Artery::unhover(){
    hovered = false;

    if(hasLeft())
        if(left->isHovered())
            left->unhover();

    if(hasRight())
        if(right->isHovered())
            right->unhover();
}

/************************************************************************************
*                                       PLOTS                                       *
************************************************************************************/

void Artery::printField(ArteryParameter fieldCode, vector<QString>* raw){
    QString line;
    switch (fieldCode) {
    case POINTS:
        line = line.asprintf("%f %f %f",p.x,p.y,p.z);
        raw->push_back(line);
        break;
    case LINES:
        if(hasLeft()){
            line = line.asprintf("2  %d  %d", id, left->getID());
            raw->push_back(line);
        }

        if(hasRight()){
            line = line.asprintf("2  %d  %d", id, left->getID());
            raw->push_back(line);
        }
        break;
    case PRESSURE:
        if(pressure != -complex<double> (-1.0,0.0)){
            line = line.asprintf("%f",pressure);
            raw->push_back(line);
        }
        break;
    case FLOW:
        if(flow != -complex<double> (-1.0,0.0)){
            line = line.asprintf("%f",flow);
            raw->push_back(line);
        }
        break;
    case RADIUS:
        line = line.asprintf("%f",r);
        raw->push_back(line);
        break;
    case YOUNG_MOD:
        line = line.asprintf("%f",E);
        raw->push_back(line);
        break;
    case DENSITY:
        line = line.asprintf("%f",density);
        raw->push_back(line);
        break;
    case VISCOSITY:
        line = line.asprintf("%f",viscosity);
        raw->push_back(line);
        break;
    case LENGTH:
        if(length != -ONE){
            line = line.asprintf("%f",length);
            raw->push_back(line);
        }
        break;
    case WAVESPEED:
        if(c != -ONE){
            line = line.asprintf("%f",c);
            raw->push_back(line);
        }
        break;
    case BETA:
        if(beta != -complex<double> (-1.0,0.0)){
            line = line.asprintf("%f",beta);
            raw->push_back(line);
        }
        break;
    case EFFECTIVE_ADMITTANCE:
        if(Ye != -complex<double> (-1.0,0.0)){
            line = line.asprintf("%f",Ye);
            raw->push_back(line);
        }
        break;
    case MEDIUM_PRESSURE:
        if(_pressure != -complex<double> (-1.0,0.0)){
            line = line.asprintf("%f",_pressure);
            raw->push_back(line);
        }
        break;
    default:
        return;
    }
    if(hasLeft())
        left->printField(fieldCode,raw);

    if(hasRight())
        right->printField(fieldCode,raw);
}

/********************************************************
*                  GETS & SETS                          *
********************************************************/

complex<double> Artery::getAdmittance(){
    return _Y;
}

bool Artery::setAdmittance(){
    return setAdmittance(false, 0.0, 0.0);
}

bool Artery::setAdmittance(bool viscous, double viscousMultiplier, double phi0){
    if (c == -1)
        return false;

    if(!viscous){
        _Y = (PI * r * r  / (density * c ));
    } else {
        //Sets parameters for viscous flow
        if(!setAlpha(viscousMultiplier))
            return false;

        if(!setViscousFactor())
            return false;

        if(!setPhaseAngle(phi0))
            return false;

        if(!setViscousYoungsModulus())
            return false;

        if(!setViscousWaveSpeed())
            return false;
        
        if(!setViscousBeta())
            return false;

        _Y = (PI * r * r  / (density * cv )) *  sqrt(viscousFactor);
    }


    return true;
}

double Artery::getAlpha(){
    return alpha;
}

bool Artery::setAlpha(double viscousMultiplier){
    if(w == -1)
        return false;

    if(viscousMultiplier > 0.0)
        alpha = r * sqrt((w * density)/ (viscosity * viscousMultiplier));
    else
        alpha = 0;

    return true;
}

double Artery::getAngularFrequency(){
    return w;
}

bool Artery::setAngularFrequency(double frequency){
    w = 2 * PI * frequency;

    return true;
}

double Artery::getArea(){
    if(r == -1)
        return -1;

    return PI * pow(r,2);
}

complex<double> Artery::getBeta(){
    return beta;
}

bool Artery::setBeta(){
    if(c == -1)
        return false;

    if(w == -1)
        return false;

    beta = complex<double> ((w * length )/( c ), 0.0);

    return true;
}

double Artery::getDensity(){
    return density;
}

void Artery::setDensity(double val){
    if(!MathHelper::isPositive(val))
        return;

    density = val;
}

double Artery::getDiameter(){
    return r * 2;
}

complex<double> Artery::getEffectiveAdmittance(){
    return Ye;
}

bool Artery::setEffectiveAdmittance(){
    if(isLeaf())
    {
        Ye = _Y;
    }
    else
    {
        if (R == complex<double> (-1.0, 0.0))
            return false;

        if (beta == complex<double> (-1.0 , 0.0))
            return false;

        complex<double> aux (2 * imag(beta) , -2 * real(beta));
        complex<double> one (1.0 , 0.0);

        Ye = _Y * ((one - (R *  exp(aux)))/(one + (R *  exp(aux))));
    }

    return true;
}

Artery* Artery::getFather(){
    return father;
}

void Artery::setFather(Artery* a){
    father = a;
}

complex<double> Artery::getFlow(){
    return flow;
}

vector<Point*> Artery::getFlowWave(){
    return flow_wave->getData();
}

complex<double> Artery::getFlow(double x, complex<double> rootAdmittance){
    if(x < 0 || x > 1)
        return -1;

    if(_pressure == complex<double>(-1.0,0.0))
        return -1;

    if(beta == complex<double> (-1.0 , 0.0))
        return -1;

    if(Ye == complex<double>(-1.0,0.0))
        return -1;

    complex<double> M = Ye / rootAdmittance;
    complex<double> aux1 (imag(beta) * x , real(-beta) * x);
    complex<double> aux2 (imag(-beta) * x , real(beta) * x);
    complex<double> aux3 (imag(beta) * 2 , real(-beta) * 2);

    return M * _pressure * exp(aux1) - R * exp(aux2) * exp(aux3);
}

bool Artery::setFlow(complex<double> rootAdmittance){
    GraphicObject* gObj = GraphicObject::getGraphic(ArteryTreeGID);

    if(_pressure == complex<double>(-1.0,0.0))
        return -1;

    if(beta == complex<double> (-1.0 , 0.0))
        return -1;

    if(Ye == complex<double>(-1.0,0.0))
        return -1;

    if(!flow_wave){
        //CREATES WAVE GRAPHIC
        QString aux;
        aux = aux.asprintf("ARTERY #%d / %d - FLOW WAVE GRAPHIC",ArteryTreeSID,id);
        flow_wave = new StaticGraphic(aux,this);
        gObj->addSlave(flow_wave);
    }

    flow_wave->clearData();

    //SETS FLOW WAVE
    for(double i = 0.0; i <= SIZE; i = i + (SIZE/graphicSize)){
       complex<double> fl = getFlow(i,rootAdmittance);
       Point* p = new Point(i, i, abs(fl), 0.0);
       flow_wave->addData(p);

       if(abs(fl) > flow_max)
           flow_max = abs(fl);

       if(abs(fl) < flow_min)
           flow_min = abs(fl);

       if(i == SIZE)
           flow = fl;
    }

    return true;
}

double Artery::getFrequency(){
    return w / (2 * PI);
}

int Artery::getID(){
    return id;
}

Artery* Artery::getLeft(){
    return left;
}

void Artery::setLeft(Artery* a){
    left = a;
}

void Artery::setLeaf(bool val){
    leaf = val;
}

double Artery::getLength(){
    return length;
}

void Artery::setLength(double val){
    if(!MathHelper::isPositive(val))
        return;

    length = val;
}

complex<double> Artery::getMediumPressure(){
    return _pressure;
}

bool Artery::setMediumPressure(){
    if(R == complex<double>(-1.0,0.0))
        return false;

    if(beta == complex<double> (-1.0 , 0.0))
        return false;

    if(!hasFather())
        return false;

    if(father->getMediumPressure() == complex<double>(-1.0,0.0))
        return false;

    complex<double> _pf = father->getMediumPressure();
    complex<double> Rf = father->getReflectionCoef();
    complex<double> betaf = father->getBeta();

    complex<double> aux1 (imag(betaf), real(-betaf));
    complex<double> aux2 (2 * imag(beta), -2 * real(beta));
    complex<double> one (1.0,0.0);

    _pressure = (_pf * (one + Rf) * exp(aux1))/(one + R *exp(aux2));

    return true;
}

bool Artery::setMediumPressure(double p0){
    if(hasFather())
        return false;

    _pressure = complex<double> (p0 , 0.0);

    return true;
}

Point Artery::getPoint(){
    return p;
}

void Artery::setPoint(Point p){
    this->p = p;
}

double Artery::getPhaseAngle(){
    return phi;
}

bool Artery::setPhaseAngle(double phi0){
    if(w == -1)
        return false;

    phi = phi0 * (1 - exp(-w));

    return true;
}

bool Artery::setPhaseOne(double frequency){
    return setPhaseOne(frequency,false,0.0,0.0);
}

bool Artery::setPhaseOne(double frequency, bool viscous, double viscousMultiplier, double phi0){
    if(isLeaf())
    {

        if(!setWaveSpeed())
            return false;

        if(!setAngularFrequency(frequency))
            return false;

        if(!setBeta())
            return false;

        if(!setAdmittance(viscous,viscousMultiplier,phi0))
            return false;

        if(!setReflectionCoef())
            return false;

        if(!setEffectiveAdmittance())
            return false;
    }
    else
    {
        if(hasRight())
            if(!right->setPhaseOne(frequency,viscous,viscousMultiplier,phi0))
                return false;

        if(hasLeft())
            if(!left->setPhaseOne(frequency,viscous,viscousMultiplier,phi0))
                return false;

        if(!setWaveSpeed())
            return false;

        if(!setAngularFrequency(frequency))
            return false;

        if(!setBeta())
            return false;

        if(!setAdmittance(viscous,viscousMultiplier,phi0))
            return false;

        if(!setReflectionCoef())
            return false;

        if(!setEffectiveAdmittance())
            return false;
    }

    return true;
}

bool Artery::setPhaseTwo(double p0, complex<double> rootAdmittance){
    if(!setMediumPressure(p0))
        return false;

    if(!setPressure())
        return false;

    if(!setFlow(rootAdmittance))
        return false;

    if(hasRight())
        if(!right->setPhaseTwoAux(rootAdmittance))
            return false;

    if(hasLeft())
        if(!left->setPhaseTwoAux(rootAdmittance))
            return false;

    return true;
}

bool Artery::setPhaseTwoAux(complex<double> rootAdmittance){
    if(!setMediumPressure())
        return false;

    if(!setPressure())
        return false;

    if(!setFlow(rootAdmittance))
        return false;

    if(hasRight())
        if(!right->setPhaseTwoAux(rootAdmittance))
            return false;

    if(hasLeft())
        if(!left->setPhaseTwoAux(rootAdmittance))
            return false;

    return true;
}

complex<double> Artery::getPressure(){
    return pressure;
}

vector<Point*> Artery::getPressureWave(){
    return pressure_wave->getData();
}

complex<double> Artery::getPressure(double x){
    if(x < 0 || x > 1)
        return -1;

    if(_pressure == complex<double> (-1.0,0.0))
        return -1;

    if(beta == complex<double> (-1.0 , 0.0))
        return -1;

    complex<double> aux1 (imag(beta) * x , real(-beta) * x);
    complex<double> aux2 (imag(-beta) * x , real(beta) * x);
    complex<double> aux3 (imag(beta) * 2 , real(-beta) * 2);

    return _pressure * (exp(aux1) + R * exp(aux2) * exp(aux3));
}

bool Artery::setPressure(){
    GraphicObject* gObj = GraphicObject::getGraphic(ArteryTreeGID);

    if(_pressure == complex<double> (-1.0,0.0))
        return false;

    if(beta == complex<double> (-1.0 , 0.0))
        return false;

    if(!pressure_wave){
        //CREATES PRESSURE GRAPHIC
        QString aux;
        aux = aux.asprintf("ARTERY #%d / %d - PRESSURE WAVE GRAPHIC",ArteryTreeSID,id);
        pressure_wave = new StaticGraphic(aux,this);

        gObj->addSlave(pressure_wave);
    }

    pressure_wave->clearData();

    //SETS PRESSURE WAVE
    for(double i = 0.0; i <= SIZE; i = i + (SIZE/graphicSize)){
       complex<double> press = getPressure(i);
       Point* p = new Point(i, i, abs(press), 0.0);
       pressure_wave->addData(p);

       //MAX-MIN PRESSURE
       if(abs(press) > pressure_max)
           pressure_max = abs(press);

       if(abs(press) < pressure_min)
           pressure_min = abs(press);

       if(i == SIZE)
           pressure = press;
    }


    return true;
}

double Artery::getRadius(){
    return r;
}

void Artery::setRadius(double val){
    if(!MathHelper::isPositive(val))
        return;

    r = val;

    h = 0.1 * r;
}

complex<double> Artery::getReflectionCoef(){
    return R;
}

bool Artery::setReflectionCoef(){
    if(this->isLeaf())
    {
        R = 0.0;
    }
    else
    {
        complex<double> Ye1 = hasLeft()? left->getEffectiveAdmittance() : 0.0;
        complex<double> Ye2 = hasRight()? right->getEffectiveAdmittance() : 0.0;

        if(Ye1 == complex<double> (-1.0 , 0.0) || Ye2 == complex<double> (-1.0 , 0.0))
            return false;


        R = (_Y - (Ye2 + Ye1))/(_Y + (Ye2 + Ye1));

    }

    return true;
}

Artery* Artery::getRight(){
    return right;
}

void Artery::setRight(Artery* a){
    right = a;
}

double Artery::getThickness(){
    return h;
}

void Artery::setThickness(double val){
    if(!MathHelper::isPositive(val))
        return;

    h = val;

    r = h * 10;
}

double Artery::getViscosity(){
    return viscosity;
}

void Artery::setViscosity(double val){
    if(!MathHelper::isPositive(val))
        return;

    viscosity = val;
}

bool Artery::setViscousBeta(){
    if(cv == complex<double> (-1.0 , 0.0))
        return false;

    if(w == -1)
        return false;

    beta = w * length / cv;

    return true;
}

complex<double> Artery::getViscousFactor(){
    return viscousFactor;
}

bool Artery::setViscousFactor(){
    if(alpha == -1)
        return false;

    complex<double> one (1.0,0.0);

    if(alpha != 0){
        complex<double> i (0.0,1.0);

        viscousFactor = one - ((2.0)/(alpha * sqrt(i)));
    } else {
        viscousFactor = one;
    }

    return true;
}

complex<double> Artery::getViscousYoungsModulus(){
    return Ec;
}

bool Artery::setViscousYoungsModulus(){
    if(phi == -1)
        return false;

    complex<double> aux (0.0,MathHelper::degreeToRadian(phi));

    Ec = abs(E) * exp(aux);

    return true;
}

complex<double> Artery::getViscousWaveSpeed(){
    return cv;
}

bool Artery::setViscousWaveSpeed(){
    if(viscousFactor == complex<double> (-1.0,0.0))
        return false;

    if(Ec == complex<double> (-1.0,0.0))
        return false;

    cv = sqrt((Ec * h)/(density * 2 * r)) * sqrt(viscousFactor);

    return true;
}

double Artery::getYoungModulus(){
    return E;
}

void Artery::setYoungModulus(double val){
    if(!MathHelper::isPositive(val))
        return;

    E = val;
}

double Artery::getWaveSpeed(){
    return c;
}

bool Artery::setWaveSpeed(){
    c = sqrt((E * h)/(density * r * 2));

    return true;
}

double Artery::getPressureMax(){
    return pressure_max;
}

double Artery::getPressureMin(){
    return pressure_min;
}

double Artery::getFlowMax(){
    return flow_max;
}

double Artery::getFlowMin(){
    return flow_min;
}
