#ifndef GENERATEWEIGHTEDGRAPHWINDOW_H
#define GENERATEWEIGHTEDGRAPHWINDOW_H

#include <QDialog>
#include <QString>

namespace Ui {
class GenerateWeightedGraphWindow;
}

class GenerateWeightedGraphWindow : public QDialog
{
    Q_OBJECT

public:
    struct GenerateParams{
        QString name;
        int n = 100;
        int m = 100;
        bool treeD = false;
        int x = 10;
        int y = 10;
        int z = 10;
        bool load = true;
    };

    explicit GenerateWeightedGraphWindow(QWidget *parent = 0);
    ~GenerateWeightedGraphWindow();

signals:
    void generateGraph(GenerateWeightedGraphWindow::GenerateParams params);

private slots:
    void on_chk_2D_clicked(bool checked);

    void on_chk_3D_clicked(bool checked);

    void on_spn_Edges_editingFinished();

    void on_spn_Nodes_editingFinished();

    void on_spn_x_editingFinished();

    void on_spn_y_editingFinished();

    void on_spn_z_editingFinished();

    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

    void on_ln_name_textChanged(const QString &arg1);

    void on_chk_load_clicked(bool checked);

    void on_chk_export_clicked(bool checked);

private:
    Ui::GenerateWeightedGraphWindow *ui;

    GenerateParams params;
};

#endif // GENERATEWEIGHTEDGRAPHWINDOW_H
