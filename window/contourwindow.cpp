#include "contourwindow.h"
#include "ui_contourwindow.h"

ContourWindow::ContourWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ContourWindow)
{
    ui->setupUi(this);
}

ContourWindow::~ContourWindow()
{
    delete ui;
}

void ContourWindow::on_doubleSpinBox_editingFinished()
{
    params.scalar = ui->spn_scalar->value();
}

void ContourWindow::on_buttonBox_accepted()
{
    params.scalar = ui->spn_scalar->value();

    emit sendCountourWindowParams(params.scalar);

    this->hide();
}

void ContourWindow::on_buttonBox_rejected()
{
    this->hide();
}
