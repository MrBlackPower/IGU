#ifndef GRAPHICOBJECTTREEWIDGET_H
#define GRAPHICOBJECTTREEWIDGET_H

#include <QTreeWidget>
#include <QTreeWidgetItem>
#include "../../model/graphicobject.h"

class GraphicObjectTreeWidget : public QTreeWidget
{
public:
    GraphicObjectTreeWidget(QWidget *parent);

    void populate();

private:

    void populate_aux(QTreeWidgetItem* current, GraphicObject* current_obj, vector<int>* read);
};

#endif // GRAPHICOBJECTTREEWIDGET_H
