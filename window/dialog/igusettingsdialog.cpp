#include "igusettingsdialog.h"
#include "ui_igusettingsdialog.h"

IGUSettingsDialog::IGUSettingsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::IGUSettingsDialog)
{
    ui->setupUi(this);
    config.load();
    
    updateDialogs();
}

void IGUSettingsDialog::updateDialogs(){
    ui->chk_parallelizing->setChecked(config.raw_data.parallel);

    if(!config.raw_data.parallel){
        ui->spn_number_of_threads->setEnabled(false);
        ui->spn_thread_blocks->setEnabled(false);
        ui->spn_threads_in_blocks->setEnabled(false);
        ui->cbox_paradigma->setEnabled(false);
        return;
    } else {
        ui->spn_number_of_threads->setEnabled(true);
        ui->spn_thread_blocks->setEnabled(true);
        ui->spn_threads_in_blocks->setEnabled(true);
        ui->cbox_paradigma->setEnabled(true);

        ui->spn_number_of_threads->setValue(config.raw_data.number_of_threads);
        ui->spn_thread_blocks->setValue(config.raw_data.number_of_thread_blocks);
        ui->spn_threads_in_blocks->setValue(config.raw_data.number_of_thread_blocks);
    }

    ui->cbox_paradigma->clear();

    ui->cbox_paradigma->addItem(MathHelper::paradigmaToString(MathHelper::OPENMP));
    ui->cbox_paradigma->addItem(MathHelper::paradigmaToString(MathHelper::MPI));
    ui->cbox_paradigma->addItem(MathHelper::paradigmaToString(MathHelper::QTHREAD));

    if(config.raw_data.parallel_paradigma == MathHelper::OPENMP)
        ui->cbox_paradigma->setCurrentIndex(0);


    if(config.raw_data.parallel_paradigma == MathHelper::MPI)
        ui->cbox_paradigma->setCurrentIndex(1);


    if(config.raw_data.parallel_paradigma == MathHelper::QTHREAD)
        ui->cbox_paradigma->setCurrentIndex(2);

    ui->spn_move_per_pixel->setValue(config.raw_data.move_per_pixel);
    ui->spn_scalar_step_per_pixel->setValue(config.raw_data.scalar_step_per_pixel);
    ui->spn_angles_per_pixel->setValue(config.raw_data.angles_per_pixel);
}

IGUSettingsDialog::~IGUSettingsDialog()
{
    delete ui;
}

vector<QString> IGUSettingsDialog::print(){
    config.print();
}

void IGUSettingsDialog::print(vector<QString>* aux){
    config.print(aux);
}

void IGUSettingsDialog::update_config(SystemSettingsHelper config){
    GraphicObject::angles_per_pixel = config.raw_data.angles_per_pixel;
    GraphicObject::scalar_step_per_pixel = config.raw_data.scalar_step_per_pixel;
    GraphicObject::move_per_pixel = config.raw_data.move_per_pixel;
    GraphicObject::z_offset = config.raw_data.z_offset;

    MathHelper::parallel = config.raw_data.parallel;
    MathHelper::default_paradigma = config.raw_data.parallel_paradigma;
    MathHelper::threads = config.raw_data.number_of_threads;
    MathHelper::thread_blocks = config.raw_data.number_of_thread_blocks;
    MathHelper::threads_in_blocks = config.raw_data.number_of_threads_in_blocks;
}

void IGUSettingsDialog::on_btn_refresh_clicked()
{
    config.load();
    updateDialogs();
}

void IGUSettingsDialog::on_btnBox_accepted()
{
    update_config(config);

    raw.clear();
    print(&raw);

    emit setSettings();

    emit pushSettings(config);
    emit printConfig(raw,"config.igu");
}

void IGUSettingsDialog::on_chk_parallelizing_stateChanged(int arg1)
{
    config.raw_data.parallel = ui->chk_parallelizing->isChecked();
    updateDialogs();
}

void IGUSettingsDialog::on_cbox_paradigma_currentTextChanged(const QString &arg1)
{
    MathHelper::ParallelParadigma new_paragadigma = MathHelper::stringToParadigma(ui->cbox_paradigma->currentText());

    if(new_paragadigma != config.raw_data.parallel_paradigma){
        config.raw_data.parallel_paradigma = new_paragadigma;
        updateDialogs();
    }
}

void IGUSettingsDialog::on_spn_number_of_threads_editingFinished()
{
    config.raw_data.number_of_threads = ui->spn_number_of_threads->value();
    updateDialogs();
}

void IGUSettingsDialog::on_spn_thread_blocks_editingFinished()
{
    int new_thread_blocks = ui->spn_thread_blocks->value();

    if(new_thread_blocks != config.raw_data.number_of_thread_blocks){
        config.raw_data.number_of_thread_blocks = new_thread_blocks;
        updateDialogs();
    }
}

void IGUSettingsDialog::on_spn_threads_in_blocks_editingFinished()
{
    int new_number_of_threads_in_blocks = ui->spn_threads_in_blocks->value();

    if(new_number_of_threads_in_blocks != config.raw_data.number_of_threads_in_blocks){
        config.raw_data.number_of_threads_in_blocks = new_number_of_threads_in_blocks;
        updateDialogs();
    }
}

void IGUSettingsDialog::on_spn_angles_per_pixel_editingFinished()
{
    double new_angles_per_pixel = ui->spn_angles_per_pixel->value();

    if(new_angles_per_pixel != config.raw_data.angles_per_pixel){
        config.raw_data.angles_per_pixel = new_angles_per_pixel;
        updateDialogs();
    }
}

void IGUSettingsDialog::on_spn_scalar_step_per_pixel_editingFinished()
{
    double new_scalar_step = ui->spn_scalar_step_per_pixel->value();

    if(new_scalar_step != config.raw_data.scalar_step_per_pixel){
        config.raw_data.scalar_step_per_pixel = new_scalar_step;
        updateDialogs();
    }
}

void IGUSettingsDialog::on_spn_move_per_pixel_editingFinished()
{
    double new_move_per_pixel = ui->spn_move_per_pixel->value();

    if(new_move_per_pixel != config.raw_data.move_per_pixel){
        config.raw_data.move_per_pixel = new_move_per_pixel;
        updateDialogs();
    }
}

void IGUSettingsDialog::on_spn_z_offset_valueChanged(double arg1)
{
    double new_z_offset = ui->spn_z_offset->value();

    if(new_z_offset != config.raw_data.angles_per_pixel){
        config.raw_data.z_offset = new_z_offset;
        updateDialogs();
    }
}
