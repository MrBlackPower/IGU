#ifndef GRAPHIC3D_H
#define GRAPHIC3D_H

#include "graphicobject.h"

class Graphic3D : public GraphicObject{
public:
    Graphic3D(QString name, int M, int N, SmartObject* parent = NULL);
    Graphic3D(QString name, int M, SmartObject* parent = NULL);

    void resizeTransform();

    virtual bool addData(Point p);
    virtual bool addData(Point* p);
    virtual bool clearData();

    /********************************************************
    *  SMART OBJECT VIRTUAL METHODS                       *
    ********************************************************/
    vector<QString> print();
    vector<QString> data();
    vector<QString> raw();

    /********************************************************
    *  GRAPHIC OBJECT VIRTUAL METHODS  *
    ********************************************************/
    bool compatibility(VTKFile *file);
    bool load(VTKFile* file);
    void draw();
    vector<Field> getPointDataList();
    vector<Field> getCellDataList();
    virtual vector<Field> getStartParameters() = 0;
    virtual vector<Field> getContextVisualParameters() = 0;
    virtual vector<QString> getRanges() = 0;
    virtual bool updateStartParameter(QString field, QString value) = 0;
    virtual bool setContextRange(QString field) = 0;

    GraphicObject* extrapolateContext(QString extrapolation);


    int getM();
    int getN();
    int ID(int i, int j);

    Point operator[](int i);
    Point operator ()(int i, int j);

protected:
    void setM(int M);
    void setN(int N);

    vector<Point*> raw_data;
    int M;
    int N;

private:
    int NODES;

    /********************************************************
    *  GRAPHIC OBJECT VIRTUAL METHODS  *
    ********************************************************/
    virtual double iteration() = 0;
    virtual bool initialize() = 0;
    void finish();
    vector<QString> getExtrapolations();
    virtual bool updateContextVisualParameter(QString field, QString value) = 0;
    virtual QString getLiveParameterList() = 0;
    virtual QString getLiveParameter(QString name) = 0;
    virtual DataType getLiveParameterDataType(QString name) = 0;
    virtual void mouse_press_event(int width, int height, Point eye, QMouseEvent *event, GraphicTools tool) = 0;
    virtual void mouse_move_event(int width, int height, Point eye, QMouseEvent *event, GraphicTools tool) = 0;
};

#endif // GRAPHIC3D_H
