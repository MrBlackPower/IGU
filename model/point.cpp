#include "point.h"

/************************************************************************************
  Name:        point.cpp
  Copyright:   Version 1.0
  Author:      Igor Pires dos Santos
  Last Update: 17/02/2017 (DD/MM/YYYY)
  Release:     --/--/---- (DD/MM/YYYY)
  Description: Class implementation of a (x,y,z) float points representation.
************************************************************************************/

/************************************************************************************
        CONSTRUCTORS
************************************************************************************/
Point::Point(){
    x = 0;
    y = 0;
    z = 0;
    id = 0;

}

Point::Point(QPoint point){
    x = point.x();
    y = point.y();
    z = 0;
    id = 0;
}

Point::Point(int id, float x, float y){
    this->id = id;
    this->x = x;
    this->y = y;
    z = 0;

}
Point::Point(int id, float x, float y, float z){
    this->id = id;
    this->x = x;
    this->y = y;
    this->z = z;

}
        
/************************************************************************************
*                               DESTRUCTOR                                          *
************************************************************************************/
Point::~Point(){
    //dtor
}

/************************************************************************************
*                              CLASS METHODS                                        *                                       
************************************************************************************/
Point* Point::clone(){
    Point* p = new Point(id,x,y,z);

    return p;
}

bool Point::equals(Point* p){
    if(pow(p->x - x,2.0) < 1e-10)
        if(pow(p->y - y,2.0) < 1e-10)
            if(pow(p->z - z,2.0) < 1e-10)
                return true;

    return false;
}

bool Point::equals(Point p){
    if(pow(p.x - x,2.0) < 1e-10)
        if(pow(p.y - y,2.0) < 1e-10)
            if(pow(p.z - z,2.0) < 1e-10)
                return true;

    return false;
}

/************************************************************************************
*                               GETS & SETS                                         *
************************************************************************************/
double Point::getAbsoluteValue(){
    return sqrt(pow(x,2.0) + pow(y,2.0) + pow(z,2.0));
}

int Point::getID(){
    return id;
}

double Point::getPhi(){
    double phi = (asin(z/getAbsoluteValue()) * 180 / PI);

    if(phi < 0.0)
        while(phi< 0.0) phi += 360;

    if(phi > 360)
        while(phi > 360) phi -= 360;

    return phi;
}

double Point::getTheta(){
    double theta = (atan(y/x) * 180 / PI);

    if(x < 0.0)
        theta += 180;

    if(theta < 0.0)
        while(theta< 0.0) theta += 360;

    if(theta > 360)
        while(theta > 360) theta -= 360;

    return theta;
}

/************************************************************************************
*                               OPERATORS                                           *
************************************************************************************/
bool Point::operator = (Point b){
    x = (double)b.x;
    y = (double)b.y;
    z = (double)b.z;
}

bool Point::operator == (Point b){
    return equals(b);
}

bool Point::operator != (Point b){
    return !equals(b);
}

Point Point::operator + (Point b){
    return Point(id,x + b.x, y + b.y, z + b.z);
}

Point Point::operator + (int b){
    return Point(id,x + b, y + b, z + b);
}

Point Point::operator + (float b){
    return Point(id,x + b, y + b, z + b);
}

Point Point::operator + (double b){
    return Point(id,x + b, y + b, z + b);
}

Point Point::operator - (Point b){
    return Point(id,x - b.x, y - b.y, z - b.z);
}

Point Point::operator - (int b){
    return Point(id,x - b, y - b, z - b);
}

Point Point::operator - (float b){
    return Point(id,x - b, y - b, z - b);
}

Point Point::operator - (double b){
    return Point(id,x - b, y - b, z - b);
}

Point Point::operator * (Point b){
    return Point(id,x * b.x, y * b.y, z * b.z);
}

Point Point::operator * (double b){
    return Point(id,x * b, y * b, z * b);
}

Point Point::operator * (float b){
    return Point(id,x * b, y * b, z * b);
}

Point Point::operator * (int b){
    return Point(id,x * b, y * b, z * b);
}

Point Point::operator / (double b){
    return Point(id,x / b, y / b, z / b);
}

Point Point::operator / (float b){
    return Point(id,x / b, y / b, z / b);
}

Point Point::operator / (int b){
    return Point(id,x / b, y / b, z / b);
}
