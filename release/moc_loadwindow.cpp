/****************************************************************************
** Meta object code from reading C++ file 'loadwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../loadwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'loadwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_LoadWindow_t {
    QByteArrayData data[17];
    char stringdata0[417];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_LoadWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_LoadWindow_t qt_meta_stringdata_LoadWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "LoadWindow"
QT_MOC_LITERAL(1, 11, 11), // "treeChanged"
QT_MOC_LITERAL(2, 23, 0), // ""
QT_MOC_LITERAL(3, 24, 22), // "LoadWindow::Parameters"
QT_MOC_LITERAL(4, 47, 27), // "on_chk_Density_stateChanged"
QT_MOC_LITERAL(5, 75, 6), // "status"
QT_MOC_LITERAL(6, 82, 29), // "on_chk_Viscosity_stateChanged"
QT_MOC_LITERAL(7, 112, 32), // "on_chk_YoungModulus_stateChanged"
QT_MOC_LITERAL(8, 145, 22), // "on_LoadWindow_accepted"
QT_MOC_LITERAL(9, 168, 31), // "on_spin_density_editingFinished"
QT_MOC_LITERAL(10, 200, 28), // "on_spin_dens_editingFinished"
QT_MOC_LITERAL(11, 229, 25), // "on_spin_E_editingFinished"
QT_MOC_LITERAL(12, 255, 30), // "on_spin_points_editingFinished"
QT_MOC_LITERAL(13, 286, 30), // "on_spin_radius_editingFinished"
QT_MOC_LITERAL(14, 317, 33), // "on_spin_viscosity_editingFini..."
QT_MOC_LITERAL(15, 351, 28), // "on_spin_visc_editingFinished"
QT_MOC_LITERAL(16, 380, 36) // "on_spin_youngmodulus_editingF..."

    },
    "LoadWindow\0treeChanged\0\0LoadWindow::Parameters\0"
    "on_chk_Density_stateChanged\0status\0"
    "on_chk_Viscosity_stateChanged\0"
    "on_chk_YoungModulus_stateChanged\0"
    "on_LoadWindow_accepted\0"
    "on_spin_density_editingFinished\0"
    "on_spin_dens_editingFinished\0"
    "on_spin_E_editingFinished\0"
    "on_spin_points_editingFinished\0"
    "on_spin_radius_editingFinished\0"
    "on_spin_viscosity_editingFinished\0"
    "on_spin_visc_editingFinished\0"
    "on_spin_youngmodulus_editingFinished"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_LoadWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   79,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       4,    1,   82,    2, 0x08 /* Private */,
       6,    1,   85,    2, 0x08 /* Private */,
       7,    1,   88,    2, 0x08 /* Private */,
       8,    0,   91,    2, 0x08 /* Private */,
       9,    0,   92,    2, 0x08 /* Private */,
      10,    0,   93,    2, 0x08 /* Private */,
      11,    0,   94,    2, 0x08 /* Private */,
      12,    0,   95,    2, 0x08 /* Private */,
      13,    0,   96,    2, 0x08 /* Private */,
      14,    0,   97,    2, 0x08 /* Private */,
      15,    0,   98,    2, 0x08 /* Private */,
      16,    0,   99,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    2,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void LoadWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        LoadWindow *_t = static_cast<LoadWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->treeChanged((*reinterpret_cast< LoadWindow::Parameters(*)>(_a[1]))); break;
        case 1: _t->on_chk_Density_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->on_chk_Viscosity_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->on_chk_YoungModulus_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->on_LoadWindow_accepted(); break;
        case 5: _t->on_spin_density_editingFinished(); break;
        case 6: _t->on_spin_dens_editingFinished(); break;
        case 7: _t->on_spin_E_editingFinished(); break;
        case 8: _t->on_spin_points_editingFinished(); break;
        case 9: _t->on_spin_radius_editingFinished(); break;
        case 10: _t->on_spin_viscosity_editingFinished(); break;
        case 11: _t->on_spin_visc_editingFinished(); break;
        case 12: _t->on_spin_youngmodulus_editingFinished(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (LoadWindow::*_t)(LoadWindow::Parameters );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&LoadWindow::treeChanged)) {
                *result = 0;
                return;
            }
        }
    }
}

const QMetaObject LoadWindow::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_LoadWindow.data,
      qt_meta_data_LoadWindow,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *LoadWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *LoadWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_LoadWindow.stringdata0))
        return static_cast<void*>(const_cast< LoadWindow*>(this));
    return QDialog::qt_metacast(_clname);
}

int LoadWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 13)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 13;
    }
    return _id;
}

// SIGNAL 0
void LoadWindow::treeChanged(LoadWindow::Parameters _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
