#include "arteryTree.h"

/************************************************************************************
  Name:        arteryTree.cpp
  Copyright:   Version 1.0
  Author:      Igor Pires dos Santos
  Last Update: 17/02/2017 (DD/MM/YYYY)
  Release:     --/--/---- (DD/MM/YYYY)
  Description: Class implementation of a Tree structure of arteries.
************************************************************************************/

/************************************************************************************
        CONSTRUCTOR & DESTRUCTOR
************************************************************************************/
ArteryTree::ArteryTree(QString name, SmartObject* parent) : GraphicObject(name,parent){
    radius_scale = 1.0;
    N_arteries = 0;
    root = NULL;
    startPoint = NULL;

    imaginary_start = NULL;
    imaginary_end = NULL;

    idle_artery = idle_artery.fromRgb(163,22,33);
    hovered_artery = hovered_artery.fromRgb(135,241,255);
    selected_artery = selected_artery.fromRgb(245,187,0);
    setContextRange("PRESSURE");
}

ArteryTree::~ArteryTree(){
    //dtor
    if(N_arteries != 0){
        delete startPoint;
        delete root;
    }
}

/************************************************************************************
        CLASS METHODS
************************************************************************************/
void ArteryTree::resizeTransform(){
    vector<Point> points;

    points.push_back(*startPoint);

    if(root){
        resizeTransformAux(root,&points);
    }

    resetLimit();
    updateLimit(points);
}

void ArteryTree::resizeTransformAux(Artery* it, vector<Point>* points){
    if(it){
        points->push_back(it->getPoint());

        if(it->hasLeft())
            resizeTransformAux(it->getLeft(),points);

        if(it->hasRight())
            resizeTransformAux(it->getRight(),points);
    }
}

void ArteryTree::draw(){
    if((imaginary_start != NULL && imaginary_end != NULL) && mouse_being_used){
        QColor imaginay_color(255,0,0,100);

        double r = (*imaginary_end - *imaginary_start).getAbsoluteValue() * 0.01;

        //ARTERY PROXIMAL NODE
        drawSphere(imaginary_start, r * 1.1 * radius_scale,imaginay_color);

        //ARTERY DISTAL NODE
        drawSphere(imaginary_end,r * 1.1 * radius_scale,imaginay_color);

        //ARTERY TUBE
        drawCilynder(imaginary_start,imaginary_end,r* radius_scale);
    }

    if(getN() == 0 || root == NULL)
            return;

    Point end = root->getPoint();
    QColor c = idle_artery;


    if(!hasIterated()){
        if(root->isSelected()){
            c = selected_artery;
        } else if(root->isHovered()){
            c = hovered_artery;
        }

        setMaterialColor(c);

        //ARTERY PROXIMAL NODE
        drawSphere(startPoint,root->getRadius() * 1.1 * radius_scale,c);

        //ARTERY DISTAL NODE
        drawSphere(&end,root->getRadius() * 1.1 * radius_scale,c);

        //ARTERY TUBE
        drawCilynder(startPoint,&end,root->getRadius() * radius_scale);

        if(root->hasLeft())
            draw_aux(root->getLeft());

        if(root->hasRight())
            draw_aux(root->getRight());
    } else {

        //ARTERY PROXIMAL NODE
        double zero = 0.0;
        if(range.parameter == "PRESSURE"){
            zero = abs(root->getPressure(ZERO));
            setMinMax(zero);
        } else if(range.parameter == "FLOW"){
            zero = abs(root->getFlow(ZERO,startParams.q0));
            setMinMax(zero);
        }

        if(root->isSelected()){
            c = selected_artery;
        } else if(root->isHovered()){
            c = hovered_artery;
        } else {
            c = interpolateColor(zero);
        }

        setMaterialColor(c);

        if(root->isSelected() || root->isHovered())
            drawSphere(startPoint,root->getRadius() * 1.1 * radius_scale,c);
        else
            drawSphere(startPoint,root->getRadius() * 1.1 * radius_scale);

        //ARTERY DISTAL NODE
        double one = 1.0;
        if(range.parameter == "PRESSURE"){
            one = abs(root->getPressure(ONE));
            setMinMax(one);
        } else if(range.parameter == "FLOW"){
            one = abs(root->getFlow(ONE,startParams.q0));
            setMinMax(one);
        }

        if(root->isSelected()){
            c = selected_artery;
        } else if(root->isHovered()){
            c = hovered_artery;
        } else {
            c = interpolateColor(one);
        }

        setMaterialColor(c);
        if(root->isSelected() || root->isHovered())
            drawSphere(&end,root->getRadius() * 1.1 * radius_scale,c);
        else
            drawSphere(&end,root->getRadius() * 1.1 * radius_scale);

        //ARTERY TUBE
        vector<double> data;
        int stacks = getCilynderStacks();
        for(int i = 0; i <= stacks; i++){
            double pct = (double)i / (double)stacks;
            double p = 0.0;
            if(range.parameter == "PRESSURE"){
                p = abs(root->getPressure(pct));
                setMinMax(p);
            } else if(range.parameter == "FLOW"){
                p = abs(root->getFlow(pct,startParams.q0));
                setMinMax(p);
            }
            data.push_back(p);
        }

        if(root->isSelected() || root->isHovered())
            drawCilynder(startPoint,&end,root->getRadius() * radius_scale);
        else
            drawCilynder(startPoint,&end,root->getRadius() * radius_scale,data);

        if(root->hasLeft())
            draw_aux(root->getLeft());

        if(root->hasRight())
            draw_aux(root->getRight());
    }
}

bool ArteryTree::updateContextVisualParameter(QString field, QString value){
    if(field == "RADIUS_SCALE"){
        radius_scale = value.toDouble();
        return true;
    }

    return false;
}

bool ArteryTree::setContextRange(QString field){
    if(field == "PRESSURE"){
        range.parameter = "PRESSURE";
        range.max = -1.0;
        range.min = 1.0;
        return true;
    } else if(field == "FLOW"){
        range.parameter = "FLOW";
        range.max = -1.0;
        range.min = 1.0;
        return true;
    }

    return false;
}

QString ArteryTree::getLiveParameterList(){
    return QString("");
}

QString ArteryTree::getLiveParameter(QString name){
    return QString("");
}

DataType ArteryTree::getLiveParameterDataType(QString name){
    return undefined;
}

void ArteryTree::draw_aux(Artery* a){
    if(a == NULL)
        return;

    if(a->getFather() == NULL)
        return;

    Point start = a->getFather()->getPoint();
    Point end = a->getPoint();
    QColor c = idle_artery;

    if(!hasIterated()){
        if(a->isSelected()){
            c = selected_artery;
        } else if(a->isHovered()){
            c = hovered_artery;
        }

        setMaterialColor(c);

        //ARTERY DISTAL NODE
        drawSphere(&end,a->getRadius() * 1.1 * radius_scale,c);

        //ARTERY TUBE
        drawCilynder(&start,&end,a->getFather()->getRadius() * radius_scale,a->getRadius() * radius_scale);

        if(a->hasLeft())
            draw_aux(a->getLeft());

        if(a->hasRight())
            draw_aux(a->getRight());
    } else {
        //ARTERY DISTAL NODE
        if(root->isSelected()){
            c = selected_artery;
        } else if(root->isHovered()){
            c = hovered_artery;
        } else {
            c = interpolateColor(abs(a->getPressure(ONE)));
        }

        setMaterialColor(c);

        if(a->isSelected() || a->isHovered())
            drawSphere(&end,a->getRadius() * 1.1 * radius_scale,c);
        else
            drawSphere(&end,a->getRadius() * 1.1 * radius_scale);

        //ARTERY TUBE
        vector<double> data;
        int stacks = getCilynderStacks();
        for(int i = 0; i <= stacks; i++){
            double pct = (double)i / (double)stacks;
            double p = abs(a->getPressure(pct));
            data.push_back(p);
        }

        if(a->isSelected() || a->isHovered())
            drawCilynder(&start,&end,a->getFather()->getRadius() * radius_scale,a->getRadius() * radius_scale);
        else
            drawCilynder(&start,&end,a->getFather()->getRadius() * radius_scale,a->getRadius() * radius_scale,data);

        if(a->hasLeft())
            draw_aux(a->getLeft());

        if(a->hasRight())
            draw_aux(a->getRight());
    }
}

bool ArteryTree::addNode(Point* start, Point* finish, double radius, double youngModulus, double density, double viscosity){
    if(hasIterated())
        return false;

    Artery* a;

    //Treatments for the father artery (if it has one)
    if(N_arteries != 0){
        Artery* f = getArtery(start);

        //If father has not yet been set
        if(!f){
            return false;
        }

        //Parent is not a leaf anymore
        f->setLeaf(false);

        //Father-Son relationship
        if(!f->hasLeft()){
            a = new Artery (N_arteries,finish,MathHelper::euclideanDistance(start,finish),youngModulus,density,viscosity,radius,true,this,getGID());

            f->setLeft(a);
            a->setFather(f);

        } else if(!f->hasRight()){
            a = new Artery (N_arteries,finish,MathHelper::euclideanDistance(start,finish),youngModulus,density,viscosity,radius,true,this,getGID());

            f->setRight(a);
            a->setFather(f);

        } else {


            return false;

        }
    } else {
        this->startPoint = start;
        a = new Artery (N_arteries,finish,MathHelper::euclideanDistance(start,finish),youngModulus,density,viscosity,radius,true,this,getGID());
        root = a;
        this->updateLimit(start);
    }

    if(!a)
        return false;

    //Fixes Max and Min
    this->updateLimit(finish);

    N_arteries++;

    return true;
}

bool ArteryTree::compatibility(VTKFile *file){
    emit emit_log(SmartLogMessage(getSID(),"ARTERY TREE COMPATIBILITY RUN",SmartLogType::LOG_COMPATIBILITY));
    int n = file->getN();

    vector<QString> cellData = file->getCellDataInfo();

    //CHECKS IF REPRESENTS A BINARY TREE
    vector<int> sons;
    vector<int> fathers;

    for(int i = 0; i < n; i++){
        sons.push_back(0);
        fathers.push_back(0);
    }

    vector<Line> lines = file->getLines();
    for(int i = 0; i < lines.size(); i++){
        Line l = lines[i];

        if(l.n != TWO)
            return false;

        sons[l.points[ZERO]]++;
        fathers[l.points[ONE]]++;

        if(sons[l.points[ZERO]] > TWO)
            return false;

        if(fathers[l.points[ZERO]] > ONE)
            return false;
    }

    return true;
}

bool ArteryTree::load(VTKFile* file){
    delete root;
    delete startPoint;
    emit emit_log(SmartLogMessage(getSID(),"ARTERY TREE LOAD RUN",SmartLogType::LOG_LOAD));

    N_arteries = 0;

    if(!compatible(file))
        return false;

    if(!file->checkIntegrity())
        return false;

    vector<Point> points;
    vector<Line> lines;
    vector<Field> cell_data;
    vector<double> radius;
    vector<double> youngModulus;
    vector<double> density;
    vector<double> viscosity;
    vector<QString> radius_raw;
    vector<QString> youngModulus_raw;
    vector<QString> density_raw;
    vector<QString> viscosity_raw;

    //SETS PARAMETERS
    points = file->getPoints();
    lines = file->getLines();
    cell_data = getCellDataList();
    radius_raw = file->getCellData(cell_data[0].names);
    youngModulus_raw = file->getCellData(cell_data[1].names);
    density_raw = file->getCellData(cell_data[2].names);
    viscosity_raw = file->getCellData(cell_data[3].names);

    //RADIUS
    if(radius_raw.empty())
        return false;

    for(int i = 0; i < radius_raw.size(); i++){
        QString aux = radius_raw[i];
        radius.push_back(aux.toDouble());
    }

    //YOUNG MODULUS
    if(!youngModulus_raw.empty()){
        for(int i = 0; i < youngModulus_raw.size(); i++){
            QString aux = youngModulus_raw[i];
            youngModulus.push_back(aux.toDouble());
        }
    } else {
        for(int i = 0; i < lines.size(); i++){
            double aux = 1000000;
            youngModulus.push_back(aux);
        }
    }

    //DENSITY
    if(!density_raw.empty()){
        for(int i = 0; i < density_raw.size(); i++){
            QString aux = density_raw[i];
            density.push_back(aux.toDouble());
        }
    } else {
        for(int i = 0; i < lines.size(); i++){
            double aux = 1;
            density.push_back(aux);
        }
    }

    //VISCOSITY
    if(!viscosity_raw.empty()){
        for(int i = 0; i < viscosity_raw.size(); i++){
            QString aux = viscosity_raw[i];
            viscosity.push_back(aux.toDouble());
        }
    } else {
        for(int i = 0; i < lines.size(); i++){
            double aux = 0;
            viscosity.push_back(aux);
        }
    }

    if(points.size() < 2 || lines.empty())
        return false;

    //ADDS ARTERIES
    for(int i = 0; i < lines.size(); i++){
        Line l = lines[i];

        if(l.n != 2)
            return false;

        Point a = points[(l.points[0])];
        Point b = points[(l.points[1])];

        addNode(a.clone(),b.clone(),radius[i],youngModulus[i],density[i],viscosity[i]);
    }

    return true;
}

vector<Field> ArteryTree::getPointDataList(){
    vector<Field> list;
    return list;
}

vector<Field> ArteryTree::getCellDataList(){
    vector<Field> list;

    Field f;

    //RADIUS
    f.names = "raio radius RAIO RADIUS";
    f.stream = FIELD_IN;
    f.required = true;
    f.type = doublep;
    list.push_back(f);

    //YOUNG MODULUS
    f.names = "modulo_young young_modulus MODULO_YOUNG YOUNG_MODULUS";
    f.stream = FIELD_IN;
    f.required = false;
    f.type = doublep;
    list.push_back(f);

    //DENSITY
    f.names = "densidade density DENSIDADE DENSITY";
    f.stream = FIELD_IN;
    f.required = false;
    f.type = doublep;
    list.push_back(f);

    //VISCOSITY
    f.names = "viscosidade viscosity VISCOSIDADE VISCOSITY";
    f.stream = FIELD_IN;
    f.required = false;
    f.type = doublep;
    list.push_back(f);

    return list;
}

bool ArteryTree::iterate(double frequency, bool viscous, double viscousMultiplier, double phi0, double p, double q){
    if(N_arteries == 0)
        return false;

    if(!root)
        return false;

    root->setPhaseOne(frequency, viscous, viscousMultiplier, phi0);

    root->setPhaseTwo(p,root->getAdmittance());

    return true;
}

void ArteryTree::mouse_press_event(int width, int height, Point eye, QMouseEvent *event, GraphicTools tool){
    switch (tool){
    case SELECT:{
        if(getN() == 0 || root == NULL)
                return;

        if (event->buttons() & Qt::LeftButton) {
            Artery* a = mouse_ray(width,height,eye,mouse_last_pos);

            if(a){
                a->select();
            } else {
                root->unselect();
            }
        } else if (event->buttons() & Qt::RightButton) {
            root->unselect();
        }

        break;
    }
    case ADD_NODE:{
        if (event->buttons() & Qt::LeftButton) {
            if(mouse_being_used){
                mouse_being_used = false;
                GraphicObjectComposer component = new_artery();

                setGraphicComposerDialog(&component);

                new_artery(component);
            } else {
                mouse_being_used = true;
            }
        } else if (event->buttons() & Qt::RightButton) {

        } else if(mouse_being_used) {
            mouse_being_used = false;
        }

        break;
    }
    case EDIT_NODE:{
        if(getN() == 0 || root == NULL)
                return;

        if (event->buttons() & Qt::LeftButton) {

        } else if (event->buttons() & Qt::RightButton) {

        }

        break;
    }
    case REMOVE_NODE:{
        if(getN() == 0 || root == NULL)
                return;

        if (event->buttons() & Qt::LeftButton) {

        } else if (event->buttons() & Qt::RightButton) {

        }

        break;
    }
    }
}

void ArteryTree::mouse_move_event(int width, int height, Point eye, QMouseEvent *event, GraphicTools tool){
    switch (tool){
    case SELECT:{
        if(getN() == 0 || root == NULL)
                return;


        if (event->buttons() & Qt::LeftButton) {

        } else if (event->buttons() & Qt::RightButton) {

        } else {
            Artery* a = mouse_ray(width,height,eye,mouse_last_pos);

            if(a){
                a->hover();
            } else {
                root->unhover();
            }
        }

        break;
    }
    case ADD_NODE:{
        if (event->buttons() & Qt::LeftButton) {
            if(mouse_being_used){
                if(getN() == 0){
                    if(imaginary_end == NULL && imaginary_start == NULL){
                        imaginary_start = new Point();
                        imaginary_start->operator=(getModelPoint(mouse_last_pos,eye,width,height));
                        imaginary_end = imaginary_start->clone();
                    } else {
                        delete imaginary_end;
                        imaginary_end = new Point();
                        imaginary_end->operator=(getModelPoint(mouse_last_pos,eye,width,height));
                    }
                } else {
                    Artery* selected = NULL;

                    if(hasSelected())
                        selected = getSelectedLeaf();
                    else if (root != NULL)
                        selected = root;
                    else {
                        mouse_being_used = false;
                        return;
                    }

                    if(imaginary_end == NULL && imaginary_start == NULL){
                        imaginary_start = new Point();
                        imaginary_start->operator=(getModelPoint(mouse_last_pos,eye,width,height));
                        imaginary_end = imaginary_start->clone();
                    } else {
                        delete imaginary_end;
                        imaginary_end = new Point();
                        imaginary_end->operator=(getModelPoint(mouse_last_pos,eye,width,height));
                    }
                }
            }
        } else if (event->buttons() & Qt::RightButton) {

        } else {
            if(mouse_being_used){
                if(getN() == 0){
                    if(imaginary_end == NULL && imaginary_start == NULL){
                        imaginary_start = new Point();
                        imaginary_start->operator=(getModelPoint(mouse_last_pos,eye,width,height));
                        imaginary_end = imaginary_start->clone();
                    } else {
                        delete imaginary_end;
                        imaginary_end = new Point();
                        imaginary_end->operator=(getModelPoint(mouse_last_pos,eye,width,height));
                    }
                } else {
                    Artery* selected = NULL;

                    if(hasSelected())
                        selected = getSelectedLeaf();
                    else if (root != NULL)
                        selected = root;
                    else {
                        mouse_being_used = false;
                        return;
                    }

                    if(imaginary_end == NULL && imaginary_start == NULL){
                        imaginary_start = new Point();
                        imaginary_start->operator=(getModelPoint(mouse_last_pos,eye,width,height));
                        imaginary_end = imaginary_start->clone();
                    } else {
                        delete imaginary_end;
                        imaginary_end = new Point();
                        imaginary_end->operator=(getModelPoint(mouse_last_pos,eye,width,height));
                    }
                }
            }
        }

        break;
    }
    case EDIT_NODE:{
        if(getN() == 0 || root == NULL)
                return;

        if (event->buttons() & Qt::LeftButton) {

        } else if (event->buttons() & Qt::RightButton) {

        } else {

        }

        break;
    }
    case REMOVE_NODE:{
        if(getN() == 0 || root == NULL)
                return;

        if (event->buttons() & Qt::LeftButton) {

        } else if (event->buttons() & Qt::RightButton) {

        } else {
            Artery* a = mouse_ray(width,height,eye,mouse_last_pos);

            if(a){
                a->hover();
            } else {
                root->unhover();
            }
        }

        break;
    }
    }
}

vector<Field> ArteryTree::getStartParameters(){
    vector<Field> params;

   Field aux;
   aux.names = "FREQUENCY";
   aux.required = false;
   aux.stream = FIELD_IN;
   aux.type = doublep;
   aux.current_value = aux.names.asprintf("%f",startParams.frequency);

    params.push_back(aux);

    aux.names = "VISCOUS";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = _boolean;
    aux.current_value = aux.names.asprintf("%s",((startParams.viscous)?"true":"false"));

    params.push_back(aux);

    aux.names = "VISCOUS_MULTIPLIER";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = doublep;
    aux.current_value = aux.names.asprintf("%f",startParams.viscousMultiplier);

    params.push_back(aux);

    aux.names = "P0";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = doublep;
    aux.current_value = aux.names.asprintf("%f",startParams.p0);

    params.push_back(aux);

    aux.names = "Q0";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = doublep;
    aux.current_value = aux.names.asprintf("%f",startParams.q0);

    params.push_back(aux);

    aux.names = "PHI0";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = doublep;
    aux.current_value = aux.names.asprintf("%f",startParams.phi0);

    params.push_back(aux);

    aux.names = "DELTA_FREQUENCY";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = doublep;
    aux.current_value = aux.names.asprintf("%f",startParams.deltaFrequency);

    params.push_back(aux);

    aux.names = "DELTA_PHI0";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = doublep;
    aux.current_value = aux.names.asprintf("%f",startParams.deltaPhi0);

    params.push_back(aux);

    aux.names = "DELTA_VISCOUS_MULTIPLIER";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = doublep;
    aux.current_value = aux.names.asprintf("%f",startParams.deltaViscousMultiplier);

    params.push_back(aux);

    return params;
}

vector<Field> ArteryTree::getContextVisualParameters(){
    vector<Field> params;

    Field aux;
    aux.names = "RADIUS_SCALE";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = doublep;
    aux.current_value = aux.names.asprintf("%f",radius_scale);

    params.push_back(aux);

    return params;
}

vector<QString> ArteryTree::getRanges(){
    vector<QString> params;

    params.push_back("PRESSURE");

    params.push_back("FLOW");

    return params;
}

bool ArteryTree::updateStartParameter(QString field, QString value){
    if(field == "FREQUENCY"){
        startParams.frequency = value.toDouble();
        return true;
    } else if(field == "VISCOUS"){
        startParams.viscous = (value.toInt() == 1)? true : false;
        return true;
    } else if(field == "VISCOUS_MULTIPLIER"){
        startParams.viscousMultiplier = value.toDouble();
        return true;
    } else if(field == "P0"){
        startParams.p0 = value.toDouble();
        return true;
    } else if(field == "Q0"){
        startParams.q0 = value.toDouble();
        return true;
    } else if(field == "PHI0"){
        startParams.phi0 = value.toDouble();
        return true;
    } else if(field == "DELTA_FREQUENCY"){
        startParams.deltaFrequency = value.toDouble();
        return true;
    } else if(field == "DELTA_PHI0"){
        startParams.deltaPhi0 = value.toDouble();
        return true;
    } else if(field == "DELTA_VISCOUS_MULTIPLIER"){
        startParams.deltaViscousMultiplier = value.toDouble();
        return true;
    }

    return false;
}

vector<QString> ArteryTree::getExtrapolations(){
    vector<QString> aux;

    aux.push_back("LIVE_ARTERY_PARAM_GRAPHIC");

    return aux;
}

GraphicObject* ArteryTree::extrapolateContext(QString extrapolation){
    GraphicObject* r;
    if(extrapolation == "LIVE_ARTERY_PARAM_GRAPHIC"){
        r = new LiveArteryParamGraphic(getSelected(),"Live Graphic",this);
        r->start();
        addSlave(r);
    } else {
        r = NULL;
    }

    return r;
}

void ArteryTree::updateRadiusScale(double value){
    if(value <= 0.01 || value > 10)
        return;

    radius_scale = value;
}

bool ArteryTree::hasSelected(){
    if(root != NULL)
        if(root->isSelected())
            return true;

    return false;
}

Artery* ArteryTree::getSelectedLeaf(){
    Artery* it = root;

    if(!root->isSelected())
        return NULL;

    while (it != NULL && it->isSelected()) {
        if(it->getLeft()->isSelected()){
            it = it->getLeft();
        } else if (it->getRight()->isSelected()) {
            it = it ->getRight();
        } else {
            return it;
        }
    }

    return it;
}

GraphicObjectComposer ArteryTree::new_artery(){
    GraphicObjectComposer component;

    component.name = "ARTERY";

    if(imaginary_end != NULL)
        component.point = *imaginary_end;

    vector<Field> fields;
    Field f;

    f.names = "RADIUS";
    f.stream = FIELD_IN;
    f.required = false;
    f.type = doublep;
    f.current_value = QString::asprintf("%f",DEFAULT_RADIUS);

    fields.push_back(f);

    f.names = "YOUNG_MODULUS";
    f.stream = FIELD_IN;
    f.required = false;
    f.type = doublep;
    f.current_value = QString::asprintf("%f",DEFAULT_YOUNG_MODULUS);

    fields.push_back(f);

    f.names = "DENSITY";
    f.stream = FIELD_IN;
    f.required = false;
    f.type = doublep;
    f.current_value = QString::asprintf("%f",DEFAULT_DENSITY);

    fields.push_back(f);

    f.names = "VISCOSITY";
    f.stream = FIELD_IN;
    f.required = false;
    f.type = doublep;
    f.current_value = QString::asprintf("%f",DEFAULT_VISCOSITY);

    fields.push_back(f);

    component.fields = fields;

    return component;
}

GraphicObjectComposer ArteryTree::edit_artery(Artery* a){
    GraphicObjectComposer component;

    component.name = QString::asprintf("ARTERY ID=%d",a->getID());
    component.point = a->getPoint();

    vector<Field> fields;
    Field f;

    f.names = "RADIUS";
    f.stream = FIELD_IN;
    f.required = false;
    f.type = doublep;
    f.current_value = QString::asprintf("%f",a->getRadius());

    fields.push_back(f);

    f.names = "YOUNG_MODULUS";
    f.stream = FIELD_IN;
    f.required = false;
    f.type = doublep;
    f.current_value = QString::asprintf("%f",a->getViscosity());

    fields.push_back(f);

    f.names = "DENSITY";
    f.stream = FIELD_IN;
    f.required = false;
    f.type = doublep;
    f.current_value = QString::asprintf("%f",a->getDensity());

    fields.push_back(f);

    component.fields = fields;

    return component;
}

bool ArteryTree::new_artery(GraphicObjectComposer artery_component){
    if(imaginary_start == NULL || imaginary_end == NULL)
        return false;

    double radius = 0.0;
    double youngModulus = 0.0;
    double viscosity = 0.0;
    double density = 0.0;
    int read = 0;

    for(int i = 0; i < artery_component.fields.size(); i++){
        Field f = artery_component.fields[i];

        if(f.names == "RADIUS"){
            radius = f.current_value.toDouble();
            read++;
        } else if (f.names == "YOUNG_MODULUS") {
            youngModulus = f.current_value.toDouble();
            read++;
        } else if (f.names == "VISCOSITY") {
            viscosity = f.current_value.toDouble();
            read++;
        } else if (f.names == "DENSITY") {
            density = f.current_value.toDouble();
            read++;
        } else {
            return false;
        }
    }

    if(read != 4){
        return false;
    }

    return addNode(imaginary_start,imaginary_end,radius,youngModulus,density,viscosity);
}

bool ArteryTree::edit_artery(GraphicObjectComposer artery_component, Artery* a){
    if(imaginary_start == NULL || imaginary_end == NULL)
        return false;

    double radius = 0.0;
    double youngModulus = 0.0;
    double viscosity = 0.0;
    double density = 0.0;
    int read = 0;

    for(int i = 0; i < artery_component.fields.size(); i++){
        Field f = artery_component.fields[i];

        if(f.names == "RADIUS"){
            radius = f.current_value.toDouble();
            read++;
        } else if (f.names == "YOUNG_MODULUS") {
            youngModulus = f.current_value.toDouble();
            read++;
        } else if (f.names == "VISCOSITY") {
            viscosity = f.current_value.toDouble();
            read++;
        } else if (f.names == "DENSITY") {
            density = f.current_value.toDouble();
            read++;
        } else {
            return false;
        }
    }

    if(read != 4){
        return false;
    }

    a->setRadius(radius);
    a->setYoungModulus(youngModulus);
    a->setViscosity(viscosity);
    a->setDensity(density);

    return true;
}

vector<QString> ArteryTree::print(){
    QString buffer= "";
    vector<QString> cmds;

    cmds.push_back("/************************************************************************************");
    cmds.push_back("*                          ARTERY TREE                                              *");
    cmds.push_back("************************************************************************************/");

    buffer = ("NUMBER OF NODES: " + buffer.asprintf("%d",N_arteries) );
    cmds.push_back(buffer);
    if(root){
        buffer = (buffer.fromStdString("FREQ: ") + buffer.asprintf("%.4f",root->getFrequency()));
        cmds.push_back(buffer);
        buffer = (buffer.fromStdString("ANGULAR FREQ: ") + buffer.asprintf("%.4f",root->getAngularFrequency()));
        cmds.push_back(buffer);
        buffer = (buffer.fromStdString("ITERATED: ") + buffer.asprintf("%s", hasIterated() ? "YES" : "NO") );
        cmds.push_back(buffer);

        printArteriesLog(root,cmds);
    }

    
    cmds.push_back("***************                   END     OF     TREE                   ************/");

    emit emit_log(SmartLogMessage(getSID(),"ARTERY TREE PRINTED"));
    return cmds;
}

void ArteryTree::printArteriesLog(Artery* at, vector<QString> &commands){
    complex<double> Ye1 = (at->hasRight())?at->getRight()->getEffectiveAdmittance() : 0.0;
    complex<double> Ye2 = (at->hasLeft())?at->getLeft()->getEffectiveAdmittance() : 0.0;
    complex<double> Yt = Ye1 + Ye2;

    QString buffer = "";
    Point p = at->getPoint();

    if(at->hasFather()){//Every other artery
        Point fp = at->getFather()->getPoint();
        buffer = buffer.fromStdString("(") + buffer.asprintf("%.4f",fp.x) + "," + buffer.asprintf("%.4f",fp.y) + ") -> (" + buffer.asprintf("%.4f",p.x) + "," + buffer.asprintf("%.4f",p.y) + ")";
        commands.push_back(buffer);

        Point* aux = p.clone();
        aux->operator -(fp);
        buffer = buffer.fromStdString("(") + buffer.asprintf("%.4f",aux->x) + "," + buffer.asprintf("%.4f",aux->y) + ")";
        commands.push_back(buffer);
        delete aux;
    } else { // Root artery
        buffer = buffer.fromStdString("(") + buffer.asprintf("%.4f",startPoint->x) + "," + buffer.asprintf("%.4f",startPoint->y) + ") -> (" + buffer.asprintf("%.4f",p.x) + "," + buffer.asprintf("%.4f",p.y) + ")";
        commands.push_back(buffer);

        Point* aux = p.clone();
        aux->operator -(*startPoint);
        buffer = buffer.fromStdString("(") + buffer.asprintf("%.4f",aux->x) + "," + buffer.asprintf("%.4f",aux->y) + ")";
        commands.push_back(buffer);
        delete aux;
    }

   buffer = buffer.fromStdString(" -- ARTERY #") + buffer.asprintf("%d",at->getID()) + " -- ";
   commands.push_back(buffer);

   if(at->hasFather()){
       buffer = buffer.fromStdString(" -- FATHER #") + buffer.asprintf("%d",at->getFather()->getID()) + " -- ";
       commands.push_back(buffer);
   }
   if(at->hasRight()){
       buffer = buffer.fromStdString(" -- RIGHT #") + buffer.asprintf("%d",at->getRight()->getID()) + " -- ";
       commands.push_back(buffer);
   }
   if(at->hasLeft()){
       buffer = buffer.fromStdString(" -- LEFT #") + buffer.asprintf("%d",at->getLeft()->getID()) + " -- ";
       commands.push_back(buffer);
   }
    buffer = buffer.fromStdString(" RADIUS.........................") +  buffer.asprintf("%.4f",at->getRadius())  +   " in [cm] ";
    commands.push_back(buffer);
    buffer = buffer.fromStdString(" LENGTH.........................") +  buffer.asprintf("%.4f",at->getLength())  +   " in [cm] ";
    commands.push_back(buffer);
    buffer = buffer.fromStdString(" WALL THICKNESS.................") + buffer.asprintf("%.4f",at->getThickness()) +   " in [cm] ";
    commands.push_back(buffer);
    buffer = buffer.fromStdString(" YOUNGs MODULUS.................") +  buffer.asprintf("%.4f",at->getYoungModulus())  + " in [g/cm*s^2] ";
    commands.push_back(buffer);
    buffer = buffer.fromStdString(" DENSITY........................") +  buffer.asprintf("%.4f",at->getDensity()) + " in [g/cm^3] ";
    commands.push_back(buffer);
    buffer = buffer.fromStdString(" VISCOSITY......................") + buffer.asprintf("%.4f",at->getViscosity()) + " in [g/cm*s] ";
    commands.push_back(buffer);
    buffer = buffer.fromStdString(" WAVE SPEED.....................") + buffer.asprintf("%.4f",at->getWaveSpeed()) + "  in [cm/s] ";
    commands.push_back(buffer);
    buffer = buffer.fromStdString(" ALPHA..........................") +   buffer.asprintf("%.4f",at->getAlpha())  +    "  in [] ";
    commands.push_back(buffer);
    buffer = buffer.fromStdString(" VISCOUS FACTOR.................(") + buffer.asprintf("%.4f",at->getViscousFactor().real()) + "," + buffer.asprintf("%.4f",at->getViscousFactor().imag()) + ")  in [] ";
    commands.push_back(buffer);
    buffer = buffer.fromStdString(" PHASE ANGLE....................") + buffer.asprintf("%.4f",at->getPhaseAngle()) +    "  in [] ";
    commands.push_back(buffer);
    buffer = buffer.fromStdString(" VISCOUS YOUNGs MODULUS.........(") + buffer.asprintf("%.4f",at->getViscousYoungsModulus().real()) + "," + buffer.asprintf("%.4f",at->getViscousYoungsModulus().imag()) + ")  in [] ";
    commands.push_back(buffer);
    buffer = buffer.fromStdString(" VISCOUS WAVESPEED..............(") + buffer.asprintf("%.4f",at->getViscousWaveSpeed().real()) + "," + buffer.asprintf("%.4f",at->getViscousWaveSpeed().imag()) + ")  in [] ";
    commands.push_back(buffer);
    buffer = buffer.fromStdString(" ADMITTANCE.....................(") + buffer.asprintf("%.4f",at->getAdmittance().real()) + "," + buffer.asprintf("%.4f",at->getAdmittance().imag()) + ")  in [cm^4*s/g] ";
    commands.push_back(buffer);
    buffer = buffer.fromStdString(" BETA...........................(") + buffer.asprintf("%.4f",at->getBeta().real()) + "," + buffer.asprintf("%.4f",at->getBeta().imag()) + ")  in [] ";
    commands.push_back(buffer);
    buffer = buffer.fromStdString(" REFLECTION COEF................(") + buffer.asprintf("%.4f",at->getReflectionCoef().real()) + "," + buffer.asprintf("%.4f",at->getReflectionCoef().imag()) + ")  in [] ";
    commands.push_back(buffer);
    buffer = buffer.fromStdString(" EFFECTIVE ADMITTANCE...........(") + buffer.asprintf("%.4f",at->getEffectiveAdmittance().real()) + "," + buffer.asprintf("%.4f",at->getEffectiveAdmittance().imag()) + ")  in [cm^4/g] ";
    commands.push_back(buffer);
    complex<double> deltaY = at->getEffectiveAdmittance() - at->getAdmittance();
    buffer = buffer.fromStdString("               Ye - Y..........(") + buffer.asprintf("%.4f",deltaY.real()) + "," + buffer.asprintf("%.4f",deltaY.imag()) + ")  in [cm^4/g] ";
    commands.push_back(buffer);
    buffer = buffer.fromStdString("               Ye1..............(") + buffer.asprintf("%.4f",Ye1.real()) + "," + buffer.asprintf("%.4f",Ye1.imag()) + ")  in [cm^4/g] ";
    commands.push_back(buffer);
    buffer = buffer.fromStdString("               Ye2..............(") + buffer.asprintf("%.4f",Ye2.real()) + "," + buffer.asprintf("%.4f",Ye2.imag()) + ")  in [cm^4/g] ";
    commands.push_back(buffer);
    buffer = buffer.fromStdString("               Yet..............(") + buffer.asprintf("%.4f",Yt.real())  + "," + buffer.asprintf("%.4f",Yt.imag()) + ")  in [cm^4/g] ";
    commands.push_back(buffer);
    buffer = buffer.fromStdString(" MEDIUM_PRESSURE................(") + buffer.asprintf("%.4f",at->getMediumPressure().real()) + "," + buffer.asprintf("%.4f",at->getMediumPressure().imag()) + ")";
    commands.push_back(buffer);
    buffer = buffer.fromStdString(" PRESSURE.( X = 0.0 )...........(") + buffer.asprintf("%.4f",at->getPressure(0.0).real()) + "," + buffer.asprintf("%.4f",at->getPressure(0.0).imag()) + ")";
    commands.push_back(buffer);
    buffer = buffer.fromStdString(" PRESSURE.( X = 1.0 )...........(") + buffer.asprintf("%.4f",at->getPressure().real()) + "," + buffer.asprintf("%.4f",at->getPressure().imag()) + ")";
    commands.push_back(buffer);
    buffer = buffer.fromStdString(" MEDIUM_FLOW................(") + buffer.asprintf("%.4f",at->getFlow().real()) + "," + buffer.asprintf("%.4f",at->getFlow().imag()) + ")";
    commands.push_back(buffer);
    buffer = buffer.fromStdString(" FLOW.( X = 0.0 )...........(") + buffer.asprintf("%.4f",at->getFlow(0.0,root->getEffectiveAdmittance()).real()) + "," + buffer.asprintf("%.4f",at->getFlow(0.0,root->getEffectiveAdmittance()).imag()) + ")";
    commands.push_back(buffer);
    buffer = buffer.fromStdString(" FLOW.( X = 1.0 )...........(") + buffer.asprintf("%.4f",at->getFlow(1.0,root->getEffectiveAdmittance()).real()) + "," + buffer.asprintf("%.4f",at->getFlow(1.0,root->getEffectiveAdmittance()).imag()) + ")";
    commands.push_back(buffer);
    buffer = buffer.fromStdString(" STATUS.........................(") + buffer.asprintf(((at->isSelected())?"SELECTED":(at->isHovered())?"HOVERED":"IDLE")) + ")";
    commands.push_back(buffer);
    buffer = buffer.fromStdString(" LEAF...........................") +   buffer.asprintf("%s", at->isLeaf() ? "true" : "false") ;
    commands.push_back(buffer);

    if(at->hasRight()){
        printArteriesLog(at->getRight(),commands);
    }
    if(at->hasLeft()){
        printArteriesLog(at->getLeft(),commands);
    }
}

vector<QString> ArteryTree::raw(){
    vector<QString> raw;
    QString line;

    line = line.asprintf("# vtk DataFile Version 3.0");
    raw.push_back(line);
    line = line.asprintf("PRESSURE PEAKING ARTERY TREE OUTPUT SmartID = %d",getID());
    raw.push_back(line);
    line = line.asprintf("ASCII");
    raw.push_back(line);
    line = line.asprintf("DATASET POLYDATA");
    raw.push_back(line);
    line = line.asprintf("POINTS  %d  double", N_arteries + 1);
    raw.push_back(line);
    line = line.asprintf("%f  %f  %f", startPoint->x, startPoint->y, startPoint->z);
    raw.push_back(line);

    root->printField(Artery::POINTS,&raw);

    line = line.asprintf("");
    raw.push_back(line);
    line = line.asprintf("LINES %d %d", N_arteries, N_arteries * 3);
    raw.push_back(line);
    line = line.asprintf("2  0  1");
    raw.push_back(line);


    root->printField(Artery::LINES,&raw);


    line = line.asprintf("");
    raw.push_back(line);

    if(hasIterated()){
        line = line.asprintf("POINT_DATA  %d", N_arteries + 1);
        raw.push_back(line);
        line = line.asprintf("scalars pressao double");
        raw.push_back(line);
        line = line.asprintf("LOOKUP_TABLE default");
        raw.push_back(line);
        line = line.asprintf("%f",root->getPressure(0.0));
        raw.push_back(line);

        root->printField(Artery::PRESSURE,&raw);

        line = line.asprintf("scalars fluxo double");
        raw.push_back(line);
        line = line.asprintf("LOOKUP_TABLE default");
        raw.push_back(line);
        line = line.asprintf("%f",root->getFlow());
        raw.push_back(line);

        root->printField(Artery::FLOW,&raw);

        line = line.asprintf("");
        raw.push_back(line);
    }

    line = line.asprintf("CELL_DATA  %d",N_arteries);
    raw.push_back(line);
    line = line.asprintf("scalars raio double");
    raw.push_back(line);
    line = line.asprintf("LOOKUP_TABLE default");
    raw.push_back(line);

    root->printField(Artery::RADIUS,&raw);

    line = line.asprintf("scalars modulo_young double");
    raw.push_back(line);
    line = line.asprintf("LOOKUP_TABLE default");
    raw.push_back(line);

    root->printField(Artery::YOUNG_MOD,&raw);

    line = line.asprintf("scalars densidade double");
    raw.push_back(line);
    line = line.asprintf("LOOKUP_TABLE default");
    raw.push_back(line);

    root->printField(Artery::DENSITY,&raw);

    line = line.asprintf("scalars viscosidade double");
    raw.push_back(line);
    line = line.asprintf("LOOKUP_TABLE default");
    raw.push_back(line);

    root->printField(Artery::VISCOSITY,&raw);

    if(hasIterated()){
        line = line.asprintf("scalars tamanho double");
        raw.push_back(line);
        line = line.asprintf("LOOKUP_TABLE default");
        raw.push_back(line);

        root->printField(Artery::LENGTH,&raw);

        line = line.asprintf("scalars velocidade_onda double");
        raw.push_back(line);
        line = line.asprintf("LOOKUP_TABLE default");
        raw.push_back(line);

        root->printField(Artery::WAVESPEED,&raw);

        line = line.asprintf("scalars beta double");
        raw.push_back(line);
        line = line.asprintf("LOOKUP_TABLE default");
        raw.push_back(line);

        root->printField(Artery::BETA,&raw);

        line = line.asprintf("scalars admitancia double");
        raw.push_back(line);
        line = line.asprintf("LOOKUP_TABLE default");
        raw.push_back(line);

        root->printField(Artery::ADMITTANCE,&raw);

        line = line.asprintf("scalars coef_reflexao double");
        raw.push_back(line);
        line = line.asprintf("LOOKUP_TABLE default");
        raw.push_back(line);

        root->printField(Artery::REFLECTION_COEF,&raw);

        line = line.asprintf("scalars admitancia_efetiva double");
        raw.push_back(line);
        line = line.asprintf("LOOKUP_TABLE default");
        raw.push_back(line);

        root->printField(Artery::EFFECTIVE_ADMITTANCE,&raw);

        line = line.asprintf("scalars pressao_media double");
        raw.push_back(line);
        line = line.asprintf("LOOKUP_TABLE default");
        raw.push_back(line);

        root->printField(Artery::MEDIUM_PRESSURE,&raw);

    }

    emit emit_log(SmartLogMessage(getSID(),"ARTERY TREE RAW DATA SAVED"));
    return raw;
}


vector<QString> ArteryTree::data(){
    vector<QString> log;
    printArteriesLog(root,log);

    emit emit_log(SmartLogMessage(getSID(),"ARTERY TREE DATA SAVED"));
    return log;
}

bool ArteryTree::initialize(){
    time.deltaT = 0.1;

    return iterate(startParams.frequency,startParams.viscous,startParams.viscousMultiplier,startParams.phi0,startParams.p0,startParams.q0);
}

double ArteryTree::iteration(){
    clock_t begin = std::clock();

    if(time.iterations == 0){
        if(!iterate(startParams.frequency,startParams.viscous,startParams.viscousMultiplier,startParams.phi0,startParams.p0,startParams.q0)){
            crash();
            return -1;
        }
    }

    //ADDS DELTA TO START PARAMETERS
    startParams.frequency += startParams.deltaFrequency;
    startParams.viscousMultiplier += startParams.deltaViscousMultiplier;
    startParams.phi0 += startParams.deltaPhi0;

    clock_t end = std::clock();
    return double(end - begin) / CLOCKS_PER_SEC;
}

void ArteryTree::finish(){
    status = Finished;
}

/************************************************************************************
*                                  GETS & SETS                                      *
************************************************************************************/
double ArteryTree::getAngularFrequency(){
    return root->getAngularFrequency();
}

Artery* ArteryTree::getArtery(Point* finish){
    if(!root)
        return NULL;

    return root->findArtery(finish);
}

double ArteryTree::getDeltaT(){
    return time.deltaT;
}

void ArteryTree::getLeafAux(Artery* a, vector<Artery*> list){
    if(a->isLeaf()){
        list.push_back(a);
    } else {
        if(a->hasRight())
            getLeafAux(a->getRight(),list);

        if(a->hasLeft())
            getLeafAux(a->getLeft(),list);
    }
}

vector<Artery*> ArteryTree::getLeafs(){
    vector<Artery*> list;

    getLeafAux(root, list);

    return list;
}

vector<Artery*> ArteryTree::getSelected(){
    vector<Artery*> aux;

    if(root->isSelected()){
        Artery* it = root;

        while(it != NULL){
            aux.push_back(it);

            if(!it->hasLeft() && !it->hasRight()){
                it = NULL;
            }else if(it->hasLeft() && it->hasRight()){
                if(it->getLeft()->isSelected())
                    it = it->getLeft();
                else if(it->getRight()->isSelected())
                    it = it->getRight();
                else
                    it = NULL;
            } else if(it->hasLeft()){
                if(it->getLeft()->isSelected())
                    it = it->getLeft();
                else
                    it = NULL;
            } else {
                if(it->getRight()->isSelected())
                    it = it->getRight();
                else
                    it = NULL;
            }
        }
    } else {
        Artery* it = root;

        while(it != NULL){
            aux.push_back(it);

            if(it->hasLeft()){
                it = it->getLeft();
            } else {
                it = NULL;
            }
        }
    }

    return aux;
}

int ArteryTree::getN(){
    return N_arteries;
}

double ArteryTree::getP0(){
    return startParams.p0;
}

double ArteryTree::getRootImpedance(){
    return 1 / abs(root->getEffectiveAdmittance());
}

Point* ArteryTree::getStart(){
    return startPoint;
}

void ArteryTree::setDensity(float density){
    setDensityAux(root,density);
}

void ArteryTree::setDensityAux(Artery* it, float density){
    if(it == NULL)
        return;

    it->setDensity(density);

    if(it->hasLeft())
        setDensityAux(it->getLeft(),density);

    if(it->hasRight())
        setDensityAux(it->getRight(),density);
}

void ArteryTree::setViscosity(float viscosity){
    setViscosityAux(root,viscosity);
}

void ArteryTree::setViscosityAux(Artery* it, float viscosity){
    if(it == NULL)
        return;

    it->setDensity(viscosity);

    if(it->hasLeft())
        setViscosityAux(it->getLeft(),viscosity);

    if(it->hasRight())
        setViscosityAux(it->getRight(),viscosity);
}

void ArteryTree::setYoungModulus(float E){
    setDensityAux(root,E);
}

void ArteryTree::setYoungModulusAux(Artery* it, float E){
    if(it == NULL)
        return;

    it->setYoungModulus(E);

    if(it->hasLeft())
        setYoungModulusAux(it->getLeft(),E);

    if(it->hasRight())
        setYoungModulusAux(it->getRight(),E);
}

Artery* ArteryTree::mouse_ray(int width, int height, Point eye, Point mouse){
    return mouse_ray(width,height,eye,mouse,root);
}

Artery* ArteryTree::mouse_ray(int width, int height, Point eye, Point mouse, Artery* it){
    if(it == NULL)
        return NULL;

    //GETS DISTAL POINT
    Point p = it->getPoint();

    //PUTS DISTAL POINT IN [-1,1] FORMAT
    //DOES TRANSFORMATIONS DONE IN THE POINT
    p = getCanvasPoint(p,eye,(float)width,(float)height);
    float prox = MathHelper::euclideanDistance(&p,&mouse);

    if(prox <= MOUSE_PROX)
        return it;

    if(it->hasLeft()){
        Artery* a = mouse_ray(width,height,eye,mouse,it->getLeft());
        if(a)
            return a;
    }

    if(it->hasRight()){
        Artery* a = mouse_ray(width,height,eye,mouse,it->getRight());
        if(a)
            return a;
    }

    return NULL;
}
