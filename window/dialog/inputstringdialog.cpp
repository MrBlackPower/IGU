#include "inputstringdialog.h"
#include "ui_inputstringdialog.h"

InputStringDialog::InputStringDialog(QString name, QString current_value, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::InputStringDialog)
{
    ui->setupUi(this);

    string = current_value;
    this->name = name;
    set = false;

    updateInterface();
}

InputStringDialog::~InputStringDialog()
{
    delete ui;
}

QString InputStringDialog::getString(){
    return string;
}

bool InputStringDialog::Set(){
    return set;
}

void InputStringDialog::updateInterface(){
    ui->ln_string->setText(string);
    ui->label->setText(QString::asprintf("Set the string `%s`:",name.toStdString().data()));
}

void InputStringDialog::on_buttonBox_accepted()
{
    set = true;

    emit stringSet();
    emit pushString(string);
}

void InputStringDialog::on_buttonBox_rejected()
{
    emit cancelled();
}

void InputStringDialog::on_ln_string_textChanged(const QString &arg1)
{
    string = ui->ln_string->text();
}
