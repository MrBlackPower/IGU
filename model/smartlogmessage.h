#ifndef SMARTLOGMESSAGE_H
#define SMARTLOGMESSAGE_H

#include <QDateTime>

enum SmartLogType{
    LOG_LOAD,
    LOG_CREATE,
    LOG_DELETE,
    LOG_COMPATIBILITY,
    LOG_CRASH,
    LOG_SAVE,
    LOG_PRINT,
    LOG_DUMP,
    LOG_ITERATION,
    LOG_DEFAULT
};

class SmartLogMessage
{
public:
    SmartLogMessage(int SID = 0, QString msg = "", SmartLogType type = SmartLogType::LOG_DEFAULT);

    QString getMessage();

    void setMessage(QString msg);

    void setType(SmartLogType type = SmartLogType::LOG_DEFAULT);

    void operator=(QString msg);

    void operator=(SmartLogMessage msg);

    int getSID();

    QString toString();
private:
    QDateTime time;
    QString msg;
    int SID;
    SmartLogType type;
};

#endif // SMARTLOGMESSAGE_H
