#include "smartobject.h"

unsigned int SmartObject::CURR_SID = 0;
SmartObject* SmartObject::first_smart = NULL;
SmartObject* SmartObject::last_smart = NULL;
SmartObject* SmartObject::default_log = NULL;

SmartObject::SmartObject(QString name, SmartObject* parent) : SID(CURR_SID ++){    
    if(default_log){
        QObject::connect(this,SIGNAL(emit_log(SmartLogMessage)),default_log,SLOT(addToBuffer(SmartLogMessage)));
        QObject::connect(this,SIGNAL(emit_log_to_file(SmartLogMessage,QString)),default_log,SLOT(addToBuffer(SmartLogMessage,QString)));
    }


    this->parent = parent;
    this->name = name;
    classStack = new SmartClassStack((int) SID);
    SmartLogMessage aux(getSID(),"");

    aux = name.asprintf("OBJECT #SID= %d  NAME = '%s' CREATED ",SID,name.toStdString().data());
    aux.setType(SmartLogType::LOG_CREATE);

    emit emit_log(aux);

    next_smart = NULL;
    previous_smart= NULL;

    if(parent != NULL){
        parent->registerChild(this);
    } else {
        aux = name.asprintf(" LOOSE OBJECT #SID= %d ",SID);
        aux.setType();

        emit emit_log(aux);
    }

    if(first_smart == NULL && last_smart == NULL){
        first_smart = this;
        last_smart = this;
    } else {
        if(last_smart == NULL){
            aux = name.asprintf("ERROR IN CANVAS LINKAGE!");
            aux.setType(SmartLogType::LOG_CRASH);

            emit emit_log(aux);
            last_smart = this;
        } else {
            previous_smart = last_smart;
            last_smart->next_smart = this;
            last_smart = this;
        }
    }
}

bool SmartObject::savePrint(QString fileName){
    return FileHelper::saveRaw(print(),fileName.toUtf8().data());
}

bool SmartObject::saveData(QString fileName){
    return FileHelper::saveRaw(print(),fileName.toUtf8().data());
}

bool SmartObject::saveRaw(QString fileName){
    return FileHelper::saveRaw(print(),fileName.toUtf8().data());
}

vector<SmartObject*> SmartObject::getSmarts(){
    SmartObject* smart = first_smart;
    vector<SmartObject*> res;

    while(smart != NULL){
        res.push_back(smart);
        smart = smart->next_smart;
    }

    return res;
}

SmartObject* SmartObject::getSmart(int sid){
    SmartObject* smart = first_smart;

    while(smart != NULL){
        if(smart->SID == sid)
            return smart;

        smart = smart->next_smart;
    }

    return NULL;

}

bool SmartObject::operator ==(SmartObject* b){
    if(b->getID() == SID)
        return true;

    return false;
}

int SmartObject::getID(){
    return SID;
}

SmartObject* SmartObject::getParent(){
    return parent;
}

vector<SmartObject*> SmartObject::getChildren(){
    return children;
}

void SmartObject::registerChild(SmartObject* child){
    children.push_back(child);

    emit emit_log(SmartLogMessage(SID,QString::asprintf("<SMART CHILD REGISTERED FATHER(#SID=%d) - CHILD (#SID= %d)",getID(),child->getID())));
}

void SmartObject::removeChild(SmartObject* child){
    for(int i = 0; i < children.size(); i++){
        SmartObject* aux = children[i];
        if(child == aux){
            const int I = i;
            children.erase(children.begin() + I);
            return;
        }
    }
}

bool SmartObject::log_as_also(QString filename){
    if(default_log){
        if(logs.empty())
            QObject::connect(this,SIGNAL(emit_log(SmartLogMessage)),this,SLOT(pass_log_to_file(SmartLogMessage)));

        logs.push_back(filename);

        return true;
    }
    return false;
}

void SmartObject::pass_log_to_file(SmartLogMessage msg){
    for (int i = 0; i < logs.size(); i++) {
        emit_log_to_file(msg,logs[i]);
    }
}

SmartObject::~SmartObject(){
    for(int i = 0; i < children.size(); i++){
        delete children[i];
    }

    delete classStack;

    emit emit_log(SmartLogMessage(SID,QString::asprintf("<SMART OBJECT #SID=%d DELETED >",SID)));

    if(parent)
        parent->removeChild(this);

    if(next_smart != NULL){
        if(first_smart == this){
            first_smart = next_smart;
            next_smart->previous_smart = NULL;
        } else if(previous_smart != NULL){
            next_smart->previous_smart = previous_smart;
            previous_smart->next_smart = next_smart;
        } else {
            next_smart->previous_smart = NULL;
        }
    } else if(previous_smart != NULL){
        previous_smart->next_smart = NULL;

        if(last_smart == this)
            last_smart = previous_smart;
    } else if(last_smart == this && first_smart == this){
        first_smart = NULL;
        last_smart = NULL;
    }
}

QString SmartObject::getName(){
    return name;
}

int SmartObject::getSID(){
    return SID;
}

void SmartObject::setDefaultLogPool(SmartObject* default_log){
    if(SmartObject::default_log != NULL){
        purgeDefaultLogPool();
    }

    if(default_log == NULL)
        return;

    SmartObject::default_log = default_log;
    SmartObject* it = first_smart;

    while(it != NULL){
        QObject::connect(it,SIGNAL(emit_log(SmartLogMessage)),default_log,SLOT(addToBuffer(SmartLogMessage)));
        QObject::connect(it,SIGNAL(emit_log_to_file(SmartLogMessage,QString)),default_log,SLOT(addToBuffer(SmartLogMessage,QString)));


        it = it->next_smart;
    }
}

void SmartObject::purgeDefaultLogPool(){
    SmartObject* it = first_smart;

    while(it != NULL){
        QObject::disconnect(it,SIGNAL(emit_log(SmartLogMessage)),default_log,SLOT(addToBuffer(SmartLogMessage)));

        it = it->next_smart;
    }

    default_log = NULL;
}
