#ifndef POINTDATALISTWIDGET_H
#define POINTDATALISTWIDGET_H

#include <QListWidget>
#include "../../helper/vtkfile.h"

class PointDataListWidget : public QListWidget
{
    Q_OBJECT
public:
    explicit PointDataListWidget(QWidget *parent = 0);

public slots:

    bool addDataList(VTKFile file);
};

#endif // POINTDATALISTWIDGET_H
