set key vertical bottom right
plot '/home/igor/Documents/pressure-peaking/data/misc/TreeArtigo_PPLOT_f3.650vM0.000p00.000.dat' with lines title 'phi0 = 0º, visc0 = 0.0' dashtype 7 lw 6 ,\
'/home/igor/Documents/pressure-peaking/data/misc/TreeArtigo_PPLOT_f3.650vM1.000p08.000.dat' with lines title 'phi0 = 8º, visc0 = 1.0' dashtype 7 lw 6
set autoscale
set terminal postscript portrait enhanced mono dashed lw 1 "Helvetica" 14
set title "Amplitude da pressão x Nível da árvore (Frequência 3.65Hz)"
set xlabel "|X|"
set ylabel "|P|"
set terminal pngcairo size 1920,1080 enhanced font 'Latin Modern Roman Dunhil 10 Oblique,18'
set output '/home/igor/Documents/pressure-peaking/data/p_plot_vviscphi_f3_65.png'
replot

plot '/home/igor/Documents/pressure-peaking/data/misc/TreeArtigo_PPLOT_f7.300vM0.000p00.000.dat' with lines title 'phi0 = 0º, visc0 = 0.0' dashtype 7 lw 6 ,\
'/home/igor/Documents/pressure-peaking/data/misc/TreeArtigo_PPLOT_f7.300vM1.000p08.000.dat' with lines title 'phi0 = 8º, visc0 = 1.0' dashtype 8 lw 6
set autoscale
set terminal postscript portrait enhanced mono dashed lw 1 "Helvetica" 14
set title "Amplitude da pressão x Nível da árvore (Frequência 7.30Hz)"
set xlabel "|X|"
set ylabel "|P|"
set terminal pngcairo size 1920,1080 enhanced font 'Latin Modern Roman Dunhil 10 Oblique,18'
set output '/home/igor/Documents/pressure-peaking/data/p_plot_vviscphi_f7_3.png'
replot

plot '/home/igor/Documents/pressure-peaking/data/misc/TreeArtigo_PPLOT_f10.950vM0.000p00.000.dat' with lines title 'phi0 = 0º, visc0 = 0.0' dashtype 7 lw 6 ,\
'/home/igor/Documents/pressure-peaking/data/misc/TreeArtigo_PPLOT_f10.950vM1.000p08.000.dat' with lines title 'phi0 = 8º, visc0 = 1.0' dashtype 8 lw 6
set autoscale
set terminal postscript portrait enhanced mono dashed lw 1 "Helvetica" 14
set title "Amplitude da pressão x Nível da árvore (Frequência 10.95Hz)"
set xlabel "|X|"
set ylabel "|P|"
set terminal pngcairo size 1920,1080 enhanced font 'Latin Modern Roman Dunhil 10 Oblique,18'
set output '/home/igor/Documents/pressure-peaking/data/p_plot_vviscphi_f10_95.png'
replot

plot '/home/igor/Documents/pressure-peaking/data/misc/TreeArtigo_PPLOT_f14.600vM0.000p00.000.dat' with lines title 'phi0 = 0º, visc0 = 0.0' dashtype 7 lw 6 ,\
'/home/igor/Documents/pressure-peaking/data/misc/TreeArtigo_PPLOT_f14.600vM1.000p08.000.dat' with lines title 'phi0 = 8º, visc0 = 1.0' dashtype 8 lw 6
set autoscale
set terminal postscript portrait enhanced mono dashed lw 1 "Helvetica" 14
set title "Amplitude da pressão x Nível da árvore (Frequência 14.60Hz)"
set xlabel "|X|"
set ylabel "|P|"
set terminal pngcairo size 1920,1080 enhanced font 'Latin Modern Roman Dunhil 10 Oblique,18'
set output '/home/igor/Documents/pressure-peaking/data/p_plot_vviscphi_f14_6.png'
replot
