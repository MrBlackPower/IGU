#ifndef NODEGRAPH_H
#define NODEGRAPH_H

#include "node.h"
#include <vector>
#include <QString>

using namespace std;

template <class n_type, class e_type>

class NodeGraph : public Node<n_type>
{
public:
    NodeGraph(int id, QString name, n_type data, double x, double y, double z = 0.0, SmartObject* parent = NULL);

    int getE();
    bool neighboorOf(NodeGraph<n_type,e_type>* n);
    void addEdge(NodeGraph* n, e_type cost);
    void removeEdge(NodeGraph* n);

    vector<NodeGraph<n_type,e_type>*> getEdges();
    void getEdges(vector<NodeGraph<n_type,e_type>*>* vec);

    vector<e_type> getCosts();
    vector<bool> getHovered();
    vector<bool> getSelected();
    bool updateCost(NodeGraph<n_type,e_type>* endNode, e_type cost);

    void hover();
    void unhover();

    void select();
    void unselect();

    void hover_edge(int m);
    void unhover_edge(int m);

    void select_edge(int m);
    void unselect_edge(int m);

    ~NodeGraph();

private:
    bool hovered;
    bool selected;

    vector<NodeGraph*> edges;
    vector<e_type> cost;
    vector<bool> edge_selected;
    vector<bool> edge_hovered;
    int E; //NUMBER OF EDGES
};

#endif // NODEGRAPH_H
