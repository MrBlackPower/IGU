#pragma once

#include <iostream>
#include <thread>
#include <mutex>
#include <limits>
#include <cmath>
#include <chrono>
#include <random>
#include <iomanip>

#include "Constants.h"
#include "Geometry.h"
#include "FileHelper.h"
#include "Miscelaneous.h"
#include "Optimization.h"

#include <flann/flann.hpp>

using namespace std;
using namespace flann;
#ifdef __linux__
	#define DllExport
#elif _WIN32
	#define DllExport   __declspec(dllexport)
#else
	#error "OS not supported!"
#endif

namespace ArterialTreeCore
{
#define VNAME(x) #x

#define flannPointDim       2
#define newPointCount       1
#define queryPointCount     1

struct No;
typedef struct No *ptrNo;

struct No
{
	int idLeft;
    ptrNo esq;

	int idRight;
    ptrNo dir;

	int idRoot;
    ptrNo pai;

    int chave;
    double x;
    double y;
    double raioRelative;
    double raioAbsolute;
    double tamanho;
    double resistencia_distal;
    double fluxo;
    int qtd_term_distal;
    int strahler;
    int mainpath;
    double deltap;
};

struct TreeVector
{
    ptrNo nodeProximal = NULL;
    ptrNo nodeDistal = NULL;
};

struct BifPossivel
{
	int chave1;
	int chave2;
    double x1, y1;
    double x2, y2;

    ptrNo pant = NULL;
    ptrNo patual = NULL;

};

enum RunMode
{
    NewestMode, LegacyMode
};

struct CCOParams
{
	int Nterm_Final = 1000;
	double ksi_low = 0;
	int dominio = 0;
	double gamma = 3;
	int fixa_area_perf = 1;
	int idsemente = 1;
	int caso = 1;

	// Parâmetros usados para o uso de fluxo constante ou variável nos nós
	bool useConstFlow = true;
};

struct ThreadParams
{
	bool enableThreads = true;
	int maxThreads = thread::hardware_concurrency();
};

enum FlannSearchMode
{
    FlannMaxPointSearch, FlannMaxPercentSearch, FlannRadiusSearch
};

struct FlannParams
{
	bool enableFlann = true;
	float enableFactor = 0;
    FlannSearchMode flannSearchMode = FlannMaxPercentSearch;
    int maxPointSearch = 100;
    float maxPercentSearch = 30;
	float radiusSearch = 0.01;
};

struct AutosaveParams
{
	bool enableAutosave = false;
	bool enablePause = true;
	int autosaveParam = 1000;
};

struct StateProgram
{
	bool isValidState = false;

	int NtermArvore;
	int conta_estagio;
	int ContaNumSorteado;
	int TamVetorNumRandomico;
	//double *VetorNumRandomico;
	double Q_perf;
	double pressao_term;
	double pressao_perfusao;
	double queda_pressao;
	double raio_Area_Infiltracao;
	double r_supp;
	double Area_Infiltracao;
	int Chave_No;
	int N_tot;
	int coord_sorteada_OK;
	int N_toss;
	int conta_N_toss;
	int conecta;
	int ContaVetorNumRandomico;
	double sub_area_atual;
	double dist_threshold;
	//double Coord_Sorteada[2];
	int num_linha_BP;
	int auxChave;
	double raio_supp0;
	int Num_Term_Atual;
	int sorteio_tipo;
	int total_potencial_bif;
	double auxFluxo;
	double Q_term;
	double visc;
	int indexJ;
	int Ncon;
	int Nint;
	double fator;

	static void write(const std::string& file_name, StateProgram *state)
	{
		std::ofstream out;
		out.open(file_name);
		out.precision(19);

		if (out.is_open())
		{
			out << state->NtermArvore << endl;
			out << state->conta_estagio << endl;
			out << state->ContaNumSorteado << endl;
			out << state->TamVetorNumRandomico << endl;
			out << state->Q_perf << endl;
			out << state->pressao_term << endl;
			out << state->pressao_perfusao << endl;
			out << state->queda_pressao << endl;
			out << state->raio_Area_Infiltracao << endl;
			out << state->r_supp << endl;
			out << state->Area_Infiltracao << endl;
			out << state->Chave_No << endl;
			out << state->N_tot << endl;
			out << state->coord_sorteada_OK << endl;
			out << state->N_toss << endl;
			out << state->conta_N_toss << endl;
			out << state->conecta << endl;
			out << state->ContaVetorNumRandomico << endl;
			out << state->sub_area_atual << endl;
			out << state->dist_threshold << endl;
			out << state->num_linha_BP << endl;
			out << state->auxChave << endl;
			out << state->raio_supp0 << endl;
			out << state->Num_Term_Atual << endl;
			out << state->sorteio_tipo << endl;
			out << state->total_potencial_bif << endl;
			out << state->auxFluxo << endl;
			out << state->Q_term << endl;
			out << state->visc << endl;
			out << state->indexJ << endl;
			out << state->Ncon << endl;
			out << state->Nint << endl;
			out << state->fator << endl;

			out.close();
		}
		else
		{
			cout << "ERROR >> state file not writed!" << endl;
		}
	};

	static StateProgram read(const std::string& file_name)
	{
		StateProgram state;

		ifstream in;
		in.open(file_name, std::ios::binary);
		in.precision(19);

		if (in.is_open())
		{
			in >> state.NtermArvore;
			in >> state.conta_estagio;
			in >> state.ContaNumSorteado;
			in >> state.TamVetorNumRandomico;
			in >> state.Q_perf;
			in >> state.pressao_term;
			in >> state.pressao_perfusao;
			in >> state.queda_pressao;
			in >> state.raio_Area_Infiltracao;
			in >> state.r_supp;
			in >> state.Area_Infiltracao;
			in >> state.Chave_No;
			in >> state.N_tot;
			in >> state.coord_sorteada_OK;
			in >> state.N_toss;
			in >> state.conta_N_toss;
			in >> state.conecta;
			in >> state.ContaVetorNumRandomico;
			in >> state.sub_area_atual;
			in >> state.dist_threshold;
			in >> state.num_linha_BP;
			in >> state.auxChave;
			in >> state.raio_supp0;
			in >> state.Num_Term_Atual;
			in >> state.sorteio_tipo;
			in >> state.total_potencial_bif;
			in >> state.auxFluxo;
			in >> state.Q_term;
			in >> state.visc;
			in >> state.indexJ;
			in >> state.Ncon;
			in >> state.Nint;
			in >> state.fator;

			in.close();
			state.isValidState = true;
		}

		return state;
	};
};

struct RuntimeParams
{
	// Define quais funções serão usadas. O código mais novo ou o legado
	RunMode mode = NewestMode;

	// Parâmetros da geração da árvore
	CCOParams ccoParams;

	// Parâmetros usados para as threads
	ThreadParams threadParams;

    // Parâmetros usados para a busca FLANN
    FlannParams flannParams;

	// Parâmetros usados para o autosave na geração da árvore
	AutosaveParams autosaveParams;

	StateProgram state;
};

enum SwitchCaseLabel
{
	undefined,
	runMode,
	ntermFinal,
	ksi_low,
	dominio,
	gamma,
	fixa_area_perf,
	idsemente,
	caso,
	useConstFlow,
	enableThread,
	maxThreads,
	enableFlann,
	enableFactor,
	flannSearchMode,
	maxPointSearch,
	maxPercentSearch,
	radiusSearch,
	enableAutosave,
	autosaveParam,
	idFlow,
	value,
	idTreeVect,
	treeProxNode,
	treeDistNode
};

struct SwitchCaseLabelResolver : public std::map<string, SwitchCaseLabel>
{
	SwitchCaseLabelResolver()
	{
		this->operator[]("undefined") = undefined;
		this->operator[]("runMode") = runMode;
		this->operator[]("ntermFinal") = ntermFinal;
		this->operator[]("ksi_low") = ksi_low;
		this->operator[]("dominio") = dominio;
		this->operator[]("gamma") = gamma;
		this->operator[]("fixa_area_perf") = fixa_area_perf;
		this->operator[]("idsemente") = idsemente;
		this->operator[]("caso") = caso;
		this->operator[]("useConstFlow") = useConstFlow;
		this->operator[]("enableThread") = enableThread;
		this->operator[]("maxThreads") = maxThreads;
		this->operator[]("enableFlann") = enableFlann;
		this->operator[]("enableFactor") = enableFactor;
		this->operator[]("flannSearchMode") = flannSearchMode;
		this->operator[]("maxPointSearch") = maxPointSearch;
		this->operator[]("maxPercentSearch") = maxPercentSearch;
		this->operator[]("radiusSearch") = radiusSearch;
		this->operator[]("enableAutosave") = enableAutosave;
		this->operator[]("autosaveParam") = autosaveParam;
		this->operator[]("idFluxo") = idFlow;
		this->operator[]("value") = value;
		this->operator[]("idTreeVect") = idTreeVect;
		this->operator[]("treeProxNode") = treeProxNode;
		this->operator[]("treeDistNode") = treeDistNode;
	};
};

class DllExport ArterialTree
{
protected:

private:
	// Parâmetros de execução
	RuntimeParams runParams;

    // Nó raiz da árvore
    ptrNo topo;

	/*---------- Cache Optimization ----------*/

	/*
	*   Guarda a estrutura da árvore na forma de um vetor.
	*   Cada TreeVector contém um segmento da árvore (Nó proximal/Nó distal)
	*/
	int treeVectSize = 0;
	TreeVector *treeVect;

	/*---------- End Cache Optimization ----------*/


	/*---------- FLANN EXPERIMENTAL ---------- */

	/*
	*   flannNewPoint -> Novo ponto que será adicionado na estrutura da FLANN
	*   Dimensão da matriz = linhas (queryPointCount -> Quantidade de pontos que serão adicionados) * colunas (flannPointDim -> Dimensão dos pontos de coordenada)
	*/
	//Matrix<double> flannNewPoint = Matrix<double>(new double[newPointCount * flannPointDim], newPointCount, flannPointDim);

	/*
	*   flannQueryPoint -> Ponto que será usado na busca pelos vizinhos mais próximos.
	*   Dimensão da matriz = linhas (queryPointCount -> Quantidade de pontos de pesquisa) * colunas (flannPointDim -> Dimensão dos pontos de coordenada)
	*/
	Matrix<double> flannQueryPoint = Matrix<double>(new double[queryPointCount * flannPointDim], queryPointCount, flannPointDim);

	// FLANN Index. Usando coordenadas float, com cálculo de distância euclidiana.
	Index<L2_Simple<double> > *flannIndex;

	/*----------  END FLANN ---------- */


	/*---------- FLUXO VARIÁVEL ----------*/

	// Id do próximo fluxo que será usado durante a construção
	int idFlowVect = 0;

	// Vetor de fluxos
	double *flowVect;

	/*---------- END FLUXO VARIÁVEL ----------*/

    // Funções de manipulação da estrutura de dados da árvore (Miscelânea)
    ptrNo cria_ptrNo(int id, double coord[], double flow, ptrNo pai_No);
    ptrNo clone_Node(ptrNo nodeSrc, ptrNo nodeDest);
    ptrNo copy_Node(ptrNo node);
    ptrNo copy_Tree(ptrNo node);
    ptrNo searchNode(ptrNo node, int id);
    void searchNodes(ptrNo root, int idNode[], ptrNo *nodeFound, int nodeMax, int *nodeCount);
    ptrNo del_Tree(ptrNo node);
    ptrNo apagaArvore(ptrNo topo, int *qtdseg);

	int searchIndexInTreeVect(int nodeIdProx, int nodeIdDist);
	ptrNo searchNodeInTreeVect(int nodeId);

    /*
    *	Conecta um novo segmento na árvore, checando a partir de uma lista de segmentos viáveis para bifurcação,
    *	qual conexão trás a melhor otimização para a árvore.
    *	Modo atual: Cada segmento da lista de bifurcações viáveis é testado em uma thread separada, desta forma, todo o processo é paralelo.
    *	Modo legado: Cada segmento da lista de bifurcações viáveis é testado em uma única thread, desta forma, o processo não é paralelo.
    */
    ptrNo conectaSegmentoTerminalThread(ptrNo topo, int total_potencial_bif, double pressao_perfusao, double pressao_term, BifPossivel *bifPoss, int *Chave_No, double Coord_Sorteada[], int *coord_sorteada_OK, int *conecta, int *auxChave, double ksi_low, double Q_term, double gamma, double visc, int Nint, int maxThread);
    void conectaSegmentoTemp(ptrNo topo, double pressao_perfusao, double pressao_term, BifPossivel *bifPoss, int conta_potencial_bif, BifPossivel *bifOtim, int indexOpt, double *Volume_Arvore, int conta_vol_arvore, int Chave_No, double Coord_Sorteada[], double ksi_low, int idFluxo, double *nodeFluxo, double Q_term, double gamma, double visc, int Nint, bool *threadState);
    ptrNo conectaSegmentoTerminalLegacy(ptrNo topo, int total_potencial_bif, double pressao_perfusao, double pressao_term, BifPossivel *bifPoss, int *Chave_No, double Coord_Sorteada[], int *coord_sorteada_OK, int *conecta, int *auxChave, double ksi_low, double Q_term, double gamma, double visc, int Nint);

    /*
    *	Recalculam o fluxo de um nó e a quantidade de terminais abaixo de seu nível na árvore,
    *	quando um novo segmento é adicionado/removido da árvore
    */
    void aumentaFluxoTerm(ptrNo arv_topo, ptrNo sub_topo);
    void diminuiFluxoTerm(ptrNo arv_topo);

    /*
    *	Recalculam a quantidade de terminais abaixo de seu nível na árvore,
    *	quando um novo segmento é adicionado/removido da árvore
    */
    void aumentaContadorTerm(ptrNo topo_arvore, ptrNo sub_topo);
    void diminuiContadorTerm(ptrNo topo_arvore);

    /*
    *	Verificam se a nova coordenada sorteada atende o critério de distância crítica.
    *	Modo atual: Usa a biblioteca FLANN para checar apenas os vizinhos mais próximos
    *	Modo legado: Checa em relação a todos os segmento da árvore
    */
    int distCritNovoSeg(double Coord_Sorteada[], double dist_threshold, ptrNo topo, FlannParams flannParams);
    int distCritNovoSegLegacy(double Coord_Sorteada[], double dist_threshold, ptrNo topo);

    // Cria o primeiro segmento da árvore, o segmento raiz
    ptrNo plantaSegmentoRaiz(double area_Infiltracao, double *raio_Atual, double *dist_Threshold, int *conta_N_Toss, int nTerm_Final, double *sub_Area_Atual, int n_Toss, double vetorNumRandom[], int tamVetorNumRandom, int *contaVetorNumRandom, int * chave_No, double q_Term, double queda_Pressao, int dominio, double visc, double fator);

    // Encontra segmentos viáveis para bifurcação
    int segPotencialBifurcacao(double Coord_Sorteada[], double dist_threshold, ptrNo topo, BifPossivel *bifPoss, FlannParams flannParams);
	int segPotencialBifurcacaoLegacy(double Coord_Sorteada[], double dist_threshold, ptrNo topo, BifPossivel *bifPoss, FlannParams flannParams);

    // Verifica se há cruzamento de segmentos
	int cruzamentoSeg(double nova_Coord_seg[], int bif_patual_chave, FlannParams flannParams);
	int cruzamentoSegLegacy(double nova_Coord_seg[], ptrNo topo, int bif_patual_chave);

    // Recalcula a resistência dos segmentos, caso um segmento seja removido
    void removeResistenciaTerm(ptrNo sub_topo, double P_perf, double P_term, double Q_term, double gamma, double visc);

    // Conta quantos segmentos terminais a árvore possui
    int contaSegTerminal(ptrNo topo);

public:
    // Retorna o raio absoluto de um segmento da árvore
    static double raioSegmento(ptrNo topo, ptrNo psegj);

    // Retorna o tamanho de cada segmento envolvido em uma bifurcação
    static void tamanhoSegBifurcacao(ptrNo topo_arvore, ptrNo sub_topo, double vtamanho[]);

    // Recalcula a resistência dos segmentos da árvore, caso um novo segmento seja adicionado
    static void acrescentaResistenciaTerm(ptrNo sub_topo, double P_perf, double P_term, double Q_term, double gamma, double visc);

    // Gera uma árvore arterial a partir dos parâmetros informados
    void create(RuntimeParams params);
};
}
