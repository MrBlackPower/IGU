#ifndef CONTOURWINDOW_H
#define CONTOURWINDOW_H

#include <QDialog>



namespace Ui {
class ContourWindow;
}

class ContourWindow : public QDialog
{
    Q_OBJECT
public:
    struct ContourWindowParams{
        double scalar = 0.0;
    };

    explicit ContourWindow(QWidget *parent = 0);
    ~ContourWindow();

signals:
    void sendCountourWindowParams(double val);

private slots:
    void on_doubleSpinBox_editingFinished();

    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

private:
    Ui::ContourWindow *ui;

    ContourWindowParams params;
};

#endif // CONTOURWINDOW_H
