#include "selectfromstringlistdialog.h"
#include "ui_selectfromstringlistdialog.h"

SelectFromStringListDialog::SelectFromStringListDialog(QString name, QString possible_values, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SelectFromStringListDialog)
{
    ui->setupUi(this);
    QString lblText = DEFAULT_TEXT;
    answer= "";

    //CHANGES LABEL
    lblText += QString::asprintf("<%s>.",name.toStdString().data());

    ui->lblStringList->setText(lblText);

    //ADDS OPTIONS TO COMBO BOX
    QTextStream stream(&possible_values);
    QString aux;
    while(!stream.atEnd()){
        stream >> aux;
        ui->cbxStringList->addItem(aux);
    }

}

SelectFromStringListDialog::~SelectFromStringListDialog()
{
    delete ui;
}

QString SelectFromStringListDialog::getString(){
    return answer;
}

void SelectFromStringListDialog::on_cbxStringList_activated(const QString &arg1)
{
    answer = arg1;
}

void SelectFromStringListDialog::on_buttonBox_accepted()
{
    emit stringListSet();
    this->hide();
}

void SelectFromStringListDialog::on_buttonBox_rejected()
{
    this->close();
}

void SelectFromStringListDialog::on_cbxStringList_currentIndexChanged(const QString &arg1)
{
    answer = arg1;
}
