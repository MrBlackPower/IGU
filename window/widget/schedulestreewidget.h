#ifndef SCHEDULESTREEWIDGET_H
#define SCHEDULESTREEWIDGET_H

#include "../../helper/schedule.h"

#include <vector>
#include <QString>
#include <QTreeWidget>

class SchedulesTreeWidget : public QTreeWidget
{
public:
    SchedulesTreeWidget(QWidget *parent = NULL);

    void populate(vector<Schedule>* schedules);
};

#endif // SCHEDULESTREEWIDGET_H
