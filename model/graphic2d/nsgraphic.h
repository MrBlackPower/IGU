#ifndef NSGRAPHIC_H
#define NSGRAPHIC_H

#define FOU 1
#define TOPUS 2
#define EXACT 3

#define OLD 0
#define NEW 1

#include "../graphic3d.h"
#include "../graphic2d.h"
#include "../colorbar/blue.h"
#include "staticgraphic.h"
#include "math.h"

struct NSGraphicStartParams{
    int example = 1;
    int method = 1;
    double alpha = 1;
    double a = 0.0;
    double b = 2;
    int n = 400;
    double h = (b -a);
    double K = 1;
};

class NSGraphic : public Graphic2D
{
public:
    NSGraphic(QString name = "NSGRAPHIC", int example = 0, SmartObject* parent = 0);

    /********************************************************
    *  GRAPHIC OBJECT VIRTUAL METHODS  *
    ********************************************************/
    vector<Field> getStartParameters();
    vector<Field> getContextVisualParameters();
    vector<QString> getRanges();
    bool updateStartParameter(QString field, QString value);
    bool setContextRange(QString field);

private:
    NSGraphicStartParams startParams;

    vector<vector<Point>> u;

    /********************************************************
    *  GRAPHIC OBJECT VIRTUAL METHODS  *
    ********************************************************/
    bool initialize();
    double iteration();
    bool updateContextVisualParameter(QString field, QString value);
    QString getLiveParameterList();
    QString getLiveParameter(QString name);
    DataType getLiveParameterDataType(QString name);

    double f(double x);

    double facef(double uminus, double u, double uplus, double alpha);
    double faceg(double uminus2, double uminus, double u, double alpha);
    void mouse_press_event(int width, int height, Point eye, QMouseEvent *event, GraphicTools tool);
    void mouse_move_event(int width, int height, Point eye, QMouseEvent *event, GraphicTools tool);
};

#endif // NSGRAPHIC_H
