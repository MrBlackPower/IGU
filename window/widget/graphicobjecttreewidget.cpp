#include "graphicobjecttreewidget.h"

GraphicObjectTreeWidget::GraphicObjectTreeWidget(QWidget *parent) : QTreeWidget(parent)
{
    populate();
}

void GraphicObjectTreeWidget::populate(){
    QTreeWidget::clear();
    vector<GraphicObject*> list = GraphicObject::getGraphics();
    vector<int> read;
    for(int i = 0; i < list.size(); i++){
        bool found = false;
        GraphicObject* g = list[i];

        for(int j = 0; j < read.size() && !found; j++)
            if(g->operator ==(read[j]))
                found = true;

        if(!found){
            QTreeWidgetItem* root = new QTreeWidgetItem(this);
            QString aux = GraphicObject::statusToString(g->getStatus());
            root->setText(0,aux.asprintf("%d",g->getGID()));
            root->setText(1,aux.asprintf("%d : %s",g->getGID(), g->getName().toStdString().data()));
            root->setText(2,aux);
            read.push_back(g->getGID());

            populate_aux(root,g,&read);
        }
    }
}

void GraphicObjectTreeWidget::populate_aux(QTreeWidgetItem* current, GraphicObject* current_obj, vector<int>* read){
    vector<GraphicObject*> children = current_obj->getSlaves();

    for(int i = 0; i < children.size(); i++){
        GraphicObject* child = children[i];
        bool found = false;

        for(int j = 0; j < read->size() && !found; j++)
            if(child->operator ==(read->operator [](j)))
                found = true;


        if(!found){
            QTreeWidgetItem* i = new QTreeWidgetItem();
            QString aux = GraphicObject::statusToString(child->getStatus());
            i->setText(0,aux.asprintf("%d",child->getGID()));
            i->setText(1,aux.asprintf("%d : %s",child->getGID(), child->getName().toStdString().data()));
            i->setText(2,aux);
            read->push_back(child->getGID());

            current->addChild(i);

            populate_aux(i,child,read);
        }
    }
}
