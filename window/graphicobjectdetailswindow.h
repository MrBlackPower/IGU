#ifndef GRAPHICOBJECTDETAILSWINDOW_H
#define GRAPHICOBJECTDETAILSWINDOW_H

#include <QDialog>
#include <QListWidget>
#include <QListWidgetItem>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QTimer>
#include <QFileDialog>
#include "../model/graphicobject.h"
#include "dialog/inputstringdialog.h"
#include "dialog/inputbooleandialog.h"
#include "dialog/inputscalardialog.h"
#include "dialog/selectfromstringlistdialog.h"
#include "../helper/schedule.h"

#define TIMEOUT 30000

namespace Ui {
class GraphicObjectDetailsWindow;
}

class GraphicObjectDetailsWindow : public QDialog
{
    Q_OBJECT

public:
    explicit GraphicObjectDetailsWindow(GraphicObject* gObj, QWidget *parent = 0);
    ~GraphicObjectDetailsWindow();

private slots:
    void on_btnOK_clicked();

    void on_btnStart_clicked();

    void on_btnUpdate_clicked();

    void on_startListWidget_itemDoubleClicked(QListWidgetItem *item);

    void on_visualListWidget_itemDoubleClicked(QListWidgetItem *item);

    void on_rangeListWidget_itemDoubleClicked(QListWidgetItem *item);

    void on_graphicObjectTreeWidget_itemDoubleClicked(QTreeWidgetItem *item, int column);

    void on_concurrentGraphicObjectsListWidget_itemDoubleClicked(QListWidgetItem *item);

    void on_extrapolationListWidget_itemDoubleClicked(QListWidgetItem *item);

    void on_ColorbarListWidget_itemDoubleClicked(QListWidgetItem *item);

    void on_btnSchedule_clicked();

private:
    Ui::GraphicObjectDetailsWindow *ui;

    GraphicObject* gObj;

    void update();
};

#endif // GRAPHICOBJECTDETAILSWINDOW_H
