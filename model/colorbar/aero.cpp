#include "aero.h"

Aero::Aero() : ColorBar("AERO"){
    QColor c;
    addColor(c.fromRgb(198,40,40));
    addColor(c.fromRgb(255,255,255));
    addColor(c.fromRgb(21,101,192));
}
