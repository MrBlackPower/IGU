#ifndef SCHEDULERTHREADMANAGER_H
#define SCHEDULERTHREADMANAGER_H

#include "../schedule.h"
#include "../../model/smartobject.h"
#include "../../model/graph/weightedgraph.h"
#include "../vtkfile.h"

#include <vector>
#include <QTimer>

#define DEFAULT_WAIT_LOOP 1000
#define DEFAULT_WAIT_CANVAS 1000

using namespace std;

enum LoadType{
    WEIGHTED_GRAPH_INSTANCE,
    VTK_FILE,
    ARTERY_TREE
};

class SchedulerThreadManager : public SmartObject
{
Q_OBJECT
public:
    SchedulerThreadManager(QString name, SmartObject* parent = NULL);
    SchedulerThreadManager(vector<Schedule> jobs, QString name, SmartObject* parent = NULL);

    vector<QString> print();
    vector<QString> data();
    vector<QString> raw();

signals:
    void setCanvas(GraphicObject* g);
    void iterate_object();
    void start_object();
    void purgeCanvas();
    void finished();
    void pct(unsigned int pct);

public slots:
    void process();

    void addToBuffer(Schedule job);

    void addToBuffer(vector<Schedule> job);

private:
    bool waiting_for_job;

    LoadType load_type;

    vector<Schedule> jobs;

};

#endif // SCHEDULERTHREADMANAGER_H
