#include "schedulerthreadmanager.h"

SchedulerThreadManager::SchedulerThreadManager(QString name, SmartObject* parent) : SmartObject (name,parent)
{
    load_type = WEIGHTED_GRAPH_INSTANCE;
    waiting_for_job = false;
}

SchedulerThreadManager::SchedulerThreadManager(vector<Schedule> jobs, QString name, SmartObject* parent) : SmartObject (name,parent)
{
    load_type = WEIGHTED_GRAPH_INSTANCE;
    waiting_for_job = false;
    this->jobs.operator=(jobs);
}

vector<QString> SchedulerThreadManager::print(){
    vector<QString> aux;

    return aux;
}

vector<QString> SchedulerThreadManager::data(){
    vector<QString> aux;

    return aux;
}

vector<QString> SchedulerThreadManager::raw(){
    vector<QString> aux;

    return aux;
}

void SchedulerThreadManager::process(){
    for (int i = 0; i < jobs.size(); i++) {
        Schedule current_job = jobs[i];

        emit emit_log(SmartLogMessage(getSID(),QString::asprintf("STARTED JOB <%d>",current_job.id())));

        QString filename = current_job.getFileName();
        QString log_filename = current_job.getLogFileName();
        int times = current_job.getTimes();

        vector<QString> start = current_job.getStartParams();
        vector<QString> visual = current_job.getVisualParams();

        for (int l = 0;l < times; l++){
            GraphicObject* g = NULL;
            QString current_log = QString::asprintf("%s_%d.igu",log_filename.toStdString().data(),l);

            switch (load_type) {
            case WEIGHTED_GRAPH_INSTANCE:{
                g = new WeightedGraph(filename,this);
                VTKFile f;

                f.loadMWCDSInstance(filename);
                g->log_as_also(current_log);
                g->load(&f);

                for (int i = 0; i < start.size(); i++) {
                    QString name, value;
                    QTextStream s(&start[i]);
                    s >> name >> value;
                    g->updateStartParameter(name,value);
                }

                for (int i = 0; i < visual.size(); i++) {
                    QString name, value;
                    QTextStream s(&visual[i]);
                    s >> name >> value;
                    g->updateVisualParameter(name,value);
                }

                break;
            }
            }

            if(g == NULL){
                emit_log(SmartLogMessage(getSID(),"LOAD NOT SUCCESFULL!"));
            } else {
                QEventLoop loop;
                QTimer timeout;

                connect(&timeout,SIGNAL(timeout()),&loop,SLOT(quit()));

                emit setCanvas(g); // SENDS GRAPHIC OBJECT TO A CANVAS

                timeout.start(DEFAULT_WAIT_CANVAS);

                loop.exec(); // WAITS A WHILE FOR CANVAS TO LOAD

                emit iterate_object(); // ITERATES THE GRAPHIC OBJECT

                emit start_object(); //STARTS THE GRAPHIC OBJECT

                while(g->getStatus() != Finished){
                    waiting_for_job = true;
                    QEventLoop loop;
                    QTimer timeout;

                    connect(&timeout,SIGNAL(timeout()),&loop,SLOT(quit()));

                    timeout.start(DEFAULT_WAIT_LOOP);

                    loop.exec();
                }

                emit emit_log(SmartLogMessage(getSID(),QString::asprintf("FINISHED JOB <%d>",current_job.id())));

                emit purgeCanvas();

                timeout.start(DEFAULT_WAIT_CANVAS);

                loop.exec(); // WAITS A WHILE FOR CANVAS TO DELETE

                g->deleteLater();

            }


            unsigned int p = round((((double) (i +((l+1)/times))) /jobs.size()) * 100);
            emit pct(p);

        }
    }

    emit finished();
}

void SchedulerThreadManager::addToBuffer(Schedule job){
    jobs.push_back(job);
}

void SchedulerThreadManager::addToBuffer(vector<Schedule> job){
    for (int i = 0; i < job.size(); i++) {
        addToBuffer(job[i]);
    }
}
