#include "nsgraphic.h"

NSGraphic::NSGraphic(QString name, int example, SmartObject* parent) : Graphic2D((name),parent){
    time.deltaT = 0.00025;
    setContextRange("Y-AXIS");
}

void NSGraphic::mouse_press_event(int width, int height, Point eye, QMouseEvent *event, GraphicTools tool){

}

void NSGraphic::mouse_move_event(int width, int height, Point eye, QMouseEvent *event, GraphicTools tool){

}

vector<Field> NSGraphic::getStartParameters(){
    vector<Field> params;

   Field aux;
   aux.names = "EXAMPLE";
   aux.required = false;
   aux.stream = FIELD_IN;
   aux.type = integer;
   aux.current_value = aux.current_value.asprintf("%d",startParams.example);

    params.push_back(aux);

    aux.names = "METHOD";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = integer;
    aux.current_value = aux.current_value.asprintf("%d",startParams.method);

     params.push_back(aux);

     aux.names = "DELTAT";
     aux.required = false;
     aux.stream = FIELD_IN;
     aux.type = doublep;
     aux.current_value = aux.current_value.asprintf("%.5f",time.deltaT);

      params.push_back(aux);

    return params;
}

vector<Field> NSGraphic::getContextVisualParameters(){
    vector<Field> params;

    return params;
}

vector<QString> NSGraphic::getRanges(){
    vector<QString> params;

    params.push_back("Y-AXIS");

    return params;
}

bool NSGraphic::updateStartParameter(QString field, QString value){
    if(field == "EXAMPLE"){
        startParams.example = (int) value.toDouble();
        return true;
    }
    if(field == "METHOD"){
        startParams.method = (int) value.toDouble();
        return true;
    }
    if(field == "DELTAT"){
        time.deltaT = value.toDouble();
        return true;
    }
    return false;
}

bool NSGraphic::initialize(){
    clearData();
    time.modelTime = 0.0;
    time.deltaT = 0.00025;
    switch (startParams.example) {
    case 1:
        startParams.a = 0;
        startParams.b = 2;
        startParams.h = (startParams.b - startParams.a)/startParams.n;

        for(int i = 0; i <= startParams.n; i++){
            double x = i * startParams.h;

            if(i != 0 && i != startParams.n){
                Point p = Point(i,x,f(x),time.modelTime);
                addData(p);
            } else {
                Point p = Point(i,x,0,time.modelTime);
                addData(p);
            }
        }

        break;
    case 2:
        startParams.a = -1;
        startParams.b = 1;
        startParams.h = (startParams.b - startParams.a)/startParams.n;

        for(int i = 0; i <= startParams.n; i++){
            double x = i * startParams.h;

            if(i != 0 && i != startParams.n){
                Point p = Point(i,x,f(x),time.modelTime);
                addData(p);
            } else {
                Point p = Point(i,x,0,time.modelTime);
                addData(p);
            }
        }

        break;
    default:
        startParams.a = 0;
        startParams.b = 2;
        startParams.h = (startParams.b - startParams.a)/startParams.n;

        for(int i = 0; i <= startParams.n; i++){
            double x = i * startParams.h;

            if(i != 0 && i != startParams.n){
                Point p = Point(i,x,f(x),time.modelTime);
                addData(p);
            } else {
                Point p = Point(i,x,0,time.modelTime);
                addData(p);
            }
        }

        break;
    }
}

double NSGraphic::iteration(){
    Point* start = this->operator [](0);
    Point* end = this->operator [](N-1);

    //ADDS EVERY POINT IN THE GRAPHIC TO THE MATRIX U

    vector<Point> aux;
    for(int i = 0; i < raw_data.size(); i++){
        Point* org = raw_data[i];
        Point p = Point(i,org->x,org->y,org->z);
    }

    u.push_back(aux);

    //ITERATES THE NEW TIMESTEP
    time.modelTime += time.deltaT;

    double conv[2] = {0};

    switch (startParams.method) {
    case FOU:
        for(int i = 1; i < N-1; i++){
            double uminus2 = (i > 1)? raw_data[i-2]->y : 0.0;
            Point* uminus = raw_data[i-1];
            Point* u = raw_data[i];
            Point* uplus = raw_data[i+1];
            double aux = u->y;

            aux = aux - ((time.deltaT/(startParams.h * startParams.alpha)) * (facef(uminus->y,u->y,uplus->y,startParams.alpha) - faceg(uminus2,uminus->y,u->y,startParams.alpha)));

            u->y = aux;
        }
        break;
    case TOPUS:
        for(int i = 1; i < N-1; i++){
            double uminus2 = (i > 1)? raw_data[i-2]->y : 0.0;
            Point* uminus = raw_data[i-1];
            Point* u = raw_data[i];
            Point* uplus = raw_data[i+1];
            double aux = u->y;

            aux = aux - ((time.deltaT/(startParams.h * startParams.alpha)) * (facef(uminus->y,u->y,uplus->y,startParams.alpha) - faceg(uminus2,uminus->y,u->y,startParams.alpha)));

            u->y = aux;
        }
        break;
    case EXACT:
        for(int i = 0; i < N; i++){
            Point* u = raw_data[i];

            u->y = f(u->x);
        }

        break;
    default:
        for(int i = 1; i < N-1; i++){
            double uminus2 = (i > 1)? raw_data[i-2]->y : 0.0;
            Point* uminus = raw_data[i-1];
            Point* u = raw_data[i];
            Point* uplus = raw_data[i+1];
            double aux = u->y;

            aux = aux - ((time.deltaT/(startParams.h * startParams.alpha)) * (facef(uminus->y,u->y,uplus->y,startParams.alpha) - faceg(uminus2,uminus->y,u->y,startParams.alpha)));

            u->y = aux;
        }
        break;
    }


    //CONTOUR CONDITION
    start->y = 0;
    end->y = 0;
}

bool NSGraphic::updateContextVisualParameter(QString field, QString value){
    return false;
}

QString NSGraphic::getLiveParameterList(){
    return QString("");
}

QString NSGraphic::getLiveParameter(QString name){
    return QString("");
}

DataType NSGraphic::getLiveParameterDataType(QString name){
    return undefined;
}

bool NSGraphic::setContextRange(QString field){
    if(field == "Y-AXIS"){
        range.max = limit.top;
        range.min = limit.bottom;
        range.parameter = "Y-AXIS";
        return true;
    }

    return false;
}

double NSGraphic::f(double x){
    double x_aux = (x - startParams.alpha * time.modelTime);
    switch (startParams.example) {
    case 1:
        if(x_aux < 0.2){
            return exp(-log10(50) * (pow((x_aux - 0.15)/0.05,2)));
        } else if(x_aux > 0.3 && x_aux < 0.4){
            return 1;
        } else if(x_aux > 0.5 && x_aux < 0.55) {
            return 20 * x_aux - 10;
        } else if(x_aux >= 0.55 && x_aux < 0.66) {
            return -20 * x_aux + 12;
        } else if(x_aux > 0.7 && x_aux < 0.8) {
            return sqrt(1 - pow((x_aux - 0.75)/0.05,2));
        }

        return 0;
    case 2:
        if(x_aux >= 0 && x_aux <= 0.2){
            return 1;
        } else if(x_aux > 0.2 && x_aux <= 0.4) {
            return 4 * x_aux - 0.6;
        } else if(x_aux > 0.4 && x_aux <= 0.6) {
            return -4 * x_aux + 2.6;
        } else if(x_aux > 0.6 && x_aux <= 0.8) {
            return 1;
        }
        return 0;

    default:
        if(x_aux < 0.2){
            return exp(-log10(50) * (pow((x_aux - 0.15)/0.05,2)));
        } else if(x_aux > 0.3 && x_aux < 0.4){
            return 1;
        } else if(x_aux > 0.5 && x_aux < 0.55) {
            return 20 * x_aux - 10;
        } else if(x_aux >= 0.55 && x_aux < 0.66) {
            return -20 * x_aux + 12;
        } else if(x_aux > 0.7 && x_aux < 0.8) {
            return sqrt(1 - pow((x_aux - 0.75)/0.05,2));
        }

        return 0;
    }

    return 0;
}

double NSGraphic::facef(double uminus, double u, double uplus, double alpha){
    switch (startParams.method) {
    case FOU:
        if(alpha > 0.0){
            return u;
        }

        return uplus;
    case TOPUS:
        double uU = u;
        double uD = uplus ;
        double uR = uminus;

        if(uD == uR)
            return uU;

        double uHat = (uU - uR)/(uD - uR);

        if(uHat < 0 || uHat > 1)
            return uU;

        return uR + (uD - uR) * ( 2 * (pow(uHat,4)) - 3 * (pow(uHat,3)) + 2 * uHat );
    }

    return 0;
}

double NSGraphic::faceg(double uminus2, double uminus, double u, double alpha){
    switch (startParams.method) {
    case FOU:
        if(alpha > 0.0){
            return uminus;
        }

        return u;
    case TOPUS:
        double uD = u;
        double uU = uminus;
        double uR = (uminus2 == 0)? u : uminus2;


        if(uD == uR)
            return uU;

        double uHat = (uU - uR)/(uD - uR);

        if(uHat < 0 || uHat > 1)
            return uU;

        return uR + (uD - uR) * ( 2 * (pow(uHat,4)) - 3 * (pow(uHat,3)) + 2 * uHat );
    }

    return 0;
}
