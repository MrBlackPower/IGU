#ifndef COLORBAR_H
#define COLORBAR_H

#define ONE 1
#define ZERO 0

#include <QColor>
#include <vector>

using namespace std;

enum ColorBarType{
    Continuous,
    Discrete
};

class ColorBar
{
public:
    ColorBar(QString name = "DEFAULT", ColorBarType type = ColorBarType::Continuous);
    ColorBar(QColor c,QString name, ColorBarType type = ColorBarType::Continuous);

    void addColor(QColor c);

    QColor interpolate(double val);

    QColor get_back();

    void pop_back();

    double getMax();

    void setMax(double max);

    double getMin();

    void setMin(double min);

    QString getName();

    ColorBarType getType();

    vector<QColor> getColors();

    void operator =(ColorBar c);

    void operator =(ColorBar* c);

private:
    QString name;
    ColorBarType type;
    vector<QColor> colors;
    double max;
    double min;
    int N;
};

#endif // COLORBAR_H
