#ifndef INPUTSCALARDIALOG_H
#define INPUTSCALARDIALOG_H

#include <QDialog>

namespace Ui {
class InputScalarDialog;
}

class InputScalarDialog : public QDialog
{
    Q_OBJECT

public:
    explicit InputScalarDialog(QWidget *parent = 0);
    ~InputScalarDialog();

    double getScalar();

    bool Set();

public slots:
    void setScalar(QString name);

signals:
    void pushScalar(QString name, double scalar);

    void scalarSet();

private slots:
    void on_spnScalar_editingFinished();

    void on_buttonBox_accepted();

private:
    Ui::InputScalarDialog *ui;
    QString name;
    double value;
    bool set;
};

#endif // INPUTSCALARDIALOG_H
