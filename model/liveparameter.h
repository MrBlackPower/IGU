#ifndef LIVEPARAMETER_H
#define LIVEPARAMETER_H

#include <vector>
#include "graphicobject.h"

class LiveParameter
{
public:
    LiveParameter(int GID, QString parameterName, DataType type, QString current_value = "");

    DataType get_data_type();

    void* get_current_value();
    void* update_live_parameter();

private:
    DataType parameter_type;
    int GID;
    QString current_value;
    QString parameter_name;

    static int CURRENT_LP_ID;
    const int LP_ID;
};

#endif // LIVEPARAMETER_H
