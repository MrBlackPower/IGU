pressure-peaking

/*************************************
		HOW TO COMPILE IN UBUNTU
*************************************/

1. Install QT:

	a) Run :
		wget http://download.qt.io/official_releases/qt/5.12/5.12.2/qt-opensource-linux-x64-5.12.2.run

		chmod +x qt-opensource-linux-x64-5.12.2.run
	
		./qt-opensource-linux-x64-5.12.2.run
	b)Open QT.

2.Install Secondary Liraries

	c) Run :
		sudo apt-get install build-essential
		sudo apt-get install libfontconfig1
		sudo apt-get install mesa-common-dev
		sudo apt-get install libglu1-mesa-dev -y
		sudo apt-get install freeglut3-dev
		sudo apt-get install libflann-dev

3-Open Program in QT

	a)Download
		wget https://bitbucket.org/MrBlackPower/igu/get/0172791e89e0.zip

4-Configure Project to Compile CCO Core

	a)Adicionar processos de Make para:
		-f ArterialTreeCore_2D.mak
		-f ArterialTreeCore_3D.mak

*********************************************

Caso o erro "cannot find -lGL" permaneça, rode os seguintes comandos:

		sudo rm /usr/lib/x86_64-linux-gnu/libGL.so
		sudo ln -s /usr/lib/libGL.so.1 /usr/lib/x86_64-linux-gnu/libGL.so

