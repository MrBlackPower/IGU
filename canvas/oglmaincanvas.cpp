#include "oglmaincanvas.h"

/************************************************************************************
  Name:        oglmaincanvas.cpp
  Copyright:   Version 1.0
  Author:      Igor Pires dos Santos
  Last Update: 17/02/2017 (DD/MM/YYYY)
  Release:     --/--/---- (DD/MM/YYYY)
  Description: Class implementation of an OpenGL Widget that handles Linear Grphics.
************************************************************************************/

oglMainCanvas::oglMainCanvas(QWidget *parent) : OglCanvas("MAIN CANVAS",parent)
{

}

oglMainCanvas::~oglMainCanvas(){
    //dtor
}
