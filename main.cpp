/************************************************************************************
  Name:        main.cpp
  Copyright:   Version Beta 0.5
  Author:      Igor Pires dos Santos
  Last Update: 12/09/2017 (DD/MM/YYYY)
  Release:     --/--/---- (DD/MM/YYYY)
  Description: Main class of Pressure Peaking.
************************************************************************************/

#ifdef _WIN32 || _WIN64
    #include <direct.h>
    #define getcwd _getcwd // stupid MSFT "deprecation" warning
#else
    #include <unistd.h>
#endif

#include <QApplication>
#include <stdio.h>
#include <errno.h>
#include <iostream>
#include <sstream>
#include <string>
#include <cstring>
#include "model/arteryTree.h"
#include "helper/fileHelper.h"
#include "window/mainwindow.h"

#define VERSION 1.0

/************************************************************************************
  Name:        main.cpp
  Copyright:   Version 1.0
  Author:      Igor Pires dos Santos
  Last Update: 17/02/2017 (DD/MM/YYYY)
  Release:     --/--/---- (DD/MM/YYYY)
  Description: Main class of Pressure Peaking.

  **CONTAINS DEPRECATED COMMANDS**
************************************************************************************/

using namespace std;

/************************************************************************************
              ATTRIBUTES
************************************************************************************/

/************************************************************************************
        LIST OF ATTRIBUTES

        version.............string with this code version

        aTree...............pointer to artery tree used in this model

        hasTree.............if model has a loaded tree to be used

        main_loop...........if the program should keep exectuing the main loop

        main_window.........program's main window

        line................string used as a buffer

************************************************************************************/

QApplication* app;

window::MainWindow* main_window;

/************************************************
                   MAIN CLASS
*************************************************/
int main(int argc, char** argv)
{
    app = new QApplication(argc,argv);

    main_window = new window::MainWindow();
    main_window->show();

    return app->exec();
}

