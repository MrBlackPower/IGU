#include "lines3d.h"


Lines3D::Lines3D(SmartObject* parent) : GraphicObject("LINES 3D",parent)
{
    this->setColorBar(Black());
    N = 0;
    M = 0;
    setContextRange("Z-AXIS");
}

Lines3D::Lines3D(QString name, SmartObject* parent) : GraphicObject(name,parent)
{
    this->setColorBar(Black());
    N = 0;
    M = 0;
    setContextRange("Z-AXIS");
}

Lines3D::Lines3D(ColorBar cb, SmartObject* parent) : GraphicObject("LINES 3D",parent)
{
    this->setColorBar(cb);
    N = 0;
    M = 0;
    setContextRange("Z-AXIS");
}

Lines3D::Lines3D(QString name, ColorBar cb, SmartObject* parent) : GraphicObject(name,parent)
{
    this->setColorBar(cb);
    N = 0;
    M = 0;
    setContextRange("Z-AXIS");
}

void Lines3D::resizeTransform(){
    vector<Point> points;

    for(int i = 0; i < this->points.size(); i++)
        points.push_back(*(this->points.operator[](i)));

    resetLimit();
    updateLimit(points);
}

int Lines3D::getN(){
    return N;
}

int Lines3D::getM(){
    return M;
}

vector<QString> Lines3D::print(){
    QString buffer= "";
    vector<QString> cmds;

    cmds.push_back("/************************************************************************************");
    cmds.push_back("*                          LINES                                                    *");
    cmds.push_back("************************************************************************************/");

    for(int i = 0; i < lines.size(); i++){
        Lines3DParams::Line l = lines[i];
        Point* a = points[l.A];
        Point* b = points[l.B];
        buffer = buffer.asprintf("LINE[%d]:  [#%d](%.4f,%.4f,%.4f) -> [#%d](%.4f,%.4f,%.4f)\n",i,l.A,a->x,a->y,a->z,l.B,b->x,b->y,b->z);
        cmds.push_back(buffer);
    }

    cmds.push_back("************************************************************************************/");

    emit emit_log(SmartLogMessage(getSID(),"LINES 3D PRINTED"));
    return cmds;
}

vector<QString> Lines3D::data(){

    emit emit_log(SmartLogMessage(getSID(),"LINES 3D DATA SAVED"));
    return print();
}

vector<QString> Lines3D::raw(){
    vector<QString> raw;
    QString line;

    line = line.asprintf("# vtk DataFile Version 3.0\n");
    raw.push_back(line);
    line = line.asprintf("PRESSURE PEAKING 3D LINES OUTPUT SmartID = %d\n",getID());
    raw.push_back(line);
    line = line.asprintf("ASCII\n");
    raw.push_back(line);
    line = line.asprintf("DATASET POLYDATA\n");
    raw.push_back(line);
    line = line.asprintf("POINTS  %d  double\n", N);
    raw.push_back(line);

    for(int i = 0; i < points.size(); i++){
        Point* p = points[i];
        line = line.asprintf("%f  %f  %f\n", p->x, p->y, p->z);
        raw.push_back(line);
    }

    line = line.asprintf("\n");
    raw.push_back(line);
    line = line.asprintf("LINES %d %d\n", M, M * 3);
    raw.push_back(line);

    for(int i = 0; i < lines.size(); i++){
        Lines3DParams::Line l = lines[i];
        line = line.asprintf("2  %d  %d\n",l.A,l.B);
        raw.push_back(line);
    }

    line = line.asprintf("\n");
    raw.push_back(line);

    emit emit_log(SmartLogMessage(getSID(),"LINES 3D RAW DATA SAVED"));

    return raw;
}


bool Lines3D::load(VTKFile* file){

    emit emit_log(SmartLogMessage(getSID(),"LINES 3D LOADED",SmartLogType::LOG_LOAD));
    this->points.clear();
    lines.clear();
    N = 0;
    M = 0;

    vector<Point> points = file->getPoints();
    vector<Point*> aux;

    for(int i = 0; i < points.size(); i++){
        aux.push_back(points[i].clone());
    }

    addData(aux);

    return true;
}

vector<Field> Lines3D::getPointDataList(){
    vector<Field> list;
    return list;
}

vector<Field> Lines3D::getCellDataList(){
    vector<Field> list;
    return list;
}

bool Lines3D::initialize(){
    return true;
}

double Lines3D::iteration(){
    return 0.0;
}

void Lines3D::finish(){

}

bool Lines3D::updateContextVisualParameter(QString field, QString value){
    return false;
}

bool Lines3D::setContextRange(QString field){
    if(field =="Z-AXIS"){
        range.max = limit._far;
        range.min = limit._near;
        range.parameter = "Z-AXIS";
        return true;
    }

    return false;
}

QString Lines3D::getLiveParameterList(){
    return QString("");
}

QString Lines3D::getLiveParameter(QString name){
    return QString("");
}

DataType Lines3D::getLiveParameterDataType(QString name){
    return undefined;
}

void Lines3D::mouse_press_event(int width, int height, Point eye, QMouseEvent *event, GraphicTools tool){

}

void Lines3D::mouse_move_event(int width, int height, Point eye, QMouseEvent *event, GraphicTools tool){

}

vector<Field> Lines3D::getStartParameters(){
    vector<Field> params;

    return params;
}

vector<Field> Lines3D::getContextVisualParameters(){
    vector<Field> params;

    return params;
}

vector<QString> Lines3D::getRanges(){
    vector<QString> params;

    params.push_back("Z-AXIS");

    return params;
}

bool Lines3D::updateStartParameter(QString field, QString value){
    return false;
}

vector<QString> Lines3D::getExtrapolations(){
    vector<QString> aux;

    return aux;
}

GraphicObject* Lines3D::extrapolateContext(QString extrapolation){

}

bool Lines3D::addData(vector<Point*> data){
    //CLONES AND ADDS EVERY POINT
    for(int i = 0; i < data.size(); i++){
        Point* p = data[i];
        p = p->clone();
        addData(p);
    }

    return true;
}

bool Lines3D::addData(vector<Point> data){
    //CLONES AND ADDS EVERY POINT
    for(int i = 0; i < data.size(); i++){
        Point* p = data[i].clone();
        addData(p);
    }

    return true;
}

bool Lines3D::addData(Point* p){
    points.push_back(p);
    this->updateLimit(p);
    N++;

    return true;
}

bool Lines3D::compatibility(VTKFile *file){
    emit emit_log(SmartLogMessage(getSID(),"LINES 3D COMPATIBILITY RUN",SmartLogType::LOG_COMPATIBILITY));

    //CHECKS IF ALL LINES HAVE POINT REPRESENTATIONS
    vector<vtk::Line> lines = file->getLines();
    int n = file->getN();

    for(int i = 0; i < lines.size(); i++){
        vtk::Line l = lines[i];

        if(l.n != 2){
            if(l.points[ZERO] >= n || l.points[ONE] >= n)
                return false;
        }
    }

    return true;
}

void Lines3D::draw(){
    glBegin(GL_LINES);
    for(int i = 0; i < M; i++){
        Lines3DParams::Line l = lines[i];
        Point a = *points[l.A];
        Point b = *points[l.B];

        setMaterialColor(interpolateColor(a.z));
        glVertex3f(a.x, a.y, a.z);

        setMaterialColor(interpolateColor(b.z));
        glVertex3f(b.x, b.y, b.z);
    }
    glEnd();


    glPointSize(5.0);
    glBegin(GL_POINTS);
    for(int i = 0; i < N; i++){
        Point a = *points[i];

        setMaterialColor(interpolateColor(a.z));
        glVertex3f(a.x, a.y, a.z);
    }
    glEnd();
}

bool Lines3D::addLine(int a, int b){
    if(a < 0 || a >= N)
        return false;

    if(b < 0 || b >= N)
        return false;

    Lines3DParams::Line l;
    l.A = a;
    l.B = b;

    lines.push_back(l);
    M++;

    return true;
}

bool Lines3D::addLine(){
    if(N < 2)
        return false;

    Lines3DParams::Line l;
    l.A = N-2;
    l.B = N-1;

    lines.push_back(l);
    M++;

    return true;
}
