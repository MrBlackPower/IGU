#include "staticgraphic.h"

StaticGraphic::StaticGraphic(QString name, SmartObject* parent) : Graphic2D(name,parent){
    setContextRange("Y-AXIS");
}

StaticGraphic::StaticGraphic(ColorBar cb, SmartObject* parent) : Graphic2D("STATIC GRAPHIC",cb,parent){
    setContextRange("Y-AXIS");
}

bool StaticGraphic::initialize(){
    return true;
}

double StaticGraphic::iteration(){
    return 0.0;
}

bool StaticGraphic::updateContextVisualParameter(QString field, QString value){
    return false;
}

bool StaticGraphic::setContextRange(QString field){
    if(field == "Y-AXIS"){
        range.max = limit.top;
        range.min = limit.bottom;
        range.parameter = "Y-AXIS";
        return true;
    }
    return false;
}

QString StaticGraphic::getLiveParameterList(){
    return QString("");
}

QString StaticGraphic::getLiveParameter(QString name){
    return QString("");
}

DataType StaticGraphic::getLiveParameterDataType(QString name){
    return undefined;
}

void StaticGraphic::mouse_press_event(int width, int height, Point eye, QMouseEvent *event, GraphicTools tool){

}

void StaticGraphic::mouse_move_event(int width, int height, Point eye, QMouseEvent *event, GraphicTools tool){

}

vector<Field> StaticGraphic::getStartParameters(){
    vector<Field> params;

    return params;
}

vector<Field> StaticGraphic::getContextVisualParameters(){
    vector<Field> params;

    return params;
}

bool StaticGraphic::updateStartParameter(QString field, QString value){
    return false;
}

vector<QString> StaticGraphic::getRanges(){
    vector<QString> params;

    params.push_back("Y-AXIS");

    return params;
}
