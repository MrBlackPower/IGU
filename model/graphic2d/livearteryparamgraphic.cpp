#include "livearteryparamgraphic.h"


LiveArteryParamGraphic::LiveArteryParamGraphic(vector<Artery*> arteries, QString name, SmartObject* parent): Graphic2D(name,parent){
    this->arteries = arteries;
    setContextRange("PRESSURE");
}

LiveArteryParamGraphic::LiveArteryParamGraphic(vector<Artery*> arteries, ColorBar* cb, SmartObject* parent): Graphic2D("LIVE ARTERY PARAM GRAPHIC",parent){
    this->arteries = arteries;
    setContextRange("PRESSURE");
}

bool LiveArteryParamGraphic::initialize(){
    update();

    return true;
}

double LiveArteryParamGraphic::iteration(){
    clock_t begin = std::clock();

    update();

    clock_t end = std::clock();
    return double(end - begin) / CLOCKS_PER_SEC;
}

void LiveArteryParamGraphic::update(){
    clearData();

    if(range.parameter == "PRESSURE" || range.parameter == "FLOW"){
        vector<Point> data;
        range.max = -1.0;
        range.min = 1.0;

        for(int i = 0; i < arteries.size(); i++){
             vector<Point*> points;
             Artery* a = arteries[i];
             if(a->getMediumPressure().real() != -1){
                 if(range.parameter == "PRESSURE"){
                     points = a->getPressureWave();
                 }
                 if(range.parameter == "FLOW"){
                     points = a->getFlowWave();
                 }

                 //ADDS EVERY POINT
                 for(int j = 0; j < points.size(); j++){
                     Point* point = points[j];
                     Point aux = Point(N,(point->x + i),point->y);
                     data.push_back(aux);
                 }
             }
        }

        if(!data.empty())
            addData(data);

    }

    if(range.parameter == "MEDIUM_PRESSURE"){
        vector<Point> data;
        range.max = -1.0;
        range.min = 1.0;


        for(int i = 0; i < arteries.size(); i++){
            double value;
            Artery* a = arteries[i];

            if(range.parameter == "MEDIUM_PRESSURE"){
                value = abs(a->getMediumPressure());
            }

            Point aux = Point(N,i,value);
            data.push_back(aux);
        }

        addData(data);
    }
}

bool LiveArteryParamGraphic::updateContextVisualParameter(QString field, QString value){
    return false;
}

bool LiveArteryParamGraphic::setContextRange(QString field){
    if(field == "PRESSURE"){
        range.parameter = "PRESSURE";
        range.max = -1.0;
        range.min = 1.0;
        return true;
    } else if(field == "FLOW"){
        range.parameter = "FLOW";
        range.max = -1.0;
        range.min = 1.0;
        return true;
    } else if(field == "MEDIUM_PRESSURE"){
        range.parameter = "MEDIUM_PRESSURE";
        range.max = -1.0;
        range.min = 1.0;
        return true;
    }

    return false;
}

QString LiveArteryParamGraphic::getLiveParameterList(){
    return QString("");
}

QString LiveArteryParamGraphic::getLiveParameter(QString name){
    return QString("");
}

DataType LiveArteryParamGraphic::getLiveParameterDataType(QString name){
    return undefined;
}

void LiveArteryParamGraphic::mouse_press_event(int width, int height, Point eye, QMouseEvent *event, GraphicTools tool){

}

void LiveArteryParamGraphic::mouse_move_event(int width, int height, Point eye, QMouseEvent *event, GraphicTools tool){

}

vector<Field> LiveArteryParamGraphic::getStartParameters(){
    vector<Field> params;

    return params;
}

vector<Field> LiveArteryParamGraphic::getContextVisualParameters(){
    vector<Field> params;

    return params;
}

bool LiveArteryParamGraphic::updateStartParameter(QString field, QString value){
    return false;
}

vector<QString> LiveArteryParamGraphic::getRanges(){
    vector<QString> params;

    params.push_back("PRESSURE");
    params.push_back("FLOW");
    params.push_back("MEDIUM_PRESSURE");

    return params;
}
