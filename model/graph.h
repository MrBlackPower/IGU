#ifndef GRAPH_H
#define GRAPH_H

#include "graphicobject.h"
#include "graph/nodegraph.h"
#include "graph/node.h"
#include "graph/solution.h"
#include <vector>
#include <QMouseEvent>
#include <QString>

#define ZERO 0
#define ONE  1
#define TWO  2

#define DEFAULT_N 100
#define DEFAULT_M 100
#define DEFAULT_x 10.0
#define DEFAULT_y 10.0
#define DEFAULT_z 0.0
#define DEFAULT_RADIUS 0.1

using namespace std;

template <class n_type, class e_type>
class Graph : public GraphicObject{
public:
    Graph(QString name, SmartObject* parent = NULL);

    void resizeTransform();

    void hover(NodeGraph<n_type,e_type>* node);
    void unhover();

    void select(NodeGraph<n_type,e_type>* node);
    void unselect();

    //METHODS
    bool addNode(n_type data, double x, double y, double z = 0.0);
    bool addEdge(bool directional, NodeGraph<n_type,e_type>* start, NodeGraph<n_type,e_type>* end, e_type weight);
    bool updateEdge(bool directional, NodeGraph<n_type,e_type>* start, NodeGraph<n_type,e_type>* end, e_type weight);

    void clear();

    bool deleteNode(double x, double y);
    bool deleteNode(NodeGraph<n_type,e_type>* n);

    bool deleteEdge(NodeGraph<n_type,e_type>* a,NodeGraph<n_type,e_type>* b);

    int edges();

    bool hasNode(NodeGraph<n_type,e_type>* n);

    void populate();
    void populate(int N, int M, double x = DEFAULT_x, double y = DEFAULT_y, double z = DEFAULT_z);
    void populateNodes(int N, double x = DEFAULT_x, double y = DEFAULT_y, double z = DEFAULT_z);
    void populateEdges(int M, double x = DEFAULT_x, double y = DEFAULT_y, double z = DEFAULT_z);
    void populate(vector<n_type> data);
    void populate(vector<Point> pos, vector<n_type> data);
    int size();

    NodeGraph<n_type,e_type>* operator[] (int n);
    NodeGraph<n_type,e_type>* operator() (int n,int e);

    NodeGraph<n_type,e_type>* selectNode(double x, double y, double radius);
    NodeGraph<n_type,e_type>* getStart();
    NodeGraph<n_type,e_type>* getEnd();

    bool dominant(Solution* s);


    bool dominant(Solution* s, vector<int>* dmntd, vector<int>* non_dmntd);
    bool dominant(Solution* s, vector<pair<int,int>>* dmntd, vector<int>* non_dmntd);

    bool dominant(vector<int> nodes);
    int countNonDominated(Solution* s);
    int countNonDominated(vector<int> nodes);

    bool connex();
    bool connex(Solution* s);
    bool connex(vector<int> nodes);
    bool connex(vector<NodeGraph<n_type,e_type>*> nodes);

    int depthTree(vector<int>* visited, vector<int>* parent, NodeGraph<n_type,e_type>* root);
    void depthTreeAux(vector<int>* visited, vector<int>* parent, NodeGraph<n_type,e_type>* it, int father);

    void crescentStoredSolutions();
    void decrescentStoredSolutions();

    vector<NodeGraph<n_type,e_type>*> getSolutionNodes(Solution* s);
    vector<NodeGraph<n_type,e_type>*> getSolutionNodes(vector<int> s);

    //SMART OBJECT METHODS
    vector<QString> print();
    vector<QString> data();
    vector<QString> raw();

    //GRAPHIC OBJECT METHODS
    void draw();

    //VIRTUAL GRAPH METHODS
    virtual void drawContext() = 0;
    virtual bool validSolution(Solution* s) = 0;
    virtual double scoreSolution(Solution* s) = 0;
protected:

    int N; // number of nodes
    int M; // number of edges
    vector<NodeGraph<n_type,e_type>*> nodes;
    NodeGraph<n_type,e_type>* start;
    NodeGraph<n_type,e_type>* end;
    NodeGraph<n_type,e_type>* selected;
    NodeGraph<n_type,e_type>* hovered;

    Solution* bestSolution;
    Solution* lastSolution;

    double sphere_radius;
    bool draw_node;
    bool draw_edge;

    GraphicObjectComposer new_node(Point p);
    GraphicObjectComposer edit_node(NodeGraph<n_type,e_type>* n);

    bool new_node(GraphicObjectComposer node);
    bool edit_node(NodeGraph<n_type,e_type>* n, GraphicObjectComposer node);

    GraphicObjectComposer new_edge(NodeGraph<n_type,e_type>* start, NodeGraph<n_type,e_type>* end);
    GraphicObjectComposer edit_edge(NodeGraph<n_type,e_type>* start, NodeGraph<n_type,e_type>* end);

    bool new_edge(NodeGraph<n_type,e_type>* start, NodeGraph<n_type,e_type>* end,GraphicObjectComposer node);
    bool edit_edge(NodeGraph<n_type,e_type>* start, NodeGraph<n_type,e_type>* end, GraphicObjectComposer node);

    bool hasEdge(NodeGraph<n_type,e_type>* start, NodeGraph<n_type,e_type>* end);

private:
    void connex(vector<int>* non_visited, vector<int>* visited, NodeGraph<n_type,e_type>* it);

    //VIRTUAL GRAPHIC OBJECT METHODS
    virtual QString getLiveParameterList() = 0;
    virtual QString getLiveParameter(QString name) = 0;
    virtual DataType getLiveParameterDataType(QString name) = 0;
    virtual vector<QString> getExtrapolations() = 0;
    virtual void mouse_press_event(int width, int height, Point eye, QMouseEvent *event, GraphicTools tool) = 0;
    virtual void mouse_move_event(int width, int height, Point eye, QMouseEvent *event, GraphicTools tool) = 0;
};

#endif // GRAPH_H
