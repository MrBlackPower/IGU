#include "ktgraphic3d.h"

KTGraphic3D::KTGraphic3D(int m, int n, SmartObject* parent) : Graphic3D("KT GRAPHIC 3D",m,n,parent)
{
    error.absolute = 0.0;
    startParams.m = m;
    startParams.n = n;
    startParams.dx = (double)(startParams.b - startParams.a) / (startParams.m-1);
    startParams.dy = (double)(startParams.d - startParams.c) / (startParams.n-1);
    error.numel = startParams.n;
    setContextRange("Z-AXIS");
}

KTGraphic3D::KTGraphic3D(int example, SmartObject* parent) : Graphic3D(("KT GRAPHIC 3D " + example),DEFAULT_M,DEFAULT_N,parent)
{
    error.absolute = 0.0;
    startParams.example = example;

    switch(example){
    case 0:
        startParams.m = DEFAULT_M;
        startParams.n = DEFAULT_N;
        break;
    case 1:
        startParams.a = 0.0;
        startParams.b = 1.0;
        startParams.c = 0.0;
        startParams.d = 1.0;
        startParams.m = DEFAULT2_M;
        startParams.n = DEFAULT2_N;
        break;
    default:
        startParams.m = DEFAULT_M;
        startParams.n = DEFAULT_N;
        break;
    }

    startParams.dx = (double)(startParams.b - startParams.a) / (startParams.m-1);
    startParams.dy = (double)(startParams.d - startParams.c) / (startParams.n-1);
    error.numel = startParams.n;
    setN(startParams.n);
    setM(startParams.m);
    setContextRange("Z-AXIS");
}

KTGraphic3D::KTGraphic3D(SmartObject* parent) : Graphic3D("KT GRAPHIC 3D", DEFAULT_M,DEFAULT_N, parent)
{
    error.absolute = 0.0;
    startParams.dx = (double)(startParams.b - startParams.a) / (startParams.m-1);
    startParams.dy = (double)(startParams.d - startParams.c) / (startParams.n-1);
    error.numel = startParams.n;
    setContextRange("Z-AXIS");
}

KTGraphic3D::KTGraphic3D(QString name, SmartObject* parent) : Graphic3D(name, DEFAULT_M,DEFAULT_N, parent)
{
    error.absolute = 0.0;
    startParams.dx = (double)(startParams.b - startParams.a) / (startParams.m-1);
    startParams.dy = (double)(startParams.d - startParams.c) / (startParams.n-1);
    error.numel = startParams.n;
    setContextRange("Z-AXIS");
}

bool KTGraphic3D::setUV(vector<KTVelocity> vels){
    if(vels.size() != (startParams.m * startParams.n))
        return false;

    for(int i = 0; i < (startParams.m * startParams.n); i++)
        uv.push_back(vels[i]);

    return true;
}

bool KTGraphic3D::setVV(vector<KTVelocity> vels){
    if(vels.size() != (startParams.m * startParams.n))
        return false;

    for(int i = 0; i < (startParams.m * startParams.n); i++)
        vv.push_back(vels[i]);

    return true;
}


bool KTGraphic3D::initialize(){
    if(N == 0){
        switch (startParams.example) {
        case 0:
        {
            resetLimit();
            N = startParams.m * startParams.n;
            for(int j = 0; j < startParams.n; j++){
                for(int i = 0; i < startParams.m; i++){
                    Point r1 = Point(0,0.5,0.5);
                    Point r2 = Point(0,-0.5,-0.5);
                    Point* p = new Point(0,(startParams.a + (i * startParams.dx)),(startParams.c + (j * startParams.dy)));

                    if(MathHelper::euclideanDistance(p,&r1) <= 0.4){
                        p->z = -ONE;
                    }

                    if(MathHelper::euclideanDistance(p,&r2) <= 0.4){
                        p->z = ONE;
                    }

                    raw_data.push_back(p);

                    updateLimit(p);
                }
            }

            //DEFINES VELOCITIES CAMP
            for(int i = 0; i < N; i++){
                KTVelocity vel;
                vel.a = ONE;
                vel.b = ONE;
                uv.push_back(vel);
                vv.push_back(vel);
            }
            break;
        }
        case 1:
        {
            resetLimit();
            N = startParams.m * startParams.n;
            for(int j = 0; j < startParams.n; j++){
                for(int i = 0; i < startParams.m; i++){
                    Point* p = new Point(0,(startParams.a + (i * startParams.dx)),(startParams.c + (j * startParams.dy)));

                    if(i == 0)
                        p->z = 1.0;
                    else
                        p->z = 0.21;

                    raw_data.push_back(p);

                    updateLimit(p);
                }
            }
            //SET UP VELOCITIES IF NONE WAS SET
            if(uv.size() != N || vv.size() != N){
                for(int i = 0; i < N; i++){
                    KTVelocity vel;
                    vel.a = ONE;
                    vel.b = ONE;
                    uv.push_back(vel);
                    vv.push_back(vel);
                }
            }
            break;
        }
        default:
        {
            resetLimit();
            N = startParams.m * startParams.n;
            raw_data.reserve(N);
            for(int j = 0; j < startParams.n; j++){
                for(int i = 0; i < startParams.m; i++){
                    Point r1 = Point(0,0.5,0.5);
                    Point r2 = Point(0,-0.5,-0.5);
                    Point* p = new Point(0,(startParams.a + (i * startParams.dx)),(startParams.c + (j * startParams.dy)));

                    if(MathHelper::euclideanDistance(p,&r1) <= 0.4){
                        p->z = -ONE;
                    }

                    if(MathHelper::euclideanDistance(p,&r2) <= 0.4){
                        p->z = ONE;
                    }

                    raw_data.push_back(p);

                    updateLimit(p);
                }
            }
            startParams.example = 0;
            break;
        }
        }
    }
    double max = raw_data[0]->z;

    for(int i = 1; i < N; i++)
        if(max < raw_data[i]->z)
            max = raw_data[i]->z;

    switch (startParams.example) {
    case 0:
        time.deltaT = 0.120*startParams.dx / max;
        break;
    case 1:
        time.deltaT = 0.0005*startParams.dx / max;
        break;
    default:
        time.deltaT = 0.120*startParams.dx / max;
        break;
    }


    ColorBar cb = Brasil();
    setMax(limit._far);
    setMin(limit._near);
    this->setColorBar(cb);
}

double KTGraphic3D::iteration(){
    vector<double> du = KT();
    vector<double> u;
    u.reserve(N);

    for(int i = 0; i < N; i++){
        Point* p = raw_data[i];
        u.push_back(p->z + time.deltaT * du[i]);
    }

    time.iterations ++;
    time.modelTime += time.deltaT;

    //UPDATES THE RAW DATA VALUE
    for(int i = 0; i < N; i++)
        raw_data[i]->z = u[i];

    double max = raw_data[0]->z;

    for(int i = 1; i < N; i++)
        if(max < raw_data[i]->z)
            max = raw_data[i]->z;

    switch (startParams.example) {
    case 0:
        time.deltaT = 0.120*startParams.dx / max;
        break;
    case 1:
        time.deltaT = 0.0005*startParams.dx / max;
        break;
    default:
        time.deltaT = 0.120*startParams.dx / max;
        break;
    }
}

bool KTGraphic3D::updateContextVisualParameter(QString field, QString value){
    return false;
}

QString KTGraphic3D::getLiveParameterList(){
    return QString("");
}

QString KTGraphic3D::getLiveParameter(QString name){
    return QString("");
}

DataType KTGraphic3D::getLiveParameterDataType(QString name){
    return undefined;
}

bool KTGraphic3D::setContextRange(QString field){
    if(field == "Z-AXIS"){
        range.max = limit._far;
        range.min = limit._near;
        range.parameter = "Z-AXIS";
        return true;
    }

    return false;
}

Lines3D* KTGraphic3D::contour(double f){
    Lines3D* line = new Lines3D();
    for(int i = 0; i < getM()-1; i++){
        for(int j = 0; j < getN()-1; j++){
            if(N > ID(i+1,j+1)){
                Point a = *raw_data[ID(i,j)];
                Point b = *raw_data[ID(i+1,j)];
                Point c = *raw_data[ID(i+1,j+1)];
                Point d = *raw_data[ID(i,j+1)];

                bool vInside[] = {(a.z > f),(b.z > f),(c.z > f),(d.z > f)};

                vector<int> ids;
                if((!vInside[0]&&vInside[1])||(vInside[0]&&!vInside[1])){
                   Point p = MathHelper::interpolate(a,b,a.z,b.z,f);
                   ids.push_back(line->getN());
                   line->addData(p.clone());
                }
                if((!vInside[1]&&vInside[2])||(vInside[1]&&!vInside[2])){
                   Point p = MathHelper::interpolate(b,c,b.z,c.z,f);
                   ids.push_back(line->getN());
                   line->addData(p.clone());
                }
                if((!vInside[2]&&vInside[3])||(vInside[2]&&!vInside[3])){
                   Point p = MathHelper::interpolate(c,d,c.z,d.z,f);
                   ids.push_back(line->getN());
                   line->addData(p.clone());
                }
                if((!vInside[3]&&vInside[0])||(vInside[3]&&!vInside[0])){
                   Point p = MathHelper::interpolate(d,a,d.z,a.z,f);
                   ids.push_back(line->getN());
                   line->addData(p.clone());
                }

                for(int i = 1; i < ids.size(); i++){
                    line->addLine(ids[i-1],ids[i]);
                }
            }
        }
    }

    return line;
}

void KTGraphic3D::mouse_press_event(int width, int height, Point eye, QMouseEvent *event, GraphicTools tool){

}

void KTGraphic3D::mouse_move_event(int width, int height, Point eye, QMouseEvent *event, GraphicTools tool){

}

vector<Field> KTGraphic3D::getStartParameters(){
    vector<Field> params;

   Field aux;
   aux.names = "EXAMPLE";
   aux.required = false;
   aux.stream = FIELD_IN;
   aux.type = integer;

    params.push_back(aux);

    return params;
}

vector<Field> KTGraphic3D::getContextVisualParameters(){
    vector<Field> params;

    return params;
}

vector<QString> KTGraphic3D::getRanges(){
    vector<QString> params;

    params.push_back("Z-AXIS");

    return params;
}

bool KTGraphic3D::updateStartParameter(QString field, QString value){
    if(field == "EXAMPLE"){
        startParams.example = value.toInt();
        return true;
    }

    return false;
}

vector<double> KTGraphic3D::KT(){
    vector<double> raw_data;
    for(int i = 0; i < this->raw_data.size(); i++){
        Point* p =this->raw_data[i];
        raw_data.push_back(p->z);
    }

    if(raw_data.size() != N || N != (startParams.m * startParams.n))
        return raw_data;

    vector<double> ux = doUx();
    vector<double> uy = doUy();
    int phi = ONE;
    switch (startParams.example) {
    case 0:
        break;
    case 1:
        phi = 5;
        break;
    default:
        break;
    }

    vector<double> H1x; //Hj +1/2
    vector<double> H2x; //Hj -1/2
    vector<double> a1x; //Hj +1/2
    vector<double> a2x; //Hj -1/2
    vector<double> uminus1x; //Hj +1/2
    vector<double> uminus2x; //Hj -1/2
    vector<double> uplus1x; //Hj +1/2
    vector<double> uplus2x; //Hj -1/2

    vector<double> H1y; //Hj +1/2
    vector<double> H2y; //Hj -1/2
    vector<double> a1y; //Hj +1/2
    vector<double> a2y; //Hj -1/2
    vector<double> uminus1y; //Hj +1/2
    vector<double> uminus2y; //Hj -1/2
    vector<double> uplus1y; //Hj +1/2
    vector<double> uplus2y; //Hj -1/2

    vector<double> dux;
    vector<double> duy;
    vector<double> du;

    H1x.reserve(N);
    H2x.reserve(N);
    a1x.reserve(N);
    a2x.reserve(N);

    uminus1x.reserve(N);
    uminus2x.reserve(N);
    uplus1x.reserve(N);
    uplus2x.reserve(N);

    H1y.reserve(N);
    H2y.reserve(N);
    a1y.reserve(N);
    a2y.reserve(N);

    uminus1y.reserve(N);
    uminus2y.reserve(N);
    uplus1y.reserve(N);
    uplus2y.reserve(N);

    dux.reserve(N);
    duy.reserve(N);
    du.reserve(N);

    //PARA X - U

    // U + 1/2
    switch(startParams.coutour_type){
    case 0:
        for(int j = 0; j < startParams.n; j++){
            int N = startParams.m;
            uplus1x[ID(N-1,j)] = raw_data[ID(0,j)] - startParams.dx * ux[ID(0,j)] / 2.0;
        }
        break;
    case 1:
        for(int j = 0; j < startParams.n; j++){
            int N = startParams.m;
            uplus1x[ID(N-1,j)] = raw_data[ID(N-1,j)] - startParams.dx * ux[ID(N-1,j)] / 2.0;
        }
        break;
    default:
        for(int j = 0; j < startParams.n; j++){
            int N = startParams.m;
            uplus1x[ID(N-1,j)] = raw_data[ID(0,j)] - startParams.dx * ux[ID(0,j)] / 2.0;
        }
        break;
    }

    for(int j = 0; j < startParams.n; j++){
        for(int i = 0; i < startParams.m - 1; i ++){
            uplus1x[ID(i,j)] = raw_data[ID(i+1,j)] - startParams.dx * ux[ID(i+1,j)] / 2.0;
        }
    }

    for(int j = 0; j < startParams.n; j++){
        for(int i = 0; i < startParams.m; i++){
            uminus1x[ID(i,j)] = raw_data[ID(i,j)] + startParams.dx * ux[ID(i,j)] / 2.0;
        }
    }

    // U - 1/2
    for(int i = 0; i < startParams.m; i++){
        for(int j = 0; j < startParams.n; j++){
            uplus2x[ID(i,j)] = raw_data[ID(i,j)] - startParams.dx * ux[ID(i,j)] / 2.0;
        }
    }

    switch(startParams.coutour_type){
    case 0:
        for(int j = 0; j < startParams.n; j++){
            uminus2x[ID(0,j)] = raw_data[ID(N-1,j)] - startParams.dx * ux[ID(N-1,j)] / 2.0;
        }
        break;
    case 1:
        for(int j = 0; j < startParams.n; j++){
            uminus2x[ID(0,j)] = raw_data[ID(0,j)] - startParams.dx * ux[ID(0,j)] / 2.0;
        }
        break;
    default:
        for(int j = 0; j < startParams.n; j++){
            uminus2x[ID(0,j)] = raw_data[ID(N-1,j)] - startParams.dx * ux[ID(N-1,j)] / 2.0;
        }
        break;
    }

    for(int j = 0; j < startParams.n; j++){
        for(int i = 1; i < startParams.m; i++){
            uminus2x[ID(i,j)] = raw_data[ID(i-1,j)] + startParams.dx * ux[ID(i-1,j)] / 2.0;
        }
    }

    //PARA Y - U

    // U + 1/2
    switch(startParams.coutour_type){
    case 0:
        for(int i = 0; i < startParams.m; i++){
            int N = startParams.n;
            uplus1y[ID(i,N-1)] = raw_data[ID(i,0)] - startParams.dy * uy[ID(i,0)] / 2.0;
        }
        break;
    case 1:
        for(int i = 0; i < startParams.m; i++){
            int N = startParams.n;
            uplus1y[ID(i,N-1)] = raw_data[ID(i,N-1)] - startParams.dy * uy[ID(i,N-1)] / 2.0;
        }
        break;
    default:
        for(int i = 0; i < startParams.m; i++){
            int N = startParams.n;
            uplus1y[ID(i,N-1)] = raw_data[ID(i,0)] - startParams.dy * uy[ID(i,0)] / 2.0;
        }
        break;
    }

    for(int i = 0; i < startParams.m; i++){
        for(int j = 0; j < startParams.n - 1; j ++){
            uplus1y[ID(i,j)] = raw_data[ID(i,j+1)] - startParams.dy * uy[ID(i,j+1)] / 2.0;
        }
    }

    for(int i = 0; i < startParams.m; i++){
        for(int j = 0; j < startParams.n; j++){
            uminus1y[ID(i,j)] = raw_data[ID(i,j)] + startParams.dy * uy[ID(i,j)] / 2.0;
        }
    }

    // U - 1/2
    for(int i = 0; i < startParams.m; i++){
        for(int j = 0; j < startParams.n; j++){
            uplus2y[ID(i,j)] = raw_data[ID(i,j)] - startParams.dy * uy[ID(i,j)] / 2.0;
        }
    }

    switch(startParams.coutour_type){
    case 0:
        for(int i = 0; i < startParams.m; i++){
            int N = startParams.n;
            uminus2y[ID(i,0)] = raw_data[ID(i,N-1)] - startParams.dy * uy[ID(i,N-1)] / 2.0;
        }
        break;
    case 1:
        for(int i = 0; i < startParams.m; i++){
            uminus2y[ID(i,0)] = raw_data[ID(i,0)] - startParams.dy * uy[ID(i,0)] / 2.0;
        }
        break;
    default:
        for(int i = 0; i < startParams.m; i++){
            int N = startParams.n;
            uminus2y[ID(i,0)] = raw_data[ID(i,N-1)] - startParams.dy * uy[ID(i,N-1)] / 2.0;
        }
        break;
    }

    for(int i = 0; i < startParams.m; i++){
        for(int j = 1; j < startParams.n; j++){
            uminus2y[ID(i,j)] = raw_data[ID(i,j-1)] + startParams.dy * uy[ID(i,j-1)] / 2.0;
        }
    }

    //PARA X - A

    for(int j = 0; j < startParams.n; j++){
        for(int i = 0; i < startParams.m; i++){
            vector<double> aux1;
            vector<double> aux2;

            aux1.push_back(abs((uv[ID(i,j)].b/phi)*df(uplus1x[ID(i,j)])));
            aux1.push_back(abs((uv[ID(i,j)].b/phi)*df(uminus1x[ID(i,j)])));

            aux2.push_back(abs((uv[ID(i,j)].a/phi)*df(uplus2x[ID(i,j)])));
            aux2.push_back(abs((uv[ID(i,j)].a/phi)*df(uminus2x[ID(i,j)])));

            a1x[ID(i,j)] = MathHelper::max(aux1);
            a2x[ID(i,j)] = MathHelper::max(aux2);
        }
    }

    //PARA Y - A

    for(int j = 0; j < startParams.n; j++){
        for(int i = 0; i < startParams.m; i++){
            vector<double> aux1;
            vector<double> aux2;

            aux1.push_back(abs((vv[ID(i,j)].b/phi)*df(uplus1y[ID(i,j)])));
            aux1.push_back(abs((vv[ID(i,j)].b/phi)*df(uminus1y[ID(i,j)])));

            aux2.push_back(abs((vv[ID(i,j)].a/phi)*df(uplus2y[ID(i,j)])));
            aux2.push_back(abs((vv[ID(i,j)].a/phi)*df(uminus2y[ID(i,j)])));

            a1y[ID(i,j)] = MathHelper::max(aux1);
            a2y[ID(i,j)] = MathHelper::max(aux2);
        }
    }

    //PARA X - H

    for(int j = 0; j < startParams.n; j++){
        for(int i = 0; i < startParams.m; i ++){
            H1x.push_back(( f(uplus1x[ID(i,j)]) + f(uminus1x[ID(i,j)]) )/(2.0 * phi) - a1x[ID(i,j)] * (uplus1x[ID(i,j)]-uminus1x[ID(i,j)]) / 2.0);
            H2x.push_back(( f(uplus2x[ID(i,j)])+f(uminus2x[ID(i,j)]) ) / (2.0 * phi) - a2x[ID(i,j)] * (uplus2x[ID(i,j)]-uminus2x[ID(i,j)]) / 2.0);
            dux.push_back((H2x[ID(i,j)] - H1x[ID(i,j)]) / startParams.dx);
        }
    }

    //PARA Y - H

    for(int j = 0; j < startParams.n; j++){
        for(int i = 0; i < startParams.m; i ++){
            H1y.push_back(( f(uplus1y[ID(i,j)]) + f(uminus1y[ID(i,j)]) )/(2.0 * phi) - a1y[ID(i,j)] * (uplus1y[ID(i,j)]-uminus1y[ID(i,j)]) / 2.0);
            H2y.push_back(( f(uplus2y[ID(i,j)])+f(uminus2y[ID(i,j)]) ) / (2.0 * phi) - a2y[ID(i,j)] * (uplus2y[ID(i,j)]-uminus2y[ID(i,j)]) / 2.0);
            duy.push_back((H2y[ID(i,j)] - H1y[ID(i,j)]) / startParams.dy);
        }
    }

    for(int j = 0; j < startParams.n; j++){
        for(int i = 0; i < startParams.m; i ++){
            du.push_back(dux[ID(i,j)] + duy[ID(i,j)]);
        }
    }

    return du;
}

vector<double> KTGraphic3D::doUx(){
    vector<double> raw_data;

    if(N != (startParams.m * startParams.n))
        return raw_data;

    for(int i = 0; i < this->raw_data.size(); i++){
        Point* p = this->raw_data[i];
        raw_data.push_back(p->z);
    }

    double theta = 2.0;
    vector<double> a;
    vector<double> b;
    vector<double> c;
    vector<double> ux;

    a.reserve(N);
    b.reserve(N);
    c.reserve(N);
    ux.reserve(N);

    switch (startParams.coutour_type) {
    //CIRCULAR
    case 0:
        for(int j = 0; j < startParams.n; j++){
            int N = startParams.m;
            a[ID(0,j)] = (raw_data[ID(0,j)] - raw_data[ID(N-1,j)])/startParams.dx;
            a[ID(N-1,j)] = (raw_data[ID(N-1,j)] - raw_data[ID(N-2,j)])/startParams.dx;

            b[ID(0,j)] = (raw_data[ID(1,j)] - raw_data[ID(N-1,j)])/(2*startParams.dx);
            b[ID(N-1,j)] = (raw_data[ID(0,j)] - raw_data[ID(N-2,j)])/(2*startParams.dx);

            c[ID(0,j)] = (raw_data[ID(1,j)] - raw_data[ID(0,j)])/startParams.dx;
            c[ID(N-1,j)] = (raw_data[ID(0,j)] - raw_data[ID(N-1,j)])/startParams.dx;
        }
        break;

    //DUPLICATED
    case 1:
        for(int j = 0; j < startParams.n; j++){
            int N = startParams.m;
            a[ID(0,j)] = (raw_data[ID(0,j)] - raw_data[ID(0,j)])/startParams.dx;
            a[ID(N-1,j)] = (raw_data[ID(N-1,j)] - raw_data[ID(N-2,j)])/startParams.dx;

            b[ID(0,j)] = (raw_data[ID(1,j)] - raw_data[ID(0,j)])/(2*startParams.dx);
            b[ID(N-1,j)] = (raw_data[ID(N-1,j)] - raw_data[ID(N-2,j)])/(2*startParams.dx);

            c[ID(0,j)] = (raw_data[ID(1,j)] - raw_data[ID(0,j)])/startParams.dx;
            c[ID(N-1,j)] = (raw_data[ID(N-1,j)] - raw_data[ID(N-1,j)])/startParams.dx;
        }
        break;
    default:
        for(int j = 0; j < startParams.n; j++){
            int N = startParams.m;
            a[ID(0,j)] = (raw_data[ID(0,j)] - raw_data[ID(N-1,j)])/startParams.dx;
            a[ID(N-1,j)] = (raw_data[ID(N-1,j)] - raw_data[ID(N-2,j)])/startParams.dx;

            b[ID(0,j)] = (raw_data[ID(1,j)] - raw_data[ID(N-1,j)])/(2*startParams.dx);
            b[ID(N-1,j)] = (raw_data[ID(0,j)] - raw_data[ID(N-2,j)])/(2*startParams.dx);

            c[ID(0,j)] = (raw_data[ID(1,j)] - raw_data[ID(0,j)])/startParams.dx;
            c[ID(N-1,j)] = (raw_data[ID(0,j)] - raw_data[ID(N-1,j)])/startParams.dx;
        }
        break;
    }

    for(int i = 1; i < startParams.m-1; i++){
        for(int j = 0; j < startParams.n; j++){
            a[ID(i,j)] = (raw_data[ID(i,j)] - raw_data[ID(i-1,j)])/startParams.dx;
            b[ID(i,j)] = (raw_data[ID(i+1,j)] - raw_data[ID(i-1,j)])/(2.0 * startParams.dx);
            c[ID(i,j)] = (raw_data[ID(i+1,j)] - raw_data[ID(i,j)])/startParams.dx;
        }
    }

    for(int j = 0; j < startParams.n; j++){
        for(int i = 0; i < startParams.m; i++){
            ux.push_back(minmod(theta * a[ID(i,j)],b[ID(i,j)],theta * c[ID(i,j)]));
        }
    }

    return ux;
}

vector<double> KTGraphic3D::doUy(){
    vector<double> raw_data;

    if(N != (startParams.m * startParams.n))
        return raw_data;

    for(int i = 0; i < this->raw_data.size(); i++){
        Point* p = this->raw_data[i];
        raw_data.push_back(p->z);
    }
    double theta = 2.0;
    vector<double> a;
    vector<double> b;
    vector<double> c;
    vector<double> uy;

    a.reserve(N);
    b.reserve(N);
    c.reserve(N);
    uy.reserve(N);

    switch (startParams.coutour_type) {
    //CIRCULAR
    case 0:
        for(int j = 0; j < startParams.m; j++){
            int N = startParams.n;
            a[ID(j,0)] = (raw_data[ID(j,0)] - raw_data[ID(j,N-1)])/startParams.dy;
            a[ID(j,N-1)] = (raw_data[ID(j,N-1)] - raw_data[ID(j,N-2)])/startParams.dy;

            b[ID(j,0)] = (raw_data[ID(j,1)] - raw_data[ID(j,N-1)])/(2*startParams.dy);
            b[ID(j,N-1)] = (raw_data[ID(j,0)] - raw_data[ID(j,N-2)])/(2*startParams.dy);

            c[ID(j,0)] = (raw_data[ID(j,1)] - raw_data[ID(j,0)])/startParams.dy;
            c[ID(j,N-1)] = (raw_data[ID(j,0)] - raw_data[ID(j,N-1)])/startParams.dy;
        }
        break;

    //DUPLICATED
    case 1:
        for(int j = 0; j < startParams.m; j++){
            int N = startParams.n;
            a[ID(j,0)] = (raw_data[ID(j,0)] - raw_data[ID(j,0)])/startParams.dy;
            a[ID(j,N-1)] = (raw_data[ID(j,N-1)] - raw_data[ID(j,N-2)])/startParams.dy;

            b[ID(j,0)] = (raw_data[ID(j,1)] - raw_data[ID(j,0)])/(2*startParams.dy);
            b[ID(j,N-1)] = (raw_data[ID(j,N-1)] - raw_data[ID(j,N-2)])/(2*startParams.dy);

            c[ID(j,0)] = (raw_data[ID(j,1)] - raw_data[ID(j,0)])/startParams.dy;
            c[ID(j,N-1)] = (raw_data[ID(j,N-1)] - raw_data[ID(j,N-1)])/startParams.dy;
        }
        break;
    default:
        for(int j = 0; j < startParams.m; j++){
            int N = startParams.n;
            a[ID(j,0)] = (raw_data[ID(j,0)] - raw_data[ID(j,N-1)])/startParams.dy;
            a[ID(j,N-1)] = (raw_data[ID(j,N-1)] - raw_data[ID(j,N-2)])/startParams.dy;

            b[ID(j,0)] = (raw_data[ID(j,1)] - raw_data[ID(j,N-1)])/(2*startParams.dy);
            b[ID(j,N-1)] = (raw_data[ID(j,0)] - raw_data[ID(j,N-2)])/(2*startParams.dy);

            c[ID(j,0)] = (raw_data[ID(j,1)] - raw_data[ID(j,0)])/startParams.dy;
            c[ID(j,N-1)] = (raw_data[ID(j,0)] - raw_data[ID(j,N-1)])/startParams.dy;
        }
        break;
    }

    for(int i = 1; i < startParams.m-1; i++){
        for(int j = 0; j < startParams.n; j++){
            a[ID(i,j)] = (raw_data[ID(i,j)] - raw_data[ID(i-1,j)])/startParams.dy;
            b[ID(i,j)] = (raw_data[ID(i+1,j)] - raw_data[ID(i-1,j)])/(2.0 * startParams.dy);
            c[ID(i,j)] = (raw_data[ID(i+1,j)] - raw_data[ID(i,j)])/startParams.dy;
        }
    }

    for(int j = 0; j < startParams.n; j++){
        for(int i = 0; i < startParams.m; i++){
            uy.push_back(minmod(theta * a[ID(i,j)],b[ID(i,j)],theta * c[ID(i,j)]));
        }
    }

    return uy;
}

double KTGraphic3D::f(double u){
    switch (startParams.example) {
    case 0:
    {
        return pow(u,2);
        break;
    }
    case 1:
    {
        return (pow((u - 0.2)/(0.8),2.0) * (11.0) / (pow(((u - 0.2)/(0.8)),2.0) + pow(((1-u)/(0.75)),2.0)));
    }
    default:
        return pow(u,2);
    }
}

double KTGraphic3D::df(double u){
    switch (startParams.example) {
    case 0:
    {
        return 2.0 * u;
    }
    case 1:
    {
        return (-0.876345 + 5.25807*u - 4.38172*pow(u,2.0) + 1.77636e-15*pow(u,3.0))/pow((0.550936 - 1.25156*u + pow(u,2.0)),2.0);
    }
    default:
    {
        return 2.0 * u;
    }
    }
}

double KTGraphic3D::g(double u){
    switch (startParams.example) {
    case 0:
        return pow(u,2);
        break;
    default:
        return pow(u,2);
    }
}

double KTGraphic3D::dg(double u){
    switch (startParams.example) {
    case 0:
        return 2.0 * u;
    default:
        return 2.0 * u;
    }
}


double KTGraphic3D::minmod(double a, double b, double c){
    if(a > 0 && b > 0 && c > 0){
        vector<double> aux;
        aux.push_back(a);
        aux.push_back(b);
        aux.push_back(c);
        return MathHelper::min(aux);
    } else if(a < 0 && b < 0 && c < 0) {
        vector<double> aux;
        aux.push_back(a);
        aux.push_back(b);
        aux.push_back(c);
        return MathHelper::max(aux);
    }

    return 0;
}
