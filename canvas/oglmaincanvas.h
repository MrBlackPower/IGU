#ifndef OGLMAINCANVAS_H
#define OGLMAINCANVAS_H

#define PLOT_SIZE 8.0

#define ZERO 0
#define ONE 1

#define LINE_WIDTH 2.0
#define POINT_SIZE 6.0
#define SPHERE_SLICES 8
#define SPHERE_STACKS 10
#define CILYNDER_SLICES SPHERE_SLICES
#define CILYNDER_STACKS 20

#include <QOpenGLWidget>
#include <QtGui>
#include <GL/glu.h>
#include "model/arteryTree.h"
#include "oglcanvas.h"

#define EYE_X 0.00
#define EYE_Y 0.00
#define EYE_Z 10.0

#define NEAR 1.0
#define FAR 1000.0
#define FOVY 45.0

#define ITERATED_ALPHA 1.0

#define NOT_ITERATED_ALPHA 0.8

#define SELECTED_RED       1.0
#define SELECTED_GREEN     0.656862745
#define SELECTED_BLUE      0.0

#define HOVERED_RED        1.0
#define HOVERED_GREEN      0.454901961
#define HOVERED_BLUE       0.0

#define UNSELECTED_RED     0.494117647
#define UNSELECTED_GREEN   0.098039216
#define UNSELECTED_BLUE    0.105882353

/************************************************************************************
  Name:        oglmaincanvas.h
  Copyright:   Version 1.0
  Author:      Igor Pires dos Santos
  Last Update: 17/02/2017 (DD/MM/YYYY)
  Release:     --/--/---- (DD/MM/YYYY)
  Description: Header class of an OpenGL Widget that handles Linear Grphics.
************************************************************************************/

using namespace std;

class oglMainCanvas : public OglCanvas
{
public:
/************************************************************************************
        CONSTRUCTOR
************************************************************************************/
    oglMainCanvas(QWidget *parent = NULL);

/************************************************************************************
        DESTRUCTOR
************************************************************************************/
    ~oglMainCanvas();


protected:


private:

};

#endif // OGLMAINCANVAS_H
