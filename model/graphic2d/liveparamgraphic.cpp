#include "liveparamgraphic.h"

LiveParamGraphic::LiveParamGraphic(GraphicObject* gObj, QString field ,QString name, SmartObject* parent) : Graphic2D (name,parent){
    master = gObj;
    master->addSlave(this);
    this->field = field;

    QString fields = (gObj != NULL)? gObj->liveParameterList() : "";

    if(fields.contains(field))
        parameter = new LiveParameter(gObj->getGID(),field,gObj->liveParameterDataType(field));
}

LiveParamGraphic::LiveParamGraphic(int GID, QString field ,QString name, SmartObject* parent) : Graphic2D (name,parent)
{
    master = GraphicObject::getGraphic(GID);
    this->field = field;

    QString fields = (master != NULL)? master->liveParameterList() : "";

    if(master != NULL){
        master->addSlave(this);
        if(fields.contains(field))
            parameter = new LiveParameter(GID,field,master->liveParameterDataType(field));
    }

}

bool LiveParamGraphic::initialize(){
    return true;
}

double LiveParamGraphic::iteration(){
    if(parameter != NULL){
        double x = master->getModeltime();
        double y = 0.0;

        //UPDATES PARAMETER VALUES
        parameter->update_live_parameter();

        switch (parameter->get_data_type()) {
        case integer:{
            int aux = *static_cast<int*>(parameter->get_current_value());
            y = aux;
            break;
        }
        case floatp:{
            float aux = *static_cast<float*>(parameter->get_current_value());
            y = aux;
            break;
        }
        case doublep:{
            double aux = *static_cast<double*>(parameter->get_current_value());
            y = aux;
            break;
        }
        case stringp:{
            y = 0.0;
            break;
        }
        }

        Point newP(ZERO,x,y);
        this->addData(newP);
    }
    return 0.0;
}

bool LiveParamGraphic::updateContextVisualParameter(QString field, QString value){
    return false;
}

bool LiveParamGraphic::setContextRange(QString field){
    if(field == "Y-AXIS"){
        range.max = limit.top;
        range.min = limit.bottom;
        range.parameter = "Y-AXIS";
        return true;
    }
    return false;
}

void LiveParamGraphic::mouse_press_event(int width, int height, Point eye, QMouseEvent *event, GraphicTools tool){

}

void LiveParamGraphic::mouse_move_event(int width, int height, Point eye, QMouseEvent *event, GraphicTools tool){

}

vector<Field> LiveParamGraphic::getStartParameters(){
    vector<Field> params;

    Field aux;
    aux.names = "LIVE_PARAM_FIELD";
    aux.required = false;
    aux.stream = FIELD_IN;
    aux.type = stringlist;
    aux.possible_values = (master != NULL)? master->liveParameterList() : "";
    aux.current_value = aux.names.asprintf("%s",field.toStdString().data());

    params.push_back(aux);

    return params;
}

vector<Field> LiveParamGraphic::getContextVisualParameters(){
    vector<Field> params;

    return params;
}

bool LiveParamGraphic::updateStartParameter(QString field, QString value){
    return false;
}

vector<QString> LiveParamGraphic::getRanges(){
    vector<QString> params;

    params.push_back("Y-AXIS");

    return params;
}

GraphicObject* GraphicObject::extrapolate(QString extrapolation){
    if(extrapolation == "LIVE_PARAM_GRAPHIC"){
        Field f;
        f.type = stringlist;
        f.current_value = "NONE";
        f.names = "LIVE_PARAM";
        f.possible_values = liveParameterList();
        f.stream = FIELD_IN;
        QString field;

        SelectFromStringListDialog* dialog = new SelectFromStringListDialog(f.names,f.possible_values);
        QEventLoop loop;
        QTimer timeout;

        QObject::connect(dialog,SIGNAL(stringListSet()),&loop,SLOT(quit()));
        QObject::connect(&timeout,SIGNAL(timeout()),&loop,SLOT(quit()));

        timeout.start(TIMEOUT);
        dialog->show();
        loop.exec();

        field = dialog->getString();

        delete dialog;

        GraphicObject* g = new LiveParamGraphic(GID,field,QString::asprintf("LIVE PRAM GRAPHIC OF #%d <%s>",GID,field.toStdString().data()),this);

        return g;
    } else {
        return extrapolateContext(extrapolation);
    }
    return NULL;
}

QString LiveParamGraphic::getLiveParameterList(){
    return QString("");
}

QString LiveParamGraphic::getLiveParameter(QString name){
    return QString("");
}

DataType LiveParamGraphic::getLiveParameterDataType(QString name){
    return undefined;
}
