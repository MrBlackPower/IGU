#ifndef WEIGHTEDGRAPH_H
#define WEIGHTEDGRAPH_H

#include "../graph.h"
#include "cost.h"

#include <math.h>
#include <QColor>

/***
 * DEFAULT VALUES FOR GENERATING GRAPH
 * */

#define DEFAULT_N 100
#define DEFAULT_M 100
#define DEFAULT_X 100
#define DEFAULT_Y 100

/***
 * DEFAULT VALUES FOR STATUS ITERATIONS INDEX
 * */
#define STATUS_ITERATIONS_INDEXES         4
#define GENERATING_INDEX                  0
#define CONSTRUCTING_ARTICULATION_INDEX   1
#define CONSTRUCTING_HEURISTIC_INDEX      2
#define ITERATING_INDEX                   3

enum WeightedGraphIterationStatus{
    GENERATING,
    CONSTRUCTING_ARTICULATION,
    CONSTRUCTING_HEURISTIC,
    ITERATING
};

enum WeightedGraphProblems{
    MINIMUM_WEIGHTED_CONNEX_DOMINANT_SET,
    MINIMUM_WEIGHTED_DOMINANT_SET
};

enum WeightedGraphSolutionConstructionType{
    GREEDY,
    ALPHA_GREEDY,
    REACTIVE_GREEDY
};

enum WeightedGraphLocalSearchHeuristic{
    VND,
    TABU,
    ANT_COLONY
};

enum WeightedGraphSolutionMovement{
    ADD,
    REMOVE,
    SWAP,
    D_SWAP
};

struct Tabu{
    WeightedGraphSolutionMovement movement;
    int node_id;
};

enum WeightedGraphSolutionMovementSet{
    M1_ADD_REMOVE,
    M2_ADD_REMOVE_SWAP,
    M3_ADD_REMOVE_SWAP_DSWAP,
    M4_ADD_REMOVE_DSWAP
};

enum WeightedGraphNodeCostType{
    C1_WEIGHT,
    C2_NO_OF_CONNECTIONS,
    C3_WEIGHT_NO_OF_CONNECTIONS,
    C4_DOMINANCY,
    C5_WEIGHT_DOMINANCY
};

enum WeightedGraphNodeDesirabiltyType{
    D1_WEIGHT,
    D2_NO_OF_CONNECTIONS,
    D3_WEIGHT_NO_OF_CONNECTIONS,
    D4_DOMINANCY,
    D5_WEIGHT_DOMINANCY
};

struct WeightedGraphStartParams{
    WeightedGraphProblems problem = MINIMUM_WEIGHTED_CONNEX_DOMINANT_SET;
    bool minimum = true;
    WeightedGraphSolutionConstructionType construction = REACTIVE_GREEDY;
    WeightedGraphNodeCostType costType = C2_NO_OF_CONNECTIONS;
    WeightedGraphSolutionMovementSet moveSet = M2_ADD_REMOVE_SWAP;
    WeightedGraphLocalSearchHeuristic localSearchHeuristic = TABU;
    WeightedGraphNodeDesirabiltyType desire = D2_NO_OF_CONNECTIONS;//
    bool perturbated = true;
    double perturbate_pct = 0.2;
    int nperturbation = 100;
    bool refresh_cost = true; // if refreshs costs calculation every iteration

    bool dominant_set_last_solution = true;

    bool paint_dominant = true;
    QColor paint_dominant_color = QColor("Magenta");

    bool paint_dominated;
    QColor paint_dominated_color = QColor("Cyan");

    bool paint_non_dominated;
    QColor paint_non_dominated_color = QColor("Yellow");

    vector<double> alphas;
    vector<double> q_alphas;//quatity of tickets
    vector<double> p_alphas;//percentage of tickets
    int last_alpha_id = 0;
    bool chose_alpha = true; //if it has to chose the alpha value again

    //counter of iterations
    int iterations[STATUS_ITERATIONS_INDEXES] = {0};

    //construction dependant
    double alpha = 0; //has to be between 0 and 1
    int nAlpha = 6; //number of alphas used by GRASP

    //for TABU search
    int tabu_size = 12;
    double penalize_infeasible_alpha = 1.0;
    double penalize_infeasible_alpha_max = 0.1;
    double penalize_infeasible_alpha_min = 1.1;
    double tabu_aspiration_criteria = 0.1;
    int penalize_steps = 33;
    int tabu_maximum_iterations = 20000;
    int tabu_maximum_without_improvemet = 10000;
    //Number of iterations without improvement
    int without_improvement_count = 0;

    //Parallel
    bool parallel = true;

    //CAREFUL! how many times to restart to the construction step
    int n_restart = 100;
    int n_restart_count = ZERO;
    
    //ant - colony
    int ant_colony_size = 24;//
    double ant_colony_q0 = 0.9;//
    double ant_colony_p = 0.9;//
    double ant_colony_phi = 0.9;//
    int ant_maximum_iterations = 20000;
    int ant_maximum_without_improvemet = 1000;
};

class WeightedGraph : public Graph<int,double>
{    
public:
    WeightedGraph(QString name, SmartObject* parent = NULL);
    WeightedGraph(int n, int m, int x, int y, int z, QString name = "DEFAULT_GRAPH", bool treeD = false,  SmartObject* parent = NULL);

    void addTabu(Tabu t);

    void perturbateSolution(Solution* s, double pct);

    vector<Field> getPointDataList();
    vector<Field> getCellDataList();
    vector<Field> getStartParameters();
    vector<Field> getContextVisualParameters();
    bool compatibility(VTKFile* file);
    bool load(VTKFile* file);
    vector<QString> getRanges();
    bool updateStartParameter(QString field, QString value);
    GraphicObject* extrapolateContext(QString extrapolation);
    bool setContextRange(QString field);

    vector<Solution> getNeighboorSolutions(Solution* sol, WeightedGraphSolutionMovementSet moveSet);

    vector<Solution> getNeighboorSolutions(Solution* sol, vector<WeightedGraphSolutionMovement> movement);
    vector<Solution> getNeighboorSolutions(Solution* sol, WeightedGraphSolutionMovement movement);

    vector<Cost> getCost(vector<int> dominantID, WeightedGraphNodeCostType costType = C2_NO_OF_CONNECTIONS);
    vector<Cost> getCost(vector<NodeGraph<int,double>*> dominant, WeightedGraphNodeCostType costType = C2_NO_OF_CONNECTIONS);

    Solution* addHeuristicNode(Solution* sol, WeightedGraphSolutionConstructionType construction);

    //VIRTUAL GRAPH METHODS
    bool validSolution(Solution* s);
    double scoreSolution(Solution* s);
    void drawContext();

    bool validSolution(Solution* sol, WeightedGraphProblems problem);
    double scoreSolution(Solution* sol,  WeightedGraphProblems problem, double penalize_infeasible_alpha = ONE);
    double costSolution(Solution* sol,  WeightedGraphProblems problem);

    vector<double> scoreSolution(Solution* sol,  WeightedGraphNodeDesirabiltyType desire);

    bool removeDuplicateNode(Solution* sol);

    double nodeCost(NodeGraph<int,double>* n , WeightedGraphNodeCostType costType = C2_NO_OF_CONNECTIONS);
    double nodeScore(NodeGraph<int,double>* n, WeightedGraphProblems problem = MINIMUM_WEIGHTED_DOMINANT_SET);

    static QString problemtoString(WeightedGraphProblems problem);

    static QString constructiontoString(WeightedGraphSolutionConstructionType construction);

    static QString localheuristictoString(WeightedGraphLocalSearchHeuristic heuristic);

    static QString movementtoString(WeightedGraphSolutionMovement movement);
    static WeightedGraphSolutionMovement movementFromString(QString movement);

    static QString desiretoString(WeightedGraphNodeDesirabiltyType desire);
    static WeightedGraphNodeDesirabiltyType desireFromString(QString desire);

    static QString movesettoString(WeightedGraphSolutionMovementSet moveset);
    static vector<WeightedGraphSolutionMovement> movesetMovements(WeightedGraphSolutionMovementSet moveset);

    static QString costtypetoString(WeightedGraphNodeCostType costType);

protected:

private:
    void paintGraph();

    //ACO

    vector<double> delta_tau(Solution* sol);
    void pheromone(vector<double>* tau,vector<double>* delta_tau);
    void local_pheromone(vector<double>* tau,vector<double>* local_tau,vector<double>* local_tau_0);


    WeightedGraphIterationStatus graphStatus;
    WeightedGraphStartParams params;

    vector<Cost> costs;
    
    //ant colony
    vector<Solution> ants;
    vector<vector<double>> ant_tau_0;
    vector<vector<double>> ant_tau;
    vector<double> ant_global_tau;
    vector<bool> ant_first_aprimorant_complete;

    //TABU LIST
    vector<Tabu> tabuList;
    unsigned int tabu_n;

    //GRAPHIC OBJECT METHODS
    bool initialize();
    double iteration();
    void finish();
    bool updateContextVisualParameter(QString field, QString value);
    vector<QString> getExtrapolations();
    QString getLiveParameterList();
    QString getLiveParameter(QString name);
    DataType getLiveParameterDataType(QString name);
    void mouse_press_event(int width, int height, Point eye, QMouseEvent *event, GraphicTools tool);
    void mouse_move_event(int width, int height, Point eye, QMouseEvent *event, GraphicTools tool);


    bool isTabu(Solution* s);
};

#endif // WEIGHTEDGRAPH_H
