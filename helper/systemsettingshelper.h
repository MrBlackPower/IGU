#ifndef SYSTEMSETTINGSHELPER_H
#define SYSTEMSETTINGSHELPER_H

#include "mathHelper.h"
#include "fileHelper.h"
#include "vtkfile.h"

#define SIZE_PARAMETER_LIST 9
#define DEFAULT_CONFIG_FILE "config.igu"

class SystemSettingsHelper
{

public:

    enum SystemParameter{
        PARALLEL,
        PARALLEL_PARADIGMA,
        NUMBER_OF_THREADS,
        NUMBER_OF_THREAD_BLOCKS,
        NUMBER_OF_THREADS_IN_BLOCKS,
        ANGLES_PER_PIXEL,
        SCALAR_STEP_PER_PIXEL,
        MOVE_PER_PIXEL,
        Z_OFFSET
    };

    struct System{
        bool parallel = true;
        MathHelper::ParallelParadigma parallel_paradigma = MathHelper::OPENMP;
        int number_of_threads = 16;
        int number_of_thread_blocks = 1;
        int number_of_threads_in_blocks = 16;
        double angles_per_pixel = 1;
        double scalar_step_per_pixel = 0.1;
        double move_per_pixel = 10;
        double z_offset = 10;
    };

    SystemSettingsHelper();

    QString get(SystemParameter param);

    vtk::DataType type(SystemParameter param);

    bool set(SystemParameter param, QString data);

    bool load(QString config_filename = DEFAULT_CONFIG_FILE);

    bool load(vector<QString> config_file);

    static QString parameterToString(SystemParameter parameter);

    static int parameterToInt(SystemParameter parameter);

    static SystemSettingsHelper::SystemParameter stringToParameter(QString string);

    vector<QString> print();

    void print(vector<QString>* aux);

    void operator=(SystemSettingsHelper b);
    void operator=(SystemSettingsHelper* b);


    System raw_data;

private:
};

#endif // SYSTEMSETTINGSHELPER_H
