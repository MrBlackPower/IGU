#include <QFileDialog>
#include "mainwindow.h"
#include "ui_mainwindow.h"

/************************************************************************************
  Name:        mainwindow.h
  Copyright:   Version 1.0
  Author:      Igor Pires dos Santos
  Last Update: 17/02/2017 (DD/MM/YYYY)
  Release:     --/--/---- (DD/MM/YYYY)
  Description: Class implementation of a load window for an artery tree structure.
************************************************************************************/
using namespace window;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{    
    qRegisterMetaType<SmartLogMessage>("SmartLogMessage");
    qRegisterMetaType<vector<QString>>("vector<QString>");

    selectedTool = SELECT;
    thread_pool = NULL;

    log = new SmartLog();
    SmartLogThread = new QThread();

    log->moveToThread(SmartLogThread);

    connect(log, SIGNAL (error(QString)), this, SLOT (threadError(QString)));
    connect(SmartLogThread, SIGNAL (started()), log, SLOT (process()));
    connect(log, SIGNAL (finished()), SmartLogThread, SLOT (quit()));
    connect(log, SIGNAL (finished()), log, SLOT (deleteLater()));
    connect(SmartLogThread, SIGNAL (finished()), SmartLogThread, SLOT (deleteLater()));
    connect(log, SIGNAL (updateLog(vector<QString>)), this, SLOT (updateLogWindow(vector<QString>)));
    SmartLogThread->start();

    SmartObject::setDefaultLogPool(log);
    GraphicObject::setDefaultRawPool(log);

    ui->setupUi(this);

    gObjw = NULL;
    mw = NULL;
    ctW = NULL;
    ccoW = NULL;
    graphW = NULL;
    loadWizard = NULL;
    settingsW = NULL;
    scheduler = NULL;

    //INITIALIZES AND CONNECTS CONSOLE WINDOW
    vector<QString> buffer;
    QString aux = "";

    QObject::connect(this,SIGNAL(sendConsoleText(vector<QString>)),log,SLOT(addToBuffer(vector<QString>)));
    QObject::connect(this,SIGNAL(emit_log(SmartLogMessage)),log,SLOT(addToBuffer(SmartLogMessage)));


    buffer.push_back(QString::asprintf("   /=======================================================================/"));
    buffer.push_back(QString::asprintf("  /====/       IGU (Iterador Gráfico Universal)       /===================/"));
    buffer.push_back(QString::asprintf(" /====/        (or, Universal Graphic Iterator)      /===================/ "));
    buffer.push_back(QString::asprintf("/=======================================================================/"));
    buffer.push_back(QString::asprintf(VERSION));
    buffer.push_back(QString::asprintf("/=======================================================================/"));
    buffer.push_back(QString::asprintf("%s",FileHelper::getOsName().data()));
    buffer.push_back(QDateTime::currentDateTime().toString(QString::asprintf("dddd dd/MM/yyyy hh:mm:ss zzz")));
    buffer.push_back(QString::asprintf("/=======================================================================/"));

    //FILE TIMER START
    log_timer = new QTimer();
    connect(log_timer, SIGNAL(timeout()), log, SLOT(process()));
    log_timer->start(log_timeout);


    //UPDATES CURRENT CONFIG
    SystemSettingsHelper current_config;
    if(current_config.load()){
        IGUSettingsDialog::update_config(current_config);
        updateConfig(current_config);
        buffer.push_back(QString::asprintf("CURRENT CONFIG SUCCESFULLY LOADED"));
    }

    emit sendConsoleText(buffer);
}

MainWindow::~MainWindow()
{
    delete log;
    delete ui;
    delete log_timer;
}

void MainWindow::setSideCanvas(GraphicObject *g){
    OglCanvas* canvas = ui->rightCanvas;

    canvas->setGraphicObject(g);
}

void MainWindow::setSideCanvas(int gid){
    OglCanvas* canvas = ui->rightCanvas;

    GraphicObject* g = GraphicObject::getGraphic(gid);

    if(g != NULL)
        canvas->setGraphicObject(g);
}

void MainWindow::purgeSideCanvas(){
    OglSideCanvas* canvas = ui->rightCanvas;
    canvas->purgeGraphicObject();

    emit repaint();
}

void MainWindow::updateLogWindow(vector<QString> log){
    ui->log->clear();

    for (int i = 0; i < log.size(); i++) {
        ui->log->append(log[i]);
    }
}

void MainWindow::setMainCanvas(GraphicObject *g){
    OglCanvas* canvas = ui->mainCanvas;

    canvas->setGraphicObject(g);
}

void MainWindow::setMainCanvas(int gid){
    OglCanvas* canvas = ui->mainCanvas;
    GraphicObject* g = GraphicObject::getGraphic(gid);

    canvas->setGraphicObject(g);
}

void MainWindow::purgeMainCanvas(){
    oglMainCanvas* canvas = ui->mainCanvas;
    canvas->purgeGraphicObject();

    emit repaint();
}

void MainWindow::setContour(double val){
    if(!ui->mainCanvas->hasGraphicObject())
        return;

    StaticGraphic3D* graphic = static_cast<StaticGraphic3D*>(ui->mainCanvas->getGraphicObject());

    if(!graphic)
        return;

    Lines3D* l = graphic->contour(val);

    setSideCanvas(l);
}

void MainWindow::generateCCOTree(GenerateCCOTreeWindow::GenerateParams params){
    VTKFile file = ArterialTreeCoreHelper::generateArterialTree(params,this);
    vector<QString> pointConversions;
    vector<QString> cellConversions;

    pointConversions.push_back(CCO_POINT_CONVERSION);
    cellConversions.push_back(CCO_RADIUS_CONVERSION);

    ArteryTree* a = new ArteryTree(file.getFileName(),log);
    file = file.scaleThrough(pointConversions,cellConversions);
    a->load(&file);

    if(params.load){
        setMainCanvas(a);
    } else {
        QString newFilename = QFileDialog::getSaveFileName(NULL,"New CCO Tree");
        FileHelper::saveRaw(a->raw(),newFilename.toStdString().data());

        delete a;
    }

    emit emit_log(SmartLogMessage(log->getSID(),QString::asprintf("CCO TREE GENERATED N=%d 3D=%s",params.TerminalCount,((params.threeD)? "YES" : "NO" )),SmartLogType::LOG_CREATE));
}

void MainWindow::generateGraph(GenerateWeightedGraphWindow::GenerateParams params){
    WeightedGraph* g = new WeightedGraph(params.n, params.m, params.x, params.y, params.z, params.name, params.treeD,log);

    if(params.load){
        setMainCanvas(g);
    } else {
        QString newFilename = QFileDialog::getSaveFileName(NULL,"New Graph");
        FileHelper::saveRaw(g->raw(),newFilename.toStdString().data());

        delete g;
    }
    emit emit_log(SmartLogMessage(log->getSID(),QString::asprintf("GRAPH GENERATED N=%d M=%d 3D=%s",params.n,params.m,((params.treeD)? "YES" : "NO" )),SmartLogType::LOG_CREATE));
}

void MainWindow::purge(){
    oglMainCanvas* canvas = ui->mainCanvas;
    canvas->purgeGraphicObject();

    emit repaint();
}

void window::MainWindow::threadError(QString msg){
    cout << msg.toStdString().data() << endl;
}

void MainWindow::updateConfig(SystemSettingsHelper config){
    if (config.raw_data.parallel_paradigma == MathHelper::QTHREAD && config.raw_data.parallel) {
        thread_pool = new SmartThreadPool(MathHelper::threads,"THREAD POOL",log);

        MathHelper::thread_pool = thread_pool;
    } else {
        if (thread_pool != NULL) {
            delete  thread_pool;
            thread_pool = NULL;
            MathHelper::thread_pool = NULL;
        } else {
            thread_pool = NULL;
            MathHelper::thread_pool = NULL;
        }
    }
}

void window::MainWindow::on_act_loadWizard_triggered()
{
    loadWizard = new LoadWizard(this);
    
    connect(loadWizard,SIGNAL(registerMainCanvas(GraphicObject*)),this,SLOT(setMainCanvas(GraphicObject*)));
    connect(loadWizard,SIGNAL(registerSideCanvas(GraphicObject*)),this,SLOT(setSideCanvas(GraphicObject*)));

    loadWizard->show();
}

void window::MainWindow::on_act_Main_Start_triggered()
{
    if(!ui->mainCanvas->hasGraphicObject())
        return;

    GraphicObject* gObj = ui->mainCanvas->getGraphicObject();
    gObj->start();
}

void window::MainWindow::on_act_Main_Iterate_triggered()
{
    if(!ui->mainCanvas->hasGraphicObject())
        return;

    GraphicObject* gObj = ui->mainCanvas->getGraphicObject();
    gObj->iterate();
}

void window::MainWindow::on_act_Main_Stop_triggered()
{
    if(!ui->mainCanvas->hasGraphicObject())
        return;

    GraphicObject* gObj = ui->mainCanvas->getGraphicObject();
    gObj->end();
}

void window::MainWindow::on_act_Main_Save_triggered()
{
    if(!ui->mainCanvas->hasGraphicObject())
        return;

    QString fileName = QFileDialog::getSaveFileName(this,"Save Graphic Object");
    string line = fileName.toStdString();
    GraphicObject* gObj = ui->mainCanvas->getGraphicObject();

    FileHelper::saveRaw(gObj->print(),line.data());
}

void window::MainWindow::on_act_Main_Dump_triggered()
{
    if(!ui->mainCanvas->hasGraphicObject())
        return;

    QString fileName = QFileDialog::getSaveFileName(this,"Dump Graphic Object");
    string line = fileName.toStdString();
    GraphicObject* gObj = ui->mainCanvas->getGraphicObject();

    FileHelper::saveRaw(gObj->data(),line.data());
}

void window::MainWindow::on_act_Main_Export_triggered()
{
    if(!ui->mainCanvas->hasGraphicObject())
        return;

    QString fileName = QFileDialog::getSaveFileName(this,"Export Graphic Object");
    string line = fileName.toStdString();
    GraphicObject* gObj = ui->mainCanvas->getGraphicObject();

    FileHelper::saveRaw(gObj->raw(),line.data());
}

void window::MainWindow::on_act_Side_Start_triggered()
{
    if(!ui->rightCanvas->hasGraphicObject())
        return;

    GraphicObject* gObj = ui->rightCanvas->getGraphicObject();
    gObj->start();
}

void window::MainWindow::on_act_Side_Iterate_triggered()
{
    if(!ui->rightCanvas->hasGraphicObject())
        return;

    GraphicObject* gObj = ui->rightCanvas->getGraphicObject();

    if(gObj->getModeltime() <= 0.0){
        gObj->start();
    } else {
        gObj->iterate();
    }
}

void window::MainWindow::on_act_Side_Stop_triggered()
{
    if(!ui->rightCanvas->hasGraphicObject())
        return;

    GraphicObject* gObj = ui->rightCanvas->getGraphicObject();
    gObj->end();
}

void window::MainWindow::on_act_Side_Save_triggered()
{
    if(!ui->rightCanvas->hasGraphicObject())
        return;

    QString fileName = QFileDialog::getSaveFileName(this,"Export Graphic Object");
    string line = fileName.toStdString();
    GraphicObject* gObj = ui->rightCanvas->getGraphicObject();

    FileHelper::saveRaw(gObj->print(),line.data());
}

void window::MainWindow::on_act_Side_Dump_triggered()
{
    if(!ui->rightCanvas->hasGraphicObject())
        return;

    QString fileName = QFileDialog::getSaveFileName(this,"Export Graphic Object");
    string line = fileName.toStdString();
    GraphicObject* gObj = ui->rightCanvas->getGraphicObject();

    FileHelper::saveRaw(gObj->data(),line.data());
}

void window::MainWindow::on_act_Side_Export_triggered()
{
    if(!ui->rightCanvas->hasGraphicObject())
        return;

    QString fileName = QFileDialog::getSaveFileName(this,"Export Graphic Object");
    string line = fileName.toStdString();
    GraphicObject* gObj = ui->rightCanvas->getGraphicObject();

    FileHelper::saveRaw(gObj->raw(),line.data());
}

void window::MainWindow::on_btn_main_iterate_clicked()
{
    if(!ui->mainCanvas->hasGraphicObject())
        return;

    GraphicObject* gObj = ui->mainCanvas->getGraphicObject();
    if(gObj->getModeltime() <= 0.0){
        gObj->start();
    } else {
        gObj->iterate();
    }
}

void window::MainWindow::on_btn_main_start_clicked()
{
    if(!ui->mainCanvas->hasGraphicObject())
        return;

    GraphicObject* gObj = ui->mainCanvas->getGraphicObject();

    double time = ui->spn_main_time->value();
    int it = time / gObj->getDeltaT();
    double rest = time - (it * gObj->getDeltaT());

    if(rest >= (gObj->getDeltaT() / 2))
        it ++;

    if(it >=0){
        gObj->iterate(it);
        ui->mainCanvas->update();
        ui->rightCanvas->update();
    }
}

void window::MainWindow::on_spn_main_time_valueChanged(double arg1)
{
    if(!ui->mainCanvas->hasGraphicObject())
        return;

    GraphicObject* gObj = ui->mainCanvas->getGraphicObject();
    int it = arg1 / gObj->getDeltaT();
    double rest = arg1 - (it * gObj->getDeltaT());

    if(rest >= (gObj->getDeltaT() / 2))
        it ++;

    ui->lcd_main_iterations->display(it);
    ui->lcd_main_iterations->setVisible(true);
}

void window::MainWindow::on_btn_main_pause_clicked()
{
    if(!ui->mainCanvas->hasGraphicObject())
        return;

    GraphicObject* gObj = ui->mainCanvas->getGraphicObject();
    gObj->pause();
}

void window::MainWindow::on_btn_main_stop_clicked()
{
    if(!ui->mainCanvas->hasGraphicObject())
        return;

    GraphicObject* gObj = ui->mainCanvas->getGraphicObject();
    gObj->end();
}

void window::MainWindow::on_btn_main_details_clicked()
{
    if(!ui->mainCanvas->hasGraphicObject())
        return;

    GraphicObject* gObj = ui->mainCanvas->getGraphicObject();

    GraphicObjectDetailsWindow* details = new GraphicObjectDetailsWindow(gObj,this);
    details->show();
    detailWindows.push_back(details);
}

void window::MainWindow::on_btn_side_iterate_clicked()
{
    if(!ui->rightCanvas->hasGraphicObject())
        return;

    GraphicObject* gObj = ui->rightCanvas->getGraphicObject();

    if(gObj->getModeltime() <= 0.0){
        gObj->start();
    } else {
        gObj->iterate();
    }
}

void window::MainWindow::on_btn_side_play_clicked()
{
    if(!ui->rightCanvas->hasGraphicObject())
        return;

    GraphicObject* gObj = ui->rightCanvas->getGraphicObject();
    double time = ui->spn_side_time->value();
    int it = time / gObj->getDeltaT();
    double rest = time - (it * gObj->getDeltaT());

    if(rest >= (gObj->getDeltaT() / 2))
        it ++;

    if(it >=0){
        gObj->iterate(it);
        ui->mainCanvas->update();
        ui->rightCanvas->update();
    }
}

void window::MainWindow::on_spn_side_time_valueChanged(double arg1)
{
    if(!ui->rightCanvas->hasGraphicObject())
        return;

    GraphicObject* gObj = ui->rightCanvas->getGraphicObject();
    int it = arg1 / gObj->getDeltaT();
    double rest = arg1 - (it * gObj->getDeltaT());

    if(rest >= (gObj->getDeltaT() / 2))
        it ++;

    ui->lcd_side_iterations->display(it);
    ui->lcd_side_iterations->setVisible(true);
}

void window::MainWindow::on_btn_side_pause_clicked()
{
    if(!ui->rightCanvas->hasGraphicObject())
        return;

    GraphicObject* gObj = ui->rightCanvas->getGraphicObject();
    gObj->pause();
}

void window::MainWindow::on_btn_side_details_clicked()
{
    if(!ui->rightCanvas->hasGraphicObject())
        return;

    GraphicObject* gObj = ui->rightCanvas->getGraphicObject();

    GraphicObjectDetailsWindow* details = new GraphicObjectDetailsWindow(gObj,this);
    details->show();
    detailWindows.push_back(details);
}

void window::MainWindow::on_btn_side_stop_clicked()
{
    if(!ui->rightCanvas->hasGraphicObject())
        return;

    GraphicObject* gObj = ui->rightCanvas->getGraphicObject();
    gObj->end();
}

void window::MainWindow::on_btn_log_save_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(this,"Export Log");
    string line = fileName.toStdString();

    FileHelper::saveRaw(log->print(),line.data());
}

void window::MainWindow::on_btn_log_delete_clicked()
{
    log->clean();
}

void window::MainWindow::on_act_GraphcObjectList_triggered()
{
    if(gObjw != NULL)
        gObjw->deleteLater();

    gObjw = new GraphicObjectsWindow(this);

    connect(gObjw,SIGNAL(setMainCanvas(GraphicObject*)),this,SLOT(setMainCanvas(GraphicObject*)));
    connect(gObjw,SIGNAL(setSideCanvas(GraphicObject*)),this,SLOT(setSideCanvas(GraphicObject*)));

    gObjw->show();
}

void window::MainWindow::on_act_CCOWindow_triggered()
{
    if(ccoW != NULL)
        ccoW->deleteLater();

    ccoW = new GenerateCCOTreeWindow(this);
    ccoW->show();
    QObject::connect(ccoW,SIGNAL(generateTree(GenerateCCOTreeWindow::GenerateParams)),this,SLOT(generateCCOTree(GenerateCCOTreeWindow::GenerateParams)));
}

void window::MainWindow::on_act_WeightedGraphGenerator_triggered()
{
    if(graphW != NULL)
        graphW->deleteLater();

    graphW = new GenerateWeightedGraphWindow(this);
    graphW->show();
    QObject::connect(graphW,SIGNAL(generateGraph(GenerateWeightedGraphWindow::GenerateParams)),this,SLOT(generateGraph(GenerateWeightedGraphWindow::GenerateParams)));
}

void window::MainWindow::on_act_Loadt_WeightedGraph_triggered()
{
    VTKFile file;
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open Instance File"),"/home/");
    file.loadMWCDSInstance(fileName);

    WeightedGraph* g = new WeightedGraph(fileName,log);
    g->load(&file);

    setMainCanvas(g);
}

void window::MainWindow::on_toolSelect_clicked()
{
    updateTool(SELECT);
}

void window::MainWindow::on_toolAddNode_clicked()
{
    updateTool(ADD_NODE);
}

void window::MainWindow::on_toolEditNode_clicked()
{
    updateTool(EDIT_NODE);
}

void window::MainWindow::on_toolDeleteNode_clicked()
{
    updateTool(REMOVE_NODE);
}

void window::MainWindow::updateTool(GraphicSpace::GraphicTools selection){
    switch (selection) {
    case SELECT:
        ui->toolSelect->setChecked(true);
        ui->toolAddNode->setChecked(false);
        ui->toolEditNode->setChecked(false);
        ui->toolDeleteNode->setChecked(false);
        ui->toolLimit->setChecked(false);
        ui->toolRotate->setChecked(false);
        ui->toolScale->setChecked(false);
        ui->toolMove->setChecked(false);

        selectedTool = selection;
    break;
    case ADD_NODE:
        ui->toolSelect->setChecked(false);
        ui->toolAddNode->setChecked(true);
        ui->toolEditNode->setChecked(false);
        ui->toolDeleteNode->setChecked(false);
        ui->toolLimit->setChecked(false);
        ui->toolRotate->setChecked(false);
        ui->toolScale->setChecked(false);
        ui->toolMove->setChecked(false);

        selectedTool = selection;
    break;
    case EDIT_NODE:
        ui->toolSelect->setChecked(false);
        ui->toolAddNode->setChecked(false);
        ui->toolEditNode->setChecked(true);
        ui->toolDeleteNode->setChecked(false);
        ui->toolLimit->setChecked(false);
        ui->toolRotate->setChecked(false);
        ui->toolScale->setChecked(false);
        ui->toolMove->setChecked(false);

        selectedTool = selection;
    break;
    case REMOVE_NODE:
        ui->toolSelect->setChecked(false);
        ui->toolAddNode->setChecked(false);
        ui->toolEditNode->setChecked(false);
        ui->toolDeleteNode->setChecked(true);
        ui->toolLimit->setChecked(false);
        ui->toolRotate->setChecked(false);
        ui->toolScale->setChecked(false);
        ui->toolMove->setChecked(false);

        selectedTool = selection;
    break;
    case LIMIT:
        ui->toolSelect->setChecked(false);
        ui->toolAddNode->setChecked(false);
        ui->toolEditNode->setChecked(false);
        ui->toolDeleteNode->setChecked(false);
        ui->toolLimit->setChecked(true);
        ui->toolRotate->setChecked(false);
        ui->toolScale->setChecked(false);
        ui->toolMove->setChecked(false);

        selectedTool = selection;
    break;
    case ROTATE:
        ui->toolSelect->setChecked(false);
        ui->toolAddNode->setChecked(false);
        ui->toolEditNode->setChecked(false);
        ui->toolDeleteNode->setChecked(false);
        ui->toolLimit->setChecked(false);
        ui->toolRotate->setChecked(true);
        ui->toolScale->setChecked(false);
        ui->toolMove->setChecked(false);

        selectedTool = selection;
    break;
    case SCALE:
        ui->toolSelect->setChecked(false);
        ui->toolAddNode->setChecked(false);
        ui->toolEditNode->setChecked(false);
        ui->toolDeleteNode->setChecked(false);
        ui->toolLimit->setChecked(false);
        ui->toolRotate->setChecked(false);
        ui->toolScale->setChecked(true);
        ui->toolMove->setChecked(false);

        selectedTool = selection;
    break;
    case MOVE:
        ui->toolSelect->setChecked(false);
        ui->toolAddNode->setChecked(false);
        ui->toolEditNode->setChecked(false);
        ui->toolDeleteNode->setChecked(false);
        ui->toolLimit->setChecked(false);
        ui->toolRotate->setChecked(false);
        ui->toolScale->setChecked(false);
        ui->toolMove->setChecked(true);

        selectedTool = selection;
    break;
    }

    ui->mainCanvas->changeTool(selection);
    ui->rightCanvas->changeTool(selection);
}

void window::MainWindow::on_btn_main_onetoone_clicked()
{
    if(ui->mainCanvas->hasGraphicObject()){
        GraphicObject* gObj = ui->mainCanvas->getGraphicObject();
        gObj->resizeTransform();
    }
}

void window::MainWindow::on_btn_side_onetoone_clicked()
{
    if(ui->rightCanvas->hasGraphicObject()){
        GraphicObject* gObj = ui->rightCanvas->getGraphicObject();
        gObj->resizeTransform();
    }
}

void window::MainWindow::on_toolLimit_clicked()
{
    updateTool(GraphicSpace::GraphicTools::LIMIT);
}

void window::MainWindow::on_toolRotate_clicked()
{
    updateTool(GraphicSpace::GraphicTools::ROTATE);
}

void window::MainWindow::on_toolScale_clicked()
{
    updateTool(GraphicSpace::GraphicTools::SCALE);
}

void window::MainWindow::on_toolMove_clicked()
{
    updateTool(GraphicSpace::GraphicTools::MOVE);
}

void window::MainWindow::on_act_settings_triggered()
{
    if(settingsW != NULL)
        settingsW->deleteLater();

    settingsW = new IGUSettingsDialog();
    settingsW->show();

    QObject::connect(settingsW,SIGNAL(printConfig(vector<QString>,QString)),log,SLOT(addToBuffer(vector<QString>,QString)));
}

void window::MainWindow::on_act_empty_ArteryTree_triggered()
{
    InputStringDialog* dialog = new InputStringDialog("ARTERY_TREE_NAME","");
    QEventLoop loop;
    QTimer timeout;

    connect(dialog,SIGNAL(stringSet()),&loop,SLOT(quit()));
    connect(&timeout,SIGNAL(timeout()),&loop,SLOT(quit()));

    timeout.start(TIMEOUT);
    dialog->show();
    loop.exec();

    if(dialog->Set()){
        ArteryTree* a = new ArteryTree(dialog->getString());

        ui->mainCanvas->setGraphicObject(a);
    }

    delete dialog;
}

void window::MainWindow::on_act_empty_AutomatusGraph_triggered()
{
    InputStringDialog* dialog = new InputStringDialog("ARTERY_TREE_NAME","");
    QEventLoop loop;
    QTimer timeout;

    connect(dialog,SIGNAL(stringSet()),&loop,SLOT(quit()));
    connect(&timeout,SIGNAL(timeout()),&loop,SLOT(quit()));

    timeout.start(TIMEOUT);
    dialog->show();
    loop.exec();

    if(dialog->Set()){
        AutomatusGraph* a = new AutomatusGraph(dialog->getString());

        ui->mainCanvas->setGraphicObject(a);
    }

    delete dialog;
}


void window::MainWindow::on_act_Load_AutomatusGraph_triggered()
{
    VTKFile file;
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open Instance File"),"/home/");
    file.loadAutomatusInstance(fileName);

    AutomatusGraph* g = new AutomatusGraph(fileName,log);
    g->load(&file);

    setMainCanvas(g);
}

void window::MainWindow::on_act_scheduler_triggered()
{
    if(scheduler != NULL){
        delete  scheduler;
        scheduler = NULL;
    }

    scheduler = new SchedulerDialog();


    QObject::connect(scheduler,SIGNAL(setRightCanvas(GraphicObject*)),this,SLOT(setSideCanvas(GraphicObject*)));
    QObject::connect(scheduler,SIGNAL(setMainCanvas(GraphicObject*)),this,SLOT(setMainCanvas(GraphicObject*)));
    QObject::connect(scheduler,SIGNAL(purgeRightCanvas()),this,SLOT(purgeSideCanvas()));
    QObject::connect(scheduler,SIGNAL(purgeMainCanvas()),this,SLOT(purgeMainCanvas()));

    QObject::connect(scheduler,SIGNAL(iterateRightCanvas()),this,SLOT(on_btn_side_iterate_clicked()));
    QObject::connect(scheduler,SIGNAL(iterateMainCanvas()),this,SLOT(on_btn_main_iterate_clicked()));

    QObject::connect(scheduler,SIGNAL(startRightCanvas()),this,SLOT(on_btn_side_play_clicked()));
    QObject::connect(scheduler,SIGNAL(startMainCanvas()),this,SLOT(on_btn_main_start_clicked()));

    scheduler->show();
}
