#ifndef OGLSIDECANVAS_H
#define OGLSIDECANVAS_H

#include "oglcanvas.h"

class OglSideCanvas : public OglCanvas
{
public:
    OglSideCanvas(QWidget *parent = NULL);
};

#endif // OGLSIDECANVAS_H
