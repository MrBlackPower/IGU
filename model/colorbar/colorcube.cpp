#include "colorcube.h"

ColorCube::ColorCube() : ColorBar("COLORCUBE",ColorBarType::Discrete){
    QColor c;
    addColor(c.fromRgb(241,48,48));
    addColor(c.fromRgb(241,241,48));
    addColor(c.fromRgb(48,241,48));
    addColor(c.fromRgb(48,241,241));
    addColor(c.fromRgb(48,48,241));
    addColor(c.fromRgb(241,48,241));
}
