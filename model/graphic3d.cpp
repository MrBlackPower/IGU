#include "graphic3d.h"

Graphic3D::Graphic3D(QString name, int M, int N, SmartObject* parent) : GraphicObject(name,parent)
{
    this->M = M;
    this->N = N;
    NODES = 0;
}

Graphic3D::Graphic3D(QString name, int M, SmartObject* parent) : GraphicObject(name,parent)
{
    this->M = M;
    this->N = 0;
    NODES = 0;
}

void Graphic3D::resizeTransform(){
    vector<Point> points;

    for(int i = 0; i < raw_data.size(); i++)
        points.push_back(*raw_data[i]);

    resetLimit();
    updateLimit(points);
}

vector<QString> Graphic3D::print(){
    vector<QString> ans;
    QString buffer;
    ans.push_back(buffer.asprintf("%d",N));
    for(int i = 0; i < raw_data.size(); i++){
        Point* p = raw_data[i];
        buffer = buffer.asprintf("%f %f %f",p->x,p->y,p->z);
        ans.push_back(buffer);
    }

    emit emit_log(SmartLogMessage(getSID(),"GRAPHIC 3D PRINTED"));
    return ans;
}

vector<QString> Graphic3D::data(){
    vector<QString> ans;
    QString buffer;

    buffer = buffer.asprintf("3-DIMENSIONAL GRAPHIC");
    ans.push_back(buffer);

    buffer = buffer.asprintf("(X,Y)");
    ans.push_back(buffer);

    for(int i = 0; i < raw_data.size(); i++){
        Point* p = raw_data[i];
        buffer = buffer.asprintf("(%.4f,%.4f,%.4f)",p->x,p->y,p->z);
        ans.push_back(buffer);
    }



    emit emit_log(SmartLogMessage(getSID(),"GRAPHIC 3D DATA SAVED"));
    return ans;
}

vector<QString> Graphic3D::raw(){
    vector<QString> raw;
    QString line;

    line = line.asprintf("# vtk DataFile Version 3.0\n");
    raw.push_back(line);
    line = line.asprintf("PRESSURE PEAKING 3D GRAPHIC OUTPUT SmartID = %d\n",getID());
    raw.push_back(line);
    line = line.asprintf("ASCII\n");
    raw.push_back(line);
    line = line.asprintf("DATASET POLYDATA\n");
    raw.push_back(line);
    line = line.asprintf("POINTS  %d  double\n", N);
    raw.push_back(line);

    for(int i = 0; i < raw_data.size(); i++){
        Point* p = raw_data[i];
        line = line.asprintf("%f  %f  %f\n", p->x, p->y, p->z);
        raw.push_back(line);
    }

    emit emit_log(SmartLogMessage(getSID(),"GRAPHIC 3D RAW DATA SAVED"));
    return raw;
}

void Graphic3D::draw(){
    glBegin(GL_QUADS);
    for(int i = 0; i < M - 1; i++){
        for(int j = 0; j < N - 1; j++){
            if(N > ID(i+1,j+1)){
                Point a = *raw_data[ID(i,j)];
                Point b = *raw_data[ID(i+1,j)];
                Point c = *raw_data[ID(i+1,j+1)];
                Point d = *raw_data[ID(i,j+1)];

                setMaterialColor(interpolateColor(a.z));
                glVertex3f(a.x, a.y, a.z);

                setMaterialColor(interpolateColor(b.z));
                glVertex3f(b.x, b.y, b.z);

                setMaterialColor(interpolateColor(c.z));
                glVertex3f(c.x, c.y, c.z);

                setMaterialColor(interpolateColor(d.z));
                glVertex3f(d.x, d.y, d.z);


                setMin(a.z);
                setMax(a.z);
                setMin(b.z);
                setMax(b.z);
                setMin(c.z);
                setMax(c.z);
                setMin(d.z);
                setMax(d.z);
            }
        }
    }
    glEnd();
}

int Graphic3D::getM(){
    return M;
}

int Graphic3D::getN(){
    return N;
}

void Graphic3D::setM(int M){
    this->M = M;
}

void Graphic3D::setN(int N){
    this->N = N;
}

int Graphic3D::ID(int i, int j){
    return i + (j * M);
}

bool Graphic3D::load(VTKFile* file){
    emit emit_log(SmartLogMessage(getSID(),"GRAPHIC 3D LOAD RUN",SmartLogType::LOG_LOAD));

    clearData();

    vector<Point> points = file->getPoints();

    for(int i = 0; i < points.size(); i++)
        addData(points[i]);

    return true;
}

bool Graphic3D::addData(Point p){
    raw_data.push_back(p.clone());
    updateLimit(&p);
    NODES++;

    setMin(p.z);
    setMax(p.z);

    return true;
}

bool Graphic3D::addData(Point* p){
    raw_data.push_back(p);
    updateLimit(p);
    NODES++;

    setMin(p->z);
    setMax(p->z);

    return true;
}

bool Graphic3D::clearData(){
    raw_data.clear();
    NODES = 0;
    resetLimit();

    range.max = -1;
    range.min = 1;

    return true;
}

vector<Field> Graphic3D::getPointDataList(){
    vector<Field> list;
    return list;
}

vector<Field> Graphic3D::getCellDataList(){
    vector<Field> list;
    return list;
}

vector<QString> Graphic3D::getExtrapolations(){
    vector<QString> aux;

    return aux;
}

GraphicObject* Graphic3D::extrapolateContext(QString extrapolation){

}

bool Graphic3D::compatibility(VTKFile *file){
    emit emit_log(SmartLogMessage(getSID(),"GRAPHIC 3D COMPATIBILITY RUN",SmartLogType::LOG_COMPATIBILITY));

//    if(file->getN() < 4)
//        return false;

//    //CHECKS IF POINTS ARE EQUALLY DISTED AND ALL CELLS COMPLETE
//    //number of points has to be a multiple of 4
//    if(file->getN() % 4 != 0.0)
//        return false;

//    //they have to be all equally disted
//    vector<Point> points = file->getPoints();
//    Point d1 = points[ID(i,j)] - points[ID(i+1,j)];
//    Point d2 = points[ID(i,j)] - points[ID(i,j+1)];
//    Point d3 = points[ID(i,j)] - points[ID(i+1,j+1)];

//    if(d1.x != 0.0)
//        return false;

//    if(d2.y != 0.0)
//        return false;

//    if(d3.x == 0.0 || d3.y == 0.0)
//        return false;

//    for(int i = ONE; i < points.size(); i++){
//        Point d1 = points[ID(i,j)] - points[ID(i+1,j)];
//        Point d2 = points[ID(i,j)] - points[ID(i,j+1)];
//        Point d3 = points[ID(i,j)] - points[ID(i+1,j+1)];
//    }

    return false;
}

void Graphic3D::finish(){

}

Point Graphic3D::operator[](int i){
    if(i < 0 || i >= raw_data.size())
        return Point();

    return *raw_data[i];
}

Point Graphic3D::operator ()(int i, int j){
    int id = ID(i,j);
    return operator [](id);
}
