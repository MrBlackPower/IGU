#include "arterialtreecoreHelper.h"

VTKFile ArterialTreeCoreHelper::generateArterialTree(GenerateCCOTreeWindow::GenerateParams params, QObject* parent)
{
    QString lib = (params.threeD)? (QDir::currentPath() + "/ARTERIAL_TREE_CORE_3D") : (QDir::currentPath() + "/ARTERIAL_TREE_CORE_2D");

    QStringList buffer;
    QString aux;

    buffer << aux.asprintf("%d",params.TerminalCount);
    buffer << ((params.LegacyMode)? "true" : "false");
    buffer << ((params.ThreadEnabled)? "true" : "false");
    buffer << aux.asprintf("%d",params.MaxThreads);
    buffer << ((params.ConstantFlow)? "true" : "false");
    buffer << ((params.FLANNEnabled)? "true" : "false");
    buffer << aux.asprintf("%f",params.FLANNEnableFactor);
    buffer << aux.asprintf("%f",params.FLANNParam);
    buffer << aux.asprintf("%d",ONE);

    QProcess *core = new QProcess(parent);
    core->start(lib,buffer);

    core->waitForStarted();

    QString filename = QDir::currentPath() + QString::asprintf("/TreeNterm%d_S%d.vtk",params.TerminalCount,((params.threeD)? 50 : 1));

    VTKFile file;

    core->waitForFinished();
    core->kill();

    //WILL WAIT 2 SECONDS FOR FILE TO BE CREATED
    QTime time = QTime::currentTime();

    while(!file.load(filename) && time.elapsed() < 2000){
        cout << "DID NOT FIND '" << filename.toStdString() << "' TIME REMAINING: " << (2000 - time.elapsed()) << "ms" << endl;
    }

    QFile::remove(filename);

    return file;
}
