#ifndef CELLDATALISTWIDGET_H
#define CELLDATALISTWIDGET_H

#include <QListWidget>
#include "../../helper/vtkfile.h"

class CellDataListWidget : public QListWidget
{
public:
    CellDataListWidget(QWidget *parent = 0);

public slots:

    bool addDataList(VTKFile file);
};

#endif // CELLDATALISTWIDGET_H
