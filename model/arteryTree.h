#ifndef ARTERYTREE_H
#define ARTERYTREE_H

/************************************************************************************
  Name:        arteryTree.cpp
  Copyright:   Version 1.0
  Author:      Igor Pires dos Santos
  Last Update: 17/02/2017 (DD/MM/YYYY)
  Release:     --/--/---- (DD/MM/YYYY)
  Description: Header class of an artery tree structure.
************************************************************************************/

#include <string>
#include <vector>
#include "artery.h"
#include "helper/mathHelper.h"
#include "graphic2d.h"
#include "graphic2d/staticgraphic.h"
#include "graphic2d/livearteryparamgraphic.h"
#include "graphicobject.h"

#define ZERO 0

#define MOUSE_PROX 10

#define DEFAULT_RADIUS 0.1
#define DEFAULT_YOUNG_MODULUS 100000
#define DEFAULT_VISCOSITY 0.0
#define DEFAULT_DENSITY 0.983

using namespace std;

struct ArteryTreeStartParams{
    double frequency = 3.65;
    bool viscous = false;
    double viscousMultiplier = 0.0;
    double phi0 = 0.0;
    double p0 = 1.0;
    double q0 = 1.0;
    double deltaFrequency = 0.05;
    double deltaViscousMultiplier= 0.0;
    double deltaPhi0 = 0.0;
};

class ArteryTree : public GraphicObject
{
    public:
        ArteryTree(QString name, SmartObject* parent = NULL);
        virtual ~ArteryTree();

        void resizeTransform();

        /********************************************************
        *  SMART OBJECT VIRTUAL METHODS                       *
        ********************************************************/
        vector<QString> print();
        vector<QString> raw();
        vector<QString> data();

        /********************************************************
        *  GRAPHIC OBJECT VIRTUAL METHODS  *
        ********************************************************/
        bool compatibility(VTKFile *file);
        bool load(VTKFile* file);
        void draw();
        vector<Field> getPointDataList();
        vector<Field> getCellDataList();
        vector<Field> getStartParameters();
        vector<Field> getContextVisualParameters();
        vector<QString> getRanges();
        bool updateStartParameter(QString field, QString value);
        GraphicObject* extrapolateContext(QString extrapolation);
        bool setContextRange(QString field);

        bool addNode(Point* start, Point* finish, double radius, double youngModulus, double density, double viscosity);
        bool iterate(double frequency, bool viscous, double viscousMultiplier, double phi0, double p, double q);

        void updateRadiusScale(double value);

        bool hasSelected();

        Artery* getSelectedLeaf();

        GraphicObjectComposer new_artery();
        GraphicObjectComposer edit_artery(Artery* a);

        bool new_artery(GraphicObjectComposer artery_component);
        bool edit_artery(GraphicObjectComposer artery_component, Artery* a);

        /********************************************************
        *                  PRINTS                               *
        * Recursive prints walking first to the left side.      *
        ********************************************************/
        void printArteriesLog(Artery* at, vector<QString> &commands);
        bool printFile(FILE* file);

/********************************************************
*                  GETS & SETS                          *
********************************************************/
        double getAngularFrequency();
        double getDeltaT();
        vector<Artery*> getLeafs();
        vector<Artery*> getSelected();
        int getN();
        double getP0();
        double getRootImpedance();
        Point* getStart();
        void setYoungModulus(float E);
        void setDensity(float density);
        void setViscosity(float viscosity);

    signals:
        void printLog(string emit_log);

    protected:
        void draw_aux(Artery* a);

    private:
/************************************************************************************
        LIST OF ATTRIBUTES
************************************************************************************/

        Artery* root;

        Point* startPoint;

        ArteryTreeStartParams startParams;

        int N_arteries;

        QColor idle_artery;

        QColor hovered_artery;

        QColor selected_artery;

        double radius_scale;

        //Auxilixaries
        Point* imaginary_start;
        Point* imaginary_end;

/************************************************************************************
        PRIVATE METHODS
************************************************************************************/
        Artery* getArtery(Point* finish);
        void getLeafAux(Artery* a, vector<Artery*> list);
        void setDensityAux(Artery* it, float density);
        void setViscosityAux(Artery* it, float viscosity);
        void setYoungModulusAux(Artery* it, float E);

        Artery* mouse_ray(int width, int height, Point eye, Point mouse);
        Artery* mouse_ray(int width, int height, Point eye, Point mouse, Artery* it);

        /********************************************************
        *  GRAPHIC OBJECT VIRTUAL METHODS                       *
        ********************************************************/
        double iteration();
        bool initialize();
        void finish();
        bool updateContextVisualParameter(QString field, QString value);
        vector<QString> getExtrapolations();
        QString getLiveParameterList();
        QString getLiveParameter(QString name);
        DataType getLiveParameterDataType(QString name);
        void mouse_press_event(int width, int height, Point eye, QMouseEvent *event, GraphicTools tool);
        void mouse_move_event(int width, int height, Point eye, QMouseEvent *event, GraphicTools tool);

        void resizeTransformAux(Artery* it, vector<Point>* points);
};

#endif // ARTERYTREE_H
