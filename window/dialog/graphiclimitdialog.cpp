#include "graphiclimitdialog.h"
#include "ui_graphiclimitdialog.h"

GraphicLimitDialog::GraphicLimitDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::GraphicLimitDialog)
{
    set = false;
    max = Point();
    min = Point();

    ui->setupUi(this);
    updateInterface();
}

GraphicLimitDialog::GraphicLimitDialog(Point max, Point min, QWidget *parent):
    QDialog(parent),
    ui(new Ui::GraphicLimitDialog)
{
    set = false;
    this->max = max;
    this->min = min;

    ui->setupUi(this);
    updateInterface();
}

GraphicLimitDialog::~GraphicLimitDialog()
{
    delete ui;
}

void GraphicLimitDialog::getLimit(Point* max, Point* min){
    max->operator=(this->max);
    min->operator=(this->min);
}

bool GraphicLimitDialog::Set(){
    return set;
}

void GraphicLimitDialog::updateInterface(){
    ui->spn_max_x->setValue(max.x);
    ui->spn_max_y->setValue(max.y);
    ui->spn_max_z->setValue(max.z);

    ui->spn_min_x->setValue(min.x);
    ui->spn_min_y->setValue(min.y);
    ui->spn_min_z->setValue(min.z);
}

void GraphicLimitDialog::on_spn_max_x_valueChanged(double arg1)
{
    max.x = arg1;
}

void GraphicLimitDialog::on_spn_max_y_valueChanged(double arg1)
{
    max.y = arg1;
}

void GraphicLimitDialog::on_spn_max_z_valueChanged(double arg1)
{
    max.z = arg1;
}

void GraphicLimitDialog::on_spn_min_x_valueChanged(double arg1)
{
    min.x = arg1;
}

void GraphicLimitDialog::on_spn_min_y_valueChanged(double arg1)
{
    min.y = arg1;
}

void GraphicLimitDialog::on_spn_min_z_valueChanged(double arg1)
{
    min.z = arg1;
}

void GraphicLimitDialog::on_buttonBox_accepted()
{
    set = true;

    emit limitSet();
    emit pushLimit(max,min);
}

void GraphicLimitDialog::on_buttonBox_rejected()
{
    emit cancelled();
}
