#include "oglcanvas.h"

unsigned int OglCanvas::CURR_CID = 0;
OglCanvas* OglCanvas::first_canvas = NULL;
OglCanvas* OglCanvas::last_canvas = NULL;

OglCanvas::OglCanvas(QString name, QWidget *parent) : QOpenGLWidget(parent), CID(CURR_CID++)
{
    gObj = NULL;
    objLoaded = false;
    eye = Point(1,EYE_X,EYE_Y,EYE_Z);
    lookAt = Point(1,LOOKAT_X,LOOKAT_Y,LOOKAT_Z);
    lightPosition = {0, 0, 10, 0.0f};
    lightAmbient = {1.0f, 1.0f, 1.0f, 1.0f};
    lightDiffuse = {1.0f, 1.0f, 1.0f, 1.0f};

    current_tool = SELECT;

    next_canvas = NULL;
    previous_canvas = NULL;

    width = window()->width();
    height = window()->height();

    if(first_canvas == NULL && last_canvas == NULL){
        first_canvas = this;
        last_canvas = this;
    } else {
        if(last_canvas == NULL){
            //emitLog("ERROR IN CANVAS LINKAGE!",SmartLogType::CRASH);
            last_canvas = this;
        } else {
            previous_canvas = last_canvas;
            last_canvas->next_canvas = this;
            last_canvas = this;
        }
    }
}

OglCanvas::~OglCanvas(){
    if(next_canvas != NULL){
        if(first_canvas == this){
            first_canvas = next_canvas;
            next_canvas->previous_canvas = NULL;
        } else if(previous_canvas != NULL){
            next_canvas->previous_canvas = previous_canvas;
            previous_canvas->next_canvas = next_canvas;
        } else {
            next_canvas->previous_canvas = NULL;
        }
    } else if(previous_canvas != NULL){
        previous_canvas->next_canvas = NULL;

        if(last_canvas == this)
            last_canvas = previous_canvas;
    } else if(last_canvas == this && first_canvas == this){
        first_canvas = NULL;
        last_canvas = NULL;
    }
}

void OglCanvas::purgeGraphicObject(){
    objLoaded = false;
    gObj = NULL;
    //disconnect(this,SLOT(getQPainter(Point*,QString,QColor,QFont)));

    QString aux;
    //emitLog(aux.asprintf("PURGED GRAPHIC OBJECT"));
}

void OglCanvas::setGraphicObject(GraphicObject* gObj){
    if(gObj == NULL){
        objLoaded = false;
        this->gObj = NULL;
        gObj->purgePainterMethod();

        update();
        return;
    }

    this->gObj = gObj;
    objLoaded = true;

    update();

    function<QPainter*(int)> f = bind(getQPainter,_1);
    gObj->setPainterMethod(f,CID);

    QString aux;
    //emitLog(aux.asprintf("NEW GRAPHIC OBJECT LOADED [SMART ID = %d]",gObj->getID()));
}

GraphicObject* OglCanvas::getGraphicObject(){
    return gObj;
}

void OglCanvas::initializeGL()
{
    glClearColor(0.8863,0.8863,0.8863,1.0);

    glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbient.data());
    glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse.data());

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glShadeModel(GL_SMOOTH);

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);

    gObj = NULL;
}

void OglCanvas::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    int renderMode;
    glGetIntegerv(GL_RENDER_MODE,&renderMode);

    if(renderMode != GL_SELECT){
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        const float aspect = width/height;
        gluPerspective(FOVY,aspect,NEAR,FAR);
    }

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glLightfv(GL_LIGHT0, GL_POSITION, lightPosition.data());

    gluLookAt(eye.x,eye.y,eye.z,lookAt.x,lookAt.y,lookAt.z,0.0,1.0,0.0);

    if(objLoaded){
        gObj->drawObject();

        if(gObj->isIterating()){
            gObj->iterate();

            QTimer *t = new QTimer();
            t->setSingleShot(true);
            connect(t, SIGNAL(timeout()), this, SLOT(update()));
            t->start(gObj->delay());
        }
    }
}

void OglCanvas::repaint(){
    update();
}

QPainter* OglCanvas::getQPainter(int cid){
    OglCanvas* canvas = first_canvas;
    QPainter* painter;

    while(canvas != NULL && canvas->CID != cid)
        canvas = canvas->next_canvas;

    if(canvas == NULL)
        return NULL;

    painter = new QPainter(canvas);
    return painter;
}

vector<OglCanvas*> OglCanvas::getCanvases(){
    OglCanvas* canvas = first_canvas;
    vector<OglCanvas*> res;

    while(canvas != NULL){
        res.push_back(canvas);
        canvas = canvas->next_canvas;
    }

    return res;
}

OglCanvas* OglCanvas::getCanvas(int cid){
    OglCanvas* canvas = first_canvas;

    while(canvas != NULL){
        if(canvas->CID == cid)
            return canvas;

        canvas = canvas->next_canvas;
    }

    return NULL;
}

void OglCanvas::resizeGL(int w, int h)
{
    width = w;
    height = h;

    glViewport(0, 0, w, h);
}

void OglCanvas::mousePressEvent(QMouseEvent *event)
{
    if(objLoaded)
        gObj->mouse_press(width,height,eye,event,current_tool);

    repaint();
}

void OglCanvas::mouseMoveEvent(QMouseEvent *event)
{
    if(objLoaded)
        gObj->mouse_move(width,height,eye,event,current_tool);

    repaint();
}

void OglCanvas::changeTool(GraphicSpace::GraphicTools tool){
    current_tool = tool;

    //CHANGE POINTER
    switch (tool) {
    case SELECT:
        this->setCursor(Qt::CrossCursor);
        break;
    case ADD_NODE:
        this->setCursor(Qt::ClosedHandCursor);
        break;
    case EDIT_NODE:
        this->setCursor(Qt::PointingHandCursor);
        break;
    case REMOVE_NODE:
        this->setCursor(Qt::OpenHandCursor);
        break;
    case LIMIT:
        this->setCursor(Qt::SizeAllCursor);
        break;
    case ROTATE:
        this->setCursor(Qt::SizeAllCursor);
        break;
    case SCALE:
        this->setCursor(Qt::SizeAllCursor);
        break;
    case MOVE:
        this->setCursor(Qt::SizeAllCursor);
        break;
    }
}

bool OglCanvas::hasGraphicObject(){
    return objLoaded;
}


