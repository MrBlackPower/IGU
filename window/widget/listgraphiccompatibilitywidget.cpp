#include "listgraphiccompatibilitywidget.h"

ListGraphicCompatibilityWidget::ListGraphicCompatibilityWidget(QWidget *parent) : QListWidget(parent)
{

}

void ListGraphicCompatibilityWidget::populate(VTKFile file){
    clear();

    //PARTICULAR OBJECTS TO TRY AGAINST
    ArteryTree* at = new ArteryTree("TEST");
    Graphic2D* g2d = new StaticGraphic("TEST");
    Graphic3D* g3d = new StaticGraphic3D("TEST");
    Lines3D* l3d = new Lines3D("TEST");
    WeightedGraph* g = new WeightedGraph("TEST");

    if(at->compatible(&file));
        addItem("Artery Tree");

    if(g2d->compatible(&file)){
        addItem("KT Graphic [2D]");
        addItem("LF Graphic [2D]");
        addItem("Static Graphic [2D]");
    }

    if(g3d->compatible(&file)){
        addItem("KT Graphic [3D]");
        addItem("Static Graphic [3D]");
    }

    if(l3d->compatible(&file)){
        addItem("3D Lines");
    }

    if(g->compatible(&file)){
        addItem("Graph");
    }
    
    delete at;
    delete g2d;
    delete g3d;
    delete l3d;
    delete g;

    //CHECKS AGAINST A LIST OF SEPARATE CLASS FILES    (<<<<FUTURE>>>>>)
}
