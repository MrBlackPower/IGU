#ifndef GENERATECCOTREEWINDOW_H
#define GENERATECCOTREEWINDOW_H

#include <QDialog>

namespace Ui {
class GenerateCCOTreeWindow;
}

class GenerateCCOTreeWindow : public QDialog
{
    Q_OBJECT

public:
    struct GenerateParams{
        bool load = false;
        bool threeD;
        bool LegacyMode;
        bool ThreadEnabled;
        bool ConstantFlow;
        bool FLANNEnabled;
        int MaxThreads;
        int TerminalCount;
        double FLANNEnableFactor;
        double FLANNParam;
    };

    explicit GenerateCCOTreeWindow(QWidget *parent = 0);
    ~GenerateCCOTreeWindow();

signals:
    void generateTree(GenerateCCOTreeWindow::GenerateParams params);

private slots:
    void on_chk_2D_clicked();

    void on_chk_3D_clicked();

    void on_spn_TerminalCount_editingFinished();

    void on_chk_LegacyMode_clicked();

    void on_chk_ConstantFlow_clicked();

    void on_chk_ThreadEnabled_clicked();

    void on_chk_FLANNEnabled_clicked();

    void on_spn_FLANN_editingFinished();

    void on_sldr_FLANNEnableFactor_actionTriggered(int action);

    void on_spn_MaxThreads_editingFinished();

    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

    void on_chk_Load_clicked(bool checked);

    void on_chk_Export_clicked(bool checked);

private:
    Ui::GenerateCCOTreeWindow *ui;
    GenerateParams params;
};

#endif // GENERATECCOTREEWINDOW_H
