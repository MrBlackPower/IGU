#include "fileHelper.h"

/************************************************************************************
  Name:        fileHelper.cpp
  Copyright:   Version 1.0
  Author:      Igor Pires dos Santos
  Last Update: 17/02/2017 (DD/MM/YYYY)
  Release:     --/--/---- (DD/MM/YYYY)
  Description: Class implementation of a static class to help manage files.
************************************************************************************/

struct Square{
    int A;
    int B;
    int C;
    int D;
};

bool FileHelper::jumpReadLine(char *line, FILE *file){
    if(fgets(line, 80, file) != NULL)
        return true;

    return false;
}

bool FileHelper::savePoints(vector<Point*> points, char *fileName){
    FILE *file;
    file = fopen(fileName,"w+");

    if(file == NULL)
        return false;

    for(int i = 0; i < points.size(); i++){
        fprintf(file,"%f %f\n", points[i]->x, points[i]->y);
    }

    fclose(file);

    return true;
}

bool FileHelper::saveRaw(vector<QString> raw, char *fileName){
    FILE *file;
    file = fopen(fileName,"w+");

    if(file == NULL)
        return false;

    for(int i = 0; i < raw.size(); i++){
        QString line = raw[i];
        fprintf(file,"%s\n", line.toStdString().data());
    }

    fclose(file);

    return true;
}

bool FileHelper::saveRaw(vector<QString> raw, const char *fileName){
    FILE *file;
    file = fopen(fileName,"w+");

    if(file == NULL)
        return false;

    for(int i = 0; i < raw.size(); i++){
        QString line = raw[i];
        fprintf(file,"%s\n", line.toStdString().data());
    }

    fclose(file);

    return true;
}

bool FileHelper::move(const char *fileA, const char *fileB){
    FILE *flA;
    FILE *flB;
    flA = fopen(fileA,"r");
    flB = fopen(fileB,"w+");

    if(flA == NULL){
        if(flB != NULL)
            fclose(flB);

        return false;
    }

    if(flB == NULL){
        if(flA != NULL)
            fclose(flA);

        return false;
    }

    char line[100];

    while(fgets(line, 100, flA) != NULL){
        fprintf(flB,line);
    }

    fclose(flA);
    fclose(flB);

    return true;
}

/************************************************************************************
        Identifies the current OS

        @return String value of current OS.
************************************************************************************/
string FileHelper::getOsName()
{
    #ifdef _WIN32
    return "Windows";
    #elif _WIN64
    return "Windows";
    #elif __unix || __unix__
    return "Unix";
    #elif __APPLE__ || __MACH__
    return "Mac OSX";
    #elif __linux__
    return "Linux";
    #elif __FreeBSD__
    return "FreeBSD";
    #else
    return "Other";
    #endif
}
